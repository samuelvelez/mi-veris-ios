//
//  HojaVidaViewController.swift
//  Mi Veris
//
//  Created by Jorge Merchan on 21/8/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import UIKit


class HojaVidaViewController: UIViewController, UIAlertViewDelegate, UITextFieldDelegate{
    
    // variables
    
    var codigomedico : NSInteger = 0
    var secuenciamedico : String = ""
    var nombremedico : String = ""
    var nombreUser : UILabel = UILabel()
    var imagen : UIImageView = UIImageView()
    var nombreDoctor : UILabel = UILabel()
    var especialidadmedico : NSInteger = 0
    var nombreespecialidadmedico : String = ""
    
    var ciudadDoctor : UILabel = UILabel()
    var especialidadDoctor : UILabel = UILabel()
    var ingresoDoctor : UILabel = UILabel()
    var atencionDoctor : UILabel = UILabel()
    
    var datosmedico = ObjHojaVida()
    

    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = ""
        
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        /*var fondo : UIImageView = UIImageView(frame: self.view.bounds)
        fondo.image = UIImage(named:"fondodoctor copia.png")
        self.view.addSubview(fondo)*/
        
        let barWhite : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width , height:  30))
        barWhite.backgroundColor = UIColor.white
        barWhite.layer.shadowColor = UIColor.gray.cgColor
        barWhite.layer.shadowOffset = CGSize(width: 5, height: 5)
        barWhite.layer.shadowRadius = 5
        barWhite.layer.shadowOpacity = 1.0
        let barBlue : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width / 1.8, height: 30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x: barBlue.frame.width, y: 64, width:45, height: 30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x:0, y: 70, width: self.view.frame.width - 20, height:30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "Ficha del Dr."
    
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)
        
        let usuario = Usuario.getEntity as Usuario
        
        nombreUser = UILabel(frame: CGRect(x: 10, y: 94, width: self.view.frame.width - 20, height: 30))
        nombreUser.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        nombreUser.font = UIFont(name: "helvetica-bold", size: 12)
        nombreUser.text = usuario.nombre as String
        
        //self.view.addSubview(nombreUser)
        
        let barRecuadro : UIView = UIView(frame: CGRect(x:  (UIScreen.main.bounds.width - 140) / 2, y: 130,width: 140,height: 140))
        barRecuadro.layer.cornerRadius = 70
        barRecuadro.layer.borderWidth = 2.0;
        barRecuadro.layer.borderColor = UIColor(red:0.65, green:0.65, blue:0.67, alpha:1.0).cgColor
        barRecuadro.layer.masksToBounds = false;
        barRecuadro.clipsToBounds = true;
        self.view.addSubview(barRecuadro)
        
        imagen.frame = CGRect(x: (UIScreen.main.bounds.width - 120) / 2, y: 140, width: 120,height: 120)
        //imagen.self.backgroundColor = UIColor.grayColor();
        imagen.layer.cornerRadius = 55
        imagen.clipsToBounds = true;
        imagen.layer.borderColor = UIColor.red.cgColor
        
        let directorio = "/compartidos/portal/medicos/\(secuenciamedico).JPG"
        
        let urldef = URLFactory.urldefinida()

        let urls = URL(string: "\(urldef)servicio/login/imagen?arg0=\(directorio)")
        getDataFromUrl(url: urls!) { (data, response, error)  in
            DispatchQueue.main.sync() { () -> Void in
                print("entro")
                guard let data = data, error == nil else { print(error);return }
                print(response?.suggestedFilename ?? urls?.lastPathComponent)
                print("Download Finished")
                //cell.imagen.image = UIImage(data: data)
                //cell.imagen.image = UIImage(data: data)
                if data != nil {
                    print("image no es nil")
                    self.imagen.image = UIImage(data: data)
                    self.view.addSubview(self.imagen)
                } else{
                    print("image es nil")
                    self.imagen.image = UIImage(named: "medic.png")
                    self.view.addSubview(self.imagen)
                }
                print("se ejecuto")
            }
        }
        //self.view.frame.width - 60
        nombreDoctor = UILabel(frame: CGRect(x: self.view.frame.width / 6, y: 260 , width: self.view.frame.width - 80, height: 60))
        nombreDoctor.numberOfLines = 2
        nombreDoctor.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        nombreDoctor.font = UIFont(name: "helvetica-bold", size: 14)
        nombreDoctor.text = nombremedico
        nombreDoctor.textAlignment = NSTextAlignment.left;
        self.view.addSubview(nombreDoctor)
        
        let barver : UIView = UIView(frame: CGRect(x: (self.view.frame.width / 2), y: 315, width: 1, height:140))
        barver.backgroundColor = UIColor.gray
        self.view.addSubview(barver)
        
        
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")

        let url = NSURL(string: "\(urldef)servicio/doctor/consultahv?arg0=\(codigomedico)&arg1=\(especialidadmedico)")
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                var err: NSError?
                
                //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                if (err != nil) {
                    print("JSON Error \(err!.localizedDescription)")
                }
                
                //println(jsonResult);
                print(jsonResult)
                let info =  jsonResult as NSDictionary
                let data = info["lista"] as! [[String: Any]]
                if data.count > 0 {
                    for doctor in data {
                        let doc = ObjHojaVida()
                        doc.ciudadatencion = doctor["ciudadatencion"] as! NSString
                        doc.calificacion = doctor["calificacion"] as! NSString
                        doc.valorcalificacion = doctor["valorcalificaion"] as! NSString
                        self.datosmedico=doc
                    }
                }
                
                //dispatch_async(dispatch_get_main_queue(), {
                    DispatchQueue.main.async(execute:{
                    self.mostrardatohoja()
                    ViewUtilidades.hiddenLoader(controller: self)
                })
                } catch {
                    print("ERROR")
                }
            }
            
            
        }
        //////////////////////////
        
    }
    
    func mostrardatohoja(){
        let ciudad : UILabel = UILabel(frame: CGRect(x: 10, y: 310, width: (self.view.frame.width / 2) - 20, height: 60))
        ciudad.textAlignment = NSTextAlignment.right
        ciudad.textColor = UIColor.black
        ciudad.font = UIFont(name: "helvetica-bold", size: 14)
        ciudad.text = "Ciudad de Atención"
        
        ciudadDoctor = UILabel(frame: CGRect(x: (self.view.frame.width / 2) + 20, y: 310, width: (self.view.frame.width / 2), height: 60))
        ciudadDoctor.textColor = UIColor.gray
        ciudadDoctor.font = UIFont(name: "helvetica-bold", size: 14)
        ciudadDoctor.text = datosmedico.ciudadatencion as String
        ciudadDoctor.textAlignment = NSTextAlignment.left;
        
        self.view.addSubview(ciudad)
        self.view.addSubview(ciudadDoctor)
        
        let especialidadd : UILabel = UILabel(frame: CGRect(x: 10, y: 340, width: (self.view.frame.width / 2) - 20, height: 60))
        especialidadd.textAlignment = NSTextAlignment.right
        especialidadd.textColor = UIColor.black
        especialidadd.font = UIFont(name: "helvetica-bold", size: 14)
        especialidadd.text = "Especialidad"
        
        especialidadDoctor = UILabel(frame: CGRect(x: (self.view.frame.width / 2) + 20, y: 340, width: (self.view.frame.width / 2) - 20, height: 60))
        especialidadDoctor.textColor = UIColor.gray
        especialidadDoctor.font = UIFont(name: "helvetica-bold", size: 14)
        especialidadDoctor.text = nombreespecialidadmedico
        especialidadDoctor.numberOfLines = 2
        especialidadDoctor.textAlignment = NSTextAlignment.left;
        
        self.view.addSubview(especialidadd)
        self.view.addSubview(especialidadDoctor)
        
        
        //validar solo mostrar si la calificacion es Alta caso contrario no mostrar
        if datosmedico.calificacion == "Alta" {
            let atencion : UILabel = UILabel(frame: CGRect(x: 10, y: 370, width: (self.view.frame.width / 2) - 20, height: 60))
            atencion.textAlignment = NSTextAlignment.right
            atencion.textColor = UIColor.black
            atencion.font = UIFont(name: "helvetica-bold", size: 14)
            atencion.text = "Atención Calificada"
            
            atencionDoctor = UILabel(frame: CGRect(x: (self.view.frame.width / 2) + 20, y: 370, width:self.view.frame.width - 80, height:60))
            atencionDoctor.textColor = UIColor.gray
            atencionDoctor.font = UIFont(name: "helvetica-bold", size: 14)
            atencionDoctor.text = datosmedico.calificacion as String
            atencionDoctor.textAlignment = NSTextAlignment.left;
            
            self.view.addSubview(atencion)
            self.view.addSubview(atencionDoctor)
        }
        
    }
    
    func showAlert(){
        let alert = UIAlertController(title: "Veris", message: "Se ha eliminado el doctor de sus favoritos", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertError(){
        let alert = UIAlertController(title: "Veris", message: "Ha ocurrido un error, intente nuevamente", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.view.endEditing(true)
        return false
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }

    
    
}
