//
//  CellCollectionViewCell.swift
//  Mi Veris
//
//  Created by Samuel Velez on 5/7/16.
//  Copyright © 2016 Programadores-iOS.net. All rights reserved.
//

import UIKit

class CellCollectionViewCell: UICollectionViewCell {
    
/*    var icono : UIImageView = UIImageView()
    var separator : UIImageView = UIImageView();
    var title : UILabel = UILabel();
    @IBOutlet weak var myLabel: UILabel!
*/
   
    var textLabel: UILabel!
    var imageView: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.white
        
        imageView = UIImageView(frame: CGRect(x: frame.size.width/4, y: 10, width: frame.size.width/2, height: frame.size.height/2))
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        //imageView.contentMode = UIViewContentMode.Center;
        contentView.addSubview(imageView)
        
        let textFrame = CGRect(x: 0, y: frame.size.height - 50, width: frame.size.width, height: frame.size.height/3)
        textLabel = UILabel(frame: textFrame)
        textLabel.numberOfLines = 2
        //textLabel.font = UIFont.systemFontOfSize(UIFont.smallSystemFontSize())
        textLabel.textAlignment = .center
        contentView.addSubview(textLabel)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
   /* override init(frame: CGRect) {
        //super.init(frame: CGRect)
        super.init()
        //self.backgroundColor = UIColor.clearColor()
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        let screenWidth = screenSize.width/2
        icono.frame = CGRectMake(20, 15, 25, 25)
        
        title.frame = CGRectMake(60, 0, screenWidth, 60)
        title.font = UIFont(name: "helvetica", size: 16)
        title.textColor = UIColor(red: (48/256.0), green: (48/256.0), blue: (48/256.0), alpha: 1)
        
        self.contentView.addSubview(icono)
        self.contentView.addSubview(title)
        //self.contentView.addSubview(arrow)
        
    }
   */ /*override init(style: UICollectionViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.clearColor()
        
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        let screenWidth = screenSize.width;
        icono.frame = CGRectMake(20, 15, 25, 25)
        
        
        title.frame = CGRectMake(60, 0, CGRectGetWidth(self.frame), 60)
        title.font = UIFont(name: "helvetica", size: 16)
        title.textColor = UIColor(red: (48/256.0), green: (48/256.0), blue: (48/256.0), alpha: 1)
        
        
        let arrow = UIImageView(frame: CGRectMake(screenWidth-30, 17, 15, 24))
        arrow.image = UIImage(named: "flecha.png")
        
        self.contentView.addSubview(icono)
        self.contentView.addSubview(title)
        self.contentView.addSubview(arrow)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }*/
}
