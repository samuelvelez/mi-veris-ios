//
//  SelectConvenioViewController.swift
//  Mi Veris
//
//  Created by Samuel Velez on 7/12/16.
//  Copyright © 2016 Samuel Velez. All rights reserved.
//

import UIKit

class SelectConvenioViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var tableView: UITableView  =   UITableView()
    var listaconvenios : NSMutableArray?
    var valorconvenio : String = ""
    var resultadoconvenio : String = ""
    var conveniosel : ObjConvenio!
    
    var tipodocusuario : String!
    var documentousuario : String!
    var especialidadsel : ObjEspecialidad!
    
    var delegate:SelectConvenioDelegate! = nil
    
    var establecimientosel : ObjEstablecimiento!
    var serviciosel : ObjEspecialidad! //Es un servicio pero es muy parecido a una especialidad - se puede
    var recomendacionsel : ObjRecomendacion = ObjRecomendacion()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationItem.backBarButtonItem?.title=""
        //self.navigationController?.navigationItem.title = "Seleccione una Especialidad"
        navigationItem.backBarButtonItem?.title = ""
        navigationItem.title = "Seleccione el convenio"

        cargarconvenios()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func cargarconvenios(){
        
        let ud = Usuario.getEntity as Usuario
        print("hola antes de que te vayas soy : \(ud.tipoid) \(ud.numeroidentificacion)")
        
            ViewUtilidades.showLoader(controller: self, title: "Cargando \n convenios...")
            self.listaconvenios?.removeAllObjects();
            self.conveniosel = nil
            self.valorconvenio=""; self.resultadoconvenio = ""
            
            let url2 = URLFactory.obtenerconvenios(tipoidentificacion: ud.tipoid as String, identificacion: ud.numeroidentificacion as String, idespecialidad: "\(especialidadsel.codigo)", codigoempresa: "\(establecimientosel.idempresa)", codigosucursal: "\(establecimientosel.id)",
                codigoorden: self.recomendacionsel.numeroorden as String,
                codigoempresaorden: "\(self.recomendacionsel.codigoempresa)",
                codigolineaordden: self.recomendacionsel.lineadetalle as String, codigoprestacion: "\(self.serviciosel!.codigo)", codigoempresaprestacion: "\(self.establecimientosel.idempresa)")
            
            
            print("la url de los convenios\(url2)")
            let url = NSURL(string: "\(url2)")
            //let request = NSURLRequest(URL: url!)
            let request = NSMutableURLRequest(url: url! as URL)
            
            //let str = "wsappusuario:ZA$57@9b86@$2r5"
            
            
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                
                
                if (error != nil) {
                    //print(error!.localizedDescription)
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    do{
                        var err: NSError?
                        
                        //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                        
                        let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        if (err != nil) {
                            print("JSON Error \(err!.localizedDescription)")
                        }
                        
                        //println(jsonResult)
                        let info =  jsonResult as NSDictionary
                        self.resultadoconvenio = info["resultado"] as! String
                        self.listaconvenios = NSMutableArray()
                        if self.resultadoconvenio == "ok"{
                            self.valorconvenio = info["texto"] as! String
                            if self.valorconvenio.isEmpty{
                                let data = info["lista"] as! NSArray
                                if data.count > 0 {
                                    for jsonobj in data {
                                        let con = ObjConvenio(data: jsonobj as! NSDictionary)
                                        self.listaconvenios?.add(con)
                                    }
                                }
                            }else{
                                
                            }
                        }else{
                            
                        }
                        
                        //dispatch_async(dispatch_get_main_queue(), {
                        DispatchQueue.main.async(execute: {
                            self.showconvenios()
                            self.tableView.reloadData()
                            ViewUtilidades.hiddenLoader(controller: self)
                        })
                    } catch {
                        print("ERROR")
                    }
                }
                
                
            }
            //////////////////////////
            
            
        
    }

    func showconvenios(){
        tableView.frame = CGRect(x:0, y:60, width:self.view.frame.width, height:self.view.frame.height - 60);
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.backgroundColor = UIColor.clear
        self.view.addSubview(tableView);
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaconvenios!.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "") as UITableViewCell!
            ?? UITableViewCell(style: .default, reuseIdentifier: ""),
        convenio = listaconvenios!.object(at: indexPath.row) as! ObjConvenio
        
        
        cell.textLabel?.text = convenio.descripcionconvenio
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        conveniosel = listaconvenios!.object(at: indexPath.row) as? ObjConvenio
        delegate.retornadenewconvenio(controller: self)
    }


}

protocol SelectConvenioDelegate{
    func retornadenewconvenio(controller:SelectConvenioViewController)
}

