//
//  MisPagosViewController.swift
//  Mi Veris
//
//  Created by Samuel Velez on 6/3/17.
//  Copyright © 2017 Samuel Velez. All rights reserved.
//

import UIKit

class MisPagosViewController: UIViewController, UITableViewDelegate,UITableViewDataSource  {
    
    var tableView : UITableView = UITableView()
    var citasp = [CitasPagarModel]()
    var paquetesp = [PaquetesPagarModel]()
    var carrito = [CarritoModel]()
    var total : Double = 0
    

    override func viewDidLoad() {

        super.viewDidLoad()
        
        self.navigationItem.title = ""
        
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        let fondo : UIImageView = UIImageView(frame: self.view.bounds)
        fondo.image = UIImage(named:"MisRecetasBackground")
        self.view.addSubview(fondo)
        
        let barWhite : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width , height:30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width / 3, height:  30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x: barBlue.frame.width, y: 64, width: 45, height:30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x: 0, y: 64, width: self.view.frame.width - 20, height: 30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "Carrito de Compras"
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)
        
        let usuario = Usuario.getEntity as Usuario
        
        
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        
        
        let tipo = usuario.tipoid
        let identificacion = usuario.numeroidentificacion
        let urldef = URLFactory.urldefinida()
        
        let urlAsString = "\(urldef)servicio/carrito/itemspagar?arg0=\(tipo)&arg1=\(identificacion)"
        
        self.getJSONData(url: urlAsString)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func getJSONData(url: String) {
        let url = NSURL(string: url as String)
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                let json = JSON(data: data!)
                
                //Comprobamos si todo ha salido bien, en caso contrario se le muestra el mensaje de error al usuario
                if json["resultado"].string == "ok" {
                    
                    if let lista = json["lista"].array {
                        
                        for listaDic in lista {
                            let tipoIdentificacion = listaDic["tipoIdentificacion"].stringValue
                            let numeroId = listaDic["numeroId"].stringValue
                            let nombre = listaDic["nombre"].stringValue
                            let telefono = listaDic["telefono"].stringValue
                            let direccion = listaDic["direccion"].stringValue
                            let mail = listaDic["mail"].stringValue
                            var citasPagar : Double = 0
                            var paquetesPagar : Double = 0
                            let miscitas : Array = listaDic["citasPagar"].array!;
                            self.citasp = Array()
                            for cita in miscitas {
                                let citaIdreserva : String? = cita["idReserva"].stringValue
                                let citaFecha: String? = cita["fecha"].stringValue
                                let citaCentro: String? = cita["sucursal"].stringValue
                                let citaDoctor: String? = cita["doctor"].stringValue
                                let citaEspecialidad: String? = cita["especialidad"].stringValue
                                let citaMontoIva : String? = cita["montoIva"].stringValue
                                let citaMontoGravaIva : String? = cita["montoGravaIva"].stringValue
                                let citaMontoNoGravaIva : String? = cita["montoNoGravaIva"].stringValue
                                let citasubtotalCopago : String? = cita["subtotalCopago"].stringValue
                                let citaSubtotalCliente: String? = cita["subtotalCliente"].stringValue
                                let citaSubtotalVenta: String? = cita["subtotalVenta"].stringValue
                                let citaCantidad: String? = cita["cantidad"].stringValue
                                let citaidSucursal: String? = cita["idSucursal"].stringValue
                                let citaAplicaIva : String = cita["aplicaIva"].stringValue
                                let citaNumVerTar: String? = cita["numVerTar"].stringValue
                                let citatieneCobertura : String = cita["tieneCobertura"].stringValue
                                let citaIdServicio: String? = cita["idServicio"].stringValue
                                let citaIdPrestacion: String? = cita["idPrestacion"].stringValue
                                let citaPrestacion : String = cita["prestacion"].stringValue
                                let citaIdOrden: String? = cita["idOrden"].stringValue
                                let citaIdConvenio : String = cita["idConvenio"].stringValue
                                let citaIdTarjeta: String? = cita["idTarjeta"].stringValue
                                let citaNombreBeneficio: String? = cita["nombreBeneficio"].stringValue
                                
                                let citaP = CitasPagarModel(idReserva: citaIdreserva, fecha: citaFecha, sucursal: citaCentro, doctor: citaDoctor, especialidad: citaEspecialidad, montoIva: citaMontoIva, montoGravaIva: citaMontoGravaIva, montoNoGravaIva: citaMontoNoGravaIva, subtotalCopago: citasubtotalCopago, subtotalCliente: citaSubtotalCliente, subtotalVenta: citaSubtotalVenta, cantidad: citaCantidad, idSucursal: citaidSucursal, aplicaIva: citaAplicaIva, numVerTar: citaNumVerTar, tieneCobertura: citatieneCobertura, idServicio: citaIdServicio, idPrestacion: citaIdPrestacion, prestacion: citaPrestacion, idOrden: citaIdOrden, idConvenio: citaIdConvenio, idTarjeta: citaIdTarjeta, nombreBeneficio: citaNombreBeneficio)
                                citasPagar = citasPagar + Double(citasubtotalCopago!)!
                                self.citasp.append(citaP)
                                
                            }
                            if let mispaquetes = listaDic["paquetesPagar"].array {
                                for paquete in mispaquetes{
                                    let idOrdenPaquete : String? = paquete["idOrdenPaquete"].stringValue
                                    let nombrePaquete : String? = paquete["nombrePaquete"].stringValue
                                    let descripcion : String? = paquete["descripcion"].stringValue
                                    let montoIva : String? = paquete["montoIva"].stringValue
                                    let montoGravaIva : String? = paquete["montoGravaIva"].stringValue
                                    let montoNoGravaIva : String? = paquete["montoNoGravaIva"].stringValue
                                    let valorNormal : String? = paquete["valorNormal"].stringValue
                                    let valorDcto : String? = paquete["valorDcto"].stringValue
                                    let componentes : String? = paquete["componentes"].stringValue
                                    
                                    let paqueteP = PaquetesPagarModel(idOrdenPaquete: idOrdenPaquete, nombrePaquete: nombrePaquete, descripcion: descripcion, montoIva: montoIva, montoGravaIva: montoGravaIva, montoNoGravaIva: montoNoGravaIva, valorNormal: valorNormal, valorDcto: valorDcto, componentes: componentes)
                                    //print("el valor normal es : \(valorNormal!)")
                                    paquetesPagar = paquetesPagar + Double(valorDcto!)!
                                    self.paquetesp.append(paqueteP)
                                }
                            }
                            let persona = CarritoModel(tipoIdentificacion: tipoIdentificacion, numeroId: numeroId, nombre: nombre, telefono: telefono, direccion: direccion, mail: mail, citas: self.citasp, paquetes: self.paquetesp, totalCitas: citasPagar, totalPaquetes: paquetesPagar)
                            print("\(persona.nombre) - \(persona.totalCitas) - \(persona.totalPaquetes)")
                            self.carrito.append(persona)
                            self.total = self.total + persona.totalCitas + persona.totalPaquetes
                            
                        }
                        

                    }
                    
                } else {
                    let jsonError = json["resultado"].string
                    print(jsonError!)
                }
                
                //dispatch_async(dispatch_get_main_queue(), {
                DispatchQueue.main.async(execute: {
                    self.mostrarDetalles()
                    self.tableView.reloadData()
                    ViewUtilidades.hiddenLoader(controller: self)
                })
            }
            
            
        }
    }
    
    func mostrarDetalles() {
        let labl = UILabel(frame: CGRect(x: 10, y: 90, width: self.view.frame.width - 20, height: 50))
        labl.textAlignment = .center
        labl.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        labl.text = "Total : $ \(self.total)"
        
        
//        self.view.addSubview(labl)
        
        let btn_total = UIButton(frame: CGRect(x: 50, y: 100, width: self.view.frame.width - 100, height: 40))
        btn_total.setTitle("Total : $ \(self.total)", for: UIControlState.normal)
        //btn_total.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        btn_total.layer.borderWidth = 1
        btn_total.layer.borderColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1).cgColor // UIColor.lightGray.cgColor
        btn_total.setTitleColor(UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1), for: UIControlState.normal)
        self.view.addSubview(btn_total)
        
        self.tableView.frame = CGRect(x: 0.0, y: 154.0, width: self.view.frame.width,height: self.view.frame.height - 154);
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.backgroundColor = UIColor.clear
       // self.tableView.separatorColor = UIColor.clear
        self.tableView.register(CarritoTableViewCell.self as AnyClass, forCellReuseIdentifier: "cell");
        
        self.view.addSubview(self.tableView);
        print("el carrito tiene: \(self.carrito.count)")
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CarritoTableViewCell
     
        let total = Double(self.carrito[indexPath.row].totalCitas) + Double(self.carrito[indexPath.row].totalPaquetes)
        cell.setCell(pacientetext:  self.carrito[indexPath.row].nombre, totalPagar: "\(total)")
        
        return cell
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1//self.carrito.count
    }
    
    //Devuelve número de filas en la sección
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.carrito.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print(indexPath.row)
        print(self.carrito[indexPath.row].nombre)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DetalleCarrito") as! DetalleCarritoViewController
        vc.detalle = self.carrito[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
