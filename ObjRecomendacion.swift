//
//  ObjRecomendacion.swift
//  Mi Veris
//
//  Created by Jorge Merchan on 11/2/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import Foundation
class ObjRecomendacion
{
    var numeroorden : NSString = "0"
    var codigoempresa : NSInteger = 0
    var lineadetalle : NSString = "0"
    var eschequeo : NSString = ""
    var numeroidentificacionpaciente : NSInteger = 0
    var codigotipoidentificacion : NSString = ""
    var nombrepaciente : NSString = ""
    var codigomedico : NSString = ""
    var nombremedicointerno : NSString = ""
    var nombremedicoexterno : NSString = ""
    var codigoprestacion    :NSString = "0"
    var nombreprestacion : NSString = ""
    var codigoservicio : NSString = ""
    var nombreservicio : NSString = ""
    var codigoestado : NSString = ""
    var requiereagendamientoprevio : NSString = ""
    var codigoespecialidad: NSInteger = 0
    var esprocedimiento: NSString = ""
    var fechaorden : NSString = ""
    var codigopaquete : NSInteger = 0
    var codigocpt : NSString = ""
    var codigoalterno : NSString = ""
    var esagendable : NSString = ""
    var nombreespecialidad : NSString = ""
    var codigoespecialidadmedico : NSInteger = 0
    var esconsulta : NSString = ""
}

