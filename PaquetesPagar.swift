//
//  PaquetesPagar.swift
//  Mi Veris
//
//  Created by Samuel Velez on 8/3/17.
//  Copyright © 2017 Samuel Velez. All rights reserved.
//

import Foundation

class PaquetesPagarModel: NSObject {
    let idOrdenPaquete : String
    let nombrePaquete : String
    let descripcion : String
    let montoIva : String
    let montoGravaIva : String
    let montoNoGravaIva : String
    let valorNormal : String
    let valorDcto : String
    let componentes : String
    
    override var description: String {
        return "idOrdenPaquete : \(idOrdenPaquete), nombrePaquete: \(nombrePaquete), descripcion: \(descripcion), montoIva: \(montoIva),\n montoGravaIva: \(montoGravaIva), montoNoGravaIva: \(montoNoGravaIva), valorNormal: \(valorNormal), valorDcto: \(valorDcto),\n componentes: \(componentes)"
    }
    init(idOrdenPaquete : String?, nombrePaquete : String?, descripcion : String?, montoIva : String?, montoGravaIva : String?, montoNoGravaIva : String?, valorNormal : String?, valorDcto : String?, componentes : String?) {
        self.idOrdenPaquete = idOrdenPaquete ?? ""
        self.nombrePaquete = nombrePaquete ?? ""
        self.descripcion = descripcion ?? ""
        self.montoIva = montoIva ?? ""
        self.montoGravaIva = montoGravaIva ?? ""
        self.montoNoGravaIva = montoNoGravaIva ?? ""
        self.valorNormal = valorNormal ?? ""
        self.valorDcto = valorDcto ?? ""
        self.componentes = componentes ?? ""
        
    }

}
