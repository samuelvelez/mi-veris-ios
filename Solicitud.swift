//
//  Solicitud.swift
//  Mi Veris
//
//  Created by Jorge on 28/05/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import UIKit

class Solicitud: NSObject {
    
    var codigoSolicitud : NSInteger = 0
    var tipo : NSString = ""
    var fechaSolicitud : NSString = ""
    var fechaCierre : String = ""
    var motivo : String = ""
    var fechaDesde : String = ""
    var fechaHasta : String = ""
    var estado : String = ""
    
}