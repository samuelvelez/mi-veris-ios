//
//  FamiliaViewController.swift
//  Mi Veris
//
//  Created by Samuel Velez on 15/8/16.
//  Copyright © 2016 Programadores-iOS.net. All rights reserved.
//

import UIKit

class FamiliaViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var tableView: UITableView  =   UITableView()
    var listaRegiones : NSMutableArray?
    var listaFamiliares : NSMutableArray = NSMutableArray()
    //var listaLugares : NSMutableArray = NSMutableArray()
    var isCreated : Bool = false
    var pickerData : NSMutableArray = NSMutableArray()
    var txtRegion : UILabel?
    var posicionSucursal : NSInteger = 0
    var selectedRegion : NSInteger = 0
    var fondoPick : UIView?
    var btAceptar : UIButton = UIButton()
    
    override func viewDidAppear(_ animated: Bool) {
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        print("entro a viewdidappear")
        self.showRegiones()
        ViewUtilidades.hiddenLoader(controller: self)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        self.navigationController?.navigationItem.backBarButtonItem?.title = ""
        
        let fondo : UIImageView = UIImageView(frame: self.view.bounds)
        fondo.image = UIImage(named:"fondocita.png")
        self.view.addSubview(fondo)
        
        
        let barWhite : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width , height: 30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect( x: 0, y: 64, width: self.view.frame.width / 2.6, height: 30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x: barBlue.frame.width, y: 64, width: 45, height: 30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x: 0, y: 64, width: self.view.frame.width - 20, height: 30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "MI GRUPO"
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)
        btAceptar = UIButton(frame: CGRect( x: UIScreen.main.bounds.width - 70 , y: UIScreen.main.bounds.height - 164, width: 50, height: 50))
        btAceptar.setTitle("+", for: UIControlState.normal)
        btAceptar.titleLabel!.font = UIFont(name: "helvetica", size: 25)
        btAceptar.addTarget(self, action: #selector(FamiliaViewController.agregar), for: UIControlEvents.allTouchEvents)
        btAceptar.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        btAceptar.layer.cornerRadius = 25
        btAceptar.layer.zPosition = 1
        btAceptar.isEnabled = true
        tableView.addSubview(btAceptar)
        //self.view.addSubview(btAceptar)
        
       
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        var frame: CGRect = self.btAceptar.frame
        frame.origin.y = scrollView.contentOffset.y + (UIScreen.main.bounds.height) - 164
        btAceptar.frame = frame
        
        view.bringSubview(toFront: btAceptar)
    }
    
    func showRegiones(){
            
            ViewUtilidades.showLoader(controller: self, title: "Cargando...")
            let us = Usuario.getEntity as Usuario
            let iden = "\(us.tipoid)-\(us.numeroidentificacion)"
        let urldef = URLFactory.urldefinida()
        print(urldef)

            let url = NSURL(string: "\(urldef)servicio/persona/grupo/\(iden)")
            //let request = NSURLRequest(URL: url!)
            let request = NSMutableURLRequest(url: url! as URL)
            
            let str = "wsappusuario:ZA$57@9b86@$2r5"
            
        
        let utf8str = str.data(using: String.Encoding.utf8)
        
        let base64Encoded = str.data(using: String.Encoding.utf8, allowLossyConversion: true)
        //let base64Encoded = str.dataUsingEncoding(NSUTF8StringEncoding)!.base64EncodedStringWithOptions([])
        print("Encoded: \(base64Encoded)")
      
            
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                
                if (error != nil) {
                    print(error!.localizedDescription)
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    do{
                        var err: NSError?
                        
                        let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        if (err != nil) {
                            print("JSON Error \(err!.localizedDescription)")
                        }
                        
                        let info =  jsonResult as NSDictionary
                        let data = info["lista"] as! [[String: Any]]//NSArray
                        print(data)
                        if data.count > 0 {
                            self.listaFamiliares = NSMutableArray()
                            for familia in data {
                               
                                let fam = Familiar()
                                
                                if let tipoid = familia["tipoIdentificacion"] as? String{
                                    fam.tipoIdentificacion = tipoid as NSString
                                }
                                if let numid = familia["numeroIdentificacion"] as? String{
                                    fam.numeroIdentificacion = numid as NSString
                                }
                                if let primnom = familia["primerNombre"] as? String{
                                    fam.primerNombre = primnom as NSString
                                }
                                if let segnom = familia["segundoNombre"] as? String{
                                    fam.segundoNombre = segnom as NSString
                                }
                                if let primape = familia["primerApellido"] as? String{
                                    fam.primerApellido = primape as NSString
                                }
                                if let segape = familia["segundoApellido"] as? String{
                                    fam.segundoApellido = segape as NSString
                                }
                                if let edad = familia["edad"] as? String{
                                    fam.edad = edad as NSString
                                }
                                if let codpa = familia["codigoParentesco"] as? String{
                                    fam.codigoParentesco = codpa as NSString
                                }
                                if let paren = familia["parentesco"] as? String{
                                    fam.parentesco = paren as NSString
                                }
                                if let idrel = familia["idRelacion"] as? String{
                                    fam.idRelacion = idrel as NSString
                                }
                                if let esad = familia["esAdmin"] as? String{
                                    fam.esAdmin = esad as NSString
                                }
                                self.listaFamiliares.add(fam)

                                
                            }
                        }
                        
                        
                        DispatchQueue.main.async {
                            self.showSucursales()
                            
                            self.tableView.reloadData()
                            ViewUtilidades.hiddenLoader(controller: self)
                        }
                       /* DispatchQueue.main.asynchronously(execute: {
                            self.showSucursales()
                            
                            self.tableView.reloadData()
                            ViewUtilidades.hiddenLoader(controller: self)
                        })*/
                    } catch {
                        print("ERROR")
                    }
                }
                
                
            }
            //////////////////////////
            
            
        
    }
    
    
    @IBAction func agregar2(_ sender: AnyObject) {
        print("toco el boton 2")
    }
    
    
    
    func showSucursales(){
        tableView.frame = CGRect(x: 0, y: 94, width: self.view.frame.width, height: self.view.frame.height - 94);
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.backgroundColor = UIColor.clear
        tableView.layer.zPosition = 0
        tableView.register(FamiliarTableViewCell.self as AnyClass, forCellReuseIdentifier: "FamiliarCell");
        
        self.view.addSubview(tableView);
    }
    
    func showPicker(){
        
        if !isCreated {
            isCreated = true
            fondoPick = UIView(frame: CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: 230))
            fondoPick?.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
            fondoPick?.tag = 3000
            self.view.addSubview(fondoPick!)
            
            let btCancel:UIButton = UIButton(frame: CGRect( x: 5 , y: 0, width: 70, height: 40))
            btCancel.backgroundColor = UIColor.clear
            btCancel.setTitle("Cancelar", for: UIControlState.normal)
            btCancel.addTarget(self, action: "cancelPicker", for: UIControlEvents.touchUpInside)
            btCancel.titleLabel!.font = UIFont(name: "helvetica-bold", size: 15)
            btCancel.titleLabel?.textColor = UIColor.white
            btCancel.tag = 22;
            
            fondoPick?.addSubview(btCancel)
            
            let btAceptar:UIButton = UIButton(frame: CGRect( x: self.view.frame.width - 75 , y:  0, width: 70, height: 40))
            btAceptar.backgroundColor = UIColor.clear
            btAceptar.setTitle("Aceptar", for: UIControlState.normal)
            btAceptar.addTarget(self, action: "acceptPicker", for: UIControlEvents.touchUpInside)
            btAceptar.titleLabel!.font = UIFont(name: "helvetica-bold", size: 15)
            btAceptar.titleLabel?.textColor = UIColor.white
            btAceptar.tag = 22;
            
            fondoPick?.addSubview(btAceptar)
            
     
        }
        let transitionOptions = UIViewAnimationOptions.showHideTransitionViews //TransitionNone
        
        let back = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height - 230))
        back.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
        back.alpha = 0
        back.tag = 3001
        self.view.addSubview(back)
        
        UIView.transition(with: self.view, duration: 0.4, options: transitionOptions, animations: {
            
            back.alpha = 1
            let view : UIView = self.view.viewWithTag(3000)!
            view.frame = CGRect(x: 0 , y: self.view.frame.height - 230, width: self.view.frame.width, height: 230)
            }, completion: { finished in
                
        })
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return pickerData[row] as! String
    }
   
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /** editar o borrar **/
 
   /* func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        //items.removeAtIndex(indexPath.row)
        //saveItems()
        self.eliminar()
        tableView.beginUpdates()
        tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
        
        tableView.endUpdates()
    }
    */
    func eliminar(){
        print("eliminar")
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaFamiliares.count;
    }
    
    /*func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
     cell.separatorInset = UIEdgeInsetsZero
     cell.layoutMargins = UIEdgeInsetsZero
     }*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FamiliarCell", for: indexPath as IndexPath) as! FamiliarTableViewCell
        
        let familiar = listaFamiliares.object(at: indexPath.row) as! Familiar
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        var nom_completo : String = "\(familiar.primerNombre) \(familiar.segundoNombre) \(familiar.primerApellido) \(familiar.segundoApellido)"
        nom_completo = nom_completo.removingPercentEncoding!//stringByRemovingPercentEncoding!
        nom_completo = nom_completo.replacingOccurrences(of: "+", with: " ")
        nom_completo = nom_completo.replacingOccurrences(of: "  ", with: " ")
        
        
        if(familiar.esAdmin == "S"){
            cell.imagen.image = UIImage(named: "k_blue.png")
        }else{
            cell.imagen.image = UIImage(named: "k_gray.png")
        }
        //cell.imagen.image = UIImage(named: "key_blue.png")
        print(nom_completo)
        cell.title.text = nom_completo
        var subt = (familiar.parentesco as String).removingPercentEncoding!
        subt = subt.replacingOccurrences(of: "+", with: " ")
        subt = subt.replacingOccurrences(of: "  ", with: " ")

        cell.subtitle.text = subt
        
        cell.title.adjustsFontSizeToFitWidth = true
        cell.subtitle.adjustsFontSizeToFitWidth = true
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let familiar = listaFamiliares.object(at: indexPath.row) as! Familiar
        
        print("toco \(familiar.edad)")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "EditRol") as! EditRolViewController
        
        //vc.sucursal = listaLugares.objectAtIndex(indexPath.row) as! Sucursal
        vc.fam_selected = familiar
        navigationController?.pushViewController(vc, animated: true)
 
    }
    
    func agregar(){
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "AddFamilia") as! AddFamiliaViewController
        
        viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(title: " Atrás", style: UIBarButtonItemStyle.plain, target: self, action: "goBack")
        // Creating a navigation controller with viewController at the root of the navigation stack.
        let navController = UINavigationController(rootViewController: viewController)
        self.present(navController, animated:true, completion: nil)
 
    }
    
    func goBack(){
        dismiss(animated: true, completion: nil)
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
