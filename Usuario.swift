

import UIKit

private var usuario = Usuario()
private var userParent = Usuario()
private var familiares = NSMutableArray()

class Usuario: NSObject {
    
    var logueado : Bool = false;
    var nombre : NSString = ""
    var numeroidentificacion : NSString = ""
    var primeravez : NSString = ""
    var tipoid : NSString = ""
    var aceptatermino: NSString = ""
    var rol : NSInteger = 0
    var codigoCiudad : NSInteger = 0
    var codigoPais : NSInteger = 0
    var codigoProvincia : NSInteger = 0
    var codigoRegion : NSInteger = 0
    
    // codigo token 12/julio/2016
    var codigoToken : NSString = ""
    var parentesco : NSString = ""
    class var getEntity: Usuario {
        return usuario
    }
    
    class func deleteLogin() {
        usuario = Usuario()
        userParent = Usuario()
        familiares = NSMutableArray()
    }
    
    class var getParent: Usuario {
        return userParent
    }
    
    class var getFamiliares: NSMutableArray {
        return familiares
    }
    
    class func getDefaultUser(){
        let usuario = Usuario.getEntity
        let parent = Usuario.getParent
        if parent.nombre != ""{
            usuario.nombre = parent.nombre
            usuario.numeroidentificacion = parent.numeroidentificacion
            usuario.tipoid = parent.tipoid
            usuario.rol = 0
        }
    }
    
    class func createButtonFamily( controlador : UIViewController ){
        
        let image = UIImage(named: "flabajo.png") as UIImage!
        
        let img = UIImageView(frame: CGRect(x: controlador.view.frame.width - 28, y: 102, width: 23, height: 13))
            //(controlador.view.frame.width - 28, 102, 23, 13))
        img.image = image
        //controlador.view.addSubview(img)
        
        let familyButton  = UIButton(type: UIButtonType.system)
        //familyButton.setImage(image, forState: UIControlState.Normal)
        familyButton.addTarget(controlador, action: "getFamilyService", for: UIControlEvents.touchUpInside)
        familyButton.frame = CGRect(x: 0, y: 95, width: controlador.view.frame.width, height: 28)
        //( 0, 95, controlador.view.frame.width, 28);
        //controlador.view.addSubview(familyButton)
        
    }
    
    class func createButtonFamilyTop( controlador : UIViewController ){
        
        let image = UIImage(named: "flabajo.png") as UIImage!
        
        let img = UIImageView(frame: CGRect(x: controlador.view.frame.width - 28, y: 72, width: 23, height: 13))
            //(controlador.view.frame.width - 28, 72, 23, 13))
        img.image = image
        controlador.view.addSubview(img)
        
        let familyButton  = UIButton(type: UIButtonType.system)
        //familyButton.setImage(image, forState: UIControlState.Normal)
        familyButton.addTarget(controlador, action: Selector("getFamilyService"), for: UIControlEvents.touchUpInside)
        familyButton.frame = CGRect(x: 0, y: 65, width: controlador.view.frame.width, height: 28)
        //( 0, 65, controlador.view.frame.width, 28);
        controlador.view.addSubview(familyButton)
        
    }
    
    class func newcreateButtonFamilyTop( controlador : UIViewController, text : String){
        
     
        let btAceptar = UIButton(frame: CGRect(x: 30, y: 100, width: controlador.view.frame.width - 60, height: 40)) //(30, 100, controlador.view.frame.width - 60, 40))
        btAceptar.setTitle(text, for: UIControlState.normal)
        btAceptar.addTarget(controlador, action: Selector("getFamilyService"), for: UIControlEvents.touchUpInside)
        btAceptar.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        //btAceptar.layer.cornerRadius = 30
        btAceptar.titleLabel!.adjustsFontSizeToFitWidth = true
        
        btAceptar.layer.zPosition = 0
        controlador.view.addSubview(btAceptar)
        
        
    }
    
    class func citasButtonFamilyTop( controlador : UIViewController, text : String){
        
        
        let btAceptar = UIButton(frame: CGRect(x: 30, y: 100, width: controlador.view.frame.width - 60, height: 40)) //(30, 100, controlador.view.frame.width - 60, 40))
        btAceptar.setTitle(text, for: UIControlState.normal)
        btAceptar.addTarget(controlador, action: Selector("getFamilyService"), for: UIControlEvents.touchUpInside)
        btAceptar.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        //btAceptar.layer.cornerRadius = 30
        btAceptar.titleLabel!.adjustsFontSizeToFitWidth = true
        
        btAceptar.layer.zPosition = 0
        controlador.view.addSubview(btAceptar)
        
        
    }
    
    
    
    class func getJsonFamilia( controlador : UIViewController ){
        ViewUtilidades.showLoader(controller: controlador, title: "Cargando...")
        
        var usuario : Usuario = Usuario.getParent as Usuario
        if usuario.nombre == "" && usuario.numeroidentificacion == "" {
            usuario = Usuario.getEntity as Usuario
        }
        let urldef = URLFactory.urldefinida()
        let urlAsString = "\(urldef)servicio/perfiles/listafamiliapaciente?arg0=\(usuario.tipoid)&arg1=\(usuario.numeroidentificacion)";
        //let urlAsString = "http://miap.club/?\(usuario.tipoid)&arg1=\(usuario.numeroidentificacion)";
        let params = ["jgg":""] as Dictionary
        print("lista familia: \(urlAsString)")
        
        print( "es valida la ruta?: \(self.verifyUrl(urlString: urlAsString))")
        let url = NSURL(string: urlAsString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)!
        
        let urlSession = URLSession.shared
        
        let jsonQuery = urlSession.dataTask(with: url as URL, completionHandler: { params, response, error -> Void in
            if (error != nil) {
                print(error!.localizedDescription)
            }
            
            do{
            var err : NSError? = nil
            
            let jsonResult = try JSONSerialization.jsonObject(with: params!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
            if (err != nil) {
                print("JSON Error \(err!.localizedDescription)")
            }
            
            
            let info =  jsonResult as NSDictionary
                //arreglarnsarray
            let data = info["lista"] as! [[String: Any]]
            let familia = Usuario.getFamiliares as NSMutableArray
            if data.count > 0 {
                familia.removeAllObjects()
                for use in data {
                    let user = Usuario()
                    let nombre = use["primer_nombre"] as! NSString //use["primer_nombre"] as! NSString
                    let nombredos = use["segundo_nombre"] as! NSString
                    let appelido = use["primer_apellido"] as! NSString
                    let appelidodos = use["segundo_apellido"] as! NSString
                    user.nombre = "\(nombre) \(nombredos) \(appelido) \(appelidodos)" as NSString
                    user.numeroidentificacion = use["numero_identificacion"] as! String as NSString
                    let tipo = use["codigo_tipoidentificacion"] as! NSInteger
                    user.tipoid = "\(tipo)" as NSString
                    let rol : NSString = use["codigo_rol"] as! NSString
                    user.rol = rol.integerValue
                    user.parentesco = use["nombre_tipo_parentesco"] as! NSString
                    //usuario.aceptatermino = doctor["aceptatermino"] as NSString
                    familia.add(user)
                    //println( region["nombreciudad"] )
                }
            }
            
                DispatchQueue.main.async {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "viewFamiliares") as! FamiliaresViewController
                    controlador.navigationController?.pushViewController(vc, animated: true)
                    ViewUtilidades.hiddenLoader(controller: controlador)
}
            } catch {
                print("ERROR")
            }
                
        })
        jsonQuery.resume()
    }
    class func sendAnalytics(categoria : String , action: String , label : String){
        let tracker = GAI.sharedInstance().defaultTracker
        tracker?.set(kGAIScreenName, value: "Mi Veris")
        tracker?.send(GAIDictionaryBuilder.createEvent(withCategory: categoria, action: action, label: label, value: nil).build() as [NSObject : AnyObject])
    }
    
    class func verifyUrl(urlString: String?) -> Bool {
        guard let urlString = urlString,
            let url = URL(string: urlString) else {
                return false
        }
        
        return UIApplication.shared.canOpenURL(url)
    }
    
    class func  verificarEsadmin(identificacion: String) -> String{
        print("verifivcar esadmin \(identificacion)")
        
        let log = UserDefaults.standard.object(forKey: "identificacion")
        let t_log = UserDefaults.standard.object(forKey: "tipo")
        let us = Usuario.getEntity as Usuario
        //let iden = "\(us.tipoid)-\(us.numeroidentificacion)" 0926085739
        let iden = "\(t_log!)-\(log!)"
        let urldef = URLFactory.urldefinida()

        let url = NSURL(string: "\(urldef)servicio/persona/grupo/\(iden)?arg0=2")
        let request = NSMutableURLRequest(url: url! as URL)
        var resultado = ""
        var bool  = false
        /*
         //var request = NSMutableURLRequest(URL: NSURL(string: "YOUR URL")!)
         var session = NSURLSession.sharedSession()
         request.HTTPMethod = "GET"
         
         //var params = ["username":"username", "password":"password"] as Dictionary<String, String>
         
         //request.HTTPBody = try? NSJSONSerialization.dataWithJSONObject([], options: [])
         
         request.addValue("application/json", forHTTPHeaderField: "Content-Type")
         request.addValue("application/json", forHTTPHeaderField: "Accept")
         
         var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
         print("Response: \(response)")})
         
         task.resume()
         */
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            if (error != nil) {
                print(error!.localizedDescription)
                resultado = "N"
            } else {
                do{
                    var err: NSError?
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    if (err != nil) {
                        print("JSON Error \(err!.localizedDescription)")
                    }
                    
                    let info =  jsonResult as NSDictionary
                    let data = info["lista"] as! [[String: Any]]//NSArray
                   // print(data)
                    if data.count > 0 {
                        for familia in data {
                            if let res = familia["numeroIdentificacion"] as? String {
                                print("comparando \(res) con = \(us.numeroidentificacion)")
                                if res == us.numeroidentificacion as String {
                                    resultado = (familia["esAdmin"] as? String)!
                                    print("hola hola hola \(resultado)")
                                    bool = true
                                }
                            }
                        }
                    }
                    DispatchQueue.main.async(execute: {
                        print("holis \(resultado)")
                       
                    })
                } catch {
                    print("ERROR")
                }
            }
            
            
        }
        print("sale con \(resultado) ")
        //////////////////////////
        while(!bool){
            wait()
            return resultado

        }
        return "Y"

    }

    
}
