//
//  SolicitarHistoria2ViewController.swift
//  Mi Veris
//
//  Created by Jorge on 27/05/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import UIKit

class SolicitarHistoria2ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var titleView : NSString!
    
    var listaEspecialidades : NSMutableArray!
    var especialidadesSeleccionadas : NSMutableArray!
    
    var nombreUser : UILabel!
    
    var firstStep : UILabel!
    var secondStep : UILabel!
    var thirdStep : UILabel!
    var listaDoctores : NSMutableArray = NSMutableArray()
    
    var tabla : UITableView!
    var doctoresSeleccionados : NSMutableArray = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationItem.backBarButtonItem?.title = ""
        let doctor = Doctor()
        doctor.idDoctor = -1
        listaDoctores.add(doctor)
        
        self.navigationController?.navigationItem.backBarButtonItem?.title = ""
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        let fondo : UIImageView = UIImageView(frame: self.view.bounds)
        fondo.image = UIImage(named:"fondocita.png")
        self.view.addSubview(fondo)
        
        let barWhite : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width , height:30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width / 2.8, height:30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x: barBlue.frame.width, y: 64, width: 45, height: 30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x: 0, y: 64, width: self.view.frame.width - 20, height: 30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = titleView as String
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)
        
        let usuario = Usuario.getEntity as Usuario
        
        nombreUser = UILabel(frame: CGRect(x: 10,y: 94, width: self.view.frame.width - 20, height:30))
        nombreUser.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        nombreUser.font = UIFont(name: "helvetica-bold", size: 12)
        nombreUser.text = usuario.nombre as String
        
        self.view.addSubview(nombreUser)
        
        let anchoStep = self.view.frame.width / 3
        
        firstStep = UILabel(frame: CGRect(x: 0, y: 125, width: anchoStep, height: 30))
        firstStep.text = "Especialidad"
        firstStep.textAlignment = NSTextAlignment.center
        firstStep.textColor = UIColor.white
        firstStep.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        firstStep.font = UIFont.boldSystemFont(ofSize: 13)
        self.view.addSubview(firstStep)
        
        secondStep = UILabel(frame: CGRect( x:anchoStep, y:125, width:anchoStep, height:30))
        secondStep.text = "Doctor"
        secondStep.textAlignment = NSTextAlignment.center
        secondStep.textColor = UIColor.white
        secondStep.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        secondStep.font = UIFont.boldSystemFont(ofSize: 13)
        self.view.addSubview(secondStep)
        
        thirdStep = UILabel(frame: CGRect( x: anchoStep * 2,y:  125, width: anchoStep, height:30))
        thirdStep.text = "Fechas"
        thirdStep.textAlignment = NSTextAlignment.center
        thirdStep.textColor = UIColor.black
        thirdStep.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 0.3)
        thirdStep.font = UIFont.boldSystemFont(ofSize: 13)
        self.view.addSubview(thirdStep)
        
        let btPrev = UIButton(frame: CGRect( x: 20, y: self.view.frame.height - 50 , width:100, height:40))
        btPrev.setTitle("Anterior", for: UIControlState.normal)
        btPrev.addTarget(self, action: #selector(SolicitarHistoria2ViewController.prevSolicitud), for: UIControlEvents.touchUpInside)
        btPrev.backgroundColor = UIColor.lightGray
        btPrev.layer.cornerRadius = 15
        self.view.addSubview(btPrev)
        
        getDoctores()
        
    }
    
    func prevSolicitud(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func getDoctores(){
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        let usuario = Usuario.getEntity
        
        var stringEspecialidades = "";
        var especialidad : Especialidad!
        for index in especialidadesSeleccionadas {
            especialidad = listaEspecialidades.object( at: index as! NSInteger ) as! Especialidad
            if especialidad.codigo != -1 {
                if stringEspecialidades == ""{
                stringEspecialidades = "\(especialidad.codigo)"
                } else {
                    stringEspecialidades = "\(stringEspecialidades)@\(especialidad.codigo)"
                }
                
            }
        }
        let urldef = URLFactory.urldefinida()

        let url = NSURL(string: "\(urldef)servicio/doctor/obtenerdoctoreshcxespecialidad?arg0=\(usuario.numeroidentificacion)&arg1=\(usuario.tipoid)&arg2=\(stringEspecialidades)")
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                var err: NSError?
                
                //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                if (err != nil) {
                    print("JSON Error \(err!.localizedDescription)")
                }
                
                
                //println(jsonResult);
                //print(jsonResult)
                let info =  jsonResult as NSDictionary
                    let data = info["lista"] as! [[String: Any]]
                if data.count > 0 {
                    
                    for doctor in data {
                        let doc = Doctor()
                        doc.idTipoID = doctor["id_tipodeidentificacion"] as! NSInteger
                        doc.idDoctor = doctor["id_doctor"] as! NSInteger
                        doc.idEspecialidad = doctor["id_especialidad"] as! NSInteger
                        doc.nombre = doctor["nombredoctor"] as! String
                        doc.identificacion = doctor["identificacion"] as! String
                        doc.nombreEspecialidad = doctor["nombreespecialidad"] as! String
                        doc.primerApellido = doctor["primerapellido"] as! String
                        doc.segundoApellido = doctor["segundoapellido"] as! String
                        doc.segundoNombre = doctor["segundonombre"] as! String
                        doc.secuenciaPersonal = doctor["secuenciapersonal"] as! String
                        self.listaDoctores.add(doc)
                    }
                }
                
                //dispatch_async(dispatch_get_main_queue(), {
                    DispatchQueue.main.async(execute: {
                        
                        
                    ViewUtilidades.hiddenLoader(controller: self)
                    self.showDoctores()
                })
                } catch {
                    print("ERROR")
                }
            }
            
            
        }
    }
    
    func nextSolicitud(){
        if doctoresSeleccionados.count > 0 {
            
            if doctoresSeleccionados.count == 1 {
                let posicion : NSInteger = doctoresSeleccionados.object(at: 0) as! NSInteger
                let doctor : Doctor = listaDoctores.object( at: posicion ) as! Doctor
                
                if doctor.idDoctor != -1 {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "viewSolicitarTres") as! SolicitarHistorial3ViewController
                    vc.listaEspecialidades = listaEspecialidades
                    vc.especialidadesSeleccionadas = especialidadesSeleccionadas
                    vc.listaDoctores = listaDoctores
                    vc.titleView = titleView
                    vc.doctoresSeleccionados = doctoresSeleccionados
                    navigationController?.pushViewController(vc, animated: true)
                } else {
                    let alert = UIAlertController(title: "Veris", message: "Debe seleccionar al menos un doctor", preferredStyle: UIAlertControllerStyle.alert)
                    
                    let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                        UIAlertAction in
                        
                    }
                    
                    alert.addAction(okAction)
                    self.present(alert, animated: true, completion: nil)
                }
            } else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "viewSolicitarTres") as! SolicitarHistorial3ViewController
                vc.listaEspecialidades = listaEspecialidades
                vc.especialidadesSeleccionadas = especialidadesSeleccionadas
                vc.listaDoctores = listaDoctores
                vc.titleView = titleView
                vc.doctoresSeleccionados = doctoresSeleccionados
                navigationController?.pushViewController(vc, animated: true)
            }
            
            
        } else {
            let alert = UIAlertController(title: "Veris", message: "Debe seleccionar al menos un doctor", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                UIAlertAction in
                
            }
            
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showDoctores(){
        tabla = UITableView(frame: CGRect(x: 10, y: 160, width: self.view.frame.width - 20, height:self.view.frame.height - 230))
        tabla.delegate = self
        tabla.dataSource = self
        tabla.backgroundColor = UIColor.clear
        tabla.register(DoctorTable2ViewCell.self as AnyClass, forCellReuseIdentifier: "doctorCell");
        tabla.register(UITableViewCell.self as AnyClass, forCellReuseIdentifier: "cell");
        self.view.addSubview(tabla)
        
        let btNext = UIButton(frame: CGRect( x:self.view.frame.width - 120, y: self.view.frame.height - 50 , width:100, height:40))
        btNext.setTitle("Siguiente", for: UIControlState.normal)
        btNext.addTarget(self, action: #selector(SolicitarHistoria2ViewController.nextSolicitud), for: UIControlEvents.touchUpInside)
        btNext.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        btNext.layer.cornerRadius = 15
        self.view.addSubview(btNext)
        
    }
    
    // MARK: Métodos de tableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDoctores.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as UITableViewCell
            cell.textLabel?.text = "Seleccionar Todos"
            cell.backgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 0.5)
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "doctorCell", for: indexPath as IndexPath) as! DoctorTable2ViewCell
            
            let doctor = listaDoctores.object(at: indexPath.row) as! Doctor
            
            cell.imagen.image = UIImage(named: "medic.png")
            cell.imagen.tag = indexPath.row
            //“/compartidos/portal/medicos/”+secuenciapersonal+".JPG
            
            let directorio = "/compartidos/portal/medicos/\(doctor.secuenciaPersonal).JPG"
            let urldef = URLFactory.urldefinida()

            let url = URL(string: "\(urldef)servicio/login/imagen?arg0=\(directorio)")
            getDataFromUrl(url: url!) { (data, response, error)  in
                DispatchQueue.main.sync() { () -> Void in
                    guard let data = data, error == nil else { print(error!);return }
                    //cell.imagen.image = UIImage(data: data)
                    cell.imagen.image = UIImage(data: data)
                    if data != nil {
                        cell.imagen.image = UIImage(data: data)
                    } else{
                        cell.imagen.image = UIImage(named: "medic.png")
                    }
                    print("se ejecuto")
                }
            }

            
            cell.nombre.text = "\(doctor.primerApellido) \(doctor.segundoApellido) \(doctor.nombre) \(doctor.segundoNombre)"
            cell.nombre.sizeToFit();
            cell.nombre.frame = CGRect(x: 80, y: 5, width: self.view.frame.width - 130 , height: cell.nombre.frame.height)
            cell.reservar.isEnabled = false
            cell.reservar.alpha = 0
            
            cell.especialidad.text = doctor.nombreEspecialidad
            cell.backgroundColor = UIColor.clear
            
            let posicion : Int = doctoresSeleccionados.index(of: indexPath.row)
            if (posicion != NSNotFound) {
                cell.accessoryType = UITableViewCellAccessoryType.checkmark
            }
            
            if indexPath.row == 0 {
                cell.backgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 0.5)
            }
            
            return cell
        }
        
        //cell.backgroundColor = UIColor.clearColor()
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 40
        } else {
            return 80
        }
        
    }
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell : UITableViewCell = tableView.cellForRow(at: indexPath as IndexPath)! as UITableViewCell
        
        if indexPath.row == 0 {
            if ( cell.accessoryType == UITableViewCellAccessoryType.none ) {
                let count = listaDoctores.count
                doctoresSeleccionados.removeAllObjects()
                for index in 0...count - 1 {
                    doctoresSeleccionados.add(index)
                    let indexAll : NSIndexPath = NSIndexPath(row: index, section: 0)
                    
                    if let cell : UITableViewCell = tableView.cellForRow(at: indexAll as IndexPath) {
                        cell.accessoryType = UITableViewCellAccessoryType.checkmark
                    }
                }
            } else {
                let count = listaDoctores.count
                doctoresSeleccionados.removeAllObjects()
                for index in 0...count - 1 {
                    let indexAll : NSIndexPath = NSIndexPath(row: index, section: 0)
                    if let cell : UITableViewCell = tableView.cellForRow(at: indexAll as IndexPath) {
                        cell.accessoryType = UITableViewCellAccessoryType.none
                    }
                }
            }
            
            
        } else {
            if ( cell.accessoryType == UITableViewCellAccessoryType.none ) {
                cell.accessoryType = UITableViewCellAccessoryType.checkmark
                doctoresSeleccionados.add(indexPath.row)
            } else {
                cell.accessoryType = UITableViewCellAccessoryType.none;
                doctoresSeleccionados.remove(indexPath.row)
                // And remove the peerID from the array of selections
                //[selectedRows removeObjectForKey:keyString];
            }
        }
        
        
        tableView.deselectRow(at: indexPath as IndexPath, animated: true);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
