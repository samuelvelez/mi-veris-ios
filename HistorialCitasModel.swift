//
//  HistorialCitasModel.swift
//  veris
//
//  Created by Felipe Lloret on 06/02/15.
//  Copyright (c) 2015 Felipe Lloret. All rights reserved.
//

import Foundation

class HistorialCitasModel: NSObject {
    let fecha: String
    let nombresucursal: String
    let prestacion: String
    let medico: String
    let puntuacion: String
    let reserva: String
    
    let codigoespecialidad : String
    let codigomedico : String
    let codigoempresa :String
    let codigosucursal : String
    let codigoregion : String
    
    let codigoservicio : String
    let nombreservicio : String
    
    override var description: String {
        return "fecha: \(fecha), nombresucursal: \(nombresucursal)\n, prestacion: \(prestacion)\n, medico: \(medico)\n, puntuacion: \(puntuacion)\n, reserva: \(reserva)\n, codigoservicio: \(codigoservicio)\n, nombreservicio: \(nombreservicio)\n"
    }
    
    init(fecha: String?, nombresucursal: String?, prestacion: String?, medico: String?, puntuacion: String?, reserva: String?, codigoespecialidad: String?, codigomedico: String?, codigoempresa: String?, codigosucursal: String?, codigoregion : String?, codigoservicio : String? , nombreservicio : String?) {
        self.fecha = fecha ?? ""
        self.nombresucursal = nombresucursal ?? ""
        self.prestacion = prestacion ?? ""
        self.medico = medico ?? ""
        self.puntuacion = puntuacion ?? ""
        self.reserva = reserva ?? ""
        
        self.codigoespecialidad = codigoespecialidad ?? ""
        self.codigomedico = codigomedico ?? ""
        self.codigoempresa = codigoempresa ?? ""
        self.codigosucursal = codigosucursal ?? ""
        self.codigoregion = codigoregion ?? ""
        
        self.codigoservicio = codigoservicio ?? ""
        self.nombreservicio = nombreservicio ?? ""
    }
}