//
//  ReservaViewController.swift
//  Mi Veris
//
//  Created by Andres Cantos on 2/5/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import UIKit
import EventKit

class ReservaViewController: UIViewController, ReservaEspecialidadDelegate, ReservaServicioDelegate ,ReservaEstablecimientodelegate, ReservaDisponibilidadDelegate, SelectConvenioDelegate, UIPickerViewDataSource, UIPickerViewDelegate{
    
    
    var nombreUser : UILabel = UILabel()
    
    var flecha1 : UIImageView = UIImageView()
    var flecha2 : UIImageView = UIImageView()
    var flecha3 : UIImageView = UIImageView()
    var flecha4 : UIImageView = UIImageView()
    
    var numero1 : UIImageView = UIImageView()
    var numero2 : UIImageView = UIImageView()
    var numero3 : UIImageView = UIImageView()
    var numero4 : UIImageView = UIImageView()
    var numero5 : UIImageView = UIImageView()
    
    var tipodocusuario : String = ""
    var documentousuario : String = ""
    var dateFormatter = DateFormatter()
    var meses : [String] = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"]
    var scroll : UIScrollView!
    var btnespecialidad : UIButton!
    var btnprestacion : UIButton!
    var btnestablecimiento : UIButton!
    var btndoctor : UIButton!
    var btnconvenio : UIButton!
    var lblconvenios : UILabel!
    var lblfondo : UILabel!
    
    var btnreservar : UIButton!
    var posy : CGFloat = 130
    var listaconvenios : NSMutableArray = NSMutableArray()
    var valorconvenio : String = ""
    var fondoPick : UIView?
    @IBOutlet var myPicker: UIPickerView! = UIPickerView()
    
    var resultadoconvenio : String = ""
    var resultadoreserva : String = ""
    
    
    var especialidadsel : ObjEspecialidad!
    var serviciosel : ObjEspecialidad! //Es un servicio pero es muy parecido a una especialidad - se puede crear un objeto servicio
    var establecimientosel : ObjEstablecimiento!
    var disponibilidadsel : Objdisponibilidad!
    var conveniosel : ObjConvenio!
    var recomendacionsel : ObjRecomendacion = ObjRecomendacion()
    
    /*variables para controlar el paso de datos entre vistas*/
    //var tipodecarga : NSString = ""
    var tipodecarga : TipoCargaReserva = TipoCargaReserva.nueva
    var nombredoctorsel : String = "" /*Es el nombre del doctor seleccionado*/
    var codigodoctorsel : NSInteger = 0
    var codigoreservaactualizar : NSString = ""
    var codigoprestacion : NSString = "" /*cambio 23 junio*/
    var nombreprestacion : NSString = ""
    var codigoempresaprestacion : NSString = ""
    
    var identificacion_recibida : String?
    var tipoid_recibida : String?
    var nombre_recibido : String?
    
    var codigo_servicio : String = ""
    var est_idempresa : String = ""
    var est_id : String = ""
    var esp_cod : String = ""
    
    var usuarioR : Usuario?
    var nombre : String = ""
    
    var numeroOrden : String = ""
    var lineaDetalle : String = ""
    var codigoempresa : String = ""

    var establecimento_global : ObjEstablecimiento = ObjEstablecimiento()
    var disponibilidad_global : Objdisponibilidad?
    var convenio_global : ObjConvenio?
    
    var vienedemiscitas : String?
    var estapagada : String?
    
    override func viewDidAppear(_ animated: Bool) {
        
        print("revisando2 ando el tipo de carga es: \(tipodecarga)")
        
        if (estapagada != nil && estapagada == "S" ){
            //btnconvenio.isHidden = true
        }
        if (identificacion_recibida != nil){
            let user = usuarioR!
            print("viewDidAppear --- if \(usuarioR!.nombre)")
            nombreUser.text = usuarioR!.nombre as String
            nombre = usuarioR!.nombre as String
            self.tipodocusuario = user.tipoid as String
            self.documentousuario = user.numeroidentificacion as String
        }else{
            /*self.tipodocusuario = tipoid_recibida!
            self.documentousuario = identificacion_recibida!
            nombreUser.text = nombre_recibido*/
            print("viewDidAppear --- else")
            let user = Usuario.getEntity as Usuario
            //identificacion_recibida=user.numeroidentificacion as String
            //nombreUser.text = user.nombre as String
            
            self.tipodocusuario = user.tipoid as String
            self.documentousuario = user.numeroidentificacion as String
 
        }
        
        print("quiero ver las variables al entrar \(self.tipodocusuario) \(self.documentousuario)")
        
        
 
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (identificacion_recibida != nil){
            let user = usuarioR!
            print("viewdidload --- if")
            nombreUser.text = user.nombre as String
            self.tipodocusuario = user.tipoid as String
            self.documentousuario = user.numeroidentificacion as String
            nombre = user.nombre as String
            
            
        }else{
            print("viewdidload --- else")
            let user = Usuario.getEntity as Usuario
            nombreUser.text = user.nombre as String
            self.tipodocusuario = user.tipoid as String
            self.documentousuario = user.numeroidentificacion as String
            nombre = user.nombre as String
            
            
        }
        
        dateFormatter.locale = NSLocale(localeIdentifier: "es_EC") as Locale!
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.s"
        
        // print(user)
        self.agregarbarratitulo(nombrecompletouser: nombre)
        self.agregarbotones()
        print("el tipo de carga es: \(tipodecarga)")
        if tipodecarga == TipoCargaReserva.nueva{
            nombredoctorsel = ""; codigodoctorsel = 0
        }
        if tipodecarga == TipoCargaReserva.doctorfavorito{
            /*me trae la especialidad cargada, nombredeldoctorsel y el nombre - codigo del doctor favorito seleccionado*/
            mostrardatosbotones(especialidad: true,  servicio: true, establecimiento: false, doctor: true, convenio: false)
        }
        if tipodecarga == TipoCargaReserva.reutilizarcita{
            /*Me trae la especialidad cargada, establecimiento cargado y el nombre - codigo del doctor*/
            mostrardatosbotones(especialidad: true, servicio: true, establecimiento: true, doctor: true, convenio: false)
        }
        if tipodecarga == TipoCargaReserva.reagendar{
            /*Me trae la especialidad cargada, establecimiento cargado, y el codigoreserva para actualizar */
            mostrardatosbotones(especialidad:true,  servicio: true, establecimiento: true, doctor: false, convenio: false)
        }
        if tipodecarga == TipoCargaReserva.recomendacion{
            print("ingreso por recomendacion")
            /*me debe traer la orden de recomendacion cargada*/
            self.especialidadsel = ObjEspecialidad() /*Seteo la especialidad seleccionada */
            self.especialidadsel.codigo = self.recomendacionsel.codigoespecialidad
            self.especialidadsel.descripcion = self.recomendacionsel.nombreespecialidad as String
            self.serviciosel = ObjEspecialidad()
            self.serviciosel.codigo = Int(self.recomendacionsel.codigoprestacion as String)!
            self.serviciosel.descripcion = self.recomendacionsel.nombreprestacion as String
            mostrardatosbotones(especialidad:true, servicio: true ,establecimiento: false, doctor: false, convenio: false)
            
            if self.recomendacionsel.codigoespecialidadmedico == self.especialidadsel.codigo{
                /*si el codigo de la especialidad del medico q hizo la recomendacion es  == la codigo de la especialidad seleccionada
                solo se puede agendar con ese medico*/
                
            }
        }
    }
    
    func obtenerUsuarioLog()->Usuario{
        let usuario = Usuario.getEntity as Usuario
        
       // self.existUserDefault()
        
        
        var returnValue: NSString = UserDefaults.standard.object(forKey: "nombre") as! NSString
        usuario.nombre = returnValue
        
        
        returnValue = UserDefaults.standard.object(forKey: "identificacion") as! NSString
        usuario.numeroidentificacion = returnValue
        
        returnValue = UserDefaults.standard.object(forKey: "tipo") as! NSString
        usuario.tipoid = returnValue
        
        var returnValue2 : NSInteger = UserDefaults.standard.object(forKey: "ciudad") as! NSInteger
        usuario.codigoCiudad = returnValue2
        
        returnValue2 = UserDefaults.standard.object(forKey: "pais") as! NSInteger
        usuario.codigoPais = returnValue2
        
        returnValue2 = UserDefaults.standard.object(forKey: "provincia") as! NSInteger
        usuario.codigoProvincia = returnValue2
        
        returnValue2 = UserDefaults.standard.object(forKey: "region") as! NSInteger
        usuario.codigoRegion = returnValue2
        
        return usuario
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func agregarbarratitulo(nombrecompletouser : String){
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        self.navigationController?.navigationItem.backBarButtonItem?.title = ""
        //self.viewController.navigationItem.backBarButtonItem.title = "Custom Title";
        
        let fondo : UIImageView = UIImageView(frame: self.view.bounds)
        fondo.image = UIImage(named:"fondoreserva.png")
        self.view.addSubview(fondo)
        
        
        let barWhite : UIView = UIView(frame: CGRect(x:0, y:64, width:self.view.frame.width ,height: 30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x:0, y:64, width:self.view.frame.width / 2.6, height:30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x:barBlue.frame.width, y:64, width:45, height:30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x:0, y:64, width:self.view.frame.width - 20, height: 30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0	), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "Reservar"
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)
        
        let barname : UIView = UIView(frame: CGRect(x:0, y:94, width:self.view.frame.width , height:30))
        barname.backgroundColor = UtilidadesColor.colordehexadecimal(hex: 0x000000, alpha: 0.1)
        
        nombreUser = UILabel(frame: CGRect(x:10, y:2, width:self.view.frame.width - 20,height: 30))
        nombreUser.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        nombreUser.font = UIFont(name: "helvetica-bold", size: 12)
        
        
        nombreUser.text = nombrecompletouser
        barname.addSubview(nombreUser)
        self.view.addSubview(barname)
        
        let us = Usuario.getEntity as Usuario
        let used = usuarioR
        Usuario.createButtonFamily(controlador: self)
        
    }
    
    func getFamilyService(){
        Usuario.getJsonFamilia(controlador: self)
    }
    
    func agregarbotones(){
        let fuente : UIFont = UIFont(name: "helvetica", size: 14)!
        let colornormal : UIColor = UIColor(red: (48/256.0), green: (48/256.0), blue: (48/256.0), alpha: 1)
        let flecha1 : UIImageView = UIImageView()
        flecha1.frame = CGRect(x:self.view.frame.width - 50, y:15 , width:10, height:10)
        flecha1.image = UIImage(named: "abajo.png")
        numero1.frame = CGRect(x:0, y:0 , width:28, height:38)
        numero1.image = UIImage(named: "btn1.png")
        
        let flecha2 : UIImageView = UIImageView()
        flecha2.frame = CGRect(x:self.view.frame.width - 50, y:15 , width:10, height:10)
        flecha2.image = UIImage(named: "abajo.png")
        numero2.frame = CGRect(x:0, y:0 , width:28, height:38)
        numero2.image = UIImage(named: "btn2.png")
        
        let flecha3 : UIImageView = UIImageView()
        flecha3.frame = CGRect(x:self.view.frame.width - 50, y:15 , width:10, height:10)
        flecha3.image = UIImage(named: "abajo.png")
        numero3.frame = CGRect(x:0, y:0 , width:28, height:38)
        numero3.image = UIImage(named: "btn3.png")
        
        let flecha4 : UIImageView = UIImageView()
        flecha4.frame = CGRect(x:self.view.frame.width - 50, y:15 , width:10, height:10)
        flecha4.image = UIImage(named: "abajo.png")
        numero4.frame = CGRect(x:0, y:0 , width:28, height:38)
        numero4.image = UIImage(named: "btn4.png")
        
        let flecha5 : UIImageView = UIImageView()
        flecha5.frame = CGRect(x:self.view.frame.width - 50, y:15 , width:10, height:10)
        flecha5.image = UIImage(named: "abajo.png")
        numero5.frame = CGRect(x:0, y:0 , width:28, height:38)
        numero5.image = UIImage(named: "btn5.png")
        
        btnespecialidad = UIButton(frame: CGRect( x:15, y:posy, width:self.view.frame.width - 30, height:40))
        btnespecialidad.backgroundColor = UIColor.clear
        btnespecialidad.setTitle("Especialidad", for: UIControlState.normal)
        btnespecialidad.addTarget(self, action: #selector(ReservaViewController.seleccionarespecialidad), for: UIControlEvents.touchUpInside)
        btnespecialidad.titleLabel!.font = fuente
        btnespecialidad.setTitleColor(colornormal, for: UIControlState.normal)
        btnespecialidad.setTitleColor(UIColor.white, for: UIControlState.selected)
        btnespecialidad.tag = 10;
        btnespecialidad.titleLabel?.textAlignment = NSTextAlignment.center
        btnespecialidad.titleLabel?.numberOfLines = 1
        btnespecialidad.titleLabel?.adjustsFontSizeToFitWidth = true
        btnespecialidad.contentEdgeInsets = UIEdgeInsetsMake(0,27,0,20);
        btnespecialidad.layer.borderWidth = CGFloat(0.40)
        btnespecialidad.layer.borderColor = UtilidadesColor.colordehexadecimal(hex: 0x65c2ff, alpha: 1.0).cgColor
        btnespecialidad.addSubview(numero1)
        btnespecialidad.addSubview(flecha1)
        
        //servicio
        posy = posy + 51
        btnprestacion = UIButton(frame: CGRect( x:15, y:posy, width:self.view.frame.width - 30, height:45))
        btnprestacion.backgroundColor = UIColor.clear
        btnprestacion.setTitle("Servicio", for: UIControlState.normal)
        btnprestacion.titleLabel!.font = fuente
        btnprestacion.addTarget(self, action: #selector(ReservaViewController.seleccionarservicio), for: UIControlEvents.touchUpInside)
        btnprestacion.titleLabel!.font = fuente
        btnprestacion.setTitleColor(colornormal, for: UIControlState.normal)
        btnprestacion.setTitleColor(UIColor.white, for: UIControlState.selected)
        btnprestacion.tag = 20;
        btnprestacion.titleLabel?.textAlignment = NSTextAlignment.center
        btnprestacion.titleLabel?.numberOfLines = 4
        btnprestacion.titleLabel?.adjustsFontSizeToFitWidth = true
        
        btnprestacion.layer.borderWidth = CGFloat(0.40)
        btnprestacion.layer.borderColor = UtilidadesColor.colordehexadecimal(hex: 0x65c2ff, alpha: 1.0).cgColor
        btnprestacion.addSubview(numero2)
        btnprestacion.addSubview(flecha2)
        btnprestacion.contentEdgeInsets = UIEdgeInsetsMake(0,30,0,20);
        
        
        posy = posy + 54
        btnestablecimiento = UIButton(frame: CGRect( x:15, y:posy, width:self.view.frame.width - 30, height:40))
        btnestablecimiento.backgroundColor = UIColor.clear
        btnestablecimiento.setTitle("Establecimiento", for: UIControlState.normal)
        btnestablecimiento.addTarget(self, action: #selector(ReservaViewController.seleccionarestablecimiento), for: UIControlEvents.touchUpInside)
        btnestablecimiento.titleLabel!.font = fuente
        btnestablecimiento.setTitleColor(colornormal, for: UIControlState.normal)
        btnestablecimiento.setTitleColor(UIColor.white, for: UIControlState.selected)
        btnestablecimiento.tag = 30;
        btnestablecimiento.layer.borderWidth = CGFloat(0.40)
        btnestablecimiento.layer.borderColor = UtilidadesColor.colordehexadecimal(hex: 0x65c2ff, alpha: 1.0).cgColor
        btnestablecimiento.addSubview(numero3)
        btnestablecimiento.addSubview(flecha3)
        
        
        posy = posy + 51
        btndoctor = UIButton(frame: CGRect( x:15, y:posy, width:self.view.frame.width - 30, height:38))
        btndoctor.backgroundColor = UIColor.clear
        if(self.nombredoctorsel.isEmpty){
            btndoctor.setTitle("Doctor", for: UIControlState.normal)
        }else{
            btndoctor.setTitle(self.nombredoctorsel, for: UIControlState.normal)
        }
        btndoctor.addTarget(self, action: #selector(ReservaViewController.seleccionardisponibilidad), for: UIControlEvents.touchUpInside)
        btndoctor.titleLabel!.font = fuente
        btndoctor.setTitleColor(colornormal, for: UIControlState.normal)
        btndoctor.setTitleColor(UIColor.white, for: UIControlState.selected)
        btndoctor.titleLabel?.textAlignment = NSTextAlignment.center
        btndoctor.titleLabel?.numberOfLines = 4
        btndoctor.titleLabel?.adjustsFontSizeToFitWidth = true
        btndoctor.tag = 40;
        btndoctor.layer.borderWidth = CGFloat(0.40)
        btndoctor.layer.borderColor = UtilidadesColor.colordehexadecimal(hex: 0x65c2ff, alpha: 1.0).cgColor
        btndoctor.addSubview(numero4)
        btndoctor.addSubview(flecha4)
        //btndoctor.titleEdgeInsets = UIEdgeInsetsMake(0.0f, 6.0f, 0.0f, 6.0,f);
        btndoctor.contentEdgeInsets = UIEdgeInsetsMake(0,30,0,20);
        
        btnconvenio = UIButton(frame: CGRect( x:15, y:(posy + (38*2) + 11), width:self.view.frame.width - 30, height:38))
        btnconvenio.backgroundColor = UIColor.clear
        btnconvenio.setTitle("Convenio", for: UIControlState.normal)
        //btnconvenio.addTarget(self, action: "seleccionarconvenio", for: UIControlEvents.touchUpInside)
         btnconvenio.addTarget(self, action: #selector(ReservaViewController.newconvenio), for: UIControlEvents.touchUpInside)
        btnconvenio.titleLabel!.font = fuente
        btnconvenio.setTitleColor(colornormal, for: UIControlState.normal)
        btnconvenio.setTitleColor(UIColor.white, for: UIControlState.selected)
        btnconvenio.titleLabel?.textAlignment = NSTextAlignment.center
        btndoctor.tag = 50;
        btnconvenio.layer.borderWidth = CGFloat(0.40)
        btnconvenio.layer.borderColor = UtilidadesColor.colordehexadecimal(hex: 0x65c2ff, alpha: 1.0).cgColor
        btnconvenio.isHidden = true
        btnconvenio.addSubview(numero5)
        btnconvenio.addSubview(flecha5)
        
        self.view.addSubview(btnespecialidad)
        self.view.addSubview(btnprestacion)
        self.view.addSubview(btnestablecimiento)
        self.view.addSubview(btndoctor)
        self.view.addSubview(btnconvenio)
        
        /*Diseño del fondo para el Picker.*/
        fondoPick = UIView(frame: CGRect(x:0, y:self.view.frame.height, width:self.view.frame.width, height: 230))
        fondoPick?.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        fondoPick?.tag = 3000
        self.view.addSubview(fondoPick!)
        
        let btCancel:UIButton = UIButton(frame: CGRect(x:5 , y:0, width:70, height:40))
        btCancel.backgroundColor = UIColor.clear
        btCancel.setTitle("Cancelar", for: UIControlState.normal)
        btCancel.addTarget(self, action: #selector(ReservaViewController.cancelPicker), for: UIControlEvents.touchUpInside)
        btCancel.titleLabel!.font = UIFont(name: "helvetica-bold", size: 15)
        btCancel.titleLabel?.textColor = UIColor.white
        btCancel.tag = 22;
        fondoPick?.addSubview(btCancel)
        let btAceptar:UIButton = UIButton(frame: CGRect( x:self.view.frame.width - 75, y:0, width:70, height:40))
        btAceptar.backgroundColor = UIColor.clear
        btAceptar.setTitle("Aceptar", for: UIControlState.normal)
        btAceptar.addTarget(self, action: #selector(ReservaViewController.acceptPicker), for: UIControlEvents.touchUpInside)
        btAceptar.titleLabel!.font = UIFont(name: "helvetica-bold", size: 15)
        btAceptar.titleLabel?.textColor = UIColor.white
        btAceptar.tag = 22;
        fondoPick?.addSubview(btAceptar)
        
        myPicker.dataSource = self
        myPicker.delegate = self
        myPicker.frame = CGRect(x:0, y:40, width:self.view.frame.width, height:300)
        myPicker.backgroundColor = UIColor.white
        fondoPick!.addSubview(myPicker)
        /*Fin del diseño del Picker*/
        
        lblfondo = UILabel(frame: CGRect(x: 15, y:self.view.frame.height-123, width: self.view.frame.width - 30, height:70))
        lblfondo.backgroundColor = UIColor(red:(1),green:(0), blue: (0), alpha: 1)
        lblfondo.isHidden = true
        self.view.addSubview(lblfondo)
        
        lblconvenios = UILabel(frame: CGRect(x:20, y:self.view.frame.height-123, width: self.view.frame.width - 40, height:70))
        lblconvenios.text = " Importante: Si usted es paciente por primera vez y su seguro médico no consta en el cuadro superior o el valor a pagar no es el correcto, por favor comuniquese al 6009600."
        lblconvenios.numberOfLines = 0
        lblconvenios.drawText(in: CGRect(x:50,y:self.view.frame.height-123, width: self.view.frame.width - 50, height:70))
        //lblconvenios.backgroundColor = UIColor(red:(1),green:(0), blue: (0), alpha: 1)
        lblconvenios.font = lblconvenios.font.withSize(12)
        lblconvenios.textColor = UIColor(red: (256.0/256.0), green: (256.0/256.0), blue: (256.0/256.0), alpha: 1)
        lblconvenios.isHidden = true

        
        self.view.addSubview(lblconvenios)
        /*Boton reservar*/
        btnreservar = UIButton(frame: CGRect( x:15, y:self.view.frame.height-43, width:self.view.frame.width - 30, height:38))
        btnreservar.backgroundColor = UtilidadesColor.colordehexadecimal(hex: 0x0575b4, alpha: 1.0)
        btnreservar.setTitle("Reservar", for: UIControlState.normal)
        btnreservar.addTarget(self, action: #selector(ReservaViewController.reservar), for: UIControlEvents.touchUpInside)
        btnreservar.titleLabel!.font = fuente
        btnreservar.setTitleColor(UIColor.white, for: UIControlState.normal)
        btnreservar.titleLabel?.textAlignment = NSTextAlignment.center
        btnreservar.tag = 50;
        
        btnreservar.isHidden = false
        let defaults = UserDefaults.standard
        let verreserva : Bool = defaults.bool(forKey: "reserva")
        if verreserva{
            print("es reserva S")
            btnreservar.isHidden = false
        }else{
            print("es reserva N")
            btnreservar.isHidden = true
        }
        
        self.view.addSubview(btnreservar)
    }
    
    func seleccionarespecialidad(){
        if tipodecarga == TipoCargaReserva.doctorfavorito{
            UtilidadesGeneral.mensaje(mensaje: "No se permite modificar la especialidad")
            return
        }
        if tipodecarga == TipoCargaReserva.reutilizarcita{
            UtilidadesGeneral.mensaje(mensaje: "No se permite modificar la especialidad cuando se reutiliza la cita")
            return
        }
        if tipodecarga == TipoCargaReserva.reagendar{
            UtilidadesGeneral.mensaje(mensaje: "No se permite modificar la especialidad cuando se reagenda una cita médica")
            return
        }
        if tipodecarga == TipoCargaReserva.recomendacion{
            UtilidadesGeneral.mensaje(mensaje: "No se permite modificar la especialidad cuando una cita es recomendacion médica")
            return
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "viewReservaEspecialidad") as! ReservaEspecialidadViewController
        vc.delegate = self
        if tipodecarga == TipoCargaReserva.recomendacion{
            vc.recomendacionnumeroorden = self.recomendacionsel.numeroorden
            vc.recomendacioncodempresa = "\(self.recomendacionsel.codigoempresa)" as NSString
            vc.recomendacionlineadetalle = self.recomendacionsel.lineadetalle
        }else{
            vc.recomendacionlineadetalle = "0"; vc.recomendacioncodempresa = "0"; vc.recomendacionnumeroorden = "0";
        }
        print("se va desde reservaviewcontroller")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //nueva funcionalidad
    func seleccionarservicio(){
        if self.especialidadsel != nil {
           /* if tipodecarga == TipoCargaReserva.reutilizarcita{
                UtilidadesGeneral.mensaje("No se permite modificar el servicio cuando se reutiliza la cita")
                return
            }*/
            if tipodecarga == TipoCargaReserva.reagendar{
                UtilidadesGeneral.mensaje(mensaje: "No se permite modificar el servicio cuando se reagenda una cita médica")
                return
            }
            if tipodecarga == TipoCargaReserva.recomendacion{
                UtilidadesGeneral.mensaje(mensaje: "No se permite modificar el servicio cuando una cita es recomendacion médica")
                return
            }
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "viewReservaServicio") as! ReservaServicioViewController
            vc.especialidadsel = self.especialidadsel
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            UtilidadesGeneral.mensaje(mensaje: "Debe seleccionar una especialidad")
        }
        
    }
    
    
    func seleccionarestablecimiento(){
        if self.especialidadsel != nil {
            /*if tipodecarga == TipoCargaReserva.reutilizarcita{
                UtilidadesGeneral.mensaje("No se permite modificar la especialidad cuando se reutiliza la cita")
                return
            }*/
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "viewReservaEstablecimiento") as! ReservaEstablecimientoViewController
            vc.especialidadsel = self.especialidadsel
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            UtilidadesGeneral.mensaje(mensaje: "Debe seleccionar una Especialidad")
        }
        
    }
    func seleccionardisponibilidad(){
        let ud = Usuario.getEntity as Usuario
        print("hola antes de que te vayas soy : \(ud.tipoid) \(ud.numeroidentificacion)")
        if self.establecimientosel != nil {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "viewReservaDisponibilidad") as! ReservaDisponibilidadViewController
            vc.establecimientosel = self.establecimientosel
            vc.idespecialidadsel =  self.especialidadsel.codigo.description
            vc.delegate = self
            vc.tipodocusuario = ud.tipoid as String
            vc.documentousuario = ud.numeroidentificacion as String
            vc.codigomedico = self.codigodoctorsel
            vc.nombremedico = self.nombredoctorsel as NSString
            print(vc.nombremedico)
            if tipodecarga == TipoCargaReserva.recomendacion{
                vc.codigoprestacion = self.recomendacionsel.codigoprestacion
                vc.codigoempresaprestacion = "\(self.recomendacionsel.codigoempresa)" as NSString
            }else{
                vc.codigoprestacion = "\(self.serviciosel.codigo)" as NSString
                vc.codigoempresaprestacion = "\(self.establecimientosel.idempresa)" as NSString
                
            }
            if tipodecarga == TipoCargaReserva.reagendar{
                if(self.codigoprestacion as String).isEmpty{
                    vc.codigoprestacion = "0"
                }else{
                    vc.codigoprestacion = self.codigoprestacion
                }
                if(self.codigoempresaprestacion as String).isEmpty{
                    vc.codigoempresaprestacion = "0"
                }else{
                    vc.codigoempresaprestacion = self.codigoempresaprestacion
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            UtilidadesGeneral.mensaje(mensaje: "Debe seleccionar un establecimiento")
        }
    }
    func seleccionarconvenio(){
        /*muestro el view del picker*/
        let transitionOptions = UIViewAnimationOptions.showHideTransitionViews//TransitionNone
        let back = UIView(frame: CGRect(x:0, y:0, width:self.view.frame.width, height:self.view.frame.height - 230))
        back.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
        back.alpha = 0
        back.tag = 3001
        self.view.addSubview(back)
        UIView.transition(with: self.view, duration: 0.4, options: transitionOptions, animations: {
            
            back.alpha = 1
            let view : UIView = self.view.viewWithTag(3000)! /* view de picker*/
            view.frame = CGRect(x:0 , y:self.view.frame.height - 230, width:self.view.frame.width, height:230)
            view.layer.zPosition = self.btnreservar.layer.zPosition + 1;
            }, completion: { finished in
                
        })
        myPicker.reloadAllComponents() /*Refresco los datos del picker*/
    }
    /*Retorno de delegados*/
    func retornadereservaespecialidad(controller: ReservaEspecialidadViewController) {
        print("llega de reservaesecialidadviewcontroller a reservaviewcontroller")
        self.especialidadsel = controller.especialidadsel /*recupero la especialidad seleccionada*/
        mostrardatosbotones(especialidad:true , servicio: false, establecimiento: false, doctor: false, convenio: false)
        controller.navigationController?.popViewController(animated: true) /*Cierra la ventana de seleccion de especialidad*/
        self.cargardatos()
        encerarbotones(especialidad:false, servicio: true, establecimiento: true, doctor: true, convenio : true)
    
    }
    
    func retornadereservaservicio(controller: ReservaServicioViewController) {
        self.serviciosel = controller.serviciosel /*recupero el servicio seleccionada*/
        self.codigoempresa = (controller.recomendacioncodempresa) as String

        mostrardatosbotones(especialidad:true , servicio: true, establecimiento: true, doctor: false, convenio: false)
        controller.navigationController?.popViewController(animated: true) /*Cierra la ventana de seleccion de especialidad*/
        if(tipodecarga == TipoCargaReserva.doctorfavorito){
            print("doctor favorito")
            encerarbotones(especialidad:false, servicio: false, establecimiento: true, doctor: true, convenio : true)
        }
        else{
            print("no doctor favorito")
            encerarbotones(especialidad:false, servicio: false, establecimiento: true, doctor: true, convenio : true)
        }

    }
    
    func retornareservaestablecimiento(controller: ReservaEstablecimientoViewController) {
        self.establecimientosel = controller.establecimientosel
        mostrardatosbotones(especialidad:false, servicio: false, establecimiento: true, doctor: false, convenio: false)
        controller.navigationController?.popViewController(animated: true) /*Cierra la ventana de seleccion de especialidad*/
        encerarbotones(especialidad:false, servicio: false,establecimiento: false, doctor: true, convenio : true)
    }
    func retornareservadisponibilidad(controller: ReservaDisponibilidadViewController) {
        self.disponibilidadsel = controller.disponibilidadsel
        mostrardatosbotones(especialidad:false, servicio: true, establecimiento: false, doctor: true, convenio: false)
        controller.navigationController?.popViewController(animated: true) /*Cierra la ventana de seleccion de especialidad*/
        if ((estapagada != nil && estapagada == "N") || TipoCargaReserva.reagendar != tipodecarga ){
            consultaconvenios() /*consulta convenios o valor de la consulta*/
            print("entro al if que no ha pagado")
        }else{
            print("else , el boton debe de aparecer")
            btnconvenio.isHidden = true
            btnreservar.isHidden = false
            
            
            
        }

    }
    
    func retornadenewconvenio(controller: SelectConvenioViewController){
        print("retorno")
        _ = "\(self.serviciosel.codigo)"
        
        self.conveniosel = controller.conveniosel
        controller.navigationController?.popViewController(animated: true) /*Cierra la ventana de seleccion de especialidad*/
        if(self.conveniosel.permitereserva == "S"){
            self.validarconvenio(secuencia: self.conveniosel.secuenciaafiliado, seguro: self.conveniosel.tipows)
        }else{
            let alert = UIAlertController(title: "Veris", message: self.conveniosel.mensajebloqueoreserva, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }

    }
    /*Fin de retorno de delegados*/
    
    /*Consulta la lista de convenios de un usuario o el valor a pagar.*/
    func consultaconvenios(){
        let ud = Usuario.getEntity as Usuario
        print("hola antes de que te vayas soy : \(ud.tipoid) \(ud.numeroidentificacion)")
        
        ViewUtilidades.showLoader(controller: self, title: "Cargando \n convenios...")
        print("el codigo de la prestacion es: \(self.serviciosel.codigo)")
        print(self.establecimientosel.idempresa)
        self.listaconvenios.removeAllObjects();
        self.conveniosel = nil
        self.valorconvenio=""; self.resultadoconvenio = ""
        
        let url2 = URLFactory.obtenerconvenios(tipoidentificacion: ud.tipoid as String, identificacion: ud.numeroidentificacion as String, idespecialidad: "\(especialidadsel.codigo)", codigoempresa: "\(establecimientosel.idempresa)", codigosucursal: "\(establecimientosel.id)",
            codigoorden: self.recomendacionsel.numeroorden as String,
            codigoempresaorden: "\(self.recomendacionsel.codigoempresa)",
            codigolineaordden: self.recomendacionsel.lineadetalle as String, codigoprestacion: "\(self.serviciosel.codigo)", codigoempresaprestacion: "\(self.establecimientosel.idempresa)")
        
        
        print(url2)
        let url = NSURL(string: "\(url2)")
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        _ = "wsappusuario:ZA$57@9b86@$2r5"
        
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                var err: NSError?
                
                //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                if (err != nil) {
                    print("JSON Error \(err!.localizedDescription)")
                }
                //println(jsonResult)
                let info =  jsonResult as NSDictionary
                self.resultadoconvenio = info["resultado"] as! String
                if self.resultadoconvenio == "ok"{
                    self.valorconvenio = info["texto"] as! String
                    if self.valorconvenio.isEmpty{
                        let data = info["lista"] as! NSArray
                        if data.count > 0 {
                            for jsonobj in data {
                                let con = ObjConvenio(data: jsonobj as! NSDictionary)
                                self.listaconvenios.add(con)
                            }
                        }
                    }else{
                        
                    }
                }else{
                    
                }
                
                //dispatch_async(dispatch_get_main_queue(), {
                DispatchQueue.main.async(execute: {
                    self.showconvenios()
                    ViewUtilidades.hiddenLoader(controller: self)
                })
                } catch {
                    print("ERROR")
                }
            }
            
            
        }
        //////////////////////////
        

    }
    func showconvenios(){
        print("show convenios \(self.resultadoconvenio)")
        self.btnreservar.isHidden = false;
        if self.resultadoconvenio == "ok"{
            btnconvenio.isHidden = false
            
            if self.valorconvenio.isEmpty{ /*si el valor del convenio es vacio, encerio el boton visualmente*/
                btnconvenio.backgroundColor = UIColor.clear
                btnconvenio.setTitle("Convenio", for: UIControlState.selected)
                btnconvenio.isSelected = false
            }else{
                btnconvenio.backgroundColor = UtilidadesColor.colordehexadecimal(hex: 0x65c2ff, alpha: 1.0)
                btnconvenio.setTitle("valor: $" + self.valorconvenio, for: UIControlState.selected)
                btnconvenio.isSelected = true
            }
        }else{
            UtilidadesGeneral.mensaje(mensaje: self.resultadoconvenio)
            self.resultadoconvenio = ""
        }
    }
    /*Encera botones y datos*/
    func encerarbotones(especialidad : Bool, servicio : Bool,  establecimiento : Bool, doctor : Bool, convenio : Bool){
        
        if(especialidad){
            self.especialidadsel = nil
            btnespecialidad.isSelected = false
            btnespecialidad.backgroundColor = UIColor.clear
        }
        if(servicio){
            self.serviciosel = nil
            btnprestacion.isSelected = false
            btnprestacion.backgroundColor = UIColor.clear
        }
        
        if establecimiento{
            self.establecimientosel = nil
            btnestablecimiento.isSelected = false
            btnestablecimiento.backgroundColor = UIColor.clear
        }
        
        if doctor{
            self.disponibilidadsel = nil
            
            self.nombredoctorsel = ""
            btndoctor.isSelected = false
            btndoctor.backgroundColor = UIColor.clear
            btndoctor.frame = CGRect( x:15, y:posy, width:self.view.frame.width - 30, height:38)
            (btndoctor.subviews.last as! UIImageView).frame = CGRect(x:self.view.frame.width - 50, y:15 , width:10, height:10)
        }
        
        if convenio{
            self.lblconvenios.isHidden = true
            self.lblfondo.isHidden = true
            self.conveniosel = nil
            btnconvenio.isSelected = false
            btnconvenio.isHidden = true
            btnconvenio.backgroundColor = UIColor.clear
        }
    }
    func mostrardatosbotones (especialidad : Bool, servicio : Bool, establecimiento : Bool, doctor : Bool, convenio : Bool){
        if( especialidad && self.especialidadsel != nil){
            btnespecialidad.setTitle(self.especialidadsel!.descripcion, for: UIControlState.selected)
            btnespecialidad.isSelected = true
            btnespecialidad.backgroundColor = UtilidadesColor.colordehexadecimal(hex: 0x65c2ff, alpha: 1.0)
        }
        if( servicio && self.serviciosel != nil){
            btnprestacion.setTitle(self.serviciosel!.descripcion, for: UIControlState.selected)
            btnprestacion.isSelected = true
            btnprestacion.backgroundColor = UtilidadesColor.colordehexadecimal(hex: 0x65c2ff, alpha: 1.0)
            if(self.serviciosel.codigo == 0 ){
                print("entro al if")
                self.serviciosel = nil
                btnprestacion.isSelected = false
                btnprestacion.backgroundColor = UIColor.clear
            }
        }
        if(establecimiento && self.establecimientosel != nil){
            btnestablecimiento.setTitle(self.establecimientosel.descripcion as String, for: UIControlState.selected)
            btnestablecimiento.isSelected = true
            btnestablecimiento.backgroundColor = UtilidadesColor.colordehexadecimal(hex: 0x65c2ff, alpha: 1.0)
        }
        if(doctor){
            if(disponibilidadsel != nil){
                self.nombredoctorsel = self.disponibilidadsel.nombredoctor
                let horainicio : NSDate  = dateFormatter.date( from: self.disponibilidadsel.horainicio)! as NSDate
                let myCalendar : NSCalendar = NSCalendar.current as NSCalendar
                let myComponents = myCalendar.components([NSCalendar.Unit.year, NSCalendar.Unit.day, NSCalendar.Unit.month, NSCalendar.Unit.hour, NSCalendar.Unit.minute], from: horainicio as Date)
                let t : String = self.nombredoctorsel + "\n" + self.disponibilidadsel.diasemana + ", \(myComponents.day!) de \(meses[myComponents.month! - 1]) del \(myComponents.year!)" + "\n" + String(format: "%02d", myComponents.hour!) + "H" + String(format: "%02d", myComponents.minute!)
                //println(t)
                print("el tiempo es: \(t)")
                btndoctor.setTitle(t, for: UIControlState.selected)
                btndoctor.isSelected = true
                btndoctor.backgroundColor = UtilidadesColor.colordehexadecimal(hex: 0x65c2ff, alpha: 1.0)
                btndoctor.frame = CGRect( x:15, y:posy, width:self.view.frame.width - 30, height:38*2)
                (btndoctor.subviews.last as! UIImageView).frame = CGRect(x:self.view.frame.width - 50, y:34 , width:10, height:10)
            }else{
                btndoctor.setTitle(self.nombredoctorsel, for: UIControlState.normal)
                btndoctor.isSelected = false
                btndoctor.backgroundColor = UIColor.clear
                btndoctor.frame = CGRect( x:15, y:posy, width:self.view.frame.width - 30, height:38)
                (btndoctor.subviews.last as! UIImageView).frame = CGRect(x:self.view.frame.width - 50, y:15 , width:10, height:10)
            }
        }
        if(convenio && self.conveniosel != nil){
            /*mostrar los datos del convenio.*/
            
        }
    }
    /*Fin de encerar datos*/
    /*Funciones del Delegate del Picker - convenios.*/
    /*func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }*/
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return listaconvenios.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return (listaconvenios[row] as! ObjConvenio).nombreaseguradora as String
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        conveniosel = listaconvenios[row] as! ObjConvenio
    }
    /*Fin de funciones del delegate del Picker.*/
    
    /*Funciones de seleccion del Picker...*/
    func cancelPicker(){
        let transitionOptions = UIViewAnimationOptions.showHideTransitionViews//TransitionNone
        
        UIView.transition(with: self.view, duration: 0.3, options: transitionOptions, animations: {
            let back : UIView = self.view.viewWithTag(3001)!
            back.alpha = 0
            let view : UIView = self.view.viewWithTag(3000)!
            view.frame = CGRect(x:0 , y:self.view.frame.height, width:self.view.frame.width, height:230)
            }, completion: { finished in
                let back : UIView = self.view.viewWithTag(3001)!
                back.removeFromSuperview()
        })
        
        if(self.valorconvenio.isEmpty){ /*si aun no tiene un valor convenio seleccionado, encera el convenio seleccionado*/
            self.conveniosel = nil
        }
    }
    
    func acceptPicker(){
        /*quito el view del picker*/
        let transitionOptions = UIViewAnimationOptions.showHideTransitionViews //TransitionNone
        
        UIView.transition(with: self.view, duration: 0.3, options: transitionOptions, animations: {
            let back : UIView = self.view.viewWithTag(3001)!
            back.alpha = 0
            let view : UIView = self.view.viewWithTag(3000)!
            view.frame = CGRect(x:0 , y:self.view.frame.height, width:self.view.frame.width, height:230)
            }, completion: { finished in
                let back : UIView = self.view.viewWithTag(3001)!
                back.removeFromSuperview()
        })
        
        if (self.conveniosel == nil){
            if(TipoCargaReserva.reagendar != tipodecarga){
                UtilidadesGeneral.mensaje(mensaje: "Debe seleccionar un convenio")
            }else{
                print("ahora si fue cierto")
            }
        }else{
            
            ViewUtilidades.showLoader(controller: self, title: "Cargando valor...")
            self.valorconvenio=""; self.resultadoconvenio = ""
            let codigoprestacion = "\(self.serviciosel.codigo)"
            print("\(conveniosel.codigoempresa)")
            let url2 = URLFactory.obtenervalorizarconvenio(tipoidentificacion: tipodocusuario, identificacion: documentousuario, idespecialidad: "\(especialidadsel.codigo)", codigoempresa: "\(establecimientosel.idempresa)", codigosucursal: "\(establecimientosel.id)", codigoconvenio: "\(conveniosel.codigoconvenio)", codigoempresaconvenio: "\(conveniosel.codigoempresa)", secuenciaafiliadoconvenio: "\(conveniosel.secuenciaafiliado)", codigoorden: self.recomendacionsel.numeroorden as String,
                codigoempresaorden: "\(self.recomendacionsel.codigoempresa)",
                codigolineaordden: self.recomendacionsel.lineadetalle as String, codigoprestacion: codigoprestacion,  codigoempresaprestacion: "\(conveniosel.codigoempresa)")
            
            let url = NSURL(string: "\(url2)")
            print("codigo sucursal: \(establecimientosel.id)")
            print(url!)
            //let request = NSURLRequest(URL: url!)
            let request = NSMutableURLRequest(url: url! as URL)
            
            
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                
                
                if (error != nil) {
                    print(error!.localizedDescription)
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    do{
                    var err: NSError?
                    
                    //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    if (err != nil) {
                        print("JSON Error \(err!.localizedDescription)")
                    }
                    
                    //println(jsonResult)
                    let info =  jsonResult as NSDictionary
                    self.resultadoconvenio = info["resultado"] as! String
                    if self.resultadoconvenio == "ok"{
                        self.valorconvenio = info["texto"] as! String
                        if self.valorconvenio.isEmpty{
                            self.resultadoconvenio = "Error obteniendo el valor del convenio"
                        }else{
                            
                        }
                    }else{
                        
                    }
                    
                    //dispatch_async(dispatch_get_main_queue(), {
                        DispatchQueue.main.async(execute: {
                        self.showconvenios() /*envio a mostrar el dato del valor del convenio*/
                        ViewUtilidades.hiddenLoader(controller: self)
                            //self.btnreservar.isHidden = false;
                    })
                    } catch {
                        print("ERROR")
                    }
                }
                
                
            }
            //////////////////////////
            
            
            
        }
    }
    /*Fin de funciones de seleccion del picker*/
    
    /*Funciço que realiza la reserva*/
    func reservar(){
        self.recomendacionsel.numeroorden = self.numeroOrden as NSString;
        self.recomendacionsel.lineadetalle = self.lineaDetalle as NSString;
        print(" codigo de la empresa: \(self.codigoempresa)")
        
        if especialidadsel == nil {
            UtilidadesGeneral.mensaje(mensaje: "Debe seleccionar una especialidad")
            return
        }
        if serviciosel == nil {
            UtilidadesGeneral.mensaje(mensaje: "Debe seleccionar un servicio")
            return
        }
        if establecimientosel == nil {
            UtilidadesGeneral.mensaje(mensaje: "Debe seleccionar un establecimiento")
            return
        }
        if disponibilidadsel == nil {
            UtilidadesGeneral.mensaje(mensaje: "Debe seleccionar un horario")
            return
        }
        if self.valorconvenio.isEmpty {
            if(self.estapagada != "S"){
                UtilidadesGeneral.mensaje(mensaje: "Debe seleccionar un convenio")
                return
            }
        }
        
        print("el codigo que va en reservar: \(self.serviciosel.codigo)")
        
        self.est_idempresa = "\(self.establecimientosel.idempresa)"
        self.est_id = "\(self.establecimientosel.id)"
        self.esp_cod = "\(self.especialidadsel.codigo)"
        

        self.codigo_servicio = "\(self.serviciosel.codigo)"
        let refreshAlert = UIAlertController(title: "Veris", message: "Está seguro de enviar la solicitud?", preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Cancelar", style: .default, handler: { (action: UIAlertAction) in
            //println("Handle Cancel Logic here")
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { (action: UIAlertAction) in
            if self.tipodecarga == TipoCargaReserva.reagendar{
                self.accionreagendar()
            }else{
            
               
                self.accionreservar()
            }
            
            
        }))
        
        present(refreshAlert, animated: true, completion: nil)
        
    }
    
    /*
        10:00 PM
        Sra. Amalia
        Bentacur de banderillo
        8111119
    */
    
    func saveEventCalendar(){
        let alert = UIAlertController(title: "Veris", message: "¿Desea sincronizar la cita en el calendario de ios?", preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
            UIAlertAction in
            
            let nombreDoc = self.disponibilidadsel.nombredoctor
            let nombreEsp = self.especialidadsel.descripcion
            let nombreEst = self.establecimientosel.descripcion
            let horaInit = self.disponibilidadsel.horainicio
            
            let date2 : NSDate  = self.dateFormatter.date( from: horaInit )! as NSDate
            //let myCalendar : NSCalendar = NSCalendar.current as NSCalendar
           // let myComponents = myCalendar.components([NSCalendar.Unit.year, NSCalendar.Unit.day, NSCalendar.Unit.month, NSCalendar.Unit.hour, NSCalendar.Unit.minute], from: date2 as Date)
            
            let formatter = DateFormatter()
            formatter.locale = NSLocale(localeIdentifier: "es_EC") as Locale!
            formatter.dateFormat = "yyyy-MM-dd"
            //var date : NSDate = formatter.dateFromString(dateString)!
            //println("date: \(date!)") // date: 2014-10-09 13:57:00 +0000
            
            
            
            let eventStore : EKEventStore = EKEventStore()
            // 'EKEntityTypeReminder' or 'EKEntityTypeEvent'
            eventStore.requestAccess(to: EKEntityType.event, completion: {
                granted, error in
                if (granted) && (error == nil) {
                    
                    let event:EKEvent = EKEvent(eventStore: eventStore)
                    event.title = "CONSULTA: \(nombreEsp)"
                    event.startDate = date2 as Date
                    event.endDate = date2 as Date
                    event.notes = "DOCTOR: \(nombreDoc) \n\(nombreEsp) \n\(nombreEst)"
                    event.calendar = eventStore.defaultCalendarForNewEvents
                    do {
                        try eventStore.save(event, span: .thisEvent)
                    } catch _ {
                    }
                    print("Saved Event")
                }
            })
            
            
            // This lists every reminder
            let predicate = eventStore.predicateForReminders(in: [])
            eventStore.fetchReminders(matching: predicate) { reminders in
                for reminder in reminders! {
                    print(reminder.title)
                }}
            
            
            // What about Calendar entries?
            let startDate=NSDate().addingTimeInterval(-60*60*24)
            let endDate=NSDate().addingTimeInterval(60*60*24*3)
            let predicate2 = eventStore.predicateForEvents(withStart: startDate as Date, end: endDate as Date, calendars: nil)
            
            print("startDate:\(startDate) endDate:\(endDate)")
            let eV = eventStore.events(matching: predicate2) as [EKEvent]!
            
            if eV != nil {
                for i in eV! {
                    print("Title  \(i.title)" )
                    print("stareDate: \(i.startDate)" )
                    print("endDate: \(i.endDate)" )
                    
                    if i.title == "Consulta \(nombreEsp)" {
                        print("YES" )
                        // Uncomment if you want to delete
                        //eventStore.removeEvent(i, span: EKSpanThisEvent, error: nil)
                    }
                }
            }
            
            self.encerarbotones(especialidad:true, servicio: true, establecimiento: true, doctor: true, convenio : true)
            if(self.vienedemiscitas == "si"){
                //self.navigationController?.popToRootViewController(animated: true)
                print("reservaviewcontroller - if - deberia de remover 2")
                self.navigationController!.viewControllers.removeLast(1)
            }else{
                //self.enviaravercitas()
                print("ok - else")
                self.navigationController!.viewControllers.removeLast(1)
            }
        }
        let cancelAction = UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            self.encerarbotones(especialidad:true, servicio: true, establecimiento: true, doctor: true, convenio : true)
            if(self.vienedemiscitas == "si"){
                //self.navigationController?.popToRootViewController(animated: true)
                print("reservaviewcontroller - cancelar - if - deberia de remover 2")
                self.navigationController!.viewControllers.removeLast(1)
            }else{
                //self.enviaravercitas()
                print("cancel - else")
                self.navigationController!.viewControllers.removeLast(1)
            }
        }
        
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func accionreservar(){
        self.resultadoreserva = ""
        
        var codconvenioreserva : NSInteger = 0
        var codempresareservaconvenio : NSInteger = 0
        var esnil : String = ""
        esnil = "\(self.conveniosel)"
        if (esnil != "nil"){
            print("igual entra con \(esnil)")
            if let cc = self.conveniosel{
                codconvenioreserva =  cc.codigoconvenio
                codempresareservaconvenio =  cc.codigoempresa
            }
            
        }

        /*if let co = self.conveniosel as? ObjConvenio {
            codconvenioreserva =  co.codigoconvenio
            codempresareservaconvenio = co.codigoempresa

        }*/
        else{
            print("no entro")
            codconvenioreserva = 0; codempresareservaconvenio = 0;
        }
        print("------------")
        print("tipo documento: \(tipodocusuario)")
        print("documento: \(documentousuario)")
        var inter : String = ""
        if let idint = self.disponibilidadsel{
              inter = idint.idsintervalos
        }
        var conv : NSInteger = 1
        if let cosel = self.conveniosel{
            conv = cosel.codigoempresa
        }
        print("codempresaprestacion: \(conv)")
        var cdpre : NSInteger = 0
        if let cosel = self.serviciosel{
            cdpre = cosel.codigo
        }
        
        //ViewUtilidades.showLoader(self, title: "Procesando...")
        let codigoprestacion = cdpre //self.serviciosel.codigo as! String
        
        Usuario.sendAnalytics(categoria: "Opciones Menu MiVeris", action: "Abrir", label: "Reservar")
        
        
        let iden_log = UserDefaults.standard.object(forKey: "identificacion")
        
        let urldef = URLFactory.urldefinida()

        let url2 = "\(urldef)servicio/v2/agenda/reservar2?arg0=\(tipodocusuario)&arg1=\(documentousuario)&arg2=\(inter)&arg3=\(codconvenioreserva)&arg4=\(codempresareservaconvenio)&arg5=\(self.est_idempresa)&arg6=\(self.est_id)&arg7=\(self.esp_cod)&arg8=\(self.recomendacionsel.numeroorden)&arg9=\(self.codigoempresa)&arg10=\(self.recomendacionsel.lineadetalle)&arg11=\(conv)&arg12=\(codigoprestacion)&arg13=\(iden_log)"
        
        
        let url = NSURL(string: "\(url2)")
        let request = NSMutableURLRequest(url: url! as URL)
        
        let str = "wsappusuario:ZA$57@9b86@$2r5"
        
        _ = str.data(using: String.Encoding.utf8)
        
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                //var err: NSError? = nil
                
                //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                /*if (err != nil) {
                    print("JSON Error \(err!.localizedDescription)")
                }*/
                
                print(jsonResult)
                let info =  jsonResult as NSDictionary
                self.resultadoreserva = info["resultado"] as! String
                
                    //if (self.resultadoreserva.containsString("ok")){
                        //let not : notificacion = notificacion()
                        //not.addItem(self.disponibilidadsel.horainicio)

                    //}
                //dispatch_async(dispatch_get_main_queue(), {
                    DispatchQueue.main.async(execute: {
                        self.terminoreserva() /*envia a encerar la reserva*/
                        ViewUtilidades.hiddenLoader(controller: self)
                    
                    })
                } catch {
                    print("ERROR")
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: "Revise su conexión a Datos o Internet", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            
            
        }
        //////////////////////////
        
        
    }
    
    func validarconvenio(secuencia: Int, seguro: String){
        print("validando convenio")
        var stringurl = ""
        var buscaseguro = false
        var buscabmi = false
        let urldef = URLFactory.urldefinida()

        if seguro == "SALUD"{
            buscaseguro = true
            stringurl = "\(urldef)servicio/v2/agenda/validarsalud?arg0=\(secuencia)"
        }else if( seguro == "BMI"){
            buscabmi = false
            stringurl = "\(urldef)servicio/v2/agenda/validarbmi?arg0=\(secuencia)"
        }else{
            stringurl = "no debe"
            self.valorizar(precioparticular: "N", cobertura: "S")
        }
        print("la url es \(stringurl) y es \(buscaseguro)")
        if buscaseguro{
            let url = NSURL(string: stringurl)
            let request = NSMutableURLRequest(url: url! as URL)
            
            
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                if (error != nil) {
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    do{
                        //print("llego")
                        var err: NSError?
                        
                        let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        if (err != nil) {
                            print("JSON Error \(err!.localizedDescription)")
                        }
                        //print(jsonResult)
                        let info =  jsonResult as NSDictionary
                        let resultado = info["resultado"] as! String
                        print("elresultado es : \(resultado)")
                        
                        if(resultado == "ok"){
                            let texto = info["texto"] as! String
                            print(texto)
                            let ver = texto.components(separatedBy: "||")
                            print(ver.count)
                            
                            if (ver[0] == "TIMEOUT"){
                                let alert = UIAlertController(title: "Veris", message: "La aseguradora no responde en este momento, el valor que aparece ​será sin cobertura​.​", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                self.valorizar(precioparticular: "S", cobertura: "N")
                            }else if(ver[0] == "ELM"){
                                let alert = UIAlertController(title: "Veris", message: "Tu​ seguro médico no está vigente, el valor ​será de ​una ​tarifa normal​. Si tienes duda​s​ comunícate con tu aseguradora", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                self.valorizar(precioparticular: "S", cobertura: "N")
                            }
                            for i in 0 ..< ver.count {
                                print("valor[\(i)]: \(ver[i])")
                                //print (i) //i will increment up one with each iteration of the for loop
                                
                            }
                            if(ver.count>2){
                            if(ver[0] == "N" && ver.count>2){
                                let alert = UIAlertController(title: "Veris", message: "La aseguradora no aprobo cobertura en este monento, el valor que aparece no aplicara cobertura", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                self.valorizar(precioparticular: "S", cobertura: "N")
                                
                            }else if(ver[1] == "S" && ver.count>2){
                                let alert = UIAlertController(title: "Veris", message: "Tu seguro está en período de carencia, el valor que aparece está sin cobertura​.Si tienes duda​s​ comunícate con tu aseguradora​​.", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                self.valorizar(precioparticular: "S", cobertura: "N")
                            }
                            else if(ver[2] == "S" && ver.count>2){
                                let alert = UIAlertController(title: "Veris", message: "Tu seguro médico está ​en ​mora, el valor ​será sin cobertura​. Si tienes duda​s​ comunícate con tu aseguradora​​", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                self.valorizar(precioparticular: "S", cobertura: "N")
                            }else if(ver[0] == "S" && ver[1] == "N" && ver[2] == "N"){
                                self.valorizar(precioparticular: "N", cobertura: "S")
                            }
                            }else{
                                self.valorizar(precioparticular: "S", cobertura: "N")
                            }
                        }else{
                            let alert = UIAlertController(title: "Veris", message: resultado, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            
                        }
                        DispatchQueue.main.async(execute: {
                            ViewUtilidades.hiddenLoader(controller: self)
                        })
                    } catch {
                        ViewUtilidades.hiddenLoader(controller: self)
                        let alert = UIAlertController(title: "Veris", message: "Revise su conexión a Datos o Internet", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }else if (buscabmi){
                print("entro a bmi")
                let url = NSURL(string: stringurl)
                let request = NSMutableURLRequest(url: url! as URL)
                
                
                NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                    if (error != nil) {
                        ViewUtilidades.hiddenLoader(controller: self)
                        let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        do{
                            //print("llego")
                            var err: NSError?
                            let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                            if (err != nil) {
                                print("JSON Error \(err!.localizedDescription)")
                            }
                            let info =  jsonResult as NSDictionary
                            let resultado = info["resultado"] as! String
                            if(resultado == "ok"){
                                let texto = info["texto"] as! String
                                if (texto == "TIMEOUT"){
                                    let alert = UIAlertController(title: "Veris", message: "La aseguradora no responde en este momento, el valor que aparece ​será sin cobertura​.​", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                    self.valorizar(precioparticular: "S", cobertura: "N")
                                }else if(texto == "ELM"){
                                    let alert = UIAlertController(title: "Veris", message: "Tu​ seguro médico no está vigente, el valor ​será de ​una ​tarifa normal​. Si tienes duda​s​ comunícate con tu aseguradora", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                    self.valorizar(precioparticular: "S", cobertura: "N")
                                }else if(texto == "CUBRE"){
                                    self.valorizar(precioparticular: "N", cobertura: "S")
                                }
                                else{
                                    self.valorizar(precioparticular: "S", cobertura: "N")
                                }
                                
                            }else{
                                let alert = UIAlertController(title: "Veris", message: resultado, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                
                            }
                            DispatchQueue.main.async(execute: {
                                ViewUtilidades.hiddenLoader(controller: self)
                            })
                        } catch {
                            ViewUtilidades.hiddenLoader(controller: self)
                            let alert = UIAlertController(title: "Veris", message: "Revise su conexión a Datos o Internet", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }
            }
        }
    
    func valorizar(precioparticular : String, cobertura : String){
        print("valorizar")
        let ud = Usuario.getEntity as Usuario
        
        let codigoprestacion = "\(self.serviciosel.codigo)"
        let url2 = URLFactory.obtenervalorizarconvenio2(tipoidentificacion: ud.tipoid as String, identificacion: ud.numeroidentificacion as String, idespecialidad: "\(especialidadsel.codigo)", codigoempresa: "\(establecimientosel.idempresa)", codigosucursal: "\(establecimientosel.id)", codigoconvenio: "\(conveniosel.codigoconvenio)", codigoempresaconvenio: "\(conveniosel.codigoempresa)", secuenciaafiliadoconvenio: "\(conveniosel.secuenciaafiliado)", codigoorden: self.recomendacionsel.numeroorden as String, codigoempresaorden: "\(self.recomendacionsel.codigoempresa)", codigolineaordden: self.recomendacionsel.lineadetalle as String, codigoprestacion: codigoprestacion as String,  codigoempresaprestacion: "\(conveniosel.codigoempresa)",aplicarprecioparticular: precioparticular, aplicacobertura: cobertura)
        print("el codogi de prestacion \(codigoprestacion)")
        
        let url = NSURL(string: "\(url2)")
        
        let request = NSMutableURLRequest(url: url! as URL)
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            
            if (error != nil) {
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                    var err: NSError?
                    
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    if (err != nil) {
                        print("JSON Error \(err!.localizedDescription)")
                    }
                    
                    let info =  jsonResult as NSDictionary
                    print(jsonResult)
                    self.resultadoconvenio = info["resultado"] as! String
                    if self.resultadoconvenio == "ok"{
                        self.valorconvenio = info["texto"] as! String
                        if self.valorconvenio.isEmpty{
                            self.resultadoconvenio = "Error obteniendo el valor del convenio"
                        }else{
                            
                        }
                    }else{
                        
                    }
                    DispatchQueue.main.async(execute: {
                        self.showconvenios() /*envio a mostrar el dato del valor del convenio*/
                        ViewUtilidades.hiddenLoader(controller: self)
                    })
                } catch {
                    print("ERROR")
                }
            }
        }
    }

    func accionreagendar(){        
        self.resultadoreserva = ""
        
        var codconvenioreserva : NSInteger = 0
        var codempresareservaconvenio : NSInteger = 0
        var esnil : String = ""
        esnil = "\(self.conveniosel)"
        if (esnil != "nil"){
            print("igual entra con \(esnil)")
            if let cc = self.conveniosel{
                codconvenioreserva =  cc.codigoconvenio
                codempresareservaconvenio =  cc.codigoempresa
            }
            
        }
        else{
            print("no entro")
            codconvenioreserva = 0; codempresareservaconvenio = 0;
        }
        var inter : String = ""
        if let idint = self.disponibilidadsel{
            inter = idint.idsintervalos
        }
        var conv : NSInteger = 1
        if let cosel = self.conveniosel{
            conv = cosel.codigoempresa
        }
        
        var cdpre : NSInteger = 0
        if let cosel = self.serviciosel{
            cdpre = cosel.codigo
        }
        
        ViewUtilidades.showLoader(controller: self, title: "Procesando...")
        let ud = Usuario.getEntity as Usuario
        
        let url2 = URLFactory.obtenerreagendar(tipoidentificacion: ud.tipoid as String, identificacion: ud.numeroidentificacion as String, codreserva: self.codigoreservaactualizar as String, idintervalo: "\(disponibilidadsel.idsintervalos)", codigoconvenio: "\(codconvenioreserva)", codigoempresaconvenio: "\(codempresareservaconvenio)", idempresaestablecimiento: "\(establecimientosel.idempresa)", idsucursal: "\(establecimientosel.id)", codigoprofesional: "\(disponibilidadsel.codigomedico)")
        _ = Usuario.getEntity as Usuario
        
        // let url2 = "http://200.31.28.222:8080/MiverisWsrest/servicio/v2/agenda/reservar2?arg0=\(tipodocusuario)&arg1=\(documentousuario)&arg2=\(inter)&arg3=\(codconvenioreserva)&arg4=\(codempresareservaconvenio)&arg5=\(self.est_idempresa)&arg6=\(self.est_id)&arg7=\(self.esp_cod)&arg8=\(self.recomendacionsel.numeroorden)&arg9=\(self.recomendacionsel.codigoempresa)&arg10=\(self.recomendacionsel.lineadetalle)&arg11=\(conv)&arg12=\(codigoprestacion)&arg13=\(usuario.numeroidentificacion)"
        
        
        
        let url = NSURL(string: "\(url2)")
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                var err: NSError?
                
                //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                if (err != nil) {
                    print("JSON Error \(err!.localizedDescription)")
                }
                let info =  jsonResult as NSDictionary
                self.resultadoreserva = info["resultado"] as! String
                
                //dispatch_async(dispatch_get_main_queue(), {
                DispatchQueue.main.async(execute: {
                    self.terminoreagendar() /*envia a encerar la reserva*/
                    ViewUtilidades.hiddenLoader(controller: self)
                })
                } catch {
                    print("ERROR")
                }
            }
            
            
        }
        //////////////////////////
        
        
        
    }
    func terminoreserva(){
        if self.resultadoreserva == "ok"{
            UtilidadesGeneral.mensaje(mensaje: "Su Agenda ha sido reservada exitosamente, recuerde llegar 15 minutos antes a su cita")
            self.saveEventCalendar()
            //encerarbotones(especialidad:true, establecimiento: true, doctor: true, convenio : true)
        }else{
            UtilidadesGeneral.mensaje(mensaje: self.resultadoreserva)
            self.resultadoreserva = ""
        }
    }
    func terminoreagendar(){
        if self.resultadoreserva == "ok"{
            UtilidadesGeneral.mensaje(mensaje: "Su Agenda ha sido modificada exitosamente, recuerde llegar 15 minutos antes a su cita")
            self.saveEventCalendar()
            //encerarbotones(especialidad:true, establecimiento: true, doctor: true, convenio : true)
        }else{
            UtilidadesGeneral.mensaje(mensaje: self.resultadoreserva)
            self.resultadoreserva = ""
        }
    }
    func enviaravercitas(){
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500), execute: {
            Usuario.sendAnalytics(categoria: "Opciones Menu MiVeris", action: "Abrir", label: "Citas")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let vc = storyboard.instantiateViewController(withIdentifier: "viewCitas") as! MisCitasViewController
            self.navigationController?.pushViewController(vc, animated: false)
        })
        
        
        //let source = UIViewController
        

    }
    
    
    private func cargardatos(){
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        let urldef = URLFactory.urldefinida()
        let url2 = "\(urldef)servicio/v2/agenda/consultarprestaciones?arg0=\(especialidadsel?.codigo)"
        
        let url = NSURL(string: url2)
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                    print("llego")
                    var err: NSError?
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    if (err != nil) {
                        print("JSON Error \(err!.localizedDescription)")
                    }
                    //print(jsonResult)
                    let info =  jsonResult as NSDictionary
                    let data = info["lista"] as! [[String: Any]]
                    
                    if data.count == 1 {
                        //self.listaespecialidades = NSMutableArray();
                        
                        for jsonobj  in data {
                            let esp = ObjEspecialidad()
                            esp.codigo = (jsonobj["codigoPrestacion"] as! NSString).integerValue
                            esp.descripcion = jsonobj["nombrePrestacion"] as! String
                            self.serviciosel = esp
                            if let ver = jsonobj["numeroOrden"] as? String {
                                self.numeroOrden = ver
                                self.lineaDetalle = jsonobj["lineaDetalle"] as! String
                                self.codigoempresa = jsonobj["codigoEmpresa"]! as! String
                            }else{
                                self.numeroOrden = "0"
                                self.lineaDetalle = "0"
                                self.codigoempresa = "0"
                            }
                            print("le numero de orden es: \(self.numeroOrden)")

                            self.mostrardatosbotones(especialidad:true , servicio: true, establecimiento: true, doctor: false, convenio: false)
                            
                            //self.listaespecialidades?.addObject(esp)
                        }
                        print(data.description)
                        print(data.count)
                    }else{
                        ViewUtilidades.hiddenLoader(controller: self)
                    }
                    
                    //dispatch_async(dispatch_get_main_queue(), {
                    DispatchQueue.main.async(execute: {
                        //self.showEspecialidades()
                        ViewUtilidades.hiddenLoader(controller: self)
                    })
                } catch {
                    print("ERROR")
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: "Revise su conexión a Datos o Internet", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            
        }
        
    }
    
    func newconvenio(){
        if self.establecimientosel != nil {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "newConvenio") as! SelectConvenioViewController
            vc.recomendacionsel = self.recomendacionsel
            vc.especialidadsel = self.especialidadsel
            vc.establecimientosel = self.establecimientosel
            vc.tipodocusuario = self.tipodocusuario
            vc.documentousuario = self.documentousuario
            vc.serviciosel = self.serviciosel
            vc.delegate = self
            
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            UtilidadesGeneral.mensaje(mensaje: "Debe seleccionar un establecimiento")
        }
    }
}
