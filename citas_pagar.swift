//
//  citas_pagar.swift
//  Mi Veris
//
//  Created by Samuel Velez on 7/3/17.
//  Copyright © 2017 Samuel Velez. All rights reserved.
//

import Foundation

class CitasPagarModel: NSObject {
    let idReserva: String
    let fecha : String
    let sucursal : String
    let doctor: String
    let especialidad : String
    let montoIva : String
    let montoGravaIva: String
    let montoNoGravaIva : String
    let subtotalCopago : String
    let subtotalCliente: String
    let subtotalVenta : String
    let cantidad : String
    let idSucursal: String
    let aplicaIva : String
    let numVerTar : String
    let tieneCobertura: String
    let idServicio : String
    let idPrestacion : String
    let prestacion: String
    let idOrden : String
    let idConvenio : String
    let idTarjeta: String
    let nombreBeneficio : String
    
    
    override var description: String {
        return "idReserva: \(idReserva), fecha: \(fecha)\n, sucursal: \(sucursal)\n, doctor: \(doctor)\n, especialidad: \(especialidad)\n, montoIva: \(montoIva)\n, montoGravaIva: \(montoGravaIva), montoNoGravaIva: \(montoNoGravaIva), subtotalCopago: \(subtotalCopago), subtotalCliente: \(subtotalCliente)\n, subtotalVenta: \(subtotalVenta)\n, cantidad: \(cantidad), idSucursal: \(idSucursal), aplicaIva: \(aplicaIva), numVerTar: \(numVerTar)\n, tieneCobertura: \(tieneCobertura)\n, idServicio: \(idServicio), idPrestacion: \(idPrestacion), prestacion: \(prestacion)\n, idOrden: \(idOrden), idConvenio: \(idConvenio), idTarjeta: \(idTarjeta), nombreBeneficio: \(nombreBeneficio)"
    }
    
    init(idReserva: String?, fecha : String?, sucursal : String?, doctor: String?, especialidad : String?, montoIva : String?,  montoGravaIva: String?,
    montoNoGravaIva : String?, subtotalCopago : String?, subtotalCliente: String?, subtotalVenta : String?,
    cantidad : String?, idSucursal: String?, aplicaIva : String?, numVerTar : String?,
    tieneCobertura: String?, idServicio : String?, idPrestacion : String?, prestacion: String?,
    idOrden : String?, idConvenio : String?, idTarjeta: String?, nombreBeneficio : String?) {
        
        self.idReserva = idReserva ?? ""
        self.fecha = fecha ?? ""
        self.sucursal = sucursal ?? ""
        self.doctor = doctor ?? ""
        self.especialidad = especialidad ?? ""
        self.montoIva = montoIva ?? ""
        self.montoGravaIva = montoGravaIva ?? ""
        self.montoNoGravaIva = montoNoGravaIva ?? ""
        self.subtotalCopago = subtotalCopago ?? ""
        self.subtotalCliente = subtotalCliente ?? ""
        self.subtotalVenta = subtotalVenta ?? ""
        self.cantidad = cantidad ?? ""
        self.idSucursal = idSucursal ?? ""
        self.aplicaIva = aplicaIva ?? ""
        self.numVerTar = numVerTar ?? ""
        self.tieneCobertura = tieneCobertura ?? ""
        self.idServicio = idServicio ?? ""
        self.idPrestacion = idPrestacion ?? ""
        self.prestacion = prestacion ?? ""
        self.idOrden = idOrden ?? ""
        self.idConvenio = idConvenio ?? ""
        self.idTarjeta = idTarjeta ?? ""
        self.nombreBeneficio = nombreBeneficio ?? ""
    }

    /*
     
     
     "idSucursal": "1-1",
     "aplicaIva": "N",
     "numVerTar": "12",
     "tieneCobertura": "S",
     "idServicio": "12",
     "idPrestacion": "2710-1",
     "prestacion": "VISITA EN LA OFICINA DE UN NUEVO PACIENTE QUE REQUIERE DE TRES COMPONENTES: 1.- HISTORIA DETALLADA 2.- EXAMEN DETALLADO 3.- DECISION MEDICA UNICA Y DIRECTA TIEMPO DE DURACION 20 MINUTOS",
     "idOrden": "4708951-1-1",
     "idConvenio": "1207359-15349-1",
     "idTarjeta": null,
     "nombreBeneficio": "LATINOMEDICAL SA / LATINOMEDICAL SA"
 */
    
}
