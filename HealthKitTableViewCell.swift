//
//  HealthKitTableViewCell.swift
//  Mi Veris
//
//  Created by Jorge on 04/07/16.
//  Copyright © 2016 Programadores-iOS.net. All rights reserved.
//

import UIKit

class HealthKitTableViewCell: UITableViewCell {

    @IBOutlet weak var tipo: UILabel!
    @IBOutlet weak var resultado: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
