//
//  ViewUtilidades.swift
//  Veris
//
//  Created by Jorge on 02/02/15.
//  Copyright (c) 2015 Firewall Soluciones All rights reserved.
//

import UIKit

class ViewUtilidades: NSObject {
   
     // MARK: showLoader: Muestra activtyIndicator
    
    
    class func showLoader( controller : UIViewController, title : String) {
        
        let viewFondo : UIView = UIView(frame: CGRect(x: 0, y: 64, width: controller.view.frame.width, height: controller.view.frame.height))
            //( 0, 64 , controller.view.frame.width, controller.view.frame.height))
        viewFondo.backgroundColor = UIColor.clear
        viewFondo.tag = 2003;
    
        let viewLoad : UIView = UIView(frame: CGRect(x: (controller.view.frame.width / 2 - 60), y: (controller.view.frame.height / 2 - 35), width: 120, height: 90))
            //( (controller.view.frame.width / 2 - 60), (controller.view.frame.height / 2 - 35) , 120, 90))
        viewLoad.layer.cornerRadius = 10
        viewLoad.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6)
        viewLoad.tag = 2000;
        
        let actInd : UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100)) as UIActivityIndicatorView
            //(0,0, 100, 100)) as UIActivityIndicatorView
        actInd.center = controller.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.white
        actInd.transform = CGAffineTransform(scaleX: 1.5, y: 1.5);
        actInd.startAnimating()
        actInd.tag = 2001
        
        let loadTitle = UILabel(frame: CGRect(x: 0, y: (controller.view.frame.height / 2 + 15), width: controller.view.frame.width, height: 30))
            //(0, (controller.view.frame.height / 2 + 15), controller.view.frame.width, 30))
        loadTitle.textColor = UIColor.white
        loadTitle.font = UIFont(name: "helvetica-bold", size: 16)
        loadTitle.text = title
        loadTitle.textAlignment = NSTextAlignment.center
        loadTitle.tag = 2002
        
        controller.view.addSubview(viewFondo)
        controller.view.addSubview(viewLoad)
        controller.view.addSubview(actInd)
        controller.view.addSubview(loadTitle)
    }
    
    
    // MARK: hiddenLoader: Oculta activtyIndicator
    
    class func hiddenLoader( controller : UIViewController) {
    
        let transitionOptions = UIViewAnimationOptions.showHideTransitionViews //transitionNone
        
        UIView.transition(with: controller.view, duration: 0.2, options: transitionOptions, animations: {

            if let view = controller.view.viewWithTag(2000) {
                view.alpha = 0;
            }
            if let view = controller.view.viewWithTag(2001) {
                view.alpha = 0;
            }
            if let view = controller.view.viewWithTag(2002) {
                view.alpha = 0;
            }
            
            }, completion: { finished in
                if let view = controller.view.viewWithTag(2003) {
                    view.removeFromSuperview()
                }
                if let view = controller.view.viewWithTag(2000) {
                    view.removeFromSuperview()
                }
                if let view = controller.view.viewWithTag(2001) {
                    view.removeFromSuperview()
                }
                if let view = controller.view.viewWithTag(2002) {
                    view.removeFromSuperview()
                }

        })
        
        
    }
}
