//
//  URLFactory.swift
//  SwiftPlaces
//
//  Created by Joshua Smith on 7/25/14.
//  Copyright (c) 2014 iJoshSmith. All rights reserved.
//

import Foundation

/** Builds the URLs needed to call Web services. */
class URLFactory
{
    /**
    Creates a URL that searches for places with a certain postal code.
    Only supports numeric postal codes, due to a Web service limitation.
    */
    class func obtenerparametrosapp() -> NSURL
    {
        return urlWithName(name: "servicio/catalogos/obtenerparametrosapp",
            args: [
                "arg0" : "0"])
    }
    
    class func obtenercitacercanadehoy(tipo_doc: String, num_doc: String) -> NSURL
    {
        return urlWithName(name: "servicio/agenda/citamascercanadehoy",
            args: [
                "arg0" : tipo_doc,
                "arg1" : num_doc])
    }
    
    class func obtenerEspecialidades(tipoIdentificacion: String, numeroIdentificacion: String) -> NSURL
        //numeroorden: String, codempresaorden: String, lineadetalleorden: String) -> NSURL
    {
        //cambio de servicio
        //return urlWithName(name: "servicio/especialidad/especialidadestodas",
        //http://181.198.116.238:8080/MiverisWsrest/servicio/v2/especialidad/obtenerespecialidades4?arg0=2&arg1=0926085739
        return urlWithName(name: "servicio/v2/especialidad/obtenerespecialidades4",
                args: [
                    "arg0": tipoIdentificacion,
                    "arg1": numeroIdentificacion
                ])
            /*args: [
                "arg0" : numeroorden,
                "arg1" : codempresaorden,
                "arg2" : lineadetalleorden])*/
    }
    class func obtenerResultadosLaboraios(tipo_doc: String, num_doc: String) -> NSURL {
        return urlWithName(name: "servicio/doctor/resultadoslaboratorio",
            args: [
                "arg0" : tipo_doc,
                "arg1" : num_doc])
    }
    class func obtenerResultadosProcedimientos(tipo_doc: String, num_doc: String) -> NSURL {
        return urlWithName(name: "servicio/doctor/resultadoproc",
            args: [
                "arg0" : tipo_doc,
                "arg1" : num_doc])
    }
    class func obtenerResultadosImagenes(tipo_doc: String, num_doc:
        String) -> NSURL
    {
        return urlWithName(name: "servicio/doctor/resultadosimagenes",
            args: [
                "arg0" : tipo_doc,
                "arg1" : num_doc])
    }

    class func obtenerRegionCiudades()-> NSURL
    {
        return urlWithName(name: "servicio/catalogos/regionciudades",
            args: [
                "arg0" : "0"])
    }
    
    class func obtenerRecomendaciones(tipo_doc: String, num_doc: String, desconocido: String) -> NSURL
    {
        return urlWithName(name: "servicio/agenda/ordenesxreservar",
            args: [
                "arg0" : tipo_doc,
                "arg1" : num_doc,
                "arg2" : desconocido])
    }
    
    class func obtenerDetallesCita(numeroorden: String, codempresa: String ) -> NSURL
    {
        return urlWithName(name: "servicio/agenda/detalleordennoagendable",
            args: [
                "arg0" : numeroorden,
                "arg1" : codempresa])
    }
    
    class func obtenersucursalesxregion(idpais : String, idprovincia : String, idciudad : String, idespecialidad : String) -> NSURL
    {
        return urlWithName(name: "servicio/establecimiento/sucursalesxregionciudad",
        args: [
            "arg0" : idpais,
            "arg1" : idprovincia,
            "arg2" : idciudad,
            "arg3" : idespecialidad])
    }
    
    class func obtenerdisponibilidadpaginada(codigoregion : String, codigoempresa : String, codigosucursal : String, idespecialidad : String,
        tipoidentificacion : String, identificacion : String, fecha : String,
        codigoempresaprestacion : String, codigoprestacion : String,
        codigomedico: String,
        iniciopagina: String, tamaniopagina : String) -> NSURL
    {
        return urlWithName(name: "servicio/agenda/obtenerdisponibilidadpagin",
            args: [
                "arg0" : codigoregion,
                "arg1" : codigoempresa,
                "arg2" : codigosucursal,
                "arg3" : idespecialidad,
                "arg4" : tipoidentificacion,
                "arg5" : identificacion,
                "arg6" : fecha,
                "arg7" : codigoempresaprestacion,
                "arg8" : codigoprestacion,
                "arg9" : codigomedico,
                "arg10" : iniciopagina,
                "arg11" : tamaniopagina])
    }
    
    class func obtenerconvenios(tipoidentificacion : String, identificacion : String, idespecialidad : String, codigoempresa : String, codigosucursal : String, codigoorden : String, codigoempresaorden : String, codigolineaordden : String, codigoprestacion : String, codigoempresaprestacion: String) -> NSURL{
        
        return urlWithName(name: "servicio/v2/agenda/consultarconvenios",
            args: [
                "arg0" : tipoidentificacion,
                "arg1" : identificacion,
                "arg2" : idespecialidad,
                "arg3" : codigoempresa,
                "arg4" : codigosucursal,
                "arg5" : codigoorden,
                "arg6" : codigoempresaorden,
                "arg7" : codigolineaordden,
                "arg8" : codigoprestacion,
                "arg9" : codigoempresaprestacion,
            ])
    }
    
    class func obtenervalorizarconvenio(tipoidentificacion : String, identificacion : String, idespecialidad : String, codigoempresa : String, codigosucursal : String, codigoconvenio : String, codigoempresaconvenio : String, secuenciaafiliadoconvenio : String, codigoorden : String, codigoempresaorden : String, codigolineaordden : String, codigoprestacion : String, codigoempresaprestacion : String) -> NSURL{
        
        //cambio servicio
        return urlWithName(name: "servicio/v2/agenda/valorizarconvenio",
        //return urlWithName(name: "servicio/agenda/valorizarconvenio",
            args: [
                "arg0" : tipoidentificacion,
                "arg1" : identificacion,
                "arg2" : idespecialidad,
                "arg3" : codigoempresa,
                "arg4" : codigosucursal,
                "arg5" : codigoconvenio,
                "arg6" : codigoempresaconvenio,
                "arg7" : secuenciaafiliadoconvenio,
                "arg8" : codigoorden,
                "arg9" : codigoempresaorden,
                "arg10": codigolineaordden,
                "arg11": codigoprestacion,//nuevos campos
                "arg12": codigoempresaprestacion,//nuevos campos
                //arg13 : aplicarprecioparticular S/N
                //arg14 : aplicacoberturea S/N
                
            ])
    }
    class func obtenervalorizarconvenio2(tipoidentificacion : String, identificacion : String, idespecialidad : String, codigoempresa : String, codigosucursal : String, codigoconvenio : String, codigoempresaconvenio : String, secuenciaafiliadoconvenio : String, codigoorden : String, codigoempresaorden : String, codigolineaordden : String, codigoprestacion : String, codigoempresaprestacion : String, aplicarprecioparticular : String, aplicacobertura : String) -> NSURL{
        
        //cambio servicio
        return urlWithName(name: "servicio/v2/agenda/valorizarconvenioval",
                           //return urlWithName(name: "servicio/agenda/valorizarconvenio",
            args: [
                "arg0" : tipoidentificacion,
                "arg1" : identificacion,
                "arg2" : idespecialidad,
                "arg3" : codigoempresa,
                "arg4" : codigosucursal,
                "arg5" : codigoconvenio,
                "arg6" : codigoempresaconvenio,
                "arg7" : secuenciaafiliadoconvenio,
                "arg8" : codigoorden,
                "arg9" : codigoempresaorden,
                "arg10": codigolineaordden,
                "arg11": codigoprestacion,//nuevos campos
                "arg12": codigoempresaprestacion,//nuevos campos
                "arg13": aplicarprecioparticular, // S/N
                "arg14": aplicacobertura// S/N
                
            ])
    }
    
    class func obtenerreserva(tipoidentificacion : String, identificacion : String, idintervalo : String, codigoconvenio : String, codigoempresaconvenio : String, idempresaestablecimiento : String, idsucursal : String, idespecialidad : String, numeroorden : String, codempresaorden : String, lineadetalleorden : String, codempresaprestacion : String, codprestacion : String) -> NSURL{
        
        return urlWithName(name: "servicio/v2/agenda/reservar",//"servicio/agenda/reservar",
            args: [
                "arg0" : tipoidentificacion,
                "arg1" : identificacion,
                "arg2" : idintervalo,
                "arg3" : codigoconvenio,
                "arg4" : codigoempresaconvenio,
                "arg5" : idempresaestablecimiento,
                "arg6" : idsucursal,
                "arg7" : idespecialidad,
                "arg8" : numeroorden,
                "arg9" : codempresaorden,
                "arg10": lineadetalleorden,
                "arg11": codempresaprestacion,
                "arg12": codprestacion,])
    }
    
    class func obtenerreagendar(tipoidentificacion : String, identificacion : String, codreserva : String, idintervalo : String, codigoconvenio : String, codigoempresaconvenio : String, idempresaestablecimiento : String, idsucursal : String, codigoprofesional : String) -> NSURL{
        
        return urlWithName(name: "servicio/agenda/reagendar",
            args: [
                "arg0" : tipoidentificacion,
                "arg1" : identificacion,
                "arg2" : codreserva,
                "arg3" : idintervalo,
                "arg4" : codigoconvenio,
                "arg5" : codigoempresaconvenio,
                "arg6" : idempresaestablecimiento,
                "arg7" : idsucursal,
                "arg8" : codigoprofesional])
    }
    
    class func obtenercitasvigentes(tipoidentificacion : String, identificacion : String, esadmin : String, usua_logeado : String) -> NSURL{
        
        //MiverisWsrest/MiverisWsrest/servicio/v2/agenda/citasvigentes2
        return urlWithName(name: "servicio/v2/agenda/citasvigentes3",//"servicio/agenda/citasvigentes",
            args: [
                "arg0" : tipoidentificacion,
                "arg1" : identificacion,
                "arg2" : esadmin,
                "arg3" : usua_logeado])
    }

    class func urldefinida() -> String{
        
        let baseURL      = "https://miveris.com.ec/MiverisWsrest/" //produccion
        //let baseURL      = "http://181.198.116.238:8080/MiverisWsrest/" //test
        
        return baseURL
    }
    private class func urlWithName(name: String, args: [String: String]) -> NSURL
    {
        //args["username"] = "ijoshsmith"
        let
        baseURL      = "https://miveris.com.ec/MiverisWsrest/", //produccion
        //baseURL      = "http://181.198.116.238:8080/MiverisWsrest/", //test
        queryString  = queryWithArgs(args: args),
        absolutePath = baseURL + name + "?" + queryString
        return NSURL(string: absolutePath)!
    }
    
    private class func queryWithArgs(args: [String: String]) -> String
    {
        let parts: [String] = args.reduce([])
            {
                result, pair in
                let
                key   = pair.0,
                value = pair.1,
                part  = "\(key)=\(value)"
                return result + [part]
        }
        return (parts as NSArray).componentsJoined(by: "&")
    }
}

