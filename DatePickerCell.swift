//
//  FirstResponderlCell.swift
//  HKTutorialPrototype
//
//  Created by ernesto on 18/10/14.
//  Copyright (c) 2014 cocoawithchurros. All rights reserved.
//

import UIKit

public class DatePickerCell: UITableViewCell {

    
    
    public enum DateCellInputMode
    {
        case Date, Time
        
    }
    
    public var inputMode:DateCellInputMode = .Date  {
        
        didSet {
            switch inputMode {
            case .Date:
                datePicker.datePickerMode = .date
            case .Time:
                datePicker.datePickerMode = .time
                break
            }
        }
    }
    
    
    var date:NSDate {
        
        get { return datePicker.date as NSDate }
        
        set (newDate) {
            datePicker.date = newDate as Date
            updateDateTimeLabel()
        }
        
    }
        
    lazy var dateFormatter:DateFormatter = {
        
        let formatter = DateFormatter()
        formatter.timeStyle = .none //NoStyle
        formatter.dateStyle = .medium //MediumStyle
        return formatter;
        
    }()

    lazy var timeFormatter:DateFormatter = {
        
        let formatter = DateFormatter()
        formatter.timeStyle = .short //ShortStyle
        formatter.dateStyle = .none //NoStyle
        return formatter;
        }()
    
    lazy var datePicker:UIDatePicker = {
        
        let picker = UIDatePicker()
        picker.addTarget(self, action: #selector(DatePickerCell.datePickerValueChanged), for: UIControlEvents.valueChanged)
        return picker
    }()
    
    func updateDateTimeLabel() {
        
        var dateText = ""
        let date = datePicker.date
        switch inputMode {
        case .Date:
            dateText = dateFormatter.string(from: date)
        case .Time:
            dateText = timeFormatter.string(from: date)
            break
        }
        self.detailTextLabel?.text = dateText;

    }
    
    func datePickerValueChanged( ) {
        
        updateDateTimeLabel()
    }

    
    /*public override func canBecomeFirstResponder() -> Bool {
        return true;
    }*/
    public override func becomeFirstResponder() -> Bool {
        return true;
    }
    
    public override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if( selected ) {
            self.becomeFirstResponder()
        }
        
    }
    public override var inputView: UIView! {
        get {
            return datePicker
        }
    }
    
    
    
    
    
}
