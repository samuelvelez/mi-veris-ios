
import UIKit
class VerDetalleCell: UITableViewCell
{
    
    
    var datos : UILabel = UILabel();
    var separator : UIImageView = UIImageView();
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.clear
        
        let screenSize: CGRect = UIScreen.main.bounds
        _ = screenSize.width;
        
        datos.frame = CGRect(x: 0, y: 0, width: 300.00, height: 80.00)
        //datos.borderStyle = UITextBorderStyle.Line
        datos.font = UIFont(name: "helvetica", size: 12)
        datos.textColor = UIColor.gray
        datos.adjustsFontSizeToFitWidth = true
        datos.numberOfLines = 3
        
        
        
        separator.backgroundColor = UIColor.gray
        separator.frame = CGRect(x:0, y:0, width:600, height:0.5)
        
        self.contentView.addSubview(datos)
        self.contentView.addSubview(separator)
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
