//
//  EditRolViewController.swift
//  Mi Veris
//
//  Created by Samuel Velez on 21/8/16.
//  Copyright © 2016 Programadores-iOS.net. All rights reserved.
//

import UIKit

class EditRolViewController: UIViewController, UITextFieldDelegate {

    var fam_selected : Familiar!
    
    var scroll : UIScrollView = UIScrollView()
    var txt_cedula : UITextField = UITextField()
    var txt_nombres : UITextField = UITextField()
    var txt_parentesco : UITextField = UITextField()
    var swi_admin : UISwitch = UISwitch()
    var sgm_tipo : UISegmentedControl = UISegmentedControl()
    
    
    var ls_parentesco : NSMutableArray = NSMutableArray()
    
    var pickerParentesco : PickerList!
    var dataParentesco = [""]
    //var parent : TipoParentesco = TipoParentesco()
    //var parent : Doctor = Doctor()
    var nuevo : TipoParentesco = TipoParentesco()
    var pickerSelected : PickerList!
    var changed : Bool = false

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.viewDidLoad()
        print("\(self.fam_selected?.numeroIdentificacion) - \(self.fam_selected?.edad) \(self.fam_selected?.parentesco) \(self.fam_selected?.codigoParentesco) ")
        
        // Do any additional setup after loading the view.
        scroll.frame = CGRect(x: 0, y: -60, width:  self.view.frame.width, height: self.view.frame.height+60)
        scroll.isUserInteractionEnabled = true
        
        // let singleTap = UITapGestureRecognizer(target: self, action: "hideKeyboard")
        // scroll.addGestureRecognizer(singleTap)
        self.view.addSubview(scroll)
        
        self.navigationItem.title = ""
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        let barWhite : UIView = UIView(frame: CGRect(x:0, y:64, width: self.view.frame.width , height: 30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width / 2.8, height: 30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x: barBlue.frame.width, y: 64, width: 45, height: 30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x: 0, y: 70, width: self.view.frame.width - 20, height: 30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "ASIGNAR ROL"
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)
        
        let items = ["Identificación", "Nombres"]
        sgm_tipo = UISegmentedControl(items: items)
        sgm_tipo.selectedSegmentIndex = 0
        sgm_tipo.tintColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        sgm_tipo.frame = CGRect(x:20, y: 105,width: self.view.frame.width - 40, height: 40)
        sgm_tipo.addTarget(self, action: #selector(AddFamiliaViewController.cambio), for: .valueChanged);
        //scroll.addSubview(sgm_tipo)
        
        var nom_completo : String = "\(fam_selected!.primerNombre) \(fam_selected!.segundoNombre) \(fam_selected!.primerApellido) \(fam_selected!.segundoApellido)"
        nom_completo = nom_completo.removingPercentEncoding! //stringByRemovingPercentEncoding!
        nom_completo = nom_completo.replacingOccurrences(of: "+", with: " ", options: String.CompareOptions.literal, range: nil)
        //stringByReplacingOccurrencesOfString("+", withString: " ", options: NSStringCompareOptions.LiteralSearch, range: nil)
        nom_completo = nom_completo.replacingOccurrences(of: "  ", with: " ", options: String.CompareOptions.literal, range: nil)
        
        let lbnombre = UILabel(frame: CGRect(x: 20, y: 125, width: self.view.frame.width - 40, height: 25))
        lbnombre.text = nom_completo
        lbnombre.font = UIFont.systemFont(ofSize: 16)
        lbnombre.lineBreakMode = NSLineBreakMode.byWordWrapping
        lbnombre.numberOfLines = 0
        
        lbnombre.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        lbnombre.textAlignment = NSTextAlignment.left
        
        scroll.addSubview(lbnombre)
        
        let lbseleccion = UILabel(frame: CGRect(x: 20, y: 175, width: self.view.frame.width - 40, height: 60))
        lbseleccion.text = "Selecciona el tipo de relación que tiene esta persona contigo: "
        lbseleccion.font = UIFont.systemFont(ofSize: 16)
        lbseleccion.lineBreakMode = NSLineBreakMode.byWordWrapping
        lbseleccion.numberOfLines = 0
        lbseleccion.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        lbseleccion.textAlignment = NSTextAlignment.left
        
        scroll.addSubview(lbseleccion)
        
        
        
        txt_parentesco = UITextField(frame: CGRect(x: 40, y: 225, width: self.view.frame.width - 80, height: 40))
        txt_parentesco.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)

        txt_parentesco.layer.cornerRadius = 20
        txt_parentesco.layer.borderColor = UIColor.lightGray.cgColor
        txt_parentesco.layer.borderWidth = 1
        
        txt_parentesco.textAlignment = NSTextAlignment.center
        txt_parentesco.delegate = self
        txt_parentesco.layer.borderColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1).cgColor
        txt_parentesco.layer.borderWidth = 1
        txt_parentesco.text = "Parentesco"
        //txt_parentesco.addTarget(self, action: Selector("upView:"), forControlEvents: UIControlEvents.EditingDidBegin)
        //txt_parentesco.addTarget(self, action: Selector("downView:"), forControlEvents: UIControlEvents.EditingDidEndOnExit)
        txt_parentesco.autocorrectionType = UITextAutocorrectionType.no
        //txtProvincia.enabled = false
        scroll.addSubview(txt_parentesco)
        
        let lbtexto = UILabel(frame: CGRect(x: 30, y: 275, width: self.view.frame.width - 60, height:  50))
        lbtexto.text = "Desea asignar a esta persona como administrador de mi cuenta?"
        lbtexto.font = UIFont.systemFont(ofSize: 16)
        lbtexto.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        lbtexto.textAlignment = NSTextAlignment.left
        lbtexto.lineBreakMode = NSLineBreakMode.byWordWrapping
        lbtexto.numberOfLines = 0
        scroll.addSubview(lbtexto)
        
        swi_admin = UISwitch(frame: CGRect(x: 200, y: 325, width: 40, height: 40))
        swi_admin.tintColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        print(fam_selected.esAdmin)
        swi_admin.addTarget(self, action: #selector(EditFamViewController.asignar), for: UIControlEvents.touchUpInside)
        if(fam_selected.esAdmin=="S"){
            print("entro")
            //swi_admin.on = true
            swi_admin.setOn(true, animated: true)
        }
        scroll.addSubview(swi_admin)
        
        let btAceptar = UIButton(frame: CGRect(x: 30, y: 365, width:  self.view.frame.width - 60, height: 40))
        btAceptar.setTitle("Guardar", for: UIControlState.normal)
        btAceptar.addTarget(self, action: #selector(EditRolViewController.enviar2), for: UIControlEvents.touchUpInside)
        btAceptar.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        btAceptar.layer.cornerRadius = 20
        btAceptar.layer.zPosition = 0
        
        scroll.addSubview(btAceptar)
        
        
        let bteliminar = UIButton(frame: CGRect(x: 30, y: 420, width: self.view.frame.width - 60, height: 40))
        bteliminar.setTitle("Eliminar", for: UIControlState.normal)
        bteliminar.addTarget(self, action: #selector(EditRolViewController.eliminar), for: UIControlEvents.touchUpInside)
        bteliminar.backgroundColor = UIColor(red: (245/256.0), green: (45.0/256.0), blue: (45.0/256.0), alpha: 1)
        bteliminar.layer.cornerRadius = 10
        scroll.addSubview(bteliminar)
        
        scroll.contentSize = CGSize(width:0, height: UIScreen.main.bounds.height)
        getParentesco()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func asignar(){
        if(swi_admin.isOn){
            let alert = UIAlertController(title: "Veris", message: "Al asignar a esta persona como administrador de cuenta, podrá gestionar tus citas y ver tus recetas", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            print("ya no hay alert")
        }
    }
    
    func getParentesco(){
        
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        let urldef = URLFactory.urldefinida()

        let url = NSURL(string: "\(urldef)servicio/persona/tiposparentesco")
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        let str = "wsappusuario:ZA$57@9b86@$2r5"
        
        let utf8str = str.data(using: String.Encoding.utf8)
        
        
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                    var err: NSError?
                    
                    //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    if (err != nil) {
                        print("JSON Error \(err!.localizedDescription)")
                    }
                    
                    //println(jsonResult);
                    //print(jsonResult)
                    let info =  jsonResult as NSDictionary
                    let data = info["lista"] as! [[String: Any]]
                    if data.count > 0 {
                        for item in data {
                            let parentesco = TipoParentesco()
                            if let cod = item["codigoParentesco"] as? NSString {
                                parentesco.codigoParentesco = cod
                            }
                            
                            if let des = item["descripcion"] as? NSString {
                                parentesco.descripcion = des
                            }
                            self.ls_parentesco.add(parentesco)
                        }
                    }
                    
                    
                    var lista = [""]
                   /* for provi in self.ls_parentesco {
                        lista.append(provi.valueForKey("descripcion")as! String)
                        //lista.addObject(provi.nombre)
                    }*/
                    for  i in 0..<self.ls_parentesco.count{
                        //print("encontro 1 \((self.ls_parentesco[i] as AnyObject).value(forKey: "descripcion"))")
                        let nombre = (self.ls_parentesco[i] as AnyObject).value(forKey: "descripcion")
                        lista.append(nombre as! String)
                    }

                    lista.remove(at: 0)
                    
                    self.dataParentesco = lista
                    //self.txt_parentesco.text = self.dataParentesco[0]
                    self.nuevo.codigoParentesco = self.fam_selected.codigoParentesco
                    self.nuevo.descripcion = self.fam_selected.parentesco
                    
                    var parentesco = self.nuevo.descripcion as String
                    
                    parentesco = parentesco.removingPercentEncoding!
                    self.txt_parentesco.text = parentesco.replacingOccurrences(of: "+", with: " ", options: String.CompareOptions.literal, range: nil)
                    //stringByRemovingPercentEncoding
                    self.pickerParentesco = PickerList(datos: self.dataParentesco as NSArray)
                    
                    ViewUtilidades.hiddenLoader(controller: self)
                    //self.getCiudades()
                } catch {
                    print("ERROR")
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: "Revise su conexión a Datos o Internet", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if(textField == txt_parentesco){
            self.hideKeyboard()
            
        }
        return true
    }
    
    func hideKeyboard(){
        print("hidekeyboard")
        self.txt_parentesco.resignFirstResponder()
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //print("entro a editar" + textField.description)
        
        if (textField == txt_parentesco){
            pickerParentesco.showPicker(controller: self)
            pickerSelected = pickerParentesco
            txt_parentesco.resignFirstResponder()
        }
    }
    
    func cancelPicker(){
        pickerSelected.hidePicker()
        txt_parentesco.resignFirstResponder()
        
    }
    
    func acceptPicker(){
        pickerSelected.hidePicker()
        txt_parentesco.text = dataParentesco[pickerParentesco.eleccion]
        self.changed = true
        txt_parentesco.resignFirstResponder()
    }
    
    func enviar2(){
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        
        var esadmin : String = ""
        if(swi_admin.isOn){
            esadmin="S"
        }else{
            esadmin = "N"
        }
        let fmaa = ls_parentesco.object(at: pickerParentesco.eleccion) as! TipoParentesco
        if(self.changed){
            self.nuevo.codigoParentesco = fmaa.codigoParentesco
            self.nuevo.descripcion = fmaa.descripcion
        }
        print("\(fmaa.descripcion) - \(self.nuevo.descripcion)")
        let parameters = ["tipoIdentificacion":"\(fam_selected!.tipoIdentificacion)", "numeroIdentificacion": "\(fam_selected!.numeroIdentificacion)", "primerNombre": "\(fam_selected!.primerNombre)", "segundoNombre": "\(fam_selected!.segundoNombre)", "primerApellido": "\(fam_selected!.primerApellido)", "segundoApellido": "\(fam_selected!.segundoApellido)", "edad": "\(fam_selected!.edad)", "codigoParentesco": "\(self.nuevo.codigoParentesco)", "parentesco": "\(self.nuevo.descripcion)", "idRelacion": "\(fam_selected!.idRelacion)","esAdmin": "\(esadmin)" ] as Dictionary<String, String>
        let us = Usuario.getEntity as Usuario
        let urldef = URLFactory.urldefinida()

        let request = NSMutableURLRequest(url: NSURL(string:"\(urldef)servicio/persona/grupo/\(us.tipoid)-\(us.numeroidentificacion)")! as URL)
        
       let session = URLSession.shared
        request.httpMethod = "PUT"
        
        let str = "wsappusuario:ZA$57@9b86@$2r5"
        
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        request.httpBody = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        
        let task = session.dataTask(with: request as URLRequest) { data, response, error in
            guard data != nil else {
                print("no data found: \(error)")
                return
            }
            
            do {
                if let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    print("Response: \(json)")
                    ViewUtilidades.hiddenLoader(controller: self)
                    print(response)
                    let alert = UIAlertController(title: "Veris", message: "Guardado con exito!", preferredStyle: UIAlertControllerStyle.alert)
                    let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                        UIAlertAction in
                        self.dismiss(animated: false, completion: nil)
                    }
                    alert.addAction(okAction)
                    self.present(alert, animated: true, completion: nil)
                    
                        
                    
                } else {
                    let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)// No error thrown, but not NSDictionary
                    print("Error could not parse JSON: \(jsonStr)")
                }
            } catch let parseError {
                print(parseError)// Log the error thrown by `JSONObjectWithData`
                let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("Error could not parse JSON: '\(jsonStr)'")
            }
        }
        
        task.resume()
    }

    
    
    func eliminar(){
         ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        let parameters = ["tipoIdentificacion":"\(fam_selected!.tipoIdentificacion)", "numeroIdentificacion": "\(fam_selected!.numeroIdentificacion)", "primerNombre": "\(fam_selected!.primerNombre)", "segundoNombre": "\(fam_selected!.segundoNombre)", "primerApellido": "\(fam_selected!.primerApellido)", "segundoApellido": "\(fam_selected!.segundoApellido)", "edad": "\(fam_selected!.edad)", "codigoParentesco": "", "parentesco": "", "idRelacion": "","esAdmin": "N" ] as Dictionary<String, String>
        let us = Usuario.getEntity as Usuario
        let urldef = URLFactory.urldefinida()

        let myurl = "\(urldef)servicio/persona/grupo/\(us.tipoid)-\(us.numeroidentificacion)/\(fam_selected.idRelacion)"
        print(myurl)
        let request = NSMutableURLRequest(url: NSURL(string: myurl)! as URL)
        
        let session = URLSession.shared
        request.httpMethod = "DELETE"
        
        let str = "wsappusuario:ZA$57@9b86@$2r5"
        
        let utf8str = str.data(using: String.Encoding.utf8)
        
        //let base64Encoded = str.data(using: String.Encoding.utf8, allowLossyConversion: true)
        //let base64Encoded = str.dataUsingEncoding(NSUTF8StringEncoding)!.base64EncodedStringWithOptions([])
        //print("Encoded: \(base64Encoded), \(utf8str!.base64EncodedString())")
        let base64DecodedData = utf8str!.base64EncodedString()
        print(base64DecodedData)
        //NSData(base64EncodedString: base64Encoded, options: [])!
        // let base64DecodedString = String(data: base64DecodedData as! Data, encoding: String.Encoding.utf8)!
        //print("Decoded: \(base64DecodedString)")
        
        //Note : Add the corresponding "Content-Type" and "Accept" header. In this example I had used the application/json.
        request.addValue(base64DecodedData, forHTTPHeaderField: "Authorization")
        
        //Note : Add the corresponding "Content-Type" and "Accept" header. In this example I had used the application/json.
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        request.httpBody = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        
        let task = session.dataTask(with: request as URLRequest) { data, response, error in
            guard data != nil else {
                print("no data found: \(error)")
                return
            }
            
            do {
                print("entro al doo....")
                if let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    print("Response: \(json)")
                    
                    let alert = UIAlertController(title: "Veris", message: "Eliminado correctamente!", preferredStyle: UIAlertControllerStyle.alert)
                    let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                        UIAlertAction in
                        self.dismiss(animated: false, completion: nil)
                    }
                    alert.addAction(okAction)
                    self.present(alert, animated: true, completion: nil)
                    
                    
                    
                } else {
                    let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)// No error thrown, but not NSDictionary
                    print("Error could not parse JSON: \(jsonStr)")
                }
            } catch let parseError {
                print(parseError)// Log the error thrown by `JSONObjectWithData`
                let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("Error could not parse JSON: '\(jsonStr)'")
            }
        }
        
        task.resume()
    }

    
    
}
