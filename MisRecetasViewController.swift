//
//  MisRecetasViewController.swift
//  veris
//
//  Created by Felipe Lloret on 05/02/15.
//  Copyright (c) 2015 Felipe Lloret. All rights reserved.
//

import UIKit

class MisRecetasViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var recetas = [MisRecetasModel]()
    var detailRecetas = [DetailRecetaModel]()
    var tableView : UITableView = UITableView()
    var setReload : Bool = false
    
    var recetaString: String?
    var cellIndex: Int = 0
    var nombreUser : UILabel = UILabel()
    
    override func viewDidAppear(_ animated: Bool) {
        let user = Usuario.getEntity as Usuario
        Usuario.newcreateButtonFamilyTop(controlador: self, text: user.nombre as String)
        var es = ""
        if setReload {
            self.setReload = false
            self.recetas = []
            
            let usuario = Usuario.getEntity as Usuario
            if usuario.rol < 3 {
                ViewUtilidades.showLoader(controller: self, title: "Cargando...")
                
                let usuario = Usuario.getEntity as Usuario
                let tipo = usuario.tipoid
                let identificacion = usuario.numeroidentificacion
                self.revisarPermisos()
                /*DispatchQueue.main.async(execute: {
                    let log = UserDefaults.standard.object(forKey: "identificacion")
                    let t_log = UserDefaults.standard.object(forKey: "tipo")
                    let iden = "\(t_log!)-\(log!)"
                    let urldef = URLFactory.urldefinida()
                    let url = NSURL(string: "\(urldef)servicio/persona/grupo/\(iden)?arg0=2")
                    let request = NSMutableURLRequest(url: url! as URL)
                    var resultado = ""
                    NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                        if (error != nil) {
                            print(error!.localizedDescription)
                            resultado = "N"
                        } else {
                            do{
                                var err: NSError?
                                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                                if (err != nil) {
                                    print("JSON Error \(err!.localizedDescription)")
                                }
                                let info =  jsonResult as NSDictionary
                                let data = info["lista"] as! [[String: Any]]//NSArray
                                if data.count > 0 {
                                    for familia in data {
                                        if let res = familia["numeroIdentificacion"] as? String {
                                            if res == identificacion as String {
                                                resultado = (familia["esAdmin"] as? String)!
                                                print("hola hola hola \(resultado)")
                                               // bool = true
                                            }}}}
                                DispatchQueue.main.async(execute: {
                                    print("holis \(resultado)")
                                    let us = Usuario.getEntity as Usuario
                                    let urldef = URLFactory.urldefinida()
                                    let urlAsString = "\(urldef)servicio/v2/doctor/recetas2?arg0=\(tipo)&arg1=\(identificacion)&arg2=\(us.numeroidentificacion)&arg3=\(resultado)"
                                    self.getJSONData(url: urlAsString)
                                })
                            } catch {
                                print("ERROR")
                            }
                        }
                    }
                })*/
            } else {
                tableView.reloadData()
                UtilidadesGeneral.mensaje(mensaje: "No tiene permisos para utilizar esta opción")
            }
            
        }
        //let user = Usuario.getEntity
        nombreUser.text = user.nombre as String
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = ""
        
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        let fondo : UIImageView = UIImageView(frame: self.view.bounds)
        fondo.image = UIImage(named:"MisRecetasBackground")
        self.view.addSubview(fondo)
        
        let barWhite : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width , height:30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width / 1.8, height:  30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x: barBlue.frame.width, y: 64, width: 45, height:30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x: 0, y: 64, width: self.view.frame.width - 20, height: 30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "Mis Recetas"

        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)
        
        let usuario = Usuario.getEntity as Usuario
        
        
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        self.revisarPermisos()
        ViewUtilidades.hiddenLoader(controller: self)
        //http://10.20.90.64:8080/MiverisWsrest/servicio/v2/doctor/recetas?arg0=2&arg1=0926085739

    }
    
    func getFamilyService(){
        Usuario.getJsonFamilia(controlador: self)
        setReload = true
    }
    
    func revisarPermisos(){
        self.recetas.removeAll()
        
        let  usuario = Usuario.getEntity as Usuario
        let tipo_id_p = usuario.tipoid as String
        let identificacion = usuario.numeroidentificacion as String
        
       // let verif = Usuario.verificarEsadmin(identificacion: identificacion)
        
        let us = Usuario.getEntity as Usuario
        
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        
        let log = UserDefaults.standard.object(forKey: "identificacion")
        let t_log = UserDefaults.standard.object(forKey: "tipo")
        
        let iden = "\(t_log!)-\(log!)"
        
        //let iden = "\(tipo_id_p)-\(identificacion)"
        let urldef = URLFactory.urldefinida()
        
        let url = NSURL(string: "\(urldef)servicio/persona/grupo/\(iden)?arg0=2")
        let request = NSMutableURLRequest(url: url! as URL)
        var resultado = ""
        //var bool  = false
        //Verificar si es admin
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            if (error != nil) {
                print(error!.localizedDescription)
                resultado = "N"
            } else {
                do{
                    var err: NSError?
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    if (err != nil) {
                        print("JSON Error \(err!.localizedDescription)")
                    }
                    
                    let info =  jsonResult as NSDictionary
                    let data = info["lista"] as! [[String: Any]]//NSArray
                    print(data)
                    if data.count > 0 {
                        for familia in data {
                            if let res = familia["numeroIdentificacion"] as? String {
                                if res == us.numeroidentificacion as String {
                                    resultado = (familia["esAdmin"] as? String)!
                                    
                                }
                            }
                        }
                    }
                    DispatchQueue.main.async(execute: {
                        print("holis \(resultado)")
                        if(resultado != "N"){
                            let usuario = Usuario.getEntity as Usuario
                            let tipo = usuario.tipoid
                            let identificacion = usuario.numeroidentificacion
                            let us = Usuario.getEntity as Usuario
                            let urldef = URLFactory.urldefinida()
                            
                            let urlAsString = "\(urldef)servicio/v2/doctor/recetas2?arg0=\(tipo)&arg1=\(identificacion)&arg2=\(identificacion)&arg3=S"
                            
                            self.getJSONData(url: urlAsString)

                            
                        }
                        self.recetas.removeAll()
                        self.tableView.reloadData()
                        ViewUtilidades.hiddenLoader(controller: self)
                        
                        ViewUtilidades.hiddenLoader(controller: self)
                        
                        
                    })
                } catch {
                    print("ERROR")
                }
            }
            
            
        }
        
        
    }

    
    func getJSONData(url: String) {
        let url = NSURL(string: url as String)
        let request = NSMutableURLRequest(url: url! as URL)
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
        if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
        }else{
            let json = JSON(data: data!)
            if json["resultado"].string == "ok" {
            if let recetasArray = json["lista"].array {
                        
            print(recetasArray)
            //Creamos nuestro objeto Recetas con los datos obtenidos del JSON
            for recetasDic in recetasArray {
                let recetaFecha: String? = recetasDic["fecha"].stringValue
                let recetaDoctor: String? = recetasDic["doctor"].stringValue
                let recetaCentro: String? = recetasDic["centro_medico"].stringValue
                let recetaEspecialidad: String? = recetasDic["nombre_especialidad"].stringValue
                let recetaReceta: String? = recetasDic["secuencia_receta"].stringValue
                            
                let receta = MisRecetasModel(fecha: recetaFecha, doctor: recetaDoctor, centromedico: recetaCentro, especialidad: recetaEspecialidad, receta: recetaReceta, paciente: "")
                self.recetas.append(receta)
            }
        }
                        //Uso para debug
                        //println(self.recetas)
                    //}
                    //Mostramos error al usuario
                } else {
                    let jsonError = json["resultado"].string
                    print(jsonError!)
                }
                
                //dispatch_async(dispatch_get_main_queue(), {
                DispatchQueue.main.async(execute: {
                    self.mostrarRecetas()
                    self.tableView.reloadData()
                    ViewUtilidades.hiddenLoader(controller: self)
                })
            }
            
            
        }
        
        ///////////////////////////////////
        
        //Mis Recetas

    }
    
    func getJSONDataForDetail(url: String) {
        
        let url = NSURL(string: url as String)
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        let str = "wsappusuario:ZA$57@9b86@$2r5"
        
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                let json = JSON(data: data!)
                
                //Comprobamos si todo ha salido bien, en caso contrario se le muestra el mensaje de error al usuario
                if json["resultado"].string == "ok" {
                    if let recetasDetailArray = json["lista"].array {
                        
                        //Creamos nuestro objeto Recetas con los datos obtenidos del JSON
                        for recetasDetailDic in recetasDetailArray {
                            let recetaDetailDenominacion: String? = recetasDetailDic["denominacion"].stringValue
                            let recetaDetailConcentracion: String? = recetasDetailDic["concentracion"].stringValue
                            let recetaDetailForma: String? = recetasDetailDic["forma_farmaceutica"].stringValue
                            let recetaDetailCantidad: String? = recetasDetailDic["cantidad"].stringValue
                            let recetaDetailIndicaciones: String? = recetasDetailDic["indicaciones"].stringValue
                            
                            let recetaDetail = DetailRecetaModel(denominacion: recetaDetailDenominacion, concentracion: recetaDetailConcentracion, forma: recetaDetailForma, cantidad: recetaDetailCantidad, indicaciones: recetaDetailIndicaciones)
                            self.detailRecetas.append(recetaDetail)
                        }
                        
                        
                    }
                    //Mostramos error al usuario
                } else {
                    let jsonError = json["resultado"].string
                    print(jsonError!)
                }
                
                //dispatch_async(dispatch_get_main_queue(), {
                DispatchQueue.main.async(execute: {
                    var string: String?
                    var finalString: String?
                    
                    finalString = "---------------------\n"
                    //for var i = 0; i < self.detailRecetas.count; i++ {
                    for i in 0 ..< self.detailRecetas.count {
                        string = "\(self.detailRecetas[i].denominacion)\n\(self.detailRecetas[i].cantidad) \(self.detailRecetas[i].forma) \(self.detailRecetas[i].concentracion)\n\(self.detailRecetas[i].indicaciones)\n---------------------\n\n"
                        finalString = finalString! + string!
                    }
                    
                    let usuario = Usuario.getEntity as Usuario
                    
                    let vc = UIActivityViewController(activityItems: [
                        "RECETA",
                        "---------------------",
                        self.recetas[self.cellIndex].centromedico,
                        "Fecha: \(self.recetas[self.cellIndex].fecha)",
                        "Doctor(a): \(self.recetas[self.cellIndex].doctor)",
                        "Especialidad: \(self.recetas[self.cellIndex].especialidad)",
                        "Paciente: \(usuario.nombre)\n\n",
                        "---------------------",
                        "DETALLE DE LA RECETA",
                        finalString!], applicationActivities: nil)
                    
                    let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController?.present(vc, animated: true, completion: nil)
                    //vc.selecto
                    //if vc.respondsToSelector(Selector("popoverPresentationController")) {
                        // iOS 8+
                        let presentationController = vc.popoverPresentationController
                        presentationController?.sourceView = self.view
                    //}
                    self.detailRecetas = []
                })
            }
            
            
        }
        
        ///////////////////////
        
        

    }
    
    
    
    func mostrarRecetas() {
        self.tableView.frame = CGRect(x: 0.0, y: 154.0, width: self.view.frame.width,height: self.view.frame.height - 154);
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.separatorColor = UIColor.clear
        self.tableView.register(MisRecetasTableViewCell.self as AnyClass, forCellReuseIdentifier: "cell");
        
        self.view.addSubview(self.tableView);
    }
    
    func verDetalleReceta(sender: UIButton) {
        self.setReload = true
        self.modalPresentationStyle = UIModalPresentationStyle.currentContext
        
        let detailRecetaViewController: DetailRecetaViewController = self.storyboard?.instantiateViewController(withIdentifier: "DetailReceta") as! DetailRecetaViewController
        
        let usuario = Usuario.getEntity as Usuario
        
        detailRecetaViewController.centroString = self.recetas[sender.tag].centromedico
        detailRecetaViewController.fechaString = self.recetas[sender.tag].fecha
        detailRecetaViewController.doctorString = self.recetas[sender.tag].doctor
        detailRecetaViewController.especialidadString = self.recetas[sender.tag].especialidad
        detailRecetaViewController.pacienteString = usuario.nombre as String
        detailRecetaViewController.recetaString = self.recetas[sender.tag].receta
        
        self.present(detailRecetaViewController, animated: true, completion: nil)
    }
    
    //TableViewDataSource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.recetas.count
    }
    
    //Devuelve número de filas en la sección
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MisRecetasTableViewCell
            
            //Hacemos que las celdas no tenga estilo al ser seleccionadas
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.verRecetaButton.tag = indexPath.section
            //cell.verRecetaButton.addTarget(self, action: "verDetalleReceta:", for: UIControlEvents.touchUpInside)
        //arreglarsender
        cell.verRecetaButton.addTarget(self, action: #selector(verDetalleReceta(sender:)), for: UIControlEvents.touchUpInside)
        
            let dr = "Dr(a) "
            let medicoString = dr + self.recetas[indexPath.section].doctor
            
            cell.setCell(sucursalLabelText: self.recetas[indexPath.section].centromedico,
                fechaLabelText: self.recetas[indexPath.section].fecha,
                doctorLabelText: medicoString,
                prestacionLabelText: self.recetas[indexPath.section].especialidad, pacienteText: self.recetas[indexPath.section].paciente)
            
            return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.cellIndex = indexPath.section
        self.recetaString = self.recetas[indexPath.section].receta
        let urldef = URLFactory.urldefinida()

        let urlAsString = "\(urldef)servicio/doctor/detallerecetas?arg0="
        self.getJSONDataForDetail(url: urlAsString)
    }
    
}
