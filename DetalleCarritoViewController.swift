//
//  DetalleCarritoViewController.swift
//  Mi Veris
//
//  Created by Samuel Velez on 13/3/17.
//  Copyright © 2017 Samuel Velez. All rights reserved.
//

import UIKit

class DetalleCarritoViewController: UIViewController , UITableViewDelegate,UITableViewDataSource {

    var detalle : CarritoModel? = nil // = CarritoModel
    var titulo : String? = nil
    var tableView : UITableView = UITableView()
    
    var btn_paquetes : UIButton = UIButton()
    var btn_citas : UIButton = UIButton()
    var cipaor : String = "citas"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = ""
        
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        let fondo : UIImageView = UIImageView(frame: self.view.bounds)
        fondo.image = UIImage(named:"MisRecetasBackground")
        self.view.addSubview(fondo)
        
        let barWhite : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width , height:30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width / 3, height:  30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x: barBlue.frame.width, y: 64, width: 45, height:30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x: 0, y: 64, width: self.view.frame.width - 20, height: 30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "Detalle de Compras"
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)

        print(detalle?.nombre ?? "no hay")
        //print(detalle)
        for citas in (detalle?.citasPagar)! {
            print(citas.doctor)
        }
        for paquetes in (detalle?.paquetesPagar)!{
            print(paquetes.nombrePaquete)
        }
        btn_citas = UIButton(frame: CGRect(x: 0, y: 94, width: self.view.frame.width/3, height: 40))
        btn_citas.setTitle("Citas", for: UIControlState.normal)
        btn_citas.backgroundColor = UIColor.white
        btn_citas.layer.borderWidth = 1
        btn_citas.addTarget(self, action: #selector(DetalleCarritoViewController.misCitas), for: UIControlEvents.touchUpInside)
        
        btn_citas.layer.borderColor = UIColor.lightGray.cgColor
        btn_citas.setTitleColor(UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1), for: UIControlState.normal)
        self.view.addSubview(btn_citas)
        
         btn_paquetes = UIButton(frame: CGRect(x: self.view.frame.width/3, y: 94, width: self.view.frame.width/3, height: 40))
        btn_paquetes.setTitle("Paquetes", for: UIControlState.normal)
        btn_paquetes.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        btn_paquetes.addTarget(self, action: #selector(DetalleCarritoViewController.mostrarPaquetes), for: UIControlEvents.touchUpInside)
        btn_paquetes.layer.borderWidth = 1
        btn_paquetes.layer.borderColor = UIColor.lightGray.cgColor
        btn_paquetes.setTitleColor(UIColor.white, for: UIControlState.normal)
        self.view.addSubview(btn_paquetes)
        
        let btn_ordenes = UIButton(frame: CGRect(x: (self.view.frame.width*2)/3, y: 94, width: self.view.frame.width/3, height: 40))
        btn_ordenes.setTitle("Ordenes", for: UIControlState.normal)
        btn_ordenes.layer.borderWidth = 1
        btn_ordenes.layer.borderColor = UIColor.lightGray.cgColor
        btn_ordenes.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        btn_ordenes.setTitleColor(UIColor.white, for: UIControlState.normal)
        self.view.addSubview(btn_ordenes)
        
        
        self.mostrarCitas()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func mostrarCitas() {
        let labl = UILabel(frame: CGRect(x: 10, y: 90, width: self.view.frame.width - 20, height: 50))
        labl.textAlignment = .center
        labl.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        //labl.text = "Total : $ \(self.total)"
        
        
        //self.view.addSubview(labl)
        
        self.tableView.frame = CGRect(x: 0.0, y: 154.0, width: self.view.frame.width,height: self.view.frame.height - 154);
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.backgroundColor = UIColor.clear
        // self.tableView.separatorColor = UIColor.clear
        self.tableView.register(CarritoTableViewCell.self as AnyClass, forCellReuseIdentifier: "cell");
        
        self.view.addSubview(self.tableView);
       // print("el carrito tiene: \(self.carrito.count)")
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(self.cipaor == "citas"){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CarritoTableViewCell
            //let total = Double(self.carrito[indexPath.row].totalCitas) + Double(self.carrito[indexPath.row].totalPaquetes)
            cell.setCell(pacientetext:  (self.detalle?.citasPagar[indexPath.row].especialidad)!, totalPagar: (self.detalle?.citasPagar[indexPath.row].subtotalCopago)!)
            return cell
        }else if(self.cipaor == "paquetes"){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CarritoTableViewCell
            //let total = Double(self.carrito[indexPath.row].totalCitas) + Double(self.carrito[indexPath.row].totalPaquetes)
            cell.setCell(pacientetext:  (self.detalle?.paquetesPagar[indexPath.row].nombrePaquete)!, totalPagar: (self.detalle?.paquetesPagar[indexPath.row].valorDcto)!)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            return cell
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1//self.carrito.count
    }
    
    //Devuelve número de filas en la sección
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.cipaor == "citas"){
            return (self.detalle?.citasPagar.count)!
        }else if(self.cipaor == "paquetes"){
            return (self.detalle?.paquetesPagar.count)!
        }else{
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func misCitas(){
        self.cipaor = "citas"
        self.btn_paquetes.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        self.btn_paquetes.setTitleColor(UIColor.white, for: .normal)
        self.btn_citas.backgroundColor = UIColor.white
        self.btn_citas.setTitleColor(UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1), for: UIControlState.normal)
        self.tableView.reloadData()
        //self.tableView.clear
        
    }
    
    func mostrarPaquetes(){
        self.cipaor = "paquetes"
        self.btn_citas.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        self.btn_citas.setTitleColor(UIColor.white, for: .normal)
        self.btn_paquetes.backgroundColor = UIColor.white
        self.btn_paquetes.setTitleColor(UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1), for: UIControlState.normal)
        self.tableView.reloadData()
        //self.tableView.clear
        
    }
    
    /*func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print(indexPath.row)
        print(self.carrito[indexPath.row].nombre)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DetalleCarrito") as! DetalleCarritoViewController
        vc.detalle = self.carrito[indexPath.row]
        //vc.email = self.email
        //vc.vieneFamilia3 = self.vieneFamilia2
        self.navigationController?.pushViewController(vc, animated: true)
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
