//
//  ObjConvenio.swift
//  Mi Veris
//
//  Created by Andres Cantos on 2/9/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import UIKit

// MARK: Region: Clase región

class ObjConvenio: NSObject {
    
    var codigoconvenio : NSInteger = 0
    var codigoempresa : NSInteger = 0
    var secuenciaafiliado : NSInteger = 0
    var nombreaseguradora : String = ""
    var nombreempresa : String = ""
    var numerocontrato : String = ""
    var descripcionconvenio : String = ""
    var tipows : String = ""
    var permitereserva : String = ""
    var mensajebloqueoreserva : String = ""
    
    
    init(data : NSDictionary) {
        codigoconvenio = data["codigoconvenio"] as! NSInteger
        codigoempresa = data["codigoempresa"] as! NSInteger
        secuenciaafiliado =  data["secuenciaafiliado"] as! NSInteger
        nombreaseguradora = data["nombreaseguradora"] as! NSString as String
        nombreempresa = data["nombreempresa"] as! NSString as String
        numerocontrato = data["numerocontrato"] as! NSString as String
        descripcionconvenio = data["descripcionconvenio"] as! NSString as String
        tipows = data["tipows"] as! NSString as String
        permitereserva = data["permitereserva"] as! NSString as String
        mensajebloqueoreserva = data["mensajebloqueoreserva"] as! NSString as String
    }
}
