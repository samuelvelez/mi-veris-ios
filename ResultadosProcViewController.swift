//
//  ResultadosLabController.swift
//  Mi Veris
//
//  Created by Jorge Merchan on 5/2/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import UIKit
class ResultadosProcController: UIViewController,UITableViewDelegate, UITableViewDataSource{
    // var tableView: UITableView  =   UITableView()
    var tableView: UITableView? /*Table view que muestra la lista de especailidades*/
    var lista_resul_lab : NSMutableArray =  NSMutableArray()
    var meses:[String] = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
    var resultadosel : ObjResulatado?
    var dateFormatter = DateFormatter()
    var delegate = DownloadSessionDelegate.sharedInstance
    let urldef : String = URLFactory.urldefinida()

    var data : String = "servicio/doctor/archivo?arg0="
    
    var tipodocusuario : String = ""
    var documentousuario : String = ""
    
    var nombreUser : UILabel = UILabel()
    var setReload : Bool = false;
    var not : Bool = false
    
    override func viewDidAppear(_ animated: Bool) {
        let user = Usuario.getEntity
        nombreUser.text = user.nombre as String
        if setReload {
            setReload = false
            if user.rol < 3 {
                //cargardatos()
                revisarPermisos()
            } else {
                self.lista_resul_lab = NSMutableArray()
                UtilidadesGeneral.mensaje(mensaje: "No tiene permisos para utilizar esta opción")
            }
            self.tableView!.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        dateFormatter.locale = NSLocale(localeIdentifier: "es_EC") as Locale!
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.s"
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        let user = Usuario.getEntity as Usuario
        self.tipodocusuario = user.tipoid as String
        self.documentousuario = user.numeroidentificacion as String
        self.navigationController?.navigationItem.backBarButtonItem?.title = ""
        //self.viewController.navigationItem.backBarButtonItem.title = "Custom Title";
        if(not){
            let backButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(ResultadosProcController.goBack))
            navigationItem.leftBarButtonItem = backButton
        }

        let fondo : UIImageView = UIImageView(frame: self.view.bounds)
        fondo.image = UIImage(named:"fondoresultado.png")
        self.view.addSubview(fondo)
        
        let barWhite : UIView = UIView(frame: CGRect(x:0, y:64, width:self.view.frame.width ,height: 30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width / 2.6, height: 30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x:barBlue.frame.width, y: 64, width: 45, height:30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x: 0, y: 64, width: self.view.frame.width - 20, height:30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "Resultados"
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)
        let barname : UIView = UIView(frame: CGRect(x:0, y:94, width:self.view.frame.width , height:30))
        barname.backgroundColor = UtilidadesColor.colordehexadecimal(hex: 0x000000, alpha: 0.1)
        nombreUser = UILabel(frame: CGRect(x: 10, y: 2, width: self.view.frame.width - 20, height:30))
        nombreUser.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        nombreUser.font = UIFont(name: "helvetica-bold", size: 12)
        nombreUser.text = user.nombre as String
        barname.addSubview(nombreUser)
        self.view.addSubview(barname)
        //cargardatos()
        revisarPermisos()
        
        Usuario.createButtonFamily(controlador: self)
    }
    
    func goBack(){
        dismiss(animated: true, completion: nil)
    }

    
    func getFamilyService(){
        Usuario.getJsonFamilia(controlador: self)
        setReload = true
    }
    
    func revisarPermisos(){
        
        let  usuario = Usuario.getEntity as Usuario
        let tipo_id_p = usuario.tipoid as String
        let identificacion = usuario.numeroidentificacion as String
        
        let verif = Usuario.verificarEsadmin(identificacion: identificacion)
        
        let us = Usuario.getEntity as Usuario
        
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        
        let log = UserDefaults.standard.object(forKey: "identificacion")
        let t_log = UserDefaults.standard.object(forKey: "tipo")
        
        let iden = "\(t_log!)-\(log!)"
        
        //let iden = "\(tipo_id_p)-\(identificacion)"
        let urldef = URLFactory.urldefinida()
        
        let url = NSURL(string: "\(urldef)servicio/persona/grupo/\(iden)?arg0=2")
        let request = NSMutableURLRequest(url: url! as URL)
        var resultado = ""
        //var bool  = false
        //Verificar si es admin
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            if (error != nil) {
                print(error!.localizedDescription)
                resultado = "N"
            } else {
                do{
                    var err: NSError?
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    if (err != nil) {
                        print("JSON Error \(err!.localizedDescription)")
                    }
                    
                    let info =  jsonResult as NSDictionary
                    let data = info["lista"] as! [[String: Any]]//NSArray
                    print(data)
                    if data.count > 0 {
                        for familia in data {
                            if let res = familia["numeroIdentificacion"] as? String {
                                if res == us.numeroidentificacion as String {
                                    resultado = (familia["esAdmin"] as? String)!
                                    
                                }
                            }
                        }
                    }
                    DispatchQueue.main.async(execute: {
                        print("holis \(resultado)")
                        
                        
                        if(resultado != "N"){
                            self.cargardatos()
                            
                        }
                        ViewUtilidades.hiddenLoader(controller: self)
                        
                        
                    })
                } catch {
                    print("ERROR")
                }
            }
            
            
        }
        
        
    }

    
    private func cargardatos(){
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        let url2 = URLFactory.obtenerResultadosProcedimientos(tipo_doc: self.tipodocusuario,  num_doc: self.documentousuario)
        
        
        let url = NSURL(string: "\(url2)" )
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        let str = "wsappusuario:ZA$57@9b86@$2r5"
        
        let utf8str = str.data(using: String.Encoding.utf8)
        
        /*if let base64Encoded = utf8str?.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0)){
            
            print("Encoded:  \(base64Encoded)")
            //request.addValue("Authorization", forHTTPHeaderField: "Basic \(base64Encoded)")
            request.setValue("Basic \(base64Encoded)", forHTTPHeaderField: "Authorization")
            
            if let base64Decoded = NSData(base64EncodedString: base64Encoded, options:   NSDataBase64DecodingOptions(rawValue: 0))
                .map({ NSString(data: $0, encoding: NSUTF8StringEncoding) })
            {
                // Convert back to a string
                print("Decoded:  \(base64Decoded)")
            }
        }*/
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                var err: NSError?
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                if (err != nil) {
                    print("JSON Error \(err!.localizedDescription)")
                }
                
                
                let info =  jsonResult as NSDictionary
                let data = info["lista"] as! [[String: Any]]
                
                
                if data.count > 0 {
                    self.lista_resul_lab = NSMutableArray()
                    
                    for jsonobj  in data {
                        let r_lab = ObjResulatado()
                        print("--test data--")
                        
                        if (jsonobj["numeroorden"] is NSNull){
                            r_lab.numeroorden = " "
                            print("--1 --")
                        }else
                        {
                            r_lab.numeroorden = jsonobj["numeroorden"] as! String
                            print("--11--")
                        }
                        
                        
                        if (jsonobj["codigoempresa"] is NSNull){
                            r_lab.codigoempresa = 0
                            print("--2--")
                        }else
                        {
                            r_lab.codigoempresa = jsonobj["codigoempresa"] as! NSInteger
                            print("--22--")
                        }
                        
                        
                        if (jsonobj["codigosucursal"] is NSNull){
                            r_lab.codigosucursal = 0
                            print("--3--")
                        }else
                        {
                            r_lab.codigosucursal = jsonobj["codigosucursal"] as! NSInteger
                            print("--33--")
                        }
                        
                        
                        
                        if (jsonobj["codigomedicoremite"] is NSNull){
                            r_lab.codigomedicoremite = 0
                            print("--4--")
                        }else
                        {
                            r_lab.codigomedicoremite = jsonobj["codigomedicoremite"] as! NSInteger
                            print("--44--")
                        }
                        
                        
                        
                        if (jsonobj["nombremedicoremite"] is NSNull){
                            r_lab.nombremedicoremite = ""
                            print("--5--")
                        }else
                        {
                            r_lab.nombremedicoremite = jsonobj["nombremedicoremite"] as! String
                            print("--55--")
                        }
                        
                        
                        
                        if (jsonobj["fechaorden"] is NSNull){
                            r_lab.fechaorden = ""
                            print("--6--")
                        }else
                        {
                            r_lab.fechaorden = jsonobj["fechaorden"] as! String
                            print("--66--")
                        }
                        
                        
                        
                        
                        if (jsonobj["rutaarchivo"] is NSNull){
                            r_lab.rutaarchivo = ""
                            print("--7--")
                        }else
                        {
                            r_lab.rutaarchivo = jsonobj["rutaarchivo"] as! String
                            print("--77--")
                        }
                        
                        
                        
                        
                        if (jsonobj["numero_factura"] is NSNull){
                            r_lab.numero_factura = ""
                            print("--8--")
                        }else
                        {
                            r_lab.numero_factura = jsonobj["numero_factura"] as!String
                            print("--88--")
                        }
                        
                        
                        if (jsonobj["tipo"] is NSNull){
                            r_lab.tipo = ""
                            print("--9--")
                        }else
                        {
                            r_lab.tipo = jsonobj["tipo"] as! NSString as String
                            print("--99--")
                        }
                        
                        
                        
                        
                        self.lista_resul_lab.add(r_lab)
                        
                    }
                }
                
                //dispatch_async(dispatch_get_main_queue(), {
                    DispatchQueue.main.async(execute:{
                        
                    self.show_resu_lab()
                    ViewUtilidades.hiddenLoader(controller: self)
                })
                } catch {
                    print("ERROR")
                }
            }
            
        }
        
        ///////////////////////
        
        
    }
    func show_resu_lab(){
        if tableView == nil {
            tableView = UITableView()
            tableView!.frame = CGRect(x:0, y:125, width:self.view.frame.width,height: self.view.frame.height - 150)
            
            tableView!.delegate = self
            tableView!.dataSource = self
            tableView!.backgroundColor = UIColor.clear
            tableView!.layer.cornerRadius = 10;
            tableView!.isScrollEnabled = false
            tableView!.rowHeight = 10
            
            tableView!.separatorColor = UIColor.clear
            
            tableView!.register(ResuImgCell.self as AnyClass, forCellReuseIdentifier: "resulatdoCell");
            
            self.view.addSubview(tableView!)
        } else {
            tableView!.reloadData()
        }
        
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lista_resul_lab.count
        
    }
    
    /*func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.separatorInset = UIEdgeInsetsZero
        cell.layoutMargins = UIEdgeInsetsZero
    }*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "resulatdoCell", for: indexPath as IndexPath) as! ResuImgCell
        let resul_l = lista_resul_lab.object(at: indexPath.row) as! ObjResulatado
        cell.numero_exa.text="Resultado Laboratorio-\(indexPath.row)"
        cell.descargar.text="Descargar"
        cell.pdf.text="PDF"
        print("test Posicion : ", indexPath.row)
        print(resul_l.fechaorden)
        let fecha : NSDate  = dateFormatter.date(from: resul_l.fechaorden)! as NSDate
        let myCalendar : NSCalendar = NSCalendar.current as NSCalendar
        let myComponents = myCalendar.components([NSCalendar.Unit.year, NSCalendar.Unit.day, NSCalendar.Unit.month, NSCalendar.Unit.hour, NSCalendar.Unit.minute], from: fecha as Date)
        cell.fecha.text = "\(myComponents.day!)/\(meses[myComponents.month! - 1])/\(myComponents.year!)"
        cell.btn_descarga.tag = indexPath.row;
        cell.btn_descarga.addTarget(self, action: #selector(miEvento(sender:)), for: .touchUpInside)
        
        return cell
        
    }
    
    func miEvento(sender:UIButton!)
    {
        let resul_l = lista_resul_lab.object(at: sender.tag) as! ObjResulatado
        let test_ruta : String = resul_l.rutaarchivo
        let ruta_descarga : String = urldef + data + test_ruta
        //UtilidadesGeneral.mensaje(resul_l.rutaarchivo)
        print("ruta archivo:")
        print(test_ruta)
        print("test pdf:")
        print(data)
        
        download(data: urldef + data + test_ruta)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "viewResulLabPdf") as! VisorResultado
        vc.ruta = ruta_descarga as NSString
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    //func tableView(_ heightForRowAttableView:UITableView!, heightForRowAtIndexPath indexPath:IndexPath)->CGFloat {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func download(data: String!) {
        let configuration = URLSessionConfiguration.background(withIdentifier: "url_session_background_download")
        let backgroundSession = URLSession(configuration: configuration, delegate: self.delegate, delegateQueue: nil)
        //        for stringUrl in data {
        let url = NSURLRequest(url: NSURL(string: data)! as URL)
        let downloadTask = backgroundSession.downloadTask(with: url as URLRequest)
        //let downloadTask = backgroundSession.dataTask(with: url)
        //let downloadTask = backgroundSession.downloadTask(with: url)
        
        downloadTask.resume()
        
    }
}



