//
//  SolicitarHistoriaViewController.swift
//  Mi Veris
//
//  Created by Jorge on 27/05/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import UIKit

class SolicitarHistoriaViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var titleView : NSString!
    
    var nombreUser : UILabel!
    
    var firstStep : UILabel!
    var secondStep : UILabel!
    var thirdStep : UILabel!
    var listaEspecialidades : NSMutableArray = NSMutableArray()
    
    var tabla : UITableView!
    var especialidadesSeleccionadas : NSMutableArray = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let especialidad = Especialidad()
        especialidad.descripcion = "Seleccionar Todas"
        especialidad.codigo = -1
        listaEspecialidades.add( especialidad )

        self.navigationController?.navigationItem.backBarButtonItem?.title = ""
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        let fondo : UIImageView = UIImageView(frame: self.view.bounds)
        fondo.image = UIImage(named:"fondocita.png")
        self.view.addSubview(fondo)
        
        let barWhite : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width , height: 30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width / 2.8, height: 30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x: barBlue.frame.width, y: 64, width: 45, height: 30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x: 0, y: 64, width: self.view.frame.width - 20, height: 30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = titleView as String
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)
        
        let usuario = Usuario.getEntity as Usuario
        
        
        nombreUser = UILabel(frame: CGRect(x: 10, y: 94, width: self.view.frame.width - 20, height: 30))
        nombreUser.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        nombreUser.font = UIFont(name: "helvetica-bold", size: 12)
        nombreUser.text = usuario.nombre as String
        
        self.view.addSubview(nombreUser)
        
        let anchoStep = self.view.frame.width / 3
        
        firstStep = UILabel(frame: CGRect(x: 0, y: 125, width: anchoStep, height: 30))
        firstStep.text = "Especialidad"
        firstStep.textAlignment = NSTextAlignment.center
        firstStep.textColor = UIColor.white
        firstStep.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        firstStep.font = UIFont.boldSystemFont(ofSize: 13)
        self.view.addSubview(firstStep)
        
        secondStep = UILabel(frame: CGRect( x: anchoStep, y: 125, width: anchoStep,height:  30))
        secondStep.text = "Doctor"
        secondStep.textAlignment = NSTextAlignment.center
        secondStep.textColor = UIColor.black
        secondStep.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 0.3)
        secondStep.font = UIFont.boldSystemFont(ofSize: 13)
        self.view.addSubview(secondStep)
        
        thirdStep = UILabel(frame: CGRect( x: anchoStep * 2, y: 125,width: anchoStep, height: 30))
        thirdStep.text = "Fechas"
        thirdStep.textAlignment = NSTextAlignment.center
        thirdStep.textColor = UIColor.black
        thirdStep.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 0.3)
        thirdStep.font = UIFont.boldSystemFont(ofSize: 13)
        self.view.addSubview(thirdStep)
        
        
        getEspecialidades()
        
    }
    
    func getEspecialidades(){
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        let usuario = Usuario.getEntity
        let urldef = URLFactory.urldefinida()

        let url = NSURL(string: "\(urldef)servicio/especialidad/obtenerespecialidadeshc?arg0=\(usuario.numeroidentificacion)&arg1=\(usuario.tipoid)")
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
       
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                var err: NSError?
                
                //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                if (err != nil) {
                    print("JSON Error \(err!.localizedDescription)")
                }
                
                
                //println(jsonResult);
                print(jsonResult)
                let info =  jsonResult as NSDictionary
                    let data = info["lista"] as! [[String: Any]]
                if data.count > 0 {
                    for especialidad in data {
                        let objeto = Especialidad()
                        objeto.codigo = especialidad["codigo"] as! NSInteger
                        objeto.descripcion = especialidad["descripcion"] as! String
                        self.listaEspecialidades.add(objeto)
                    }
                }
                
                //dispatch_async(dispatch_get_main_queue(), {
                    DispatchQueue.main.async(execute: {
                    ViewUtilidades.hiddenLoader(controller: self)
                    self.showEspecialidades()
                })
                } catch {
                    print("ERROR")
                }
            }
            
            
        }
    }
    
    func showEspecialidades(){
        tabla = UITableView(frame: CGRect(x: 10, y: 160, width:self.view.frame.width - 20, height:self.view.frame.height - 230))
        tabla.delegate = self
        tabla.dataSource = self
        tabla.backgroundColor = UIColor.clear
        tabla.register(UITableViewCell.self as AnyClass, forCellReuseIdentifier: "cell");
        self.view.addSubview(tabla)
        
        let btNext = UIButton(frame: CGRect(x:self.view.frame.width - 120, y:self.view.frame.height - 50 ,   width: 100, height:40))
        btNext.setTitle("Siguiente", for: UIControlState.normal)
        btNext.addTarget(self, action: #selector(SolicitarHistoriaViewController.nextSolicitud), for: UIControlEvents.touchUpInside)
        btNext.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        btNext.layer.cornerRadius = 15
        self.view.addSubview(btNext)
        
    }
    
    func nextSolicitud(){
        if especialidadesSeleccionadas.count > 0 {
            
            if especialidadesSeleccionadas.count == 1 {
                let posicion : NSInteger = especialidadesSeleccionadas.object(at: 0) as! NSInteger
                let especialidad : Especialidad = listaEspecialidades.object( at: posicion ) as! Especialidad
                
                if especialidad.codigo != -1 {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "viewSolicitarDos") as! SolicitarHistoria2ViewController
                    vc.titleView = titleView
                    vc.listaEspecialidades = listaEspecialidades
                    vc.especialidadesSeleccionadas = especialidadesSeleccionadas
                    navigationController?.pushViewController(vc, animated: true)
                } else {
                    let alert = UIAlertController(title: "Veris", message: "Debe seleccionar al menos una especialidad", preferredStyle: UIAlertControllerStyle.alert)
                    
                    let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                        UIAlertAction in
                        
                    }
                    
                    alert.addAction(okAction)
                    self.present(alert, animated: true, completion: nil)
                }
            } else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "viewSolicitarDos") as! SolicitarHistoria2ViewController
                vc.titleView = titleView
                vc.listaEspecialidades = listaEspecialidades
                vc.especialidadesSeleccionadas = especialidadesSeleccionadas
                navigationController?.pushViewController(vc, animated: true)
            }
            
            
        } else {
            let alert = UIAlertController(title: "Veris", message: "Debe seleccionar al menos una especialidad", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                UIAlertAction in
                
            }
            
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    // MARK: Métodos de tableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaEspecialidades.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as UITableViewCell
        let especialidad = listaEspecialidades.object(at: indexPath.row) as! Especialidad
        cell.textLabel?.font = cell.textLabel?.font.withSize(14)
        cell.textLabel?.text = "  \(especialidad.descripcion)"
        cell.backgroundColor = UIColor.clear
        
        let posicion : Int = especialidadesSeleccionadas.index(of: indexPath.row)
        if (posicion != NSNotFound) {
            cell.accessoryType = UITableViewCellAccessoryType.checkmark
        }
        
        if indexPath.row == 0 {
            cell.backgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 0.5)
        }
        
        //cell.backgroundColor = UIColor.clearColor()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell : UITableViewCell = tableView.cellForRow(at: indexPath as IndexPath)! as UITableViewCell
        
        if indexPath.row == 0 {
            if ( cell.accessoryType == UITableViewCellAccessoryType.none ) {
                let count = listaEspecialidades.count
                especialidadesSeleccionadas.removeAllObjects()
                for index in 0...count - 1 {
                    especialidadesSeleccionadas.add(index)
                    //let indexAll : NSIndexPath = NSIndexPath(forRow: index, inSection: 0)
                    let indexAll : NSIndexPath = NSIndexPath(row: index, section: 0)
                    if let cell : UITableViewCell = tableView.cellForRow(at: indexAll as IndexPath) {
                        cell.accessoryType = UITableViewCellAccessoryType.checkmark
                    }
                }
            } else {
                let count = listaEspecialidades.count
                especialidadesSeleccionadas.removeAllObjects()
                for index in 0...count - 1 {
                    let indexAll : NSIndexPath = NSIndexPath(row: index, section: 0)
                    if let cell : UITableViewCell = tableView.cellForRow(at: indexAll as IndexPath) {
                        cell.accessoryType = UITableViewCellAccessoryType.none
                    }
                }
            }
            
            
        } else {
            if ( cell.accessoryType == UITableViewCellAccessoryType.none ) {
                cell.accessoryType = UITableViewCellAccessoryType.checkmark
                especialidadesSeleccionadas.add(indexPath.row)
            } else {
                cell.accessoryType = UITableViewCellAccessoryType.none;
                especialidadesSeleccionadas.remove(indexPath.row)
                // And remove the peerID from the array of selections
                //[selectedRows removeObjectForKey:keyString];
            }
        }
        
        
        tableView.deselectRow(at: indexPath as IndexPath, animated: true);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
