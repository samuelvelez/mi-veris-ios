//
//  Carrito.swift
//  Mi Veris
//
//  Created by Samuel Velez on 8/3/17.
//  Copyright © 2017 Samuel Velez. All rights reserved.
//

import Foundation

class CarritoModel: NSObject {
    
    let tipoIdentificacion : String
    let numeroId : String
    let nombre : String
    let telefono : String
    let direccion : String
    let mail : String
    //let ordenes : []
    let citasPagar : [CitasPagarModel]
    let paquetesPagar : [PaquetesPagarModel]
    
    let totalCitas : Double
    let totalPaquetes : Double
    
    
    init(tipoIdentificacion : String?, numeroId : String?, nombre : String?, telefono : String?, direccion : String?, mail : String?, citas : [CitasPagarModel]?, paquetes : [PaquetesPagarModel]?, totalCitas : Double?, totalPaquetes : Double?) {
        self.tipoIdentificacion = tipoIdentificacion ?? ""
        self.numeroId = numeroId ?? ""
        self.nombre = nombre ?? ""
        self.telefono = telefono ?? ""
        self.direccion = direccion ?? ""
        self.mail = mail ?? ""
        
        self.citasPagar = citas!
        self.paquetesPagar = paquetes!
        self.totalCitas = totalCitas ?? 0
        self.totalPaquetes = totalPaquetes ?? 0
        
    }
}
