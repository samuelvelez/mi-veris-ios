//
//  ReservaServicioViewController.swift
//  Mi Veris
//
//  Created by Samuel Velez on 21/6/16.
//  Copyright © 2016 Programadores-iOS.net. All rights reserved.
//

//import Foundation


import UIKit

class ReservaServicioViewController: UIViewController,UITableViewDelegate, UITableViewDataSource{
    
    var tableView: UITableView  =   UITableView()
    var listaespecialidades : NSMutableArray?
    var delegate : ReservaServicioDelegate! = nil
    var especialidadsel : ObjEspecialidad!
    var serviciosel : ObjEspecialidad?
    var recomendacionnumeroorden : NSString = "0"
    var recomendacioncodempresa : NSString = "0"
    var recomendacionlineadetalle : NSString = "0"
    
    
    
    var lista_ordenes : NSMutableArray?
    var servicio2_sel : ObjEspecialidad?
    
    
    var id_llega : String?
    var tip_id_llega : String?
    
    var nuevo_id : String = ""
    var nuevo_tipoId : String = ""
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationItem.backBarButtonItem?.title=""
        navigationItem.backBarButtonItem?.title = ""
        navigationItem.title = "Seleccione un Servicio"
        //cargardatos()
        cargarServicios()
        
    }
    private func cargarServicios(){
        let us = Usuario.getEntity as Usuario
        
        if(id_llega != nil && tip_id_llega != nil){
            nuevo_id = id_llega!
            nuevo_tipoId = tip_id_llega!
        }else{
            nuevo_tipoId = us.numeroidentificacion as String
            nuevo_id = us.tipoid as String
        }
        let naj = UserDefaults.standard.object(forKey: "identificacion")
        
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        let urldef = URLFactory.urldefinida()
        let url2 = "\(urldef)servicio/v2/agenda/consultarservicios?arg0=\(especialidadsel!.codigo)&arg1=\(nuevo_id)&arg2=\(nuevo_tipoId)&arg3=\(naj!)"
        
        let url = NSURL(string: url2)
        let request = NSMutableURLRequest(url: url! as URL)
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                   // print("llego")
                    var err: NSError?
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    if (err != nil) {
                        print("JSON Error \(err!.localizedDescription)")
                    }
                    
                    //print(jsonResult)
                    
                    
                    let info =  jsonResult as NSDictionary
                    let data = info["lista"] as! [[String: Any]]
                    
                    
                    if data.count > 0 {
                        self.listaespecialidades = NSMutableArray();
                        self.lista_ordenes = NSMutableArray()
                        for jsonobj  in data {
                            let or = ObjEspecialidad()
                            print("estos valors \(jsonobj["lineaDetalle"]) \(jsonobj["numeroOrden"])")
                            if let linea = jsonobj["lineaDetalle"] as? NSString{
                                or.codigo = (linea).integerValue
                            }
                            if let numero = jsonobj["numeroOrden"] as? String{
                                or.descripcion = numero
                            }
                            self.lista_ordenes?.add(or)
                            let esp = ObjEspecialidad()
                            esp.codigo = (jsonobj["codigoPrestacion"] as! NSString).integerValue
                            esp.descripcion = jsonobj["nombrePrestacion"] as! String
                            self.recomendacioncodempresa = jsonobj["codigoEmpresa"] as! NSString
                            self.listaespecialidades?.add(esp)
                            //self.listaespecialidades?.add(ord)
                        }
                    }else{
                        //self.listaespecialidades = NSMutableArray();
                        let espnull = ObjEspecialidad()
                        //espnull.codigo= 0;
                        //es
                        self.listaespecialidades?.add(espnull);
                    }
                    
                    //dispatch_async(dispatch_get_main_queue(), {
                    DispatchQueue.main.async(execute: {
                        self.showEspecialidades()
                        ViewUtilidades.hiddenLoader(controller: self)
                    })
                } catch {
                    print("ERROR")
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: "Revise su conexión a Datos o Internet", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            
        }
        
        ///////////////////////

    }
    private func cargardatos(){
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        let urldef = URLFactory.urldefinida()
        let url2 = "\(urldef)servicio/v2/agenda/consultarprestaciones?arg0=\(especialidadsel!.codigo)"
        
        let url = NSURL(string: url2)
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        let str = "wsappusuario:ZA$57@9b86@$2r5"
        
        
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                    print("llego")
                    var err: NSError?
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    if (err != nil) {
                        print("JSON Error \(err!.localizedDescription)")
                    }
                    
                    print(jsonResult)
                    
                    
                    let info =  jsonResult as NSDictionary
                    let data = info["lista"] as! [[String: Any]]

                    
                    if data.count > 0 {
                        self.listaespecialidades = NSMutableArray();
                        
                        for jsonobj  in data {
                            print("\(jsonobj["codigoPrestacion"]!)")
                            print("\(jsonobj["nombrePrestacion"]!)")
                            let esp = ObjEspecialidad()
                            //(jsonDict["totalfup"] as! NSString).doubleValue
                            esp.codigo = (jsonobj["codigoPrestacion"] as! NSString).integerValue
                            esp.descripcion = jsonobj["nombrePrestacion"] as! String
                            self.listaespecialidades?.add(esp)
                        }
                    }else{
                        self.listaespecialidades = NSMutableArray();
                        let espnull = ObjEspecialidad()
                        //espnull.codigo= 0;
                        //es
                        self.listaespecialidades?.add(espnull);
                    }
                    
                    //dispatch_async(dispatch_get_main_queue(), {
                    DispatchQueue.main.async(execute: {
                        //self.showEspecialidades()
                        self.cargarServicios()
                        ViewUtilidades.hiddenLoader(controller: self)
                    })
                } catch {
                    print("ERROR")
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: "Revise su conexión a Datos o Internet", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            
        }
        
        ///////////////////////
        
        
        
    }
    func showEspecialidades(){
        tableView.frame = CGRect(x:0, y: 60, width:self.view.frame.width, height:self.view.frame.height - 60);
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.backgroundColor = UIColor.clear
        //tableView.registerClass(DondeEstamosTableViewCell.self as AnyClass, forCellReuseIdentifier: "SucursalesCell");
        
        self.view.addSubview(tableView);
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaespecialidades!.count;
    }
    
    /*func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
     cell.separatorInset = UIEdgeInsetsZero
     cell.layoutMargins = UIEdgeInsetsZero
     }*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "") as UITableViewCell!
            ?? UITableViewCell(style: .default, reuseIdentifier: ""),
        especialidad = listaespecialidades!.object(at: indexPath.row) as! ObjEspecialidad
        
        cell.textLabel?.text = especialidad.descripcion
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        serviciosel = listaespecialidades!.object(at: indexPath.row) as? ObjEspecialidad
        servicio2_sel = lista_ordenes?.object(at: indexPath.row) as? ObjEspecialidad
        print("quiero ver estos valores que me faltan \(servicio2_sel!.codigo) \(servicio2_sel!.descripcion)")
        print("selecciono: "+"\(serviciosel?.codigo)")
        delegate.retornadereservaservicio(controller: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

protocol ReservaServicioDelegate{
    func retornadereservaservicio(controller:ReservaServicioViewController)
}
