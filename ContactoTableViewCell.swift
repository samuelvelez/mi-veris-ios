//
//  ContactoTableViewCell.swift
//  Mi Veris
//
//  Created by Jorge on 04/02/15.
//  Copyright (c) 2015 Firewall Soluciones All rights reserved.
//

import UIKit


// MARK: Configuraciçon de las celdas de

class ContactoTableViewCell: UITableViewCell {

    
    var separator : UIImageView = UIImageView();
    var title : UILabel = UILabel();
    var icono : UIImageView = UIImageView()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width;
        
        title.frame = CGRect(x: 20, y: 0, width: screenWidth - 100, height: 40)
        title.font = UIFont(name: "helvetica", size: 14)
        title.textColor = UIColor.black
        
        self.backgroundColor = UIColor.clear
        icono.frame = CGRect(x: screenWidth - 70 , y: 2, width: 36, height: 36)
        
        
        separator.backgroundColor = UIColor.gray
        separator.frame = CGRect(x: 0, y: 39, width: screenWidth, height: 1)
        
        self.contentView.addSubview(title)
        self.contentView.addSubview(icono)
        self.contentView.addSubview(separator)
        
        
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    
    

}
