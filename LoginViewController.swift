//
//  LoginViewController.swift
//  Mi Veris
//
//  Created by Jorge on 05/02/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import UIKit
//import CryptoSwift
class LoginViewController: UIViewController {
    var band : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = ""

        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        let fondo : UIImageView = UIImageView(frame: self.view.bounds)
        fondo.image = UIImage(named:"fondodoctor.png")
        self.view.addSubview(fondo)
        
        
        let icono = UIImageView(frame: CGRect(x: self.view.frame.width/2 - 100, y:  self.view.frame.height / 2 - 150, width: 200, height: 200))
        icono.image = UIImage(named: "userLogin.png")
        self.view.addSubview(icono)
        
        let etiqueta = UILabel(frame: CGRect(x: 20, y: self.view.frame.height / 2 + 60, width: self.view.frame.width - 40, height: 50))
        etiqueta.text = "Estimado usuario, necesita iniciar sesión para utilizar esta opción."
        etiqueta.numberOfLines = 2
        etiqueta.font = UIFont(name: "helvetica", size: 14)
        self.view.addSubview(etiqueta)
        
        let btAceptar = UIButton(frame: CGRect(x: 10,y: self.view.frame.height / 2 + 120,  width: self.view.frame.width - 20, height: 50))
        btAceptar.setTitle("Iniciar Sesión", for: UIControlState.normal)
        btAceptar.addTarget(self, action: #selector(LoginViewController.showForm), for: UIControlEvents.touchUpInside)
        btAceptar.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        btAceptar.layer.cornerRadius = 25
        self.view.addSubview(btAceptar)
        
        let btCrearCuenta = UIButton(frame: CGRect( x: 30, y: self.view.frame.height - 50,  width:(self.view.frame.width / 2) - 40 , height: 30))
        btCrearCuenta.setTitle("Crear Cuenta Veris", for: UIControlState.normal)
        btCrearCuenta.titleLabel!.font = UIFont(name: "Helvetica", size: 12);
        btCrearCuenta.addTarget(self, action: #selector(LoginViewController.createAccount), for: UIControlEvents.touchUpInside)
        //btCrearCuenta.backgroundColor = UIColor.grayColor()
        btCrearCuenta.setTitleColor(UIColor.black, for: UIControlState.normal)
        btCrearCuenta.setTitleColor(UIColor.gray, for: UIControlState.highlighted)
        self.view.addSubview(btCrearCuenta)
        
        let btResetearClave = UIButton(frame: CGRect( x: (self.view.frame.width / 2) + 10 , y: self.view.frame.height - 50, width: (self.view.frame.width / 2) - 40 , height: 30))
        btResetearClave.setTitle("Resetear Clave", for: UIControlState.normal)
        btResetearClave.titleLabel!.font = UIFont(name: "Helvetica", size: 12);
        btResetearClave.addTarget(self, action: #selector(LoginViewController.resetPassword), for: UIControlEvents.touchUpInside)
        //btResetearClave.backgroundColor = UIColor.grayColor()
        btResetearClave.setTitleColor(UIColor.black, for: UIControlState.normal)
        btResetearClave.setTitleColor(UIColor.gray, for: UIControlState.highlighted)
        self.view.addSubview(btResetearClave)
        
    }
    
    func showForm(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //let vc = storyboard.instantiateViewControllerWithIdentifier("viewFormLogin") as! FormLoginViewController
        //probar view controller
        //let vc = storyboard.instantiateViewControllerWithIdentifier("viewSeguridad") as! SeguridadViewController
        let vc = storyboard.instantiateViewController(withIdentifier: "newLogin") as! NewLoginViewController
        vc.band = band
        navigationController?.pushViewController(vc, animated: true)
 
        print("show form")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func createAccount(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "viewCrearCuenta") as! CreateViewController
        navigationController?.pushViewController(vc, animated: true)

    }
    
    func resetPassword(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "reseteoView") as! NewResetViewController
        navigationController?.pushViewController(vc, animated: true)
        
    }


}
