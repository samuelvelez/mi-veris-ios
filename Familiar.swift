//
//  Familiar.swift
//  Mi Veris
//
//  Created by Samuel Velez on 15/8/16.
//  Copyright © 2016 Programadores-iOS.net. All rights reserved.
//

import UIKit

class Familiar : NSObject{
    
    var tipoIdentificacion : NSString = ""
    var numeroIdentificacion : NSString = ""
    var primerNombre : NSString = ""
    var segundoNombre : NSString = ""
    var primerApellido : NSString = ""
    var segundoApellido : NSString = ""
    var edad : NSString = ""
    var codigoParentesco : NSString = ""
    var parentesco : NSString = ""
    var idRelacion : NSString = ""
    var esAdmin : NSString = ""
    
}
