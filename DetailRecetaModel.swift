//
//  DetailRecetaModel.swift
//  veris
//
//  Created by Felipe Lloret on 07/02/15.
//  Copyright (c) 2015 Felipe Lloret. All rights reserved.
//

import Foundation

class DetailRecetaModel: NSObject {
    let denominacion: String
    let concentracion: String
    let forma: String
    let cantidad: String
    let indicaciones: String
    
    override var description: String {
        return "denominacion: \(denominacion), concentracion: \(concentracion)\n, forma: \(forma)\n, cantidad: \(cantidad)\n, indicaciones: \(indicaciones)\n"
    }
    
    init(denominacion: String?, concentracion: String?, forma: String?, cantidad: String?, indicaciones: String?) {
        self.denominacion = denominacion ?? ""
        self.concentracion = concentracion ?? ""
        self.forma = forma ?? ""
        self.cantidad = cantidad ?? ""
        self.indicaciones = indicaciones ?? ""
    }
}
