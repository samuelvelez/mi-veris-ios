//
//  SucursalViewController.swift
//  Mi Veris
//
//  Created by Jorge on 04/02/15.
//  Copyright (c) 2015 Firewall Soluciones All rights reserved.
//

import UIKit

class SucursalViewController: UIViewController {
    
    var sucursal : Sucursal!

    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.style();
        let txtTitulo = UILabel(frame: CGRect(x: 10, y: 104, width: self.view.frame.width, height: 30))
        txtTitulo.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        txtTitulo.text = sucursal.descripcion
        
        self.view.addSubview(txtTitulo)
        
        let imagen = UIImageView(frame: CGRect(x: 10, y: 140, width: self.view.frame.width - 20, height: 250))
        self.view.addSubview(imagen)
        
        let urldef = URLFactory.urldefinida()

        let url = URL(string: "\(urldef)sucursal/\(sucursal.region)/\(sucursal.idSucursal).jpg")
        getDataFromUrl(url: url!) { (data, response, error)  in
            DispatchQueue.main.sync() { () -> Void in
                print("entro")
                guard let data = data, error == nil else { print(error);return }
                print(response?.suggestedFilename ?? url?.lastPathComponent)
                print("Download Finished")
                //self.imageView.image = UIImage(data: data)
                imagen.image = UIImage(data: data)
                print("se ejecuto")
            }
        }

        
        let lbDireccion = UILabel(frame: CGRect(x:10, y:400, width:self.view.frame.width, height:30))
        lbDireccion.textColor = UIColor(red: (105.0/256.0), green: (117.0/256.0), blue: (220.0/256.0), alpha: 1)
        lbDireccion.text = "Dirección"
        
        self.view.addSubview(lbDireccion)
        
        let direccion = UILabel(frame: CGRect(x:10, y:425, width:self.view.frame.width, height:30))
        direccion.textColor = UIColor.gray
        direccion.text = sucursal.direccion
        direccion.font = UIFont(name: "helvetica", size: 14)
        direccion.numberOfLines = 0
        direccion.sizeToFit()
        
        self.view.addSubview(direccion)
        
        let lbTelefono = UILabel(frame: CGRect(x:10, y:460, width:self.view.frame.width, height:30))
        lbTelefono.textColor = UIColor(red: (105.0/256.0), green: (117.0/256.0), blue: (220.0/256.0), alpha: 1)
        lbTelefono.text = "Teléfono"
        
        self.view.addSubview(lbTelefono)
        
        let telefono = UILabel(frame: CGRect(x: 10, y: 480, width:self.view.frame.width, height:30))
        telefono.textColor = UIColor.gray
        telefono.text = sucursal.telefono
        telefono.font = UIFont(name: "helvetica", size: 14)
        
        self.view.addSubview(telefono)
        
        let barraBottom = UIView(frame: CGRect(x: 0, y: self.view.frame.height - 50, width:  self.view.frame.width, height:50))
        barraBottom.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6)
        
        self.view.addSubview(barraBottom)
        
        let anchoTotal : CGFloat = ( self.view.frame.width / 4) as CGFloat
        
        let btInfo = UIImageView(frame: CGRect(x:anchoTotal  - 15, y:10, width: 30, height: 30))
        btInfo.image = UIImage(named: "info.png")
        btInfo.isUserInteractionEnabled = true
        let tapGestureInfo = UITapGestureRecognizer(target: self, action: "showInfo")
        btInfo.addGestureRecognizer(tapGestureInfo)
        
        barraBottom.addSubview(btInfo)
        
        let btEspecialidad = UIImageView(frame: CGRect(x: (anchoTotal * 2) - 15, y:10, width:30, height:30))
        btEspecialidad.image = UIImage(named: "especialidad.png")
        barraBottom.addSubview(btEspecialidad)
        let tapGestureEspecialidad = UITapGestureRecognizer(target: self, action: "showEspecialidad")
        btEspecialidad.addGestureRecognizer(tapGestureEspecialidad)
        btEspecialidad.isUserInteractionEnabled = true
        
        
        let btLocation = UIImageView(frame: CGRect( x:(anchoTotal * 3) - 15, y:10, width:30, height:30))
        btLocation.image = UIImage(named: "location.png")
        barraBottom.addSubview(btLocation)
        let tapGestureLocation = UITapGestureRecognizer(target: self, action: "showMap")
        btLocation.addGestureRecognizer(tapGestureLocation)
        btLocation.isUserInteractionEnabled = true
        
        
    }
    
   // MARK: style: Estilo del controlador     
    
     func style(){
        
        
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        self.navigationController?.navigationItem.backBarButtonItem?.title = ""
        
        
        let fondo : UIImageView = UIImageView(frame: self.view.bounds)
        fondo.image = UIImage(named:"fondocita.png")
        self.view.addSubview(fondo)
        
        
        let barWhite : UIView = UIView(frame: CGRect(x:0, y:64, width:self.view.frame.width , height:30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x:0, y:64, width:self.view.frame.width / 2.6, height:30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x:barBlue.frame.width, y:64, width:45, height:30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x:0, y:64, width:self.view.frame.width - 20, height:30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "¿Dónde Estamos?"
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)

        
    }
    
     // MARK: showInfo: Método para mostrar información
    
    func showInfo(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "viewDetalle") as! DetalleViewController
        vc.textdireccion = sucursal.direccion as NSString!
        vc.texttelefonos = sucursal.horario as NSString!
        navigationController?.pushViewController(vc, animated: true)
    }
    
      // MARK: showEspecialidad: Método para mostrar especialidades
    
    func showEspecialidad(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "viewEspecialidades") as! EspecialidadesViewController
        vc.especialidades = sucursal.especialidades
        navigationController?.pushViewController(vc, animated: true)
    }
    
      // MARK: showMap: Método para abrir mapas
    
    func showMap(){
        if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
            UIApplication.shared.openURL(NSURL(string:
                "comgooglemaps://?q=Veris&center=\(sucursal.latitud),\(sucursal.longitud)&zoom=14")! as URL)
        } else {
          
            UIApplication.shared.openURL(NSURL(string:
                "http://maps.google.com/maps?z=12&t=m&q=loc:\(sucursal.latitud)+\(sucursal.longitud)")! as URL)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    

}
