//
//  ResuImgCell.swift
//  Mi Veris
//
//  Created by Jorge Merchan on 7/2/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import UIKit
class RecomendacionesCell: UITableViewCell {
    var numero_exa : UILabel = UILabel()
    var fecha : UILabel = UILabel()
    var btn_agendar : UIButton = UIButton()
    var btn_ver_detalle : UIButton = UIButton()
    var nom_medicio : UILabel = UILabel()
    var nom_serv : UILabel = UILabel()
    var blueArrow : UIImageView = UIImageView()
    var gradientView: UIView = UIView()
    var firstLineView: UIView = UIView()
    var secondLineView: UIView = UIView()
    var thirdLineView: UIView = UIView()
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.clear
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width;
        
        
        
        
        
        numero_exa.frame = CGRect(x:5, y:3, width:200, height:24)
        numero_exa.font = UIFont(name: "helvetica-bold", size: 12)
        numero_exa.textColor = UIColor.black
        
        fecha.frame = CGRect(x:0, y:50, width:218, height:20)
        fecha.font = UIFont(name: "helvetica", size: 12)
        fecha.textColor = UIColor.gray
        fecha.textAlignment = NSTextAlignment.center
        
        nom_medicio.frame = CGRect(x:28, y:4, width:300, height:20)
        nom_medicio.font = UIFont(name: "helvetica-bold", size: 12)
        nom_medicio.textColor = UIColor.black
        
        nom_serv.frame = CGRect(x:0, y:35, width:218, height:20)
        nom_serv.font = UIFont(name: "helvetica-bold", size: 12)
        nom_serv.textColor = UIColor.gray
        nom_serv.textAlignment = NSTextAlignment.right
        
        
        btn_agendar.setTitle("AGENDAR", for: UIControlState.normal)
        btn_agendar.frame = CGRect(x: screenWidth - 100, y:38, width:80, height:35) //225
        //btn_agendar.layer.cornerRadius = 15
        btn_agendar.layer.masksToBounds = true
        btn_agendar.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        btn_agendar.setTitleColor(UIColor.white, for: UIControlState.normal)
        btn_agendar.titleLabel!.font =  UIFont(name: "helvetica-bold", size: 12)
        
        btn_ver_detalle.setTitle("Ver Detalle", for: UIControlState.normal)
        btn_ver_detalle.frame = CGRect(x:screenWidth - 100, y:38, width:80, height:35)
        //btn_agendar.layer.cornerRadius = 15
        btn_ver_detalle.layer.masksToBounds = true
        btn_ver_detalle.backgroundColor = UtilidadesColor.colordehexadecimal(hex: 0x000000, alpha: 0.70)
        btn_ver_detalle.setTitleColor(UIColor.white, for: UIControlState.normal)
        btn_ver_detalle.titleLabel!.font =  UIFont(name: "helvetica-bold", size: 12)
        
        
        gradientView.frame = CGRect(x:0.0, y:0.0, width:screenWidth, height:85.0)
        gradientView.backgroundColor = UIColor.lightGray
        gradientView.alpha = 0.3
        self.contentView.addSubview(gradientView)
        
        firstLineView.frame = CGRect(x:0.0, y:0.0, width:screenWidth, height:0.5)
        firstLineView.backgroundColor = UIColor.black
        self.contentView.addSubview(firstLineView)
        
        secondLineView.frame = CGRect(x:0.0, y:24.0, width:screenWidth, height:0.5)
        secondLineView.backgroundColor = UIColor.black
        self.contentView.addSubview(secondLineView)
        
        
        thirdLineView.frame = CGRect(x:0.0, y:85.0, width:screenWidth, height:0.5)
        thirdLineView.backgroundColor = UIColor.black
        self.contentView.addSubview(thirdLineView)
        
        blueArrow.frame = CGRect(x:5.0, y:3.0, width:22.0, height:15.0)
        blueArrow.image = UIImage(named: "flechatitulo_verde.png")
        self.contentView.addSubview(blueArrow)
        
        self.contentView.addSubview(fecha)
        self.contentView.addSubview(nom_medicio)
        self.contentView.addSubview(nom_serv)
        self.contentView.addSubview(btn_agendar)
        self.contentView.addSubview(btn_ver_detalle)
        self.backgroundColor = UIColor.clear
        self.selectionStyle = UITableViewCellSelectionStyle.none;
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
}
