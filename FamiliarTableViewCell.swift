//
//  FamiliarTableViewCell.swift
//  Mi Veris
//
//  Created by Samuel Velez on 15/8/16.
//  Copyright © 2016 Programadores-iOS.net. All rights reserved.
//

import UIKit

class FamiliarTableViewCell: UITableViewCell {

    var title : UILabel = UILabel();
    var subtitle : UILabel = UILabel()
    var imagen : UIImageView = UIImageView()
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.clear
        
        
        
        imagen.frame = CGRect(x: 10, y: 20, width: 30, height: 30)
        //imagen.self.backgroundColor = UIColor.grayColor();
        //imagen.layer.cornerRadius = 30
        //imagen.clipsToBounds = true
        //imagen.image = UIImage(named: "key_gray.png")
        self.contentView.addSubview(imagen)
        
        
        title.frame = CGRect(x: 50, y: 20, width: self.contentView.frame.width, height: 20)
        //title.textAlignment = NSTextAlignment.Center
        title.lineBreakMode = NSLineBreakMode.byWordWrapping
        title.numberOfLines = 0
        title.font = UIFont(name: "helvetica", size: 12)
        title.textColor = UIColor(red: (48/256.0), green: (48/256.0), blue: (48/256.0), alpha: 1)

        subtitle.frame = CGRect(x: 50, y: 30, width: self.contentView.frame.width, height: 40)
        //subtitle.textAlignment = NSTextAlignment.Center
        subtitle.lineBreakMode = NSLineBreakMode.byWordWrapping
        subtitle.numberOfLines = 0
        subtitle.font = UIFont(name: "helvetica", size: 10)
        subtitle.textColor = UIColor(red: (48/256.0), green: (48/256.0), blue: (48/256.0), alpha: 1)
        
        
        self.contentView.addSubview(title)
        self.contentView.addSubview(subtitle)
        //self.contentView.addSubview(arrow)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        //fatalError("init(coder:) has not been implemented")
        super.init(coder: aDecoder)
    }

    
}
