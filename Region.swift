//
//  Region.swift
//  Mi Veris
//
//  Created by Jorge on 04/02/15.
//  Copyright (c) 2015 Firewall Soluciones All rights reserved.
//

import UIKit

    // MARK: Region: Clase región

class Region: NSObject {
    
    var codigoPais : NSInteger = 0
    var codigoProvincia : NSInteger = 0
    var codigoRegion : NSInteger = 0
    var codigoCiudad : NSInteger = 0
    var nombreCiudad : String = ""
    
}
