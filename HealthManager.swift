//
//  HealthManager.swift
//  Mi Veris
//
//  Created by Samuel Velez on 1/8/16.
//  Copyright © 2016 Programadores-iOS.net. All rights reserved.
//

import Foundation
import HealthKit


class HealthManager {
    let healthKitStore:HKHealthStore = HKHealthStore()
    
    //
    //    Sexo Sexo bilogico String
    //    Sangre Tipo de sangre String
    //    Nacimiento Fecha de nacimiento String
    //    Altura La altura del usuario en metros float
    //    Peso Peso del usuario float (kg)
    //    Masa corporal Índice de masa corporal del usuario String
    //    Pasos Contador de pasos String
    //    Distancia caminando /
    //    corriendo
    //    Distancia al caminar o correr desde la última posición *
    //    guardad
    //    String
    //    Energía quemada Activa Energía (calorías) quemadas durante una actividad física String
    //    Energía quemada normal Energía (calorías) quemadas en reposo String
    //    Frecuencia cardiaca Frecuencia cardiaca en latidos por minuto String
    
    
    func authorizeHealthKit(completion: ((_ success:Bool, _ error:NSError?) -> Void)!)
    {
        // 1. Set the types you want to read from HK Store
        let healthKitTypesToRead = Set(arrayLiteral:
            HKObjectType.characteristicType(forIdentifier: HKCharacteristicTypeIdentifier.biologicalSex)!,HKObjectType.characteristicType(forIdentifier: HKCharacteristicTypeIdentifier.bloodType)!,
                                                                                                          HKObjectType.characteristicType(forIdentifier: HKCharacteristicTypeIdentifier.dateOfBirth)!,
                                                                                                          HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.height)!,HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMass)!,HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMassIndex)!,HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)!,HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.distanceWalkingRunning)!,HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.basalEnergyBurned)!,HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.activeEnergyBurned)!,HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!,HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!,
                                                                                                          HKObjectType.workoutType()
        )
        
        // 2. Set the types you want to write to HK Store
        
        //    let healthKitTypesToWrite = Set(arrayLiteral:
        //      HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMassIndex)!,
        //      HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierActiveEnergyBurned)!,
        //      HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDistanceWalkingRunning)!,
        //      HKQuantityType.workoutType()
        //      )
        
        let healthKitTypesToWrite = Set(arrayLiteral: HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMassIndex)!)
        
        
        // 3. If the store is not available (for instance, iPad) return an error and don't go on.
        if !HKHealthStore.isHealthDataAvailable()
        {
            let error = NSError(domain: "", code: 2, userInfo: [NSLocalizedDescriptionKey:"HealthKit is not available in this device"])
            if( completion != nil )
            {
                print("completion false")
                //completion(false, error: error)
                completion?(false, error)
            }
            return;
        }
        
        // 4.  Request HealthKit authorization
        healthKitStore.requestAuthorization(toShare: nil, read: healthKitTypesToRead) { (success, error) -> Void in
            
            if( completion != nil )
            {
                print("hay autorizacion")
                print("completion false")
                
                completion?(success,error as NSError? )
            }
        }
    }
    
    func readProfile() -> ( age:Int?,  biologicalsex:HKBiologicalSexObject?, bloodtype:HKBloodTypeObject?)
    {
        var error:NSError?
        var age:Int?
        
        
        
        do
        {
            let birthDay = try healthKitStore.dateOfBirth()
            let today = NSDate()
            let calendar = NSCalendar.current
            //let differenceComponents = NSCalendar.current.dateComponents(NSCalendar.Unit.year, from: birthDay, to: today)
            //let differenceComponents = NSCalendar.currentCalendar.components(NSCalendar.Unit.Year, fromDate: birthDay, toDate: today, options: NSCalendar.Options(rawValue: 0))
            // let dc = NSCalendar.current.en
            // age = differenceComponents.year
        }
        catch let error as NSError
        {
            print(error.localizedDescription)
        }
        
        
        
        var biologicalSex:HKBiologicalSexObject?
        do
        {
            biologicalSex = try healthKitStore.biologicalSex()
            
        }
        catch let error as NSError
        {
            biologicalSex = nil
            print(error.localizedDescription)
        }
        
        
        var bloodType:HKBloodTypeObject?
        do
        {
            bloodType = try healthKitStore.bloodType()
            
        }
        catch let error as NSError
        {
            bloodType = nil
            print(error.localizedDescription)
        }
        
        // 4. Return the information read in a tuple
        return (age, biologicalSex, bloodType)
    }
    
    func readMostRecentSample(sampleType:HKSampleType , completion: ((HKSample?, NSError?) -> Void)!)
    {
        
        // 1. Build the Predicate
        let past = NSDate.distantPast
        let now   = NSDate()
        //HKQuery.predicateForSamples(withStart: <#T##Date?#>, end: <#T##Date?#>, options: <#T##HKQueryOptions#>)
        let mostRecentPredicate = HKQuery.predicateForSamples(withStart: past, end: now as Date, options: .strictStartDate)
        //let mostRecentPredicate = HKQuery.predicateForSamples(withStart: past, end:now as Date, options: .none)
        
        // 2. Build the sort descriptor to return the samples in descending order
        let sortDescriptor = NSSortDescriptor(key:HKSampleSortIdentifierStartDate, ascending: false)
        // 3. we want to limit the number of samples returned by the query to just 1 (the most recent)
        let limit = 1
        
        // 4. Build samples query
        let sampleQuery = HKSampleQuery(sampleType: sampleType, predicate: mostRecentPredicate, limit: limit, sortDescriptors: [sortDescriptor])
        { (sampleQuery, results, error ) -> Void in
            
            if let queryError = error {
                print("error: \(queryError)")
                //completion(nil,error)
                return;
            }
            
            // Get the first sample
            let mostRecentSample = results!.first as? HKQuantitySample
            
            // Execute the completion closure
            if completion != nil {
                completion(mostRecentSample,nil)
            }
        }
        // 5. Execute the Query
        self.healthKitStore.execute(sampleQuery)
    }
    
    func saveBMISample(bmi:Double, date:NSDate ) {
        
        // 1. Create a BMI Sample
        let bmiType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMassIndex)
        let bmiQuantity = HKQuantity(unit: HKUnit.count(), doubleValue: bmi)
        let bmiSample = HKQuantitySample(type: bmiType!, quantity: bmiQuantity, start: date as Date, end: date as Date)
        
        // 2. Save the sample in the store
        healthKitStore.save(bmiSample, withCompletion: { (success, error) -> Void in
            if( error != nil ) {
                print("Error saving BMI sample: \(error!.localizedDescription)")
            } else {
                print("BMI sample saved successfully!")
            }
        })
    }
    
}

