//
//  TipoParentesco.swift
//  Mi Veris
//
//  Created by Samuel Velez on 17/8/16.
//  Copyright © 2016 Programadores-iOS.net. All rights reserved.
//

import UIKit

class TipoParentesco: NSObject {

    var codigoParentesco : NSString = ""
    var descripcion : NSString = ""
}
