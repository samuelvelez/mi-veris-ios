//
//  DondeEstamosTableViewCell.swift
//  Mi Veris
//
//  Created by Christian Martinez M on 4/2/15.
//  Copyright (c) 2015 Firewall Soluciones All rights reserved.
//

import UIKit

// MARK: Configuraciçon de las celdas Donde Estamos

class DondeEstamosTableViewCell: UITableViewCell {
    
    var imagen : UIImageView = UIImageView();
    var title : UILabel = UILabel();
    var descripcion : UILabel = UILabel()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.clear
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width;
        let arrow = UIImageView(frame: CGRect(x: screenWidth-30, y: 17, width: 15, height: 24))
        arrow.image = UIImage(named: "flecha.png")
        imagen = UIImageView(frame: CGRect(x: 10, y: 10, width: 70, height: 50))
        imagen.backgroundColor = UIColor.gray
        title.frame = CGRect(x: 90, y: 0, width: screenWidth - 130, height: 30)
        title.font = UIFont(name: "helvetica", size: 15)
        title.numberOfLines = 1
        descripcion.frame = CGRect(x: 90, y: 30, width: screenWidth - 130, height: 30)
        descripcion.font = UIFont(name: "helvetica-light", size: 12)
        descripcion.numberOfLines = 0
        
        self.contentView.addSubview(imagen)
        self.contentView.addSubview(title)
        self.contentView.addSubview(descripcion)
        self.contentView.addSubview(arrow)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }


}
