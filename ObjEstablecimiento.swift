//
//  ObjEstablecimiento.swift
//  Mi Veris
//
//  Created by Andres Cantos on 2/6/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import UIKit

class ObjEstablecimiento: NSObject {
    
    var id : NSInteger = 0
    var idempresa : NSInteger = 0
    var descripcion : NSString = ""
    var direccion : NSString = ""
    var telefono : NSString = ""
    var horario : NSString = ""
    var horariolaboratorio : NSString = ""
    var region : NSString = ""
    var descripcion2 : NSString = ""
    var idciudad : NSInteger = 0
    var latitud : NSString = ""
    var longitud : NSString = ""
    var especialidades : NSString = ""
    var complementarios : NSString = ""
    var mailcontacto : NSString = ""
    var telefono2 : NSString = ""
    var codigoregion : NSInteger = 0
    var urlmapa : NSString = ""
    
    init(data : NSDictionary) {
        /*{"id":7,"idempresa":1,"descripcion":"VERIS - GRANADOS","direccion":null,"telefono":null,"horario":null,"horariolaboratorio":null,"region":null,"descripcion2":null,"idciudad":0,"ciudad":null,"latitud":null,"longitud":null,"especialidades":null,"complementarios":null,"mailcontacto":null,"telefono2":null,"codigoregion":0,"urlmapa":null}*/
        
        id = data["id"] as! NSInteger
        idempresa = data["idempresa"] as! NSInteger
        descripcion = data["descripcion"] as! NSString
        if (data["direccion"] is NSNull) {
            direccion = ""
        }else{
            direccion = data["direccion"] as! NSString
        }

        if (data["telefono"] is NSNull) {
            telefono = ""
        }else{
            telefono = data["telefono"] as! NSString
        }
    }
    
    override init(){
        
    }
}