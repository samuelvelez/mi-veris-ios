//
//  ImageLoader.swift
//  Veris
//
//  Created by Jorge on 02/02/15.
//  Copyright (c) 2015 Firewall Soluciones All rights reserved.
//
import UIKit
import Foundation


  // MARK: ImageLoader: Descarga de imágenes de forma asyncrona

class ImageLoader {
    
    var cache = NSCache<AnyObject, AnyObject>()
    
    class var sharedLoader : ImageLoader {
    struct Static {
        static let instance : ImageLoader = ImageLoader()
        }
        return Static.instance
    }
    
    func imageForUrl(urlString: String, completionHandler:@escaping (_ image: UIImage?, _ url: String) -> ()) {
       // dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0).asynchronously(DispatchQueue.globalexecute: {()in
            /*var data: NSData? = self.cache.objectForKey(urlString) as? NSData
            
            if let goodData = data {
                let image = UIImage(data: goodData as Data)
                dispatch_get_main_queue().asynchronously(DispatchQueue.mainexecute: {() in
                    completionHandler(image: image, url: urlString)
                })
                return
            }
            
            var downloadTask: URLSessionDataTask = NSURLSession.sharedSession().dataTaskWithURL(NSURL(string: urlString)!, completionHandler: {(data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
                if (error != nil) {
                    completionHandler(image: nil, url: urlString)
                    return
                }
                
                if data != nil {
                    let image = UIImage(data: data!)
                    self.cache.setObject(data!, forKey: urlString)
                    dispatch_async(dispatch_get_main_queue(), {() in
                        completionHandler(image: image, url: urlString)
                    })
                    return
                }
                
            })*/
       //     downloadTask.resume()
       // })
        
    }
}
