//
//  SeleccionFamViewController.swift
//  Mi Veris
//
//  Created by Samuel Velez on 16/8/16.
//  Copyright © 2016 Programadores-iOS.net. All rights reserved.
//

import UIKit

class SeleccionFamViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var tableView: UITableView  =   UITableView() /*Table view que muestra la lista de especailidades*/
    //var listaespecialidades : NSMutableArray?
    var delegate:SeleccionFamDelegate! = nil
    //var especialidadsel : ObjEspecialidad?
    var ls_familiar : NSMutableArray?
    //var ls_famshow : NSMutableArray = NSMutableArray()
    
    var arreglo : NSMutableArray  = NSMutableArray()
    
    var familiarsel : Familiar?
    var parametros : String!
    override func viewDidLoad() {
        //if let arreglo1 = ls_familiar! as NSMutableArray{
        
        super.viewDidLoad()

         self.navigationController?.navigationItem.backBarButtonItem?.title=""
        self.navigationController?.navigationItem.title = "Resultado de Búsqueda"
        navigationItem.backBarButtonItem?.title = ""
            self.cargardatos()
            
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func cargardatos(){
        var str_param = ""
        if let p = parametros as? String{
            str_param = p
        }else{
            let us = Usuario.getEntity as Usuario
            str_param = "arg0=\(us.tipoid)-\(us.numeroidentificacion)&arg1=&arg2=&arg3=&arg4=&arg5=M&arg6=1&arg7=99&arg8=1&arg9=1&arg10=0&arg11=80"
        }
        let urldef = URLFactory.urldefinida()

        let url = NSURL(string: "\(urldef)servicio/persona/personas?\(str_param)")
    
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        
        let request = NSMutableURLRequest(url: url! as URL)
        let str = "wsappusuario:ZA$57@9b86@$2r5"
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                    var err: NSError?
                    
                    var jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    if (err != nil) {
                        print("JSON Error \(err!.localizedDescription)")
                    }
                    let info =  jsonResult as NSDictionary
                    let data = info["lista"] as! [[String: Any]]
                    if data.count > 0 {
                        self.ls_familiar = NSMutableArray();
                        print(data)
                        for jsonobj  in data {
                            let esp = Familiar()
                            esp.numeroIdentificacion = jsonobj["numeroIdentificacion"] as! NSString
                            esp.tipoIdentificacion = jsonobj["tipoIdentificacion"] as! NSString
                            esp.primerApellido = jsonobj["primerApellido"] as! NSString
                            if let seg = jsonobj["segundoApellido"] as? NSString {
                                esp.segundoApellido = seg
                            }
                            
                            esp.primerNombre = jsonobj["primerNombre"] as! NSString
                            esp.edad = jsonobj["edad"] as! NSString
                            self.ls_familiar?.add(esp)
                        }
                    }else{
                        ViewUtilidades.hiddenLoader(controller: self)
                        self.ls_familiar = NSMutableArray()
                    }
                    DispatchQueue.main.async {
                        self.showFamiliares()
                        ViewUtilidades.hiddenLoader(controller: self)
                    }
                } catch {
                    print("ERROR")
                }
            }
            
        }
        
    }
    
    func showFamiliares(){
        tableView.frame = CGRect(x: 0, y: 80, width: self.view.frame.width, height: self.view.frame.height - 80);
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.backgroundColor = UIColor.clear
        //tableView.registerClass(DondeEstamosTableViewCell.self as AnyClass, forCellReuseIdentifier: "SucursalesCell");
        
        self.view.addSubview(tableView);
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(" el valor del arreglos es: \(ls_familiar!.count)")
        if(ls_familiar!.count<1){
            
            let texto = UILabel(frame: CGRect(x: 30, y: 65 , width:  self.view.frame.width - 60, height: 80))
            texto.text = "Al parecer la persona que buscas aún no tiene cuenta de usuario Mi Veris"
            texto.numberOfLines = 0
            //texto.
            self.view.addSubview(texto)
            let btAceptar = UIButton(frame: CGRect(x: 30, y: 145, width: self.view.frame.width - 60, height: 40))
            btAceptar.setTitle("Crear Usuario", for: UIControlState.normal)
            btAceptar.addTarget(self, action: #selector(SeleccionFamViewController.crearUsuario), for: UIControlEvents.touchUpInside)
            btAceptar.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
            btAceptar.layer.cornerRadius = 20
            self.view.addSubview(btAceptar)
            
        }
        return ls_familiar!.count
    }
    func crearUsuario(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "viewCrearCuenta") as! CreateViewController
        vc.vieneFamilia = "si"
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "") as UITableViewCell!
            ?? UITableViewCell(style: .default, reuseIdentifier: ""),
        fam = ls_familiar!.object(at: indexPath.row) as! Familiar
        var nom_completo : String = "\(fam.primerNombre) \(fam.segundoNombre) \(fam.primerApellido) \(fam.segundoApellido)"
        nom_completo = nom_completo.removingPercentEncoding! //stringByRemovingPercentEncoding!
        nom_completo = nom_completo.replacingOccurrences(of: "+", with: " ", options: String.CompareOptions.literal, range: nil)
        //stringByReplacingOccurrencesOfString("+", withString: " ", options: NSStringCompareOptions.LiteralSearch, range: nil)
        nom_completo = nom_completo.replacingOccurrences(of: "  ", with: " ", options: String.CompareOptions.literal, range: nil)
        
        cell.textLabel?.text = nom_completo
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        familiarsel = ls_familiar!.object(at: indexPath.row) as? Familiar
        print("selecciono: \(familiarsel?.edad)")
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "EditFam") as! EditFamViewController
        
        viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Atrás", style: UIBarButtonItemStyle.plain, target: self, action: "goBack")
        viewController.fam_selected = familiarsel
        // Creating a navigation controller with viewController at the root of the navigation stack.
        let navController = UINavigationController(rootViewController: viewController)
        
        self.present(navController, animated:true, completion: nil)

        //delegate.retornadereservaespecialidad(self)
    }
    
    func goBack(){
        print("Aquiiiio")
        dismiss(animated: false, completion: nil)
        print("jojojo")
        dismiss(animated: false, completion: nil)
        dismiss(animated: false, completion: nil)
        
        dismiss(animated: false, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

protocol SeleccionFamDelegate{
    func retornadeFamiliar(controller:SeleccionFamViewController)
}
