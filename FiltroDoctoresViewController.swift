//
//  FiltroDoctoresViewController.swift
//  Mi Veris
//
//  Created by Jorge on 05/02/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import UIKit

class FiltroDoctoresViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    var viewEspecialidad : UIView = UIView()
    var viewSearch : UIView = UIView()
    var txtBusqueda = UITextField()
    
    var txtEspecialidad : UILabel = UILabel()
    var listaEspecialidades : NSMutableArray = NSMutableArray()
    var selectedEspecialidad : NSInteger = 16
    var isCreated : Bool = false
    
    var pickerData : NSMutableArray = NSMutableArray()
    var fondoPick : UIView?
    
    var tableView : UITableView!
    var listaDoctores : NSMutableArray = NSMutableArray()
    
    var selectedRows : NSMutableArray = NSMutableArray()
    
    @IBOutlet var myPicker: UIPickerView! = UIPickerView()
    
    var resultadoAgregar : NSString = ""
    
    var numberOfAdd : NSInteger = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        let fondo : UIImageView = UIImageView(frame: self.view.bounds)
        fondo.image = UIImage(named:"fondodoctor copia.png")
        self.view.addSubview(fondo)
        
        let barWhite : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width , height:  30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width / 1.8, height: 30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x: barBlue.frame.width, y: 64, width:45, height: 30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x: 0, y: 64, width: self.view.frame.width - 20, height: 30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "Favoritos"
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)
        
        let usuario = Usuario.getEntity as Usuario
        if usuario.logueado {
            let nombreUser : UILabel = UILabel(frame: CGRect(x: 10, y: 94, width: self.view.frame.width - 20, height: 30))
            nombreUser.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
            nombreUser.font = UIFont(name: "helvetica-bold", size: 12)
            nombreUser.text = usuario.nombre as String
            
            self.view.addSubview(nombreUser)
        }
        
        txtBusqueda.resignFirstResponder()
        
        let viewFondo = UIView(frame: CGRect(x: 0, y: 124, width: self.view.frame.width, height: 40))
        viewFondo.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        self.view.addSubview(viewFondo)
        
        viewEspecialidad.frame =  CGRect(x: 0, y: 124, width: self.view.frame.width, height: 40);
        viewEspecialidad.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        viewEspecialidad.clipsToBounds = true
        self.view.addSubview(viewEspecialidad)
        
        txtEspecialidad = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 40))
        txtEspecialidad.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        txtEspecialidad.textColor = UIColor.white
        
        txtEspecialidad.textAlignment = NSTextAlignment.center
        txtEspecialidad.textAlignment = .center
        txtEspecialidad.font = UIFont(name: "helvetica-bold", size: 16)
        txtEspecialidad.isUserInteractionEnabled = true
        viewEspecialidad.addSubview(txtEspecialidad)
        
        let img2 : UIImageView = UIImageView(frame: CGRect(x: self.view.frame.width-30, y: 15, width: 20, height: 12))
        img2.image = UIImage(named: "arrow-down.png")
        
        txtEspecialidad.addSubview(img)
        
        let imgSearchButton = UIImageView(frame: CGRect(x: 13, y: 0, width: 35, height: 35))
        imgSearchButton.image = UIImage(named: "btSearch.png")
        viewEspecialidad.addSubview(imgSearchButton)
        
        let btSearch = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 40))
        btSearch.addTarget(self, action: #selector(FiltroDoctoresViewController.showSearch), for: UIControlEvents.touchDown)
        viewEspecialidad.addSubview(btSearch)
        
        viewSearch.frame =  CGRect(x: self.view.frame.width, y: 124, width: self.view.frame.width, height: 40);
        viewSearch.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        self.viewSearch.alpha = 0
        self.view.addSubview(viewSearch)
        
        let imgBackSearch = UIImageView(frame: CGRect(x: 10, y: 10, width: 20, height: 20))
        imgBackSearch.image = UIImage(named: "btBackSearch.png")
        viewSearch.addSubview(imgBackSearch)
        
        if txtBusqueda.text == "" {
            let btBackSearch = UIButton(frame: CGRect(x: 0, y:0, width: 60, height: 40))
            //btBackSearch.setImage(UIImage(named: "btBackSearch.png"), forState: UIControlState.Normal)
            btBackSearch.addTarget(self, action: #selector(FiltroDoctoresViewController.hideSearch), for: UIControlEvents.touchDown)
            viewSearch.addSubview(btBackSearch)
        }
        
        
        txtBusqueda.frame = CGRect(x: 55, y: 5, width: self.view.frame.width - 110, height: 30)
        txtBusqueda.textColor = UIColor.white
        txtBusqueda.font = UIFont.systemFont(ofSize: 16)
        txtBusqueda.attributedPlaceholder = NSAttributedString(string:"Nombre del Doctor...",
            attributes:[NSForegroundColorAttributeName: UIColor(red: (245.0/255.0), green: (245.0/255.0), blue: (245.0/255.0), alpha: 1)])
        txtBusqueda.delegate = self
        /*[textField addTarget:self
        action:@selector(textFieldDidChange:)
        forControlEvents:UIControlEventEditingChanged];*/
        //txtBusqueda.addTarget(self, action: "getDoctoresSearch", forControlEvents: UIControlEvents.EditingChanged)
        viewSearch.addSubview(txtBusqueda)
        
        let imgOkSearch = UIImageView(frame: CGRect(x: self.view.frame.width - 40, y: 0, width: 35, height: 35))
        imgOkSearch.image = UIImage(named: "btOkSearchFilter.png" )
        viewSearch.addSubview(imgOkSearch)
        
        let btOkSeach = UIButton(frame: CGRect(x: self.view.frame.width - 60, y: 0, width: 60, height: 40))
        //btOkSeach.setImage(UIImage(named: "btOkSearch.png"), forState: UIControlState.Normal)
        btOkSeach.addTarget(self, action: #selector(FiltroDoctoresViewController.getDoctoresSearch), for: UIControlEvents.touchDown)
        viewSearch.addSubview(btOkSeach)
        
        if txtBusqueda.text != "" {
            showDoctores()
            showSearch()
            getDoctoresSearch()
        } else {
            showEspecialidad()
        }
        
        
    }
    
    func showEspecialidad() {
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        let urldef = URLFactory.urldefinida()

        let url = NSURL(string: "\(urldef)servicio/v2/especialidad/obtenerespecialidades3?arg0=0")
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                var err: NSError?
                
                //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                if (err != nil) {
                    print("JSON Error \(err!.localizedDescription)")
                }
                
                
                //println(jsonResult);
                print(jsonResult)
                let info =  jsonResult as NSDictionary
                let data = info["lista"] as! [[String: Any]]
                if data.count > 0 {
                    for especialidad in data {
                        let objeto = Especialidad()
                        objeto.codigo = especialidad["codigo"] as! NSInteger
                        objeto.descripcion = especialidad["descripcion"] as! String
                        self.pickerData.add(objeto.descripcion)
                        self.listaEspecialidades.add(objeto)
                    }
                }
                
                //dispatch_async(dispatch_get_main_queue(), {
                    DispatchQueue.main.async(execute: {
                    self.showEspecialidades()
                })
                } catch {
                    print("showEspecialidad")
                    print("ERROR")
                }
            }
            
            
        }
    }
    
    func showSearch(){
        txtBusqueda.resignFirstResponder()
        let transitionOptions = UIViewAnimationOptions.showHideTransitionViews//transitionNone
        
        UIView.transition(with: self.view, duration: 0.4, options: transitionOptions, animations: {
            self.viewEspecialidad.frame = CGRect(x:  self.view.frame.width, y: 124, width: 0, height: 40)
            self.viewEspecialidad.alpha = 0
            self.viewSearch.frame = CGRect( x: 0, y: 124, width: self.view.frame.width, height: 40)
            self.viewSearch.alpha = 1
        }, completion: { finished in
                
        })
    }
    
    func hideSearch(){
        txtBusqueda.resignFirstResponder()
        let transitionOptions = UIViewAnimationOptions.showHideTransitionViews//TransitionNone
        
        UIView.transition(with: self.view, duration: 0.4, options: transitionOptions, animations: {
            self.viewSearch.frame = CGRect( x:self.view.frame.width, y: 124, width:0, height:40)
            self.viewSearch.alpha = 0
            self.viewEspecialidad.frame = CGRect( x: 0, y:124, width:self.view.frame.width, height:40)
            self.viewEspecialidad.alpha = 1
            }, completion: { finished in
                
        })
    }
    
    func acceptSearch(){
        txtBusqueda.resignFirstResponder()
    }
    
    func showEspecialidades(){
        
        let first = listaEspecialidades.object(at: 16) as! Especialidad
        txtEspecialidad.text = first.descripcion
        //ViewUtilidades.showLoader(self, title: "Cargando...")
        let urldef = URLFactory.urldefinida()

        let url = NSURL(string: "\(urldef)servicio/especialidad/obtenerdoctoresxespecialidad?arg0=\(first.codigo)")
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                var err: NSError?
                
                //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                if (err != nil) {
                    print("JSON Error \(err!.localizedDescription)")
                }
                
                
                //println(jsonResult);
                print(jsonResult)
                let info =  jsonResult as NSDictionary
                let data = info["lista"] as! [[String: Any]]
                if data.count > 0 {
                    for doctor in data {
                        let doc = Doctor()
                        doc.idTipoID = doctor["id_tipodeidentificacion"] as! NSInteger
                        doc.idDoctor = doctor["id_doctor"] as! NSInteger
                        doc.idEspecialidad = doctor["id_especialidad"] as! NSInteger
                        doc.nombre = doctor["nombredoctor"] as! String
                        doc.identificacion = doctor["identificacion"] as! String
                        doc.nombreEspecialidad = first.descripcion
                        //doctor["nombreespecialidad"] as String
                        doc.primerApellido = doctor["primerapellido"] as! String
                        doc.segundoApellido = doctor["segundoapellido"] as! String
                        doc.segundoNombre = doctor["segundonombre"] as! String
                        doc.secuenciaPersonal = doctor["secuenciapersonal"] as! String
                        self.listaDoctores.add(doc)
                    }
                }
                
                //dispatch_async(dispatch_get_main_queue(), {
                    DispatchQueue.main.async(execute: {
                    self.showDoctores()
                    ViewUtilidades.hiddenLoader(controller: self)
                })
                } catch {
                    print("showespecialidades")
                    print("ERROR")
                }
            }
            
            
        }
        //////////////////////////
        
        
    }
    
    
    func getDoctoresSearch(){
        
        txtBusqueda.resignFirstResponder()
        
        if txtBusqueda.text == "" {
            UtilidadesGeneral.mensaje(mensaje: "No deje el campo de búsqueda vacío")
        } else {
            ViewUtilidades.showLoader(controller: self, title: "Cargando...")
            let busqueda = txtBusqueda.text!.addingPercentEscapes(using: String.Encoding.utf8)
            print(busqueda!)
            //stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
            let urldef = URLFactory.urldefinida()

            let url = NSURL(string: "\(urldef)servicio/v2/doctor/obtenerdoctoresxnombre?arg0=\(busqueda!)")
            //let request = NSURLRequest(URL: url!)
            let request = NSMutableURLRequest(url: url! as URL)
            
            
            
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                
                if (error != nil) {
                    print(error!.localizedDescription)
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    do{
                    var err: NSError?
                    
                    //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as!NSDictionary
                    if (err != nil) {
                        print("JSON Error \(err!.localizedDescription)")
                    }
                    
                    
                    //println(jsonResult);
                    print(jsonResult)
                    let info =  jsonResult as NSDictionary
                    let data = info["lista"] as! [[String: Any]]
                    if data.count > 0 {
                        self.listaDoctores.removeAllObjects()
                        self.selectedRows.removeAllObjects()
                        for doctor in data {
                            let doc = Doctor()
                            doc.idTipoID = doctor["id_tipodeidentificacion"] as! NSInteger
                            doc.idDoctor = doctor["id_doctor"] as! NSInteger
                            doc.idEspecialidad = doctor["id_especialidad"] as! NSInteger
                            doc.nombre = doctor["nombredoctor"] as! String
                            doc.identificacion = doctor["identificacion"] as! String
                            doc.nombreEspecialidad = doctor["nombreespecialidad"] as! String
                            doc.primerApellido = doctor["primerapellido"] as! String
                            doc.segundoApellido = doctor["segundoapellido"] as! String
                            doc.segundoNombre = doctor["segundonombre"] as! String
                            doc.secuenciaPersonal = doctor["secuenciapersonal"] as! String
                            self.listaDoctores.add(doc)
                        }
                    }
                    
                    //dispatch_async(dispatch_get_main_queue(), {
                        DispatchQueue.main.async(execute: {
                        ViewUtilidades.hiddenLoader(controller: self)
                        self.tableView.reloadData()
                        //[tableView setContentOffset:CGPointZero animated:YES];
                        self.tableView.setContentOffset(CGPoint.zero, animated: true)
                        
                    })
                    } catch {
                        print("getdoctoressearch")
                        print("ERROR")
                    }
                }
                
                
            }
            //////////////////////////
            
            
        }
    }
    
    func showDoctores(){
        tableView = UITableView()
        tableView.frame = CGRect(x: 0, y:164, width:self.view.frame.width, height:self.view.frame.height - 164 - 60);
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.backgroundColor = UIColor.clear
        tableView.register(DoctorFiltroTableViewCell.self as AnyClass, forCellReuseIdentifier: "cell");
        tableView.backgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 0.6)
        
        self.view.addSubview(tableView);
        
        let btAdd = UIButton(type: UIButtonType.system)
        btAdd.frame = CGRect( x: 50, y: self.view.frame.height - 50, width:self.view.frame.width - 100, height: 40)
        btAdd.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        btAdd.setTitle("AGREGAR", for: UIControlState.normal)
        btAdd.layer.cornerRadius = 20
        btAdd.setTitleColor(UIColor.white, for: UIControlState.normal)
        //btAdd.titleLabel!.font =  UIFont(name: "helvetica", size: 10)
        btAdd.addTarget(self, action: #selector(FiltroDoctoresViewController.agregarDoctores), for: UIControlEvents.touchUpInside)
        self.view.addSubview(btAdd)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(FiltroDoctoresViewController.showPicker))
        txtEspecialidad.addGestureRecognizer(tapGesture)
        
        if txtBusqueda.text == "" {
            self.showPicker()
        }
        
    }
    
    func stringToUTF8String (stringaDaConvertire stringa: String) -> String {
        
        let encodedData = stringa.data(using: String.Encoding.utf8)!
        let attributedOptions = [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType]
        let attributedString = try! NSAttributedString(data: encodedData, options: attributedOptions, documentAttributes: nil)
        //println(attributedString.string)
        return attributedString.string
    }
    
    func agregarDoctores(){
        if selectedRows.count <= 0 {
            let alert = UIAlertController(title: "Veris", message: "No ha seleccionado a ningún Médico", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            self.addDoc()
        }
    }
    
    func addDoc(){
        if numberOfAdd == 0 {
            ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        }
        self.navigationItem.hidesBackButton = true
        let posicion : NSInteger = selectedRows.object(at: numberOfAdd) as! NSInteger
        numberOfAdd += 1
        let doctor = listaDoctores.object(at: posicion) as! Doctor
        let especialidad = doctor.idEspecialidad
        let nombre = doctor.nombre.addingPercentEscapes(using: String.Encoding.utf8)
        //stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
        let segundoNombre = doctor.segundoNombre.addingPercentEscapes(using: String.Encoding.utf8)
        let primerApellido = doctor.primerApellido.addingPercentEscapes(using: String.Encoding.utf8)
        let segundoApellido = doctor.segundoApellido.addingPercentEscapes(using: String.Encoding.utf8)
        let descripcion = doctor.nombreEspecialidad.addingPercentEscapes(using: String.Encoding.utf8)
        let usuario = Usuario.getEntity as Usuario
        
        
        let urldef = URLFactory.urldefinida()

        let url = NSURL(string: "\(urldef)servicio/especialidad/insertaradoctoresfavoritos?arg0=\(usuario.tipoid)&arg1=\(usuario.numeroidentificacion)&arg2=\(doctor.idDoctor)&arg3=\(especialidad)&arg4=\(nombre!.utf8)&arg5=\(segundoNombre!.utf8)&arg6=\(primerApellido!.utf8)&arg7=\(segundoApellido!.utf8)&arg8=\(descripcion!.utf8)&arg9=\(doctor.secuenciaPersonal)")
        //let request = NSURLRequest(URL: url!)
        
        
        let request = NSMutableURLRequest(url: url! as URL)
        print(request)
        
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                var err: NSError?
                
                //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                if (err != nil) {
                    print("JSON Error \(err!.localizedDescription)")
                }
                
                print(jsonResult)
                let info =  jsonResult as NSDictionary
                self.resultadoAgregar = info["resultado"] as! NSString
                
                //dispatch_async(dispatch_get_main_queue(), {
                    DispatchQueue.main.async(execute: {
                    
                    if self.numberOfAdd >= self.selectedRows.count {
                        
                        ViewUtilidades.hiddenLoader(controller: self)
                        
                        if self.resultadoAgregar.isEqual(to: "ok") {
                            
                            let alert = UIAlertController(title: "Veris", message: "Se ha agregado el Doctor a sus Doctores Favoritos", preferredStyle: UIAlertControllerStyle.alert)
                            
                            let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                                UIAlertAction in
                                
                                let viewFavoritos = self.navigationController?.viewControllers[1] as! FavoritosViewController
                                viewFavoritos.setReload = true
                                self.navigationController?.popToViewController(viewFavoritos, animated: true)
                                
                            }
                            alert.addAction(okAction)
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        } else {
                            let alert = UIAlertController(title: "Veris", message: self.resultadoAgregar as String, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            self.navigationItem.hidesBackButton = false
                        }
                        
                        self.numberOfAdd = 0
                    } else {
                        self.addDoc()
                    }
                })
                } catch {
                    print("adddoc")
                    print("ERROR")
                }
            }
            
            
        }
        //////////////////////////
       
    }
    
    func showPicker(){
        
        if !isCreated {
            
            isCreated = true
            fondoPick = UIView(frame: CGRect(x:0, y:self.view.frame.height, width:self.view.frame.width, height:230))
            fondoPick?.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
            fondoPick?.tag = 3000
            self.view.addSubview(fondoPick!)
            
            let btCancel:UIButton = UIButton(frame: CGRect( x:5 , y:0, width:70, height:40))
            btCancel.backgroundColor = UIColor.clear
            btCancel.setTitle("Cancelar", for: UIControlState.normal)
            btCancel.addTarget(self, action: #selector(FiltroDoctoresViewController.cancelPicker), for: UIControlEvents.touchUpInside)
            btCancel.titleLabel!.font = UIFont(name: "helvetica-bold", size: 15)
            btCancel.titleLabel?.textColor = UIColor.white
            btCancel.tag = 22;
            
            fondoPick?.addSubview(btCancel)
            
            let btAceptar:UIButton = UIButton(frame: CGRect( x: self.view.frame.width - 75 , y: 0, width: 70, height: 40))
            btAceptar.backgroundColor = UIColor.clear
            btAceptar.setTitle("Aceptar", for: UIControlState.normal)
            btAceptar.addTarget(self, action: "acceptPicker", for: UIControlEvents.touchUpInside)
            btAceptar.titleLabel!.font = UIFont(name: "helvetica-bold", size: 15)
            btAceptar.titleLabel?.textColor = UIColor.white
            btAceptar.tag = 22;
            
            fondoPick?.addSubview(btAceptar)
            
            //myPicker = UIPickerView()
            myPicker.dataSource = self
            myPicker.delegate = self
            myPicker.selectRow(selectedEspecialidad, inComponent: 0, animated: true)
            myPicker.frame = CGRect(x: 0, y: 40, width: self.view.frame.width,height: 300)
            myPicker.backgroundColor = UIColor.white
            fondoPick!.addSubview(myPicker)
        }
        let transitionOptions = UIViewAnimationOptions.showHideTransitionViews//TransitionNone
        
        let back = UIView(frame: CGRect(x: 0, y: 0, width:self.view.frame.width, height: self.view.frame.height - 230))
        back.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
        back.alpha = 0
        back.tag = 3001
        self.view.addSubview(back)
        
        UIView.transition(with: self.view, duration: 0.4, options: transitionOptions, animations: {
            back.alpha = 1
            let view : UIView = self.view.viewWithTag(3000)!
            view.frame = CGRect(x: 0 , y: self.view.frame.height - 230, width:self.view.frame.width, height: 230)
            }, completion: { finished in
        })
    }
    
    func cancelPicker(){
        let transitionOptions = UIViewAnimationOptions.showHideTransitionViews//TransitionNone
        
        UIView.transition(with: self.view, duration: 0.3, options: transitionOptions, animations: {
            let back : UIView = self.view.viewWithTag(3001)!
            back.alpha = 0
            let view : UIView = self.view.viewWithTag(3000)!
            view.frame = CGRect(x: 0 , y: self.view.frame.height, width: self.view.frame.width, height: 230)
            }, completion: { finished in
                let back : UIView = self.view.viewWithTag(3001)!
                back.removeFromSuperview()
        })
    }
    
    func acceptPicker(){
        let transitionOptions = UIViewAnimationOptions.showHideTransitionViews//TransitionNone
        txtEspecialidad.text = pickerData[selectedEspecialidad] as? String
        
        UIView.transition(with: self.view, duration: 0.3, options: transitionOptions, animations: {
            let back : UIView = self.view.viewWithTag(3001)!
            back.alpha = 0
            let view : UIView = self.view.viewWithTag(3000)!
            view.frame = CGRect(x: 0 , y: self.view.frame.height, width: self.view.frame.width, height: 230)
            }, completion: { finished in
                let back : UIView = self.view.viewWithTag(3001)!
                back.removeFromSuperview()
        })
        
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        
        let especialidad = listaEspecialidades.object(at: selectedEspecialidad) as! Especialidad
        
        let urldef = URLFactory.urldefinida()

        let url = NSURL(string: "\(urldef)servicio/especialidad/obtenerdoctoresxespecialidad?arg0=\(especialidad.codigo)")
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
       
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
           
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                var err: NSError?
                
                //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                if (err != nil) {
                    print("JSON Error \(err!.localizedDescription)")
                }
                
                //println(jsonResult);
                print(jsonResult)
                let info =  jsonResult as NSDictionary
                let data = info["lista"] as! [[String: Any]]
                if data.count > 0 {
                    self.listaDoctores.removeAllObjects()
                    self.selectedRows.removeAllObjects()
                    for doctor in data {
                        let doc = Doctor()
                        doc.idTipoID = doctor["id_tipodeidentificacion"] as! NSInteger
                        doc.idDoctor = doctor["id_doctor"] as! NSInteger
                        /*doc.idEspecialidad = doctor["id_especialidad"] as NSInteger*/
                        doc.idEspecialidad = especialidad.codigo
                        doc.nombre = doctor["nombredoctor"] as! String
                        doc.identificacion = doctor["identificacion"] as! String
                        //doc.nombreEspecialidad =  self.txtEspecialidad.text!
                        doc.nombreEspecialidad = doctor["nombreespecialidad"] as! String
                        if doc.nombreEspecialidad == "" {
                            doc.nombreEspecialidad =  self.txtEspecialidad.text!
                        }
                        doc.primerApellido = doctor["primerapellido"] as! String
                        doc.segundoApellido = doctor["segundoapellido"] as! String
                        doc.segundoNombre = doctor["segundonombre"] as! String
                        doc.secuenciaPersonal = doctor["secuenciapersonal"] as! String
                        self.listaDoctores.add(doc)
                    }
                }
                
                
                //dispatch_async(dispatch_get_main_queue(), {
                DispatchQueue.main.async(execute: {
                    self.hideLoader()
                    self.tableView.reloadData()
                    //[tableView setContentOffset:CGPointZero animated:YES];
                    self.tableView.setContentOffset(CGPoint.zero, animated: true)
                    
                })
                } catch {
                    print("acceptpicker")
                    print("ERROR")
                }
            }
            
            
        }
        //////////////////////////
        
    }
    
    func hideLoader(){
        ViewUtilidades.hiddenLoader(controller: self)
    }
    
    /*func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }*/
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row] as? String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //let transitionOptions = UIViewAnimationOptions.showHideTransitionViews
        selectedEspecialidad = row
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDoctores.count
    }
    /*func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDoctores.count;
    }*/
    
    /*func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.separatorInset = UIEdgeInsetsZero
        cell.layoutMargins = UIEdgeInsetsZero
    }*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DoctorFiltroTableViewCell
        
        let doctor = listaDoctores.object(at: indexPath.row) as! Doctor
        
        cell.imagen.image = UIImage(named: "medic.png")
        cell.imagen.tag = indexPath.row
       
        let directorio = "/compartidos/portal/medicos/\(doctor.secuenciaPersonal).JPG"
        
        let urldef = URLFactory.urldefinida()

        let urls = URL(string: "\(urldef)servicio/login/imagen?arg0=\(directorio)")
        getDataFromUrl(url: urls!) { (data, response, error)  in
            DispatchQueue.main.sync() { () -> Void in
                print("entro")
                guard let data = data, error == nil else { print(error);return }
                print(response?.suggestedFilename ?? urls?.lastPathComponent)
                print("Download Finished")
                //cell.imagen.image = UIImage(data: data)
                //cell.imagen.image = UIImage(data: data)
                cell.imagen.image  = UIImage(named: "medic.png")
                let varImg : UIImage?
                varImg = UIImage(data: data)
                print("tamaño es: \(varImg?.size)")
                if varImg?.size != nil {
                    print("image no es nil")
                    cell.imagen.image = UIImage(data: data)
                    //self.view.addSubview(self.imagen)
                } else{
                    print("image es nil")
                    cell.imagen.image  = UIImage(named: "medic.png")
                    //self.view.addSubview(self.imagen)
                }
                print("se ejecuto")
            }
        }
        
        cell.nombre.text = "\(doctor.primerApellido) \(doctor.segundoApellido) \(doctor.nombre) \(doctor.segundoNombre)"
        
        cell.nombre.sizeToFit();
        cell.nombre.frame = CGRect(x: 80, y:5, width: self.view.frame.width - 130 , height: cell.nombre.frame.height)
        
        cell.especialidad.text = "\(doctor.nombreEspecialidad)"
        
        //var keyString = "\(indexPath.row)"

        let posicion : Int = selectedRows.index(of: indexPath.row)
        if (posicion != NSNotFound) {
            cell.accessoryType = UITableViewCellAccessoryType.checkmark
        }
        
        /*Enlace con hoja vida*/
        cell.verhoja.tag = indexPath.row
        cell.verhoja.addTarget(self, action: #selector(accionverhoja(sender:)), for: UIControlEvents.touchUpInside)
        return cell
    }
    
    /*Funcion para enlazar con hoja de vida*/
    func accionverhoja(sender : UIButton){
        let doctor = listaDoctores.object(at: sender.tag) as! Doctor
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "viewHojaVida") as! HojaVidaViewController
        
        vc.codigomedico = doctor.idDoctor
        vc.nombremedico = "Dr(a) \(doctor.primerApellido) \(doctor.segundoApellido) \(doctor.nombre) \(doctor.segundoNombre)"
        vc.secuenciamedico = doctor.secuenciaPersonal
        vc.especialidadmedico = doctor.idEspecialidad
        vc.nombreespecialidadmedico = doctor.nombreEspecialidad
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView:UITableView, heightForRowAt indexPath:IndexPath)->CGFloat {
        return 80
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell : DoctorFiltroTableViewCell = tableView.cellForRow(at: indexPath)! as! DoctorFiltroTableViewCell
        //NSString *keyString = [NSString stringWithFormat:@"%d", indexPath.row];
        //var keyString = "\(indexPath.row)"
        if ( cell.accessoryType == UITableViewCellAccessoryType.none ) {
            cell.accessoryType = UITableViewCellAccessoryType.checkmark
           // var peerID = cell.nombre.text
            selectedRows.add(indexPath.row)
        } else {
            cell.accessoryType = UITableViewCellAccessoryType.none;
            selectedRows.remove(indexPath.row)
            // And remove the peerID from the array of selections
            //[selectedRows removeObjectForKey:keyString];
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.view.endEditing(true)
        getDoctoresSearch()
        return false
    }

    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
}
