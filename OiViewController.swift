//
//  OiViewController.swift
//  Mi Veris
//
//  Created by Samuel Velez on 14/12/16.
//  Copyright © 2016 Samuel Velez. All rights reserved.
//

import UIKit
import HealthKit

class OiViewController: UIViewController {

    var scroll : UIScrollView = UIScrollView()
    var txtPeso : UITextField!
    var txtAltura : UITextField!
    
    var objeto : ObjetoDatosHealthKit?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //let objeto : ObjetoDatosHealthKit = ObjetoDatosHealthKit.getDatos().object(at: 0) as! ObjetoDatosHealthKit
        
        
        //print(objeto.altura)

        let singleTap = UITapGestureRecognizer(target: self, action: #selector(NewLoginViewController.hideKeyboard))
        scroll.addGestureRecognizer(singleTap)
        
        scroll.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        scroll.contentSize = CGSize(width: 0, height: 568) // 0, 568)
        scroll.backgroundColor = UIColor.white
        self.view.addSubview(scroll)
        
        self.navigationItem.title = ""
        
        let barWhite : UIView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width ,height:  30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width / 1.6, height: 30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x: barBlue.frame.width, y: 0, width: 45, height:30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 20, height: 30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "Mis Datos"
        
        scroll.addSubview(barWhite)
        scroll.addSubview(barBlue)
        scroll.addSubview(img)
        scroll.addSubview(title)
        
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo

        
        let lbtxtpeso = UILabel(frame: CGRect(x: 30, y: 64, width: self.view.frame.width - 40, height: 20))
        lbtxtpeso.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        lbtxtpeso.text = "Peso"
        scroll.addSubview(lbtxtpeso)
        
        txtPeso = UITextField(frame: CGRect(x: 10, y: 90, width: self.view.frame.width - 80, height: 40))
        txtPeso.layer.cornerRadius = 20
        txtPeso.textAlignment = NSTextAlignment.center
        //txtPeso.delegate = self
        //txtPeso.text = objeto.peso
        txtPeso.layer.borderColor = UIColor.lightGray.cgColor
        txtPeso.layer.borderWidth = 1
        txtPeso.autocorrectionType = UITextAutocorrectionType.no
        //txtPeso.addTarget(self, action: #selector(NewLoginViewController.upView), for: UIControlEvents.editingDidBegin)
        //txtPeso.addTarget(self, action: #selector(NewLoginViewController.downView), for: UIControlEvents.editingDidEndOnExit)
        scroll.addSubview(txtPeso)

        let lbkg = UILabel(frame: CGRect(x: self.view.frame.width - 60, y: 100, width: 50, height: 20))
        lbkg.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        lbkg.text = "kg"
        scroll.addSubview(lbkg)
        
        let lbtxtaltura = UILabel(frame: CGRect(x: 30, y: 144, width: self.view.frame.width - 40, height: 20))
        lbtxtaltura.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        lbtxtaltura.text = "Altura"
        scroll.addSubview(lbtxtaltura)
        //let altcm = (objeto.altura as NSString).doubleValue * 100
        //let aaltcm = "\(altcm)"
        //let saaltcm = (aaltcm as NSString).integerValue
        txtAltura = UITextField(frame: CGRect(x: 10, y: 170, width: self.view.frame.width - 80, height: 40))
        txtAltura.layer.cornerRadius = 20
        txtAltura.textAlignment = NSTextAlignment.center
        //txtPeso.delegate = self
        //txtAltura.text = "\(saaltcm)"
        txtAltura.layer.borderColor = UIColor.lightGray.cgColor
        txtAltura.layer.borderWidth = 1
        txtAltura.autocorrectionType = UITextAutocorrectionType.no
        //txtPeso.addTarget(self, action: #selector(NewLoginViewController.upView), for: UIControlEvents.editingDidBegin)
        //txtPeso.addTarget(self, action: #selector(NewLoginViewController.downView), for: UIControlEvents.editingDidEndOnExit)
        scroll.addSubview(txtAltura)
        
        let lbcm = UILabel(frame: CGRect(x: self.view.frame.width - 60, y: 180, width: 50, height: 20))
        lbcm.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        lbcm.text = "cm"
        scroll.addSubview(lbcm)

        
        let btAceptar = UIButton(frame: CGRect( x: (self.view.frame.width / 2) - 85 , y: 220,  width: 170, height: 50))
        btAceptar.setTitle("Confirmar datos", for: UIControlState.normal)
        btAceptar.addTarget(self, action: #selector(OiViewController.confirmar), for: UIControlEvents.touchUpInside)
        btAceptar.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        btAceptar.layer.cornerRadius = 25
        scroll.addSubview(btAceptar)

        //objeto = ObjetoDatosHealthKit( sexo: "", sangre: "", fechaDeNacimiento: "", altura: "", peso: "", imc: "", contadorDePasos: "", distanciaRecorrida: "", distanciaPosición: "", energiaQuemadaReposo: "", energiaQuemadaActiva: "", frecuenciaCardiaca: "")
        //ObjetoDatosHealthKit.getDatos().add(objeto!)
                // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func hideKeyboard(){
        self.txtPeso.resignFirstResponder()
        //self.txtPass.resignFirstResponder()
       // self.downView()
    }
    func confirmar(){
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        self.confirmarAltura()
        

        
    }
    func confirmarAltura(){
        objeto?.peso = txtPeso.text!
        let us = Usuario.getEntity as Usuario
        let urldef = URLFactory.urldefinida()

        let cadenaUrl = "http://miveris.com.ec/WebServiceFit/servicio/indicadores/ingresarmuestra?arg0=\(us.nombre)&arg1=\(us.numeroidentificacion)&arg2=IOS&arg3=POI&arg4=\(txtAltura.text!)&arg5=1"
        let url = NSURL(string: cadenaUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!  )
        
        let request = NSMutableURLRequest(url:  url! as URL)
        
        let str = "wsappusuario:ZA$57@9b86@$2r5" //AUTENTICACION
        
        //UIApplication.shared.isNetworkActivityIndicatorVisible = true
        request.httpMethod = "POST"
        
        let utf8str = str.data(using: String.Encoding.utf8)
        
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0)) {
            print("Encoded:  \(base64Encoded)")
            request.addValue("\(base64Encoded)", forHTTPHeaderField: "Authorization")
            
            if let base64Decoded = NSData(base64Encoded: base64Encoded, options:   NSData.Base64DecodingOptions(rawValue: 0))
                .map({ NSString(data: $0 as Data, encoding: String.Encoding.utf8.rawValue) })
            {
                // Convert back to a string
                print("Decoded:  \(base64Decoded)")
            }
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
        
            if error != nil {
                
                //dispatch_async(dispatch_get_main_queue(), {
                DispatchQueue.main.async(execute: {
                    //self.controldecarga.hideActivityIndicator(self.view)
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    
                })
                //self.refreshControl?.endRefreshing()
                print("error=\(error)")
                
                return
            }
            print("response = \(response)")
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(responseString)")
            
            let nsdata:NSData = NSData(data: data!)
            let jsonCompleto : AnyObject! = try? JSONSerialization.jsonObject(with: nsdata as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject!
            
            
            if jsonCompleto != nil
            {
                if let items = jsonCompleto["resultado"] as? NSArray
                {
                    //                    for item in items
                    //                    {
                    //                        if let address = item["res"] as? String {
                    //                            //apellidos =  item["apellidos"] as! String
                    //                        }
                    //
                    //                    }
                }
                
                
            }
            //dispatch_async(dispatch_get_main_queue(), {
            DispatchQueue.main.async(execute: {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                //self.controldecarga.hideActivityIndicator(self.view)
                self.confirmarPeso()
                            })
        }
        task.resume()
    }

    func confirmarPeso(){
        print("peso: \(txtPeso.text!) altura: \(txtAltura.text!)")
        objeto?.peso = txtPeso.text!
        let us = Usuario.getEntity as Usuario
        
        let cadenaUrl = "http://miveris.com.ec/WebServiceFit/servicio/indicadores/ingresarmuestra?arg0=\(us.nombre)&arg1=\(us.numeroidentificacion)&arg2=IOS&arg3=POI&arg4=\(txtPeso.text!)&arg5=2"
        let url = NSURL(string: cadenaUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!  )
        
        let request = NSMutableURLRequest(url: url! as URL)
        
        let str = "wsappusuario:ZA$57@9b86@$2r5" //AUTENTICACION
        
        //UIApplication.shared.isNetworkActivityIndicatorVisible = true
        request.httpMethod = "POST"
        
        let utf8str = str.data(using: String.Encoding.utf8)
        
        //let base64Encoded = str.data(using: String.Encoding.utf8, allowLossyConversion: true)
        
        //if let base64Encoded = utf8str?.base64EncodedStringWithOptions(NSData.Base64EncodingOptions(rawValue: 0)) {
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0)) {
            print("Encoded:  \(base64Encoded)")
            request.addValue("\(base64Encoded)", forHTTPHeaderField: "Authorization")
            
            //request.addValue("Authorization", forHTTPHeaderField: "Basic \(base64Encoded)")
            //////////////////request.setValue("Basic \(base64Encoded)", forHTTPHeaderField: "Authorization")
            if let base64Decoded = NSData(base64Encoded: base64Encoded, options:   NSData.Base64DecodingOptions(rawValue: 0))
                .map({ NSString(data: $0 as Data, encoding: String.Encoding.utf8.rawValue) })
            {
                // Convert back to a string
                print("Decoded:  \(base64Decoded)")
            }
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                
                //dispatch_async(dispatch_get_main_queue(), {
                DispatchQueue.main.async(execute: {
                    //self.controldecarga.hideActivityIndicator(self.view)
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    
                })
                //self.refreshControl?.endRefreshing()
                print("error=\(error)")
                
                return
            }
            print("response = \(response)")
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(responseString)")
            
            let nsdata:NSData = NSData(data: data!)
            let jsonCompleto : AnyObject! = try? JSONSerialization.jsonObject(with: nsdata as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject!
            
            
            if jsonCompleto != nil
            {
                if let items = jsonCompleto["resultado"] as? NSArray
                {
                    //                    for item in items
                    //                    {
                    //                        if let address = item["res"] as? String {
                    //                            //apellidos =  item["apellidos"] as! String
                    //                        }
                    //
                    //                    }
                }
                
                
            }
            //dispatch_async(dispatch_get_main_queue(), {
            DispatchQueue.main.async(execute: {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                //self.controldecarga.hideActivityIndicator(self.view)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: "Datos enviados correctamente", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)

            })
        }
        task.resume()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
