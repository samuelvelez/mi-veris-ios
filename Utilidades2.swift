//
//  Utilidades2.swift
//  Mi Veris
//
//  Created by Andres Cantos on 2/5/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import UIKit

class UtilidadesColor : NSObject{
    class func colordehexadecimal (hex: Int, alpha: Double = 1.0) -> UIColor {
        let red = Double((hex & 0xFF0000) >> 16) / 255.0
        let green = Double((hex & 0xFF00) >> 8) / 255.0
        let blue = Double((hex & 0xFF)) / 255.0
        let color: UIColor = UIColor( red : CGFloat(red), green : CGFloat(green), blue: CGFloat(blue), alpha : CGFloat(alpha) )
        return color
    }
}

class UtilidadesGeneral : NSObject{
    class func mensaje(mensaje : String){
        let alert: UIAlertView = UIAlertView()
        alert.title = "Mi Veris"
        alert.message = mensaje
        alert.addButton(withTitle: "Ok")
        alert.show()
    }
}

enum TipoCargaReserva{
    case nueva
    case doctorfavorito
    case reutilizarcita
    case reagendar
    case recomendacion
}
