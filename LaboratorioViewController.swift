//
//  CompleteViewController.swift
//  Mi Veris
//
//  Created by Samuel Velez on 27/4/16.
//  Copyright © 2016 Programadores-iOS.net. All rights reserved.
//

import UIKit
import CoreLocation

class LaboratorioViewController: UIViewController, UITextFieldDelegate,UINavigationControllerDelegate, CLLocationManagerDelegate, UIImagePickerControllerDelegate{
    
    var scroll : UIScrollView = UIScrollView()
    var sgm_genero : UISegmentedControl = UISegmentedControl()
    
    var txtCedula : TextField = TextField()
    var txtIdentificacion : TextField = TextField()
    
    var txtNombre : TextField = TextField()
    var txtCiudad : TextField = TextField()
    var txtCelular : TextField = TextField()
    
    let galeriaController = UIImagePickerController()
    
    var chosenImage : UIImage!
    var listaImagenes : NSMutableArray = NSMutableArray()
    var cantidadFotos = 0
    
    var posicionY : CGFloat = 15

    var pickerTipo : PickerList!
    var dataTipo = ["Cédula", "Pasaporte"]
    var pickerSelected : PickerList!

    var pickerProvincia : PickerList!
    var dataProvincia = [""]
    var provincias : NSMutableArray = NSMutableArray()
    
    
    var locationManager : CLLocationManager!
    var coord : CLLocationCoordinate2D!
    
    var pickerCiudad : PickerList!
    var dataCiudad = [""]
    var ciudades : NSMutableArray = NSMutableArray()
    
    var downKeyboard : Bool  = true
    
    var tipo : NSString!
    var identificacion : NSString!
    var clave : NSString!
    var email : NSString!
    var celular : NSString!
    var fecha : NSDate!
    
    let iOS8 = floor(NSFoundationVersionNumber) > floor(NSFoundationVersionNumber_iOS_7_1)
    
    var lat :Double = 0
    var lon :Double = 0
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        scroll.frame = CGRect(x: 0, y: -60, width: self.view.frame.width, height: self.view.frame.height+60)
        scroll.isUserInteractionEnabled = true
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(CompleteViewController.hideKeyboard))
        scroll.addGestureRecognizer(singleTap)
        self.view.addSubview(scroll)
        
        self.navigationItem.title = ""
        galeriaController.delegate = self
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        let barWhite : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width , height: 30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width / 2.8, height: 30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x: barBlue.frame.width, y: 64, width: 45, height: 30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        let title : UILabel = UILabel(frame: CGRect(x: 0, y: 70, width: self.view.frame.width - 20, height: 30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "Laboratorio"
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)
        
        let items = ["Masculino", "Femenino"]
        sgm_genero = UISegmentedControl(items: items)
        sgm_genero.selectedSegmentIndex = 0
        sgm_genero.tintColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        sgm_genero.frame = CGRect(x: 40, y: 105, width: self.view.frame.width - 80, height:  40)
        //scroll.addSubview(sgm_genero)
        
        txtCedula.frame = CGRect(x: 40, y: 105, width: self.view.frame.width - 80, height: 40)
        txtCedula.layer.cornerRadius = 20
        txtCedula.textAlignment = NSTextAlignment.center
        txtCedula.placeholder="Cédula"
        txtCedula.delegate = self
        txtCedula.layer.borderColor = UIColor.lightGray.cgColor
        txtCedula.layer.borderWidth = 1
        txtCedula.autocorrectionType = UITextAutocorrectionType.no
        txtCedula.addTarget(self, action: #selector(upView(view:)), for: UIControlEvents.editingDidBegin)
        txtCedula.addTarget(self, action: #selector(downView(view:)), for: UIControlEvents.editingDidEndOnExit)
                scroll.addSubview(txtCedula)
        
        txtIdentificacion.frame = CGRect(x: 40, y: 155, width: self.view.frame.width - 80, height: 40)
        txtIdentificacion.layer.cornerRadius = 20
        txtIdentificacion.textAlignment = NSTextAlignment.center
        txtIdentificacion.delegate = self
        txtIdentificacion.addTarget(self, action: #selector(upView(view:)), for: UIControlEvents.editingDidBegin)
        txtIdentificacion.addTarget(self, action:  #selector(downView(view:)), for: UIControlEvents.editingDidEndOnExit)
        txtIdentificacion.placeholder="Número de Identificación"
        txtIdentificacion.layer.borderColor = UIColor.lightGray.cgColor
        txtIdentificacion.layer.borderWidth = 1
        txtIdentificacion.autocorrectionType = UITextAutocorrectionType.no
        scroll.addSubview(txtIdentificacion)
        
        txtNombre.frame = CGRect(x: 40, y: 205, width: Int(self.view.frame.width - 80), height: 40)
        txtNombre.layer.cornerRadius = 20
        txtNombre.textAlignment = NSTextAlignment.center
        txtNombre.delegate = self
        //txt_segundoApellido.enabled = false
        txtNombre.addTarget(self, action:  #selector(upView(view:)), for: UIControlEvents.editingDidBegin)
        txtNombre.addTarget(self, action:  #selector(downView(view:)), for: UIControlEvents.editingDidEndOnExit)
        txtNombre.placeholder="Nombre"
        txtNombre.layer.borderColor = UIColor.lightGray.cgColor
        txtNombre.layer.borderWidth = 1
        txtNombre.autocorrectionType = UITextAutocorrectionType.no
        scroll.addSubview(txtNombre)
        
        txtCelular.frame = CGRect(x: 40, y: 255, width: Int(self.view.frame.width - 80), height: 40)
        txtCelular.layer.cornerRadius = 20
        txtCelular.textAlignment = NSTextAlignment.center
        txtCelular.delegate = self
        txtCelular.keyboardType = UIKeyboardType.numberPad

        //txt_segundoApellido.enabled = false
        txtCelular.addTarget(self, action:  #selector(upView(view:)), for: UIControlEvents.editingDidBegin)
        txtCelular.addTarget(self, action:  #selector(downView(view:)), for: UIControlEvents.editingDidEndOnExit)
        txtCelular.placeholder="Celular"
        txtCelular.layer.borderColor = UIColor.lightGray.cgColor
        txtCelular.layer.borderWidth = 1
        txtCelular.autocorrectionType = UITextAutocorrectionType.no
        scroll.addSubview(txtCelular)
        /*
        let lbProvincia = UILabel(frame: CGRect(x: 30, y: 355, width: self.view.frame.width - 40, height:  25))
        lbProvincia.text = "Provincia: *"
        lbProvincia.font = UIFont.systemFont(ofSize: 14)
        //scroll.addSubview(lbProvincia)
        
        
        txtProvincia = UITextField(frame: CGRect(x: 20, y: 305, width: self.view.frame.width - 40, height: 40))
        txtProvincia.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        txtProvincia.textColor = UIColor.white
        txtProvincia.textAlignment = NSTextAlignment.center
        txtProvincia.delegate = self
        txtProvincia.layer.borderColor = UIColor.lightGray.cgColor
        txtProvincia.layer.borderWidth = 1
        txtProvincia.text = "Provincia"
        txtProvincia.addTarget(self, action:  #selector(upView(view:)), for: UIControlEvents.editingDidBegin)
        txtProvincia.addTarget(self, action:  #selector(downView(view:)), for: UIControlEvents.editingDidEndOnExit)
        txtProvincia.autocorrectionType = UITextAutocorrectionType.no
        //txtProvincia.enabled = false
        scroll.addSubview(txtProvincia)
        */
        
        
        txtCiudad.frame = CGRect(x: 20,y: 305, width: self.view.frame.width - 40, height: 40)
        txtCiudad.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        txtCiudad.textColor = UIColor.white
        txtCiudad.textAlignment = NSTextAlignment.center
        txtCiudad.delegate = self
        txtCiudad.layer.borderColor = UIColor.lightGray.cgColor
        txtCiudad.layer.borderWidth = 1
        txtCiudad.addTarget(self, action:  #selector(upView(view:)), for: UIControlEvents.editingDidBegin)
        txtCiudad.addTarget(self, action:  #selector(downView(view:)), for: UIControlEvents.editingDidEndOnExit)
        txtCiudad.text = "Ciudad"
        txtCiudad.autocorrectionType = UITextAutocorrectionType.no
        scroll.addSubview(txtCiudad)
        
        let verFlobotomista = UIButton(type: UIButtonType.system)
        verFlobotomista.frame = CGRect(x: 13.0, y: 355, width: self.view.frame.width - 26, height: 40.0)
        verFlobotomista.layer.cornerRadius = 20
        verFlobotomista.layer.masksToBounds = true
        verFlobotomista.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        verFlobotomista.setTitle("Deseo adjuntar foto de orden de exámenes (máx 4)", for: UIControlState.normal)
        verFlobotomista.setTitleColor(UIColor.white, for: UIControlState.normal)
        verFlobotomista.titleLabel!.font =  UIFont(name: "helvetica-bold", size: 11)
        verFlobotomista.addTarget(self, action: #selector(LaboratorioViewController.anadirFoto), for: UIControlEvents.touchUpInside)
        scroll.addSubview(verFlobotomista)
        
        let btAceptar = UIButton(frame: CGRect(x: 30, y: 405, width: self.view.frame.width - 60, height: 40))
        btAceptar.setTitle("Siguiente", for: UIControlState.normal)
        btAceptar.addTarget(self, action: #selector(LaboratorioViewController.solicitar), for: UIControlEvents.touchUpInside)
        btAceptar.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        btAceptar.layer.cornerRadius = 20
        scroll.addSubview(btAceptar)
        
        pickerCiudad = PickerList(datos: dataCiudad as NSArray)
        
        scroll.contentSize = CGSize(width: 0, height: UIScreen.main.bounds.height)
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        if iOS8 {
            locationManager.requestAlwaysAuthorization()
        }
        locationManager.startUpdatingLocation()
        
        //getProvincias()
        self.pickerTipo = PickerList(datos: self.dataTipo as NSArray)

        getCiudades()
    }
    
    func locationManager(_ manager:CLLocationManager, didUpdateLocations locations:[CLLocation]) {
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        coord = locationObj.coordinate
        
        lat = coord.latitude
        lon = coord.longitude
    }
    
    
    
    func hideKeyboard(){
        print("hidekeyboard")
        self.txtCedula.resignFirstResponder()
        self.txtIdentificacion.resignFirstResponder()
        self.txtNombre.resignFirstResponder()
        //self.txt.resignFirstResponder()
        self.txtCiudad.resignFirstResponder()
        self.txtCelular.resignFirstResponder()
        
    }
    func cancelPicker(){
        pickerSelected.hidePicker()
        txtCiudad.resignFirstResponder()
        txtCedula.resignFirstResponder()
    }
    
    
    //
    func acceptPicker(){
        pickerSelected.hidePicker()
        if pickerSelected == pickerTipo {
            txtCedula.text = dataTipo[pickerTipo.eleccion]
            txtCedula.resignFirstResponder()
            self.getCiudades()
        } else if pickerSelected == pickerCiudad {
            txtCiudad.text = dataCiudad[pickerCiudad.eleccion]
            txtCiudad.resignFirstResponder()
        }
    }
    
    func upView( view : UITextField ){
        print("entro al upView")
        if (view == txtCedula || view == txtCiudad){
            print("entro al ifview")
            print("wawawawaw")
         } else {
            print("no entro al if view")
             if self.downKeyboard {
                self.downKeyboard = false
            }
        }
        
        
    }
    /*(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
     return NO;
     }*/
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if(textField == txtCiudad || textField == txtCedula){
            self.hideKeyboard()
            //return false
            print("textfieldshoudl begin")
            
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //print("entro a editar" + textField.description)
        
        if (textField == txtCedula){
            print("txtCedula")
            pickerTipo.showPicker(controller: self)
            pickerSelected = pickerTipo
            txtCedula.resignFirstResponder()
        } else if (textField == txtCiudad){
            //self.hideKeyboard()
            print("txtciudad")
            pickerCiudad.showPicker(controller: self)
            pickerSelected = pickerCiudad
            txtCiudad.resignFirstResponder()
        }
    }
    
    func downView( view : UITextField ){
        print("downview")
        self.dismissKeyboard()
        self.txtIdentificacion.resignFirstResponder()
        self.txtNombre.resignFirstResponder()
        self.txtCelular.resignFirstResponder()
        
        self.txtCelular.resignFirstResponder()
        print("valor downkeyboard" + self.downKeyboard.description)
        if !self.downKeyboard {
            print("entro a la negacion")
            self.downKeyboard = true
        }
       
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        print("dismiss")
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        let transitionOptions = UIViewAnimationOptions.showHideTransitionViews//TransitionNone
        UIView.transition(with: self.view, duration: 0.2, options: transitionOptions, animations: {
            if !self.downKeyboard {
                self.downKeyboard = true
                //self.scroll.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.scroll.frame) + 230)
            }
            
            }, completion: { finished in
                
        })
        return true;
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getProvincias() {
        
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        
        let urldef = URLFactory.urldefinida()

        let url = NSURL(string: "\(urldef)servicio/login/obtenerprovincias?arg0=1")
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        let str = "wsappusuario:ZA$57@9b86@$2r5"
        
        /*let utf8str = str.dataUsingEncoding(NSUTF8StringEncoding)
         
         if let base64Encoded = utf8str?.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0)){
         
         //print("Encoded:  \(base64Encoded)")
         //request.addValue("Authorization", forHTTPHeaderField: "Basic \(base64Encoded)")
         request.setValue("Basic \(base64Encoded)", forHTTPHeaderField: "Authorization")
         
         
         if let base64Decoded = NSData(base64EncodedString: base64Encoded, options:   NSDataBase64DecodingOptions(rawValue: 0))
         .map({ NSString(data: $0, encoding: NSUTF8StringEncoding) })
         {
         // Convert back to a string
         print("Decoded:  \(base64Decoded)")
         }
         }*/
        let utf8str = str.data(using: String.Encoding.utf8)
        
        //let base64Encoded = str.data(using: String.Encoding.utf8, allowLossyConversion: true)
        //let base64Encoded = str.dataUsingEncoding(NSUTF8StringEncoding)!.base64EncodedStringWithOptions([])
        //print("Encoded: \(base64Encoded), \(utf8str!.base64EncodedString())")
        let base64DecodedData = utf8str!.base64EncodedString()
        print(base64DecodedData)
        //NSData(base64EncodedString: base64Encoded, options: [])!
        // let base64DecodedString = String(data: base64DecodedData as! Data, encoding: String.Encoding.utf8)!
        //print("Decoded: \(base64DecodedString)")
        
        //Note : Add the corresponding "Content-Type" and "Accept" header. In this example I had used the application/json.
        request.addValue(base64DecodedData, forHTTPHeaderField: "Authorization")
        
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                    var err: NSError?
                    
                    //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    if (err != nil) {
                        print("JSON Error \(err!.localizedDescription)")
                    }
                    
                    //println(jsonResult);
                    //print(jsonResult)
                    let info =  jsonResult as NSDictionary
                    let data = info["lista"] as! [[String: Any]]
                    if data.count > 0 {
                        for item in data {
                            let provincia = Provincia()
                            if let pais = item["codigo_pais"] as? String {
                                provincia.codigoPais = pais as NSString
                            }
                            
                            if let prov = item["codigo_provincia"] as? String {
                                provincia.codigoProvincia = prov as NSString
                            }
                            
                            if let reg = item["codigo_region"] as? String {
                                provincia.codigoRegion = reg as NSString
                            }
                            
                            if let nombre = item["nombre_provincia"] as? String {
                                provincia.nombre = nombre as NSString
                            }
                            
                            self.provincias.add(provincia)
                        }
                    }
                    
                    
                    var lista = [""]
                    /*for provi in self.provincias {
                     lista.append(provi.valueForKey("nombre")as! String)
                     //lista.addObject(provi.nombre)
                     }*/
                    for  i in 0..<self.provincias.count{
                        print("encontro 1 \((self.provincias[i] as AnyObject).value(forKey: "nombre"))")
                        let nombre = (self.provincias[i] as AnyObject).value(forKey: "nombre")
                        lista.append(nombre as! String)
                    }
                    
                    lista.remove(at: 0)
                    
                    self.dataProvincia = lista
                    self.txtCedula.text = self.dataProvincia[0]
                    //
                    //
                    self.pickerProvincia = PickerList(datos: self.dataProvincia as NSArray)
                    
                    ViewUtilidades.hiddenLoader(controller: self)
                    self.getCiudades()
                } catch {
                    print("ERROR")
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: "Revise su conexión a Datos o Internet", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            
            
        }
    }
    
    func getCiudades() {
        
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        
        //let provincia = provincias.object(at: pickerProvincia.eleccion) as! Provincia
        let urldef = URLFactory.urldefinida()

        let url = NSURL(string: "\(urldef)servicio/laboratorio/ciudades")
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        let str = "wsappusuario:ZA$57@9b86@$2r5"
        
        /*let utf8str = str.dataUsingEncoding(NSUTF8StringEncoding)
         
         if let base64Encoded = utf8str?.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0)){
         
         // print("Encoded:  \(base64Encoded)")
         // request.addValue("Authorization", forHTTPHeaderField: "Basic \(base64Encoded)")
         request.setValue("Basic \(base64Encoded)", forHTTPHeaderField: "Authorization")
         
         
         if let base64Decoded = NSData(base64EncodedString: base64Encoded, options:   NSDataBase64DecodingOptions(rawValue: 0))
         .map({ NSString(data: $0, encoding: NSUTF8StringEncoding) })
         {
         // Convert back to a string
         print("Decoded:  \(base64Decoded)")
         }
         }*/
        let utf8str = str.data(using: String.Encoding.utf8)
        
        // let base64Encoded = str.data(using: String.Encoding.utf8, allowLossyConversion: true)
        //let base64Encoded = str.dataUsingEncoding(NSUTF8StringEncoding)!.base64EncodedStringWithOptions([])
        //print("Encoded: \(base64Encoded), \(utf8str!.base64EncodedString())")
        let base64DecodedData = utf8str!.base64EncodedString()
        print(base64DecodedData)
        //NSData(base64EncodedString: base64Encoded, options: [])!
        // let base64DecodedString = String(data: base64DecodedData as! Data, encoding: String.Encoding.utf8)!
        //print("Decoded: \(base64DecodedString)")
        
        //Note : Add the corresponding "Content-Type" and "Accept" header. In this example I had used the application/json.
        request.addValue(base64DecodedData, forHTTPHeaderField: "Authorization")
        
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                    var err: NSError?
                    
                    //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    if (err != nil) {
                        print("JSON Error \(err!.localizedDescription)")
                    }
                    
                    //println(jsonResult);
                    //print(jsonResult)
                    let info =  jsonResult as NSDictionary
                    let data = info["lista"] as! [[String: Any]]
                    if data.count > 0 {
                        self.ciudades.removeAllObjects()
                        for item in data {
                            let ciudad = Ciudad()
                            ciudad.codigoPais = item["codigopais"] as! NSString
                            ciudad.codigoProvincia = item["codigoprovincia"] as! NSString
                            ciudad.codigoRegion = item["codigoregion"] as! NSString
                            ciudad.codigoCiudad = item["codigociudad"] as! NSString
                            ciudad.nombre = item["nombreciudad"] as! NSString
                            self.ciudades.add(ciudad)
                        }
                    }
                    
                    var lista = [""]
                    /*for ciud in self.ciudades {
                     lista.append(ciud.valueForKey("nombre") as! String)
                     }*/
                    for  i in 0..<self.ciudades.count{
                        print("encontro 1 \((self.ciudades[i] as AnyObject).value(forKey: "nombre"))")
                        let nombre = (self.ciudades[i] as AnyObject).value(forKey: "nombre")
                        lista.append(nombre as! String)
                    }
                    lista.remove(at: 0)
                    if (self.ciudades.count < 1){
                        lista = ["-----"]
                    }
                    
                    self.dataCiudad = lista as AnyObject as! [String]
                    self.txtCiudad.text = self.dataCiudad[0]
                    
                    
                    self.pickerCiudad = PickerList(datos: self.dataCiudad as NSArray)
                    
                    ViewUtilidades.hiddenLoader(controller: self)
                } catch {
                    print("ERROR")
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: "Revise su conexión a Datos o Internet", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            
            
        }
    }
    func anadirFoto(){
        
        let alert = UIAlertController(title: "¿Que desea hacer?", message: "Eliga una opción", preferredStyle: UIAlertControllerStyle.alert)
        let action1 = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
        let action2 = UIAlertAction(title: "Tomar foto", style: UIAlertActionStyle.default) { (action) in
            
            if UIImagePickerController.availableCaptureModes(for: .rear) != nil {
                
                self.galeriaController.allowsEditing = true
                self.galeriaController.sourceType = UIImagePickerControllerSourceType.camera
                self.galeriaController.cameraCaptureMode = .photo
                self.present(self.galeriaController, animated: true, completion: nil)
            } else {
                let alertVC = UIAlertController(title: "No se encontro el dispositivo", message: "Este dispositivo no cuenta con cámara", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok", style:.default, handler: nil)
                alertVC.addAction(okAction)
                self.present(alertVC, animated: true, completion: nil)
            }

        }
        let action3 = UIAlertAction(title: "Ir a galeria", style: UIAlertActionStyle.default) { (action) in
            print("debo de ir a la galeria")
            self.galeriaController.allowsEditing = true
            self.galeriaController.sourceType = .photoLibrary
            self.galeriaController.modalPresentationStyle = .popover
            self.present(self.galeriaController, animated: true, completion: nil)//4
        }
        
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)
        
        print(self.listaImagenes.count)
        self.present(alert, animated: true, completion: nil)
    }
    
    //let alertSolicitar: UIAlertView = UIAlertView()
    /*func addFoto(){
        
        let createAccountErrorAlert: UIAlertView = UIAlertView()
        
        createAccountErrorAlert.delegate = self
        createAccountErrorAlert.title = "¿Que desea hacer?"
        createAccountErrorAlert.message = "Eliga una opción"
        createAccountErrorAlert.addButton(withTitle: "Cancelar")
        createAccountErrorAlert.addButton(withTitle: "Tomar foto")
        createAccountErrorAlert.addButton(withTitle: "Ir a galeria")
        createAccountErrorAlert.addButton(withTitle: "nuevoi")
        createAccountErrorAlert.show()
    }*/
    /*
    func alertView(View: UIAlertView!, clickedButtonAtIndex buttonIndex: Int) {
        
        print("entro al alertview clicked")
        if View == alertSolicitar {
            switch buttonIndex{
                
            case 1:
                print("Cancelar");
                
            default:
                //self.soapSolicitar()
                print("llmar a solicitar")
            }
            
        } else {
            switch buttonIndex {
                
            case 1:
                if UIImagePickerController.availableCaptureModes(for: .rear) != nil {
                    galeriaController.allowsEditing = false
                    galeriaController.sourceType = UIImagePickerControllerSourceType.camera
                    galeriaController.cameraCaptureMode = .photo
                    present(galeriaController, animated: true, completion: nil)
                } else {
                    let alertVC = UIAlertController(title: "No se encontro el dispositivo", message: "Este dispositivo no cuenta con cámara", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Ok", style:.default, handler: nil)
                    alertVC.addAction(okAction)
                    present(alertVC, animated: true, completion: nil)
                }
            case 2:
                galeriaController.allowsEditing = false
                galeriaController.sourceType = .photoLibrary
                galeriaController.modalPresentationStyle = .popover
                present(galeriaController, animated: true, completion: nil)//4
                
            default:
                print("alertView \(buttonIndex) clicked")
                
            }
        }
    }*/

    /*func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imagePost.image = image
        } else{
            print("Something went wrong")
        }
        
        self.dismiss(animated: true, completion: nil)
    }*/
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        //chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        print(info.debugDescription)
        if let miimagen = info[UIImagePickerControllerOriginalImage] as? UIImage{
            chosenImage = miimagen
            print("si if")
        }else{
            print("whaaa")
        }
        let imgAux = Toucan(image: chosenImage!).resize(size: CGSize(width: 250, height: 250), fitMode: Toucan.Resize.FitMode.Clip).image
        print("entro a esta funcion para ver que mimso esta haciedno")
        let image = UIImageView()
        if cantidadFotos == 0 {
            posicionY += 50
            image.frame = CGRect(x: 10, y: 455, width: self.view.frame.width / 2 - 20, height: 180)
            posicionY += 300
        } else if cantidadFotos == 1 {
            image.frame = CGRect(x: 10 + self.view.frame.width / 2, y: 455 , width: self.view.frame.width / 2 - 20, height: 180)
        } else if cantidadFotos == 2 {
            image.frame = CGRect(x: 10, y: 455 + 180,  width: self.view.frame.width / 2 - 20, height: 180)
            posicionY += 300
        } else if cantidadFotos == 3 {
            image.frame = CGRect(x: 10 + self.view.frame.width / 2, y: 455 + 180 , width: self.view.frame.width / 2 - 20, height: 180)
        } else{
            print("no puede añadir mas fotos")
        }
        
        image.image = imgAux
        scroll.addSubview(image)
        
        scroll.contentSize = CGSize(width: self.view.frame.width, height: posicionY)
        
        self.listaImagenes.add(imgAux)
        cantidadFotos += 1
        
        
        let imagedata = UIImageJPEGRepresentation(imgAux, 1.0)
        //print(imageData?.base64EncodedString())  <#T##Data.Base64EncodingOptions#>
        _ = imagedata?.base64EncodedString(options: .lineLength64Characters)
        
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
        print("sale")
    }
    
    func solicitar(){
            if ( txtCelular.text!.trimmingCharacters(in: NSCharacterSet.whitespaces) == "") {//stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()) == "" ){
                UtilidadesGeneral.mensaje(mensaje: "Ingrese su número de celular")
            } else {/*
                 if ( listaImagenes.count == 0 ){
                 UtilidadesGeneral.mensaje("Adjunte al menos una fotografía")
                 } else {*/
                if ( coord == nil ){
                    UtilidadesGeneral.mensaje(mensaje: "No se puede obtener su posición actual. Revise su configuración")
                } else {
                    let alert = UIAlertController(title: "Veris", message: "¿Esta seguro de enviar la solicitud?", preferredStyle: UIAlertControllerStyle.alert)
                    let action1 = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
                    let action2 = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) { (action) in
                        print("acpetio \(self.cantidadFotos)")
                        self.enviarSolicitud()
                    }
                   
                    
                    alert.addAction(action1)
                    alert.addAction(action2)
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }
                /*}*/
            }
    }


    func enviarSolicitud(){
        print("entro a enviar solicitud")
        
        var stringImagenes = ""
        var codigo_ciudad = ""
        print(self.listaImagenes.count)
        for imagen in self.listaImagenes {
            if stringImagenes != "" {
                stringImagenes += ";"
            }
            //var base64String = imagen.base64EncodedStringWithOptions(.allZeros)
            let imageData = UIImagePNGRepresentation(imagen as! UIImage)
            //print(imageData?.base64EncodedString())
            let base64String = imageData?.base64EncodedString(options: .lineLength64Characters)
            
            stringImagenes += base64String!
            
        }
        for i in 0..<self.ciudades.count{
            if((self.ciudades[i] as AnyObject).nombre as String  == dataCiudad[pickerCiudad.eleccion]){
                codigo_ciudad = (self.ciudades[i] as AnyObject).codigoRegion as! String
            }
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd" //format style. Browse online to get a format that fits your needs.
         let tipoIdentificacion = self.pickerTipo.eleccion == 0 ? "2" : "3"
        
        let currentDate = NSDate();
        let stringDate = dateFormatter.string(from: currentDate as Date)
        let urldef = URLFactory.urldefinida()

        let cadenaUrl = "\(urldef)servicio/laboratorio/registrar?arg0=1&arg1=\(self.txtNombre.text!)&arg2=\(self.txtCelular.text!)&arg3=\(stringDate)&arg4=\(lat)&arg5=\(lon)&arg6=\(codigo_ciudad)&arg7=\(tipoIdentificacion)&arg8=\(self.txtIdentificacion.text!)"
        print(cadenaUrl)
        
            let url_enviar = NSURL(string: cadenaUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)! )
            
        if url_enviar != nil {
            let request = NSMutableURLRequest(url: url_enviar! as URL)
            let session = URLSession.shared
                request.httpMethod = "POST"

            let str = "wsappusuario:ZA$57@9b86@$2r5"
            let utf8str = str.data(using: String.Encoding.utf8)
            let base64DecodedData = utf8str!.base64EncodedString()
                request.addValue(base64DecodedData, forHTTPHeaderField: "Authorization")
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
         let postData = NSData(data:stringImagenes.replacingOccurrences(of: "\n", with: "").data(using: String.Encoding.utf8)!)
                request.httpBody = postData as Data
                let task = session.dataTask(with: request as URLRequest) { data, response, error in
                    guard data != nil else {
                        print("no data found: \(error)")
                        return
                    }
                    
                    do {
                        if let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                            print("Response: \(json)")
                            ViewUtilidades.hiddenLoader(controller: self)
                            
                            let alert = UIAlertController(title: "Veris", message: "Solicitud Enviada!", preferredStyle: UIAlertControllerStyle.alert)
                            let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                                UIAlertAction in
                                
                            }
                            alert.addAction(okAction)
                            self.present(alert, animated: true, completion: nil)
                        } else {
                            let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)// No error thrown, but not NSDictionary
                            ViewUtilidades.hiddenLoader(controller: self)
                            
                            print("Error could not parse JSON: \(jsonStr)")
                        }
                    } catch let parseError {
                        print(parseError)// Log the error thrown by `JSONObjectWithData`
                        let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                        print("Error could not parse JSON: '\(jsonStr)'")
                    }
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    task.resume()
                }
            
                
            } else {
            print("url : \(url_enviar)")
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: "Ha ocurrido un error. Intente nuevamente", preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    
                }
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            }
       
    }
    
}
