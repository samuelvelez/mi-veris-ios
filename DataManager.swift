//
//  DataManager.swift
//  veris
//
//  Created by Felipe Lloret on 04/02/15.
//  Copyright (c) 2015 Felipe Lloret. All rights reserved.
//

import Foundation

//El servicio web esta introducido como constante
//aquí habría que editar las urls para pasarle los diferentes
//argumentos dependiendo del usuario

class DataManager {
    //Gestionar datos de Mis Citas
    class func getCitasVigentesDataWithSuccess(stringURL: String!, success: @escaping ((_ citasVigentesData: NSData?) -> Void)) {
        loadDataFromURL(url: NSURL(string: stringURL)!, completion:{(data, error) -> Void in
            if let urlData = data {
                success(urlData)
            }
        })
    }
    
    //Gestionar datos de Historial de Citas
    class func getHistorialCitasDataWithSuccess(stringURL: String!, success: @escaping ((_ historialCitasData: NSData?) -> Void)) {
        loadDataFromURL(url: NSURL(string: stringURL)!, completion:{(data, error) -> Void in
            if let urlData = data {
                success(urlData)
            }
        })
    }
    
    //Gestionar datos de Mis Recetas
    class func getRecetasDataWithSuccess(stringURL: String!, success: @escaping ((_ recetasData: NSData?) -> Void)) {
        loadDataFromURL(url: NSURL(string: stringURL)!, completion: {(data, error) -> Void in
            if let urlData = data {
                success(urlData)
            }
        })
    }
    
    //Gestionar datos de Detalles Receta
    class func getRecetasDetailDataWithSuccess(stringURL: String!, success: @escaping ((_ recetasDetailData: NSData?) -> Void)) {
        loadDataFromURL(url: NSURL(string: stringURL)!, completion: {(data, error) -> Void in
            if let urlData = data {
                success(urlData)
            }
        })
    }
    
    class func loadDataFromURL(url: NSURL, completion:@escaping (_ data: NSData?, _ error: NSError?) -> Void) {
        let session = URLSession.shared
        
        // Use NSURLSession to get data from an NSURL
        /*let loadDataTask = session.dataTaskWithURL(url as URL, completionHandler: { (data: NSData?, response: URLResponse?, error: NSError?) -> Void in
            if let responseError = error {
                completion(data: nil, error: responseError)
            } else if let httpResponse = response as? NSHTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    let statusError = NSError(domain:"com.domain", code:httpResponse.statusCode, userInfo:[NSLocalizedDescriptionKey : "HTTP status code has unexpected value."])
                    completion(data: nil, error: statusError)
                } else {
                    completion(data: data, error: nil)
                }
            }
        })
        */
        //loadDataTask.resume()
    }
}
