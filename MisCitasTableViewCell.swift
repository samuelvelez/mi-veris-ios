//
//  CustomTableViewCell.swift
//  veris
//
//  Created by Felipe Lloret on 05/02/15.
//  Copyright (c) 2015 Felipe Lloret. All rights reserved.
//

import UIKit

class MisCitasTableViewCell: UITableViewCell {
    
    var gradientView: UIView = UIView()
    var firstLineView: UIView = UIView()
    var secondLineView: UIView = UIView()
    var thirdLineView: UIView = UIView()
    
    var blueArrow : UIImageView = UIImageView()
    
    var pacienteLabel : UILabel = UILabel()
    var sucursalLabel: UILabel = UILabel()
    var fechaLabel: UILabel = UILabel()
    var doctorLabel: UILabel = UILabel()
    var prestacionLabel : UILabel = UILabel()
    var espagada : UILabel = UILabel()
    var imgpago : UIImageView = UIImageView()
    
    
    var reagendarButton: UIButton = UIButton()
    var reagendarButton2: UIButton = UIButton()
    var cancelarCitaButton: UIButton = UIButton()
    
    var nombre : UILabel = UILabel()
    
    var controller = MisCitasViewController()
    
    var posY : CGFloat = 0.0
    var lastPac : String = "Wnwnw"
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width;
        
        gradientView.frame = CGRect(x: 0.0, y: posY, width: screenWidth, height: 128.0)
        gradientView.backgroundColor = UIColor.lightGray
        gradientView.alpha = 0.3
        self.contentView.addSubview(gradientView)
        
        firstLineView.frame = CGRect(x: 0.0, y: posY, width: screenWidth, height: 1.0)
        firstLineView.backgroundColor = UIColor.black
        self.contentView.addSubview(firstLineView)
        
        posY = posY + 24.0
        secondLineView.frame = CGRect(x: 0.0, y: posY, width: screenWidth, height: 1.0)
        secondLineView.backgroundColor = UIColor.black
        self.contentView.addSubview(secondLineView)
        
        posY = posY + 101.0
        thirdLineView.frame = CGRect(x: 0.0, y: posY, width: screenWidth, height: 1.0)
        thirdLineView.backgroundColor = UIColor.black
        self.contentView.addSubview(thirdLineView)
        
        blueArrow.frame = CGRect(x: 0.0, y: 0.0, width: 17.0, height: 26.0)
        blueArrow.image = UIImage(named: "BlueArrow")
        self.contentView.addSubview(blueArrow)
        
        pacienteLabel.frame = CGRect(x: 21.0, y: -15.0, width: screenWidth - 42.0, height: 16.0)
        pacienteLabel.numberOfLines = 1
        pacienteLabel.font = UIFont(name: "helvetica-bold", size: 12.0)
        pacienteLabel.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        self.contentView.addSubview(pacienteLabel)
        
        sucursalLabel.frame = CGRect(x: 21.0, y: 5.0, width: screenWidth - 142.0, height: 16.0)
        sucursalLabel.numberOfLines = 1
        sucursalLabel.font = UIFont(name: "helvetica-bold", size: 12.0)
        sucursalLabel.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        self.contentView.addSubview(sucursalLabel)
        
        fechaLabel.frame = CGRect(x: screenWidth - 121.0, y: 5.0, width: 121.0, height: 16.0)
        fechaLabel.numberOfLines = 1
        fechaLabel.font = UIFont(name: "helvetica", size: 12)
        self.contentView.addSubview(fechaLabel)
        
        doctorLabel.frame = CGRect(x: 20.0, y: 30.0, width: screenWidth / 2, height:20)
        doctorLabel.numberOfLines = 1
        doctorLabel.textAlignment = .left
        
        doctorLabel.font = UIFont(name: "helvetica-bold", size: 11.0)
        self.contentView.addSubview(doctorLabel)
        
        prestacionLabel.frame = CGRect(x:20.0, y:45.0, width:(screenWidth / 2) - 20, height:40.0)
        prestacionLabel.numberOfLines = 2
        prestacionLabel.textAlignment = .left
        prestacionLabel.lineBreakMode = .byWordWrapping        
        prestacionLabel.font = UIFont(name: "helvetica", size: 12.0)
        self.contentView.addSubview(prestacionLabel)
        
        espagada.frame = CGRect(x:(screenWidth / 2) + 10, y:45.0, width:(screenWidth / 2) - 20, height:40.0)
        espagada.numberOfLines = 2
        espagada.textAlignment = .left
        espagada.lineBreakMode = .byWordWrapping
        espagada.font = UIFont(name: "helvetica", size: 12.0)
        self.contentView.addSubview(espagada)
        
        imgpago = UIImageView(frame: CGRect(x:((screenWidth / 3) * 2 ) , y: 30, width: 20, height: 20))
        imgpago.contentMode = UIViewContentMode.scaleAspectFit
        //imageView.contentMode = UIViewContentMode.Center;
        contentView.addSubview(imgpago)

        
        //imgpago = CGRect(x: Double(((screenWidth / 3) * 2 ) + 20), y: 85.0, width: 40.0, height: 40.0)
        
        
        //50.0
        reagendarButton.frame = CGRect(x:(screenWidth/2) - 120, y:85.0, width:105.0, height:35.0)
        reagendarButton.backgroundColor = UIColor(red: (11.0/256.0), green: (166.0/256.0), blue: (30.0/256.0), alpha: 1)
        reagendarButton.setTitle("CAMBIAR", for: UIControlState.normal)
        reagendarButton.setTitleColor(UIColor.white, for: UIControlState.normal)
        reagendarButton.titleLabel!.font = UIFont(name: "helvetica-bold", size: 12.0)
        self.contentView.addSubview(reagendarButton)
        
        reagendarButton2.frame = CGRect(x:(screenWidth/2) - 52.5, y:85.0, width:105.0, height:35.0)
        reagendarButton2.backgroundColor = UIColor(red: (11.0/256.0), green: (166.0/256.0), blue: (30.0/256.0), alpha: 1)
        reagendarButton2.setTitle("CAMBIAR", for: UIControlState.normal)
        reagendarButton2.setTitleColor(UIColor.white, for: UIControlState.normal)
        reagendarButton2.titleLabel!.font = UIFont(name: "helvetica-bold", size: 12.0)
        reagendarButton2.isHidden = true
        self.contentView.addSubview(reagendarButton2)
        
        cancelarCitaButton.frame = CGRect(x:(screenWidth/2) + 15, y:85.0, width:105.0, height:35.0)
        cancelarCitaButton.backgroundColor = UIColor.black
        cancelarCitaButton.setTitle("ELIMINAR", for: UIControlState.normal)
        cancelarCitaButton.setTitleColor(UIColor.white, for: UIControlState.normal)
        cancelarCitaButton.titleLabel!.font = UIFont(name: "helvetica-bold", size: 12.0)
        self.contentView.addSubview(cancelarCitaButton)
        
        self.backgroundColor = UIColor.clear
        self.selectionStyle = UITableViewCellSelectionStyle.none;
        
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    //Con esta función haremos más simple la adjudicación a las labels desde MisCitasViewController
    func setCell(sucursalLabelText: String, fechaLabelText: String, doctorLabelText: String, prestacionLabelText: String,pacientetext: String, pagada : String) {
        self.pacienteLabel.text = pacientetext
        if(pacientetext == ""){
            self.pacienteLabel.removeFromSuperview()
        }
        
        self.sucursalLabel.text = sucursalLabelText
        self.fechaLabel.text = fechaLabelText
        self.doctorLabel.text = doctorLabelText
        self.prestacionLabel.text = prestacionLabelText
        
        self.espagada.text = pagada
        print("llega \(pagada)")
        if(pagada == "CITA NO PAGADA"){
            self.imgpago.image = UIImage(named:"ic_dolar2.png")
            cancelarCitaButton.isHidden = false
            //reagendarButton.isHidden = false
            //reagendarButton2.isHidden = true
        }else if(pagada == "CITA PAGADA"){
            self.imgpago.image = UIImage(named:"ic_dolar.png")
            //self.willRemoveSubview(cancelarCitaButton)
            //self.contentView.willRemoveSubview(cancelarCitaButton)
            cancelarCitaButton.isHidden = true
            //reagendarButton.isHidden = true
            //reagendarButton2.isHidden = false
            
        }
        //cell.imageView?.image = UIImage(named: imagenes[indexPath.row])
    }

}
