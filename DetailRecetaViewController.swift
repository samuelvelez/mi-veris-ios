//
//  DetailRecetaViewController.swift
//  veris
//
//  Created by Felipe Lloret on 07/02/15.
//  Copyright (c) 2015 Felipe Lloret. All rights reserved.
//

import UIKit
import EventKit

class DetailRecetaViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var detailRecetas = [DetailRecetaModel]()
    var tableView: UITableView = UITableView()
    var contentView: UIView = UIView()
    var setReload : Bool = false
    
    var prestacionLabel: UILabel = UILabel()
    var doctorLabel: UILabel = UILabel()
    
    var cerrar: UIButton = UIButton()
    
    var closeButton: UIButton = UIButton()
    var shareButton: UIButton = UIButton()
    
    var centroString: String?
    var fechaString: String?
    var especialidadString: String?
    var pacienteString: String?
    var doctorString: String?
    var recetaString: String?
    var recetaURL: String?

    var appDelegate: AppDelegate?
    
    override func viewDidAppear(_ animated: Bool) {
        if setReload {
            ViewUtilidades.showLoader(controller: self, title: "Cargando...")
            
            let dr = "Dr(a) "
            self.doctorLabel.text = dr + doctorString!
            
            _ = Usuario.getEntity as Usuario
            let urldef = URLFactory.urldefinida()

            let urlAsString = "\(urldef)servicio/doctor/detallerecetas?arg0=";
            
            self.getJSONData(url: urlAsString)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white

        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width;
        
        let dimBackground: UIImageView = UIImageView()
        dimBackground.frame = CGRect(x: 0.0, y: 0.0, width:400.0, height: 600.0)
        dimBackground.image = UIImage(named: "DimBackground")
        dimBackground.backgroundColor = UIColor.clear
        dimBackground.alpha = 0.3
        self.view.addSubview(dimBackground)
        let posy = (UIScreen.main.bounds.height - 410)/2
        contentView.frame = CGRect(x:30.0 , y:posy,  width:screenWidth - 60.0, height:410.0) //
        contentView.backgroundColor = UIColor.white
        self.view.addSubview(contentView)
        
        let header: UIImageView = UIImageView()
        header.frame = CGRect(x: 0.0, y: 0.0, width:screenWidth - 60.0, height:40.0)
        header.image = UIImage(named: "DetalleHeader")
        self.contentView.addSubview(header)
        
        let labelReceta: UILabel = UILabel()
        labelReceta.frame = CGRect(x: 0.0, y: 40.0, width: screenWidth - 100.0, height: 40.0)
        labelReceta.backgroundColor = UIColor.lightGray
        labelReceta.text = "DETALLES DE LA RECETA"
        labelReceta.textAlignment = NSTextAlignment.center
        labelReceta.textColor = UIColor.white
        labelReceta.font = UIFont(name: "helvetica-bold", size: 14.0)
        self.contentView.addSubview(labelReceta)
        
        shareButton.frame = CGRect(x: screenWidth - 105.0, y: 35.0, width: 48.0, height: 48.0)
        shareButton.setBackgroundImage(UIImage(named: "ShareButton"), for: UIControlState.normal)
        shareButton.addTarget(self, action: #selector(shareReceta(sender:)), for: UIControlEvents.touchUpInside)
        self.contentView.addSubview(shareButton)
        
        let doctorBackground: UIView = UIView()
        doctorBackground.frame = CGRect(x: 0.0, y: 80.0, width: screenWidth - 60.0, height: 70.0)
        doctorBackground.backgroundColor = UIColor(red: (70.0/256.0), green: (249.0/256.0), blue: (235.0/256.0), alpha: 0.2)
        self.contentView.addSubview(doctorBackground)
        
        doctorLabel.frame = CGRect(x: 2.0, y: 70.0, width: 250.0, height: 53.0)
        doctorLabel.font = UIFont(name: "helvetica-bold", size: 13.0)
        doctorLabel.numberOfLines = 2
        doctorLabel.textAlignment = NSTextAlignment.center
        doctorLabel.lineBreakMode = .byWordWrapping
        doctorLabel.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        let dr = "Dr(a) "
        self.doctorLabel.text = dr + doctorString!
        self.contentView.addSubview(doctorLabel)
        
        prestacionLabel.frame = CGRect(x: 80.0, y: 70.0, width: 132.0, height: 118.0)
        prestacionLabel.numberOfLines = 2
        prestacionLabel.lineBreakMode = .byWordWrapping
        prestacionLabel.text = especialidadString
        prestacionLabel.textColor = UIColor(red: (11.0/256.0), green: (166.0/256.0), blue: (30.0/256.0), alpha: 1)
        prestacionLabel.font = UIFont(name: "helvetica-bold", size: 11.0)
        self.contentView.addSubview(prestacionLabel)
        
        closeButton.frame = CGRect(x: 10.0, y: posy + 400 , width: self.contentView.frame.width - 20.0, height: 44.0) //self.contentView.frame.height - 54.0
        closeButton.backgroundColor = UIColor.white
        let blackColor = UIColor.black
        closeButton.layer.borderColor = blackColor.cgColor
        closeButton.layer.borderWidth = 1.0
        closeButton.setTitle("Cerrar", for: UIControlState.normal)
        closeButton.setTitleColor(UIColor.black, for: UIControlState.normal)
        closeButton.titleLabel!.font = UIFont(name: "helvetica", size: 16.0)
        closeButton.layer.zPosition = 10000
        closeButton.addTarget(self, action: #selector(DetailRecetaViewController.closeModalWindow), for: UIControlEvents.touchUpInside)
        //self.contentView.addSubview(closeButton)
        
        
        //let labelcerrar: UILabel = UILabel()
        cerrar.frame = CGRect(x: 30.0 , y: posy + 400, width: screenWidth - 60.0, height: 40.0)
        cerrar.backgroundColor = UIColor.black
        cerrar.layer.borderColor = blackColor.cgColor
        cerrar.layer.borderWidth = 1.0

        //cerrar.textColor = UIColor.white
        cerrar.setTitle("Cerrar", for: UIControlState.normal)
        cerrar.titleLabel!.font = UIFont(name: "helvetica", size: 16.0)
        cerrar.setTitleColor(UIColor.white, for: UIControlState.normal)
        cerrar.addTarget(self, action: #selector(DetailRecetaViewController.closeModalWindow), for: UIControlEvents.touchUpInside)
        
        self.view.addSubview(cerrar)
        self.view.backgroundColor = UIColor.clear

        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        let urldef = URLFactory.urldefinida()

        let urlAsString = "\(urldef)servicio/doctor/detallerecetas?arg0=";
        
        self.getJSONData(url: urlAsString)
    }
    
    func getJSONData(url: String) {
        
        
        let url = NSURL(string: (url as String) + recetaString!)
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in

            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                var err: NSError?
                
                //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                
                _ = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                if (err != nil) {
                    print("JSON Error \(err!.localizedDescription)")
                }
                
                let json = JSON(data: data!)
                
                //Comprobamos si todo ha salido bien, en caso contrario se le muestra el mensaje de error al usuario
                if json["resultado"].string == "ok" {
                    if let recetasDetailArray = json["lista"].array {
                        
                        //Creamos nuestro objeto Recetas con los datos obtenidos del JSON
                        for recetasDetailDic in recetasDetailArray {
                            let recetaDetailDenominacion: String? = recetasDetailDic["denominacion"].stringValue
                            let recetaDetailConcentracion: String? = recetasDetailDic["concentracion"].stringValue
                            let recetaDetailForma: String? = recetasDetailDic["forma_farmaceutica"].stringValue
                            let recetaDetailCantidad: String? = recetasDetailDic["cantidad"].stringValue
                            let recetaDetailIndicaciones: String? = recetasDetailDic["indicaciones"].stringValue
                            
                            let recetaDetail = DetailRecetaModel(denominacion: recetaDetailDenominacion, concentracion: recetaDetailConcentracion, forma: recetaDetailForma, cantidad: recetaDetailCantidad, indicaciones: recetaDetailIndicaciones)
                            self.detailRecetas.append(recetaDetail)
                        }
                        //Uso para debud
                        print(self.detailRecetas)
                        
                        //Recargamos el TableView una vez descargados los datos del servidor
                        //dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        DispatchQueue.main.async(execute: {
                            self.mostrarDetalles()
                            self.tableView.reloadData()
                            ViewUtilidades.hiddenLoader(controller: self)
                        })
                    }
                }
                } catch {
                    print("ERROR")
                }
            }
            
                //Mostramos error al usuario
        }
        
        ///////////////////
    
    }
    
    func mostrarDetalles() {
        self.tableView.frame = CGRect(x: 0, y: 150.0, width: self.view.frame.width,height:  self.view.frame.height - 350);
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.separatorColor = UIColor.clear
        self.tableView.register(DetailRecetasTableViewCell.self as AnyClass, forCellReuseIdentifier: "cell");
        
        self.contentView.addSubview(self.tableView);
    }
    
    func closeModalWindow() {
        print("toco")
        self.dismiss(animated: true, completion: {})
    }
    
    //TableViewDataSource Methods
    
    //Devuelve número de filas en la sección
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.detailRecetas.count;
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    
    var botones : NSMutableArray = NSMutableArray()
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! DetailRecetasTableViewCell
            
            //Hacemos que las celdas no tenga estilo al ser seleccionadas
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            self.shareButton.tag = indexPath.row
        cell.addEventButton.addTarget(self, action: #selector(setReminder(sender:)), for: UIControlEvents.touchUpInside)
            botones.add(cell.addEventButton)
            
            cell.setCell(denominacionLabelText: self.detailRecetas[indexPath.row].denominacion,
                concentracionLabelText: self.detailRecetas[indexPath.row].concentracion,
                formaLabelText: self.detailRecetas[indexPath.row].forma,
                cantidadLabelText: self.detailRecetas[indexPath.row].cantidad,
                indicacionesLabelText: self.detailRecetas[indexPath.row].indicaciones)
        
            return cell
    }

    func shareReceta(sender: UIButton) {
        var string: String?
        var finalString: String?
        
        finalString = "---------------------\n"
        for i in 0 ..< self.detailRecetas.count {
            string = "\(self.detailRecetas[i].denominacion)\n\(self.detailRecetas[i].cantidad) \(self.detailRecetas[i].forma) \(self.detailRecetas[i].concentracion)\n\(self.detailRecetas[i].indicaciones)\n---------------------\n\n"
            finalString = finalString! + string!
        }
        
        let vc = UIActivityViewController(activityItems:[
            "RECETA MÉDICA",
            "---------------------",
            self.centroString!,
            "Fecha: \(self.fechaString!)",
            "Doctor(a): \(self.doctorString!)",
            "Especialidad: \(self.especialidadString!)",
            "Paciente: \(self.pacienteString!)\n\n",
            "---------------------",
            "DETALLE DE LA RECETA",
            finalString!], applicationActivities: nil)
        self.present(vc, animated: true, completion: nil)
    }
    
    //Creamos recordatorio
    func setReminder( sender: UIButton) {
        /*println("Event")
        UIApplication.sharedApplication().openURL(NSURL(string: "calshow://")!)*/
        let eventStore = EKEventStore()
        
        let posicion = botones.index(of: sender)
        
        eventStore.requestAccess(to: EKEntityType.reminder,
            completion: { (granted, error) in
                if !granted {
                    UtilidadesGeneral.mensaje(mensaje: "No se encuentran habilitados los recordatorios. Reivise su configuración");
                    print("Access to store not granted")
                }
        })
        
        let reminder = EKReminder(eventStore: eventStore)
        
        reminder.title = self.detailRecetas[posicion].denominacion
        reminder.calendar = eventStore.defaultCalendarForNewReminders()
        
        var error: NSError?
        
        do {
            try eventStore.save(reminder, commit: true)
        } catch var error1 as NSError {
            error = error1
        }
        UIApplication.shared.openURL(NSURL(string: "x-apple-reminder://")! as URL)
        //
        
    }
}
