



import UIKit

class RecomendacionesController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    var cabecera : UILabel!
    var barname : UIView!
    var btAdd : UIButton!
    var setReload : Bool = false
    
    var tableView: UITableView  =   UITableView() /*Table view que muestra la lista de recomendaciones */
    var lista_recomendacion : NSMutableArray =  NSMutableArray()
    var resultadosel : ObjRecomendacion?
    var dateFormatter = DateFormatter()
    //var meses:[String] = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Nobiembre", "Diceiembre"];
    var posicion: NSInteger = 0
    var scroll : UIScrollView = UIScrollView()
    var nombreUser : UILabel = UILabel()
    
    var txt_usuario : UITextField = UITextField()
    var ls_usuario : NSMutableArray = NSMutableArray()
    var pickerUsuario : PickerList!
    var dataUsuario = [""]
    var nueva_lsUsuario = [Usuario()]
    let user_conect = Usuario.getEntity as Usuario
    //var parent : TipoParentesco = TipoParentesco()
    var pickerSelected : PickerList!
    
    override func viewDidAppear(_ animated: Bool) {
        let user = Usuario.getEntity
        nombreUser.text = user.nombre as String
        Usuario.newcreateButtonFamilyTop(controlador: self, text: user.nombre as String)
        self.consultarrecomendaciones()
        if setReload {
            self.setReload = false
            self.consultarrecomendaciones()
        }
    }
    
   
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        let user = Usuario.getEntity as Usuario
        ls_usuario.add(user)
        dateFormatter.locale = NSLocale(localeIdentifier: "es_EC") as Locale!
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.s"
        self.navigationController?.navigationItem.backBarButtonItem?.title = ""
        //self.viewController.navigationItem.backBarButtonItem.title = "Custom Title";
        let fondo : UIImageView = UIImageView(frame: self.view.bounds)
        fondo.image = UIImage(named:"fondoreserva.png")
        self.view.addSubview(fondo)
        
        
        let barWhite : UIView = UIView(frame: CGRect(x:0, y:64, width:self.view.frame.width , height:30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x:0, y:64, width:self.view.frame.width / 2.6, height:30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x:barBlue.frame.width, y:64, width:45, height:30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x:0, y:64, width:self.view.frame.width - 20, height:30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "Recomendaciones"
       
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)
 
       
       /* txt_usuario = UITextField(frame: CGRect(x:20, y:100, width:self.view.frame.width - 40, height:40))
        txt_usuario.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        txt_usuario.textColor = UIColor.white
        txt_usuario.textAlignment = NSTextAlignment.center
        txt_usuario.delegate = self
        txt_usuario.layer.borderColor = UIColor.lightGray.cgColor
        txt_usuario.layer.borderWidth = 1
        txt_usuario.text = user.nombre as String
        //txt_parentesco.addTarget(self, action: Selector("upView:"), forControlEvents: UIControlEvents.EditingDidBegin)
        //txt_parentesco.addTarget(self, action: Selector("downView:"), forControlEvents: UIControlEvents.EditingDidEndOnExit)
        txt_usuario.autocorrectionType = UITextAutocorrectionType.no
        //txtProvincia.enabled = false
        //self.view.addSubview(txt_usuario)
*/
        
        
        btAdd = UIButton(type: UIButtonType.system)
        btAdd.frame = CGRect(x:(self.view.frame.width - 260)/2, y:150, width: 260, height:40)
        btAdd.layer.cornerRadius = 15
        btAdd.layer.masksToBounds = true
        btAdd.backgroundColor = UtilidadesColor.colordehexadecimal(hex: 0x000000, alpha: 0.35)
        btAdd.setTitle("NUEVA CITA", for: UIControlState.normal)
        btAdd.setTitleColor(UIColor.white, for: UIControlState.normal)
        btAdd.titleLabel!.font =  UIFont(name: "helvetica-bold", size: 16)
        btAdd.addTarget(self, action: #selector(agendar(sender:)), for: .touchUpInside)
        
        cabecera = UILabel(frame: CGRect(x:0, y:210, width:450, height:25))
        
        cabecera.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        cabecera.textColor = UIColor.white
        cabecera.font = UIFont(name: "helvetica-bold", size: 14)
        cabecera.text = "Tus órdenes médicas pendientes"
        
        
        /*mostrar iniciales*/
        btAdd.isHidden = true
        //barname.hidden = true
        cabecera.isHidden = true
            
        self.view.addSubview(btAdd)
        //self.view.addSubview(barname)
        self.view.addSubview(cabecera)
        
        //cargardatos()
        //self.getFamilia()
        //Usuario.createButtonFamily(controlador: self)
        
        show_resu_lab()
        
    }
    
    func getFamilyService(){
        Usuario.getJsonFamilia(controlador: self)
    }

    private func cargardatos(){
        /*Todo este codigo lo pase al menu, ahora ahi se consultan las recomendaciones*/
    }
    
    
    func show_resu_lab(){
            btAdd.isHidden = false
            //barname.hidden = false
            cabecera.isHidden = false
            
        tableView.frame = CGRect(x:0, y: 240, width: self.view.frame.width, height: self.view.frame.height - 240)
            
            tableView.delegate = self
            tableView.dataSource = self
            tableView.backgroundColor = UIColor.clear
            tableView.layer.cornerRadius = 10;
            //tableView.scrollEnabled = false
            tableView.rowHeight = 10
        tableView.layer.zPosition = 0
            
            tableView.separatorColor = UIColor.clear
            
            tableView.register(RecomendacionesCell.self as AnyClass, forCellReuseIdentifier: "recomendacionCell");
            
            self.view.addSubview(tableView)
            // self.contentView.addSubview(btn_descarga)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lista_recomendacion.count;
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "recomendacionCell", for: indexPath) as! RecomendacionesCell
        let resul_rec = lista_recomendacion.object(at: indexPath.row) as! ObjRecomendacion
        cell.nom_medicio.text = "Dr(a) "+(resul_rec.nombremedicointerno as String)
        cell.nom_serv.text = resul_rec.nombreservicio as String
        let fecha : NSDate  = dateFormatter.date(from: resul_rec.fechaorden as String)! as NSDate
        let myCalendar : NSCalendar = NSCalendar.current as NSCalendar
        let myComponents = myCalendar.components([NSCalendar.Unit.year, NSCalendar.Unit.day, NSCalendar.Unit.month, NSCalendar.Unit.hour, NSCalendar.Unit.minute], from: fecha as Date)
        /*cambio mes -1 a nada.*/
        cell.fecha.text = "\(myComponents.day!)/\(myComponents.month!)/\(myComponents.year!)"
        cell.btn_ver_detalle.tag = indexPath.row;
        //println("btn ver detalle")
        //println(indexPath.row)
        cell.btn_ver_detalle.addTarget(self, action: #selector(miEvento(sender:)), for: .touchUpInside)
        cell.btn_agendar.addTarget(self, action: #selector(agendar_recomendacion(sender:)), for: .touchUpInside)
        cell.btn_agendar.tag = indexPath.row
    
        if (resul_rec.esagendable.isEqual(to: "S"))
        {
            cell.btn_agendar.isHidden = false
            cell.btn_ver_detalle.isHidden = true
        }
        else
        {
            cell.btn_ver_detalle.isHidden = false
            cell.btn_agendar.isHidden = true
        }
        
        return cell
        
    }
    func agendar_recomendacion(sender:UIButton!)
    {
        
        let resul = lista_recomendacion.object(at: sender.tag) as! ObjRecomendacion
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "viewReservar") as! ReservaViewController
        vc.tipodecarga = TipoCargaReserva.recomendacion
        vc.recomendacionsel = resul
        
        self.setReload = true
        print("numero orden seleccionad : \(resul.numeroorden)")
        print("codprestacion seleccionad : \(resul.codigoprestacion)")
        print(resul.nombreespecialidad)
        print(resul.nombreprestacion)
        navigationController?.pushViewController(vc, animated: true)
    }
    func miEvento(sender:UIButton!)
    {
        //UtilidadesGeneral.mensaje(resul_l.rutaarchivo)
        //println("Test ver resultado")
        let resul_rec = lista_recomendacion.object(at: sender.tag) as! ObjRecomendacion
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "viewVerDetalle") as! VerDetalleController
        
        vc.cod_empresa = resul_rec.codigoempresa
        vc.cod_recomendacion = resul_rec.numeroorden
        navigationController?.pushViewController(vc, animated: true)
        
    }
    func agendar(sender:UIButton!)
    {
        let us = ls_usuario.object(at: pickerUsuario.eleccion) as! Usuario
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "viewReservar") as! ReservaViewController
        vc.tipodecarga = TipoCargaReserva.nueva
        vc.identificacion_recibida = us.numeroidentificacion as String
        print("identifiacacion que va : \(us.numeroidentificacion)")
        vc.tipoid_recibida = us.tipoid as String
        vc.usuarioR = us
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 97
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: NSURLSession download in background
    
    //cambios andres cantos 2 julio 2015
    private func consultarrecomendaciones(){
        self.lista_recomendacion.removeAllObjects()
        let user = Usuario.getEntity as Usuario
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        
        var pagodir = "N"
        let defaults = UserDefaults.standard
        let varpagodire : Bool = defaults.bool(forKey: "pagodirecto")
        
        if varpagodire{
            pagodir = "S"
            print("pago directo S")
        }else{
            pagodir = "N"
            print("pago directo N")
        }
        
        let url2 = URLFactory.obtenerRecomendaciones(tipo_doc: user.tipoid as String, num_doc: user.numeroidentificacion as String, desconocido: pagodir)
        //println(url)
        print("la url que estoy enviando es : \(url2)")
        
        let url = NSURL(string: "\(url2)")
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        let str = "wsappusuario:ZA$57@9b86@$2r5"
        
        _ = str.data(using: String.Encoding.utf8)
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            //println(NSString(data: data, encoding: NSUTF8StringEncoding))
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                var err: NSError?
                
                //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                if (err != nil) {
                    print("JSON Error \(err!.localizedDescription)")
                }
                let info =  jsonResult as NSDictionary
                let data = info["lista"] as! [[String : Any]]
                print("data \(data)")
                if data.count > 0 {
                    //self.lista_resul_img = NSMutableArray();
                    for jsonobj  in data {
                        let recomendacion = ObjRecomendacion()
                        
                        if (jsonobj["numeroorden"] is NSNull){
                            recomendacion.numeroorden = "0"
                        }else
                        {
                            recomendacion.numeroorden = jsonobj["numeroorden"] as! NSString
                        }
                        if (jsonobj["codigoempresa"] is NSNull){
                            recomendacion.codigoempresa = 0
                        }else
                        {
                            recomendacion.codigoempresa = jsonobj["codigoempresa"] as! NSInteger
                        }
                        if (jsonobj["lineadetalle"] is NSNull){
                            recomendacion.lineadetalle = "0"
                        }else
                        {
                            recomendacion.lineadetalle = jsonobj["lineadetalle"] as! NSString
                        }
                        if (jsonobj["eschequeo"] is NSNull){
                            recomendacion.eschequeo = ""
                        }else
                        {
                            recomendacion.eschequeo = jsonobj["eschequeo"] as! NSString
                        }
                        if (jsonobj["numeroidentificacionpaciente"] is NSNull){
                            recomendacion.numeroidentificacionpaciente = 0
                        }else
                        {
                            recomendacion.numeroidentificacionpaciente = jsonobj["numeroidentificacionpaciente"] as! NSInteger
                        }
                        if (jsonobj["codigotipoidentificacion"] is NSNull){
                            recomendacion.codigotipoidentificacion = ""
                        }else
                        {
                            recomendacion.codigotipoidentificacion = jsonobj["codigotipoidentificacion"] as! NSString
                        }
                        if (jsonobj["nombrepaciente"] is NSNull){
                            recomendacion.nombrepaciente = ""
                        }else
                        {
                            recomendacion.nombrepaciente = jsonobj["nombrepaciente"] as! NSString
                        }
                        if (jsonobj["codigomedico"] is NSNull){
                            recomendacion.codigomedico = ""
                        }else
                        {
                            recomendacion.codigomedico = jsonobj["codigomedico"] as! NSString
                        }
                        if (jsonobj["nombremedicointerno"] is NSNull){
                            recomendacion.nombremedicointerno = ""
                        }else
                        {
                            recomendacion.nombremedicointerno = jsonobj["nombremedicointerno"] as! NSString
                        }
                        
                        if (jsonobj["nombremedicoexterno"] is NSNull){
                            recomendacion.nombremedicoexterno = ""
                        }else
                        {
                            recomendacion.nombremedicoexterno = jsonobj["nombremedicoexterno"] as! NSString
                        }
                        if (jsonobj["codigoprestacion"] is NSNull){
                            recomendacion.codigoprestacion = "0"
                        }else
                        {
                            recomendacion.codigoprestacion = jsonobj["codigoprestacion"] as! NSString
                        }
                        if (jsonobj["nombreprestacion"] is NSNull){
                            recomendacion.nombreprestacion = ""
                        }else
                        {
                            recomendacion.nombreprestacion = jsonobj["nombreprestacion"] as! NSString
                        }
                        if (jsonobj["codigoservicio"] is NSNull){
                            recomendacion.codigoservicio = ""
                        }else
                        {
                            recomendacion.codigoservicio = jsonobj["codigoservicio"] as! NSString
                        }
                        if (jsonobj["nombreservicio"] is NSNull){
                            recomendacion.nombreservicio = ""
                        }else
                        {
                            recomendacion.nombreservicio = jsonobj["nombreservicio"] as! NSString
                        }
                        if (jsonobj["codigoestado"] is NSNull){
                            recomendacion.codigoestado = ""
                        }else
                        {
                            recomendacion.codigoestado = jsonobj["codigoestado"] as! NSString
                        }
                        if (jsonobj["requiereagendamientoprevio"] is NSNull){
                            recomendacion.requiereagendamientoprevio = ""
                        }else
                        {
                            recomendacion.requiereagendamientoprevio = jsonobj["requiereagendamientoprevio"] as! NSString
                        }
                        if (jsonobj["codigoespecialidad"] is NSNull){
                            recomendacion.codigoespecialidad = 0
                        }else
                        {
                            recomendacion.codigoespecialidad = jsonobj["codigoespecialidad"] as! NSInteger
                        }
                        if (jsonobj["esprocedimiento"] is NSNull){
                            recomendacion.esprocedimiento = ""
                        }else
                        {
                            recomendacion.esprocedimiento = jsonobj["esprocedimiento"] as! NSString
                        }
                        if (jsonobj["fechaorden"] is NSNull){
                            recomendacion.fechaorden = ""
                        }else
                        {
                            recomendacion.fechaorden = jsonobj["fechaorden"] as! NSString
                        }
                        if (jsonobj["codigopaquete"] is NSNull){
                            recomendacion.codigopaquete = 0
                        }else
                        {
                            recomendacion.codigopaquete = jsonobj["codigopaquete"] as! NSInteger
                        }
                        if (jsonobj["codigocpt"] is NSNull){
                            recomendacion.codigocpt = ""
                        }else
                        {
                            recomendacion.codigocpt = jsonobj["codigocpt"] as! NSString
                        }
                        if (jsonobj["codigoalterno"] is NSNull){
                            recomendacion.codigoalterno = ""
                        }else
                        {
                            recomendacion.codigoalterno = jsonobj["codigoalterno"] as! NSString
                        }
                        if (jsonobj["esagendable"] is NSNull){
                            recomendacion.esagendable = ""
                        }else
                        {
                            recomendacion.esagendable = jsonobj["esagendable"] as! NSString
                        }
                        if (jsonobj["nombreespecialidad"] is NSNull){
                            recomendacion.nombreespecialidad = ""
                        }else
                        {
                            recomendacion.nombreespecialidad = jsonobj["nombreespecialidad"] as! NSString
                        }
                        if (jsonobj["codigoespecialidadmedico"] is NSNull){
                            recomendacion.codigoespecialidadmedico = 0
                        }else
                        {
                            recomendacion.codigoespecialidadmedico = jsonobj["codigoespecialidadmedico"] as! NSInteger
                        }
                        if (jsonobj["esconsulta"] is NSNull){
                            recomendacion.esconsulta = ""
                        }else
                        {
                            recomendacion.esconsulta = jsonobj["esconsulta"] as! NSString
                        }
                        self.lista_recomendacion.add(recomendacion)
                       // self.getFamilia()
                    }
                }
                
                //dispatch_async(dispatch_get_main_queue(), {
                DispatchQueue.main.async(execute: {
                    //self.cargapantallarecomendacion()
                    self.show_resu_lab()
                    self.tableView.reloadData()
                    self.getFamilia()
                    ViewUtilidades.hiddenLoader(controller: self)
                })
                } catch {
                    print("ERROR")
                }
            }
            
            
        }
        //////////////////////////
        
        
    }
    /*funcion para traer los miembros de la familia */
    
    func getFamilia(){
        //self.ls_usuario.removeAllObjects()
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        let user = Usuario.getEntity as Usuario
        let urldef = URLFactory.urldefinida()

        let url = NSURL(string: "\(urldef)servicio/persona/grupo/\(user.tipoid)-\(user.numeroidentificacion)")
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
       
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                    var err: NSError?
                    
                    //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    if (err != nil) {
                        print("JSON Error \(err!.localizedDescription)")
                    }
                    
                    //println(jsonResult);
                    //print(jsonResult)
                    let info =  jsonResult as NSDictionary
                    let data = info["lista"] as! [[String : Any]]
                    if data.count > 0 {
                        for item in data {
                            let familiar = Usuario()
                            
                            if let tip = item["tipoIdentificacion"] as? String {
                                familiar.tipoid = tip as NSString
                            }
                            if let num = item["numeroIdentificacion"] as? String {
                                familiar.numeroidentificacion = num as NSString
                            }
                            if let num = item["numeroIdentificacion"] as? String {
                                familiar.numeroidentificacion = num as NSString
                            }
                            if let nombre1 = item["primerNombre"] as? String {
                                familiar.nombre = nombre1 as NSString
                            }
                            if let apellido1 = item["primerApellido"] as? String {
                                familiar.nombre = "\(familiar.nombre) \(apellido1)" as NSString
                            }
                            if let apellido2 = item["segundoApellido"] as? String {
                                familiar.nombre = "\(familiar.nombre) \(apellido2)" as NSString
                            }
                            familiar.nombre = familiar.nombre.removingPercentEncoding! as NSString
                            //as? String).removingPercentEncoding as NSString
                            //stringByRemovingPercentEncoding!
                            var nombres : String = familiar.nombre as String
                            nombres = nombres.replacingOccurrences(of: "+", with: "", options: String.CompareOptions.literal, range: nil)
                            
                            nombres = nombres.replacingOccurrences(of: "  ", with: "", options: String.CompareOptions.literal, range: nil)
                            //print(nombres)
                            familiar.nombre = nombres as NSString
                            
                            self.ls_usuario.add(familiar)
                        }
                    }
                    
                    
                    var lista = [""]
                    /*for provi in self.ls_usuario {
                        let nombre = (provi as AnyObject).value("nombre")
                      //  let nombre = "\(provi.valueForKey("nombre")!)"
                        lista.append(nombre)
                        //lista.append(provi.value("nombre") as! String)
                        //lista.addObject(provi.nombre)
                    }*/
                    //arreglarfor in
                    for  i in 0..<self.ls_usuario.count{
                     //print("encontro 1 \((self.ls_usuario[i] as AnyObject).value(forKey: "nombre"))")
                        let nombre = (self.ls_usuario[i] as AnyObject).value(forKey: "nombre")
                        lista.append(nombre as! String)
                    }
                   /* if let a = self.ls_usuario as? NSMutableArray {
                                print("a: \(a.value(forKey: "nombre"))")
                        //let nombre = a.value(forKey: "nombre")"
                        //lista.append(a.value(forKey: "nombre") as! String)
                    }*/
                    lista.remove(at: 0)
                    
                    self.dataUsuario = lista
                    self.txt_usuario.text = self.dataUsuario[0]
                    self.pickerUsuario = PickerList(datos: self.dataUsuario as NSArray)
                    
                    ViewUtilidades.hiddenLoader(controller: self)
                    //self.getCiudades()
                } catch {
                    print("ERROR")
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: "Revise su conexión a Datos o Internet", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }

    /*funciones para elegir usuario*/
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if(textField == txt_usuario){
            self.hideKeyboard()
            
        }
        return true
    }
    
    func hideKeyboard(){
        print("hidekeyboard")
        self.txt_usuario.resignFirstResponder()
        
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if (textField == txt_usuario){
            pickerUsuario.showPicker(controller: self)
            pickerSelected = pickerUsuario
            txt_usuario.resignFirstResponder()
        }
    }
    
    func cancelPicker(){
        pickerSelected.hidePicker()
        txt_usuario.resignFirstResponder()
    }
    
    func acceptPicker(){
        pickerSelected.hidePicker()
        txt_usuario.text = dataUsuario[pickerUsuario.eleccion]
        
        txt_usuario.resignFirstResponder()
        //consultarrecomendaciones()
        verlasrecomendaciones()
    }
    func verlasrecomendaciones(){
        let user = Usuario.getEntity as Usuario
        print("el usuario activo es: \(user.nombre)")
        var tipo : String = user.tipoid as String
        var numero : String = user.numeroidentificacion as String
        
        for i in 0..<ls_usuario.count{
            if (ls_usuario[i] as AnyObject).nombre as String == txt_usuario.text!{
                print("entro y debo de busar con id : \((ls_usuario[i] as AnyObject).numeroidentificacion! )")
                tipo = (ls_usuario[i] as AnyObject).tipoid as String
                numero = (ls_usuario[i] as AnyObject).numeroidentificacion as String
                
            }
        }
        self.consultarrecomendaciones2(tipo: tipo, numero: numero)
    }
    
    private func consultarrecomendaciones2(tipo: String, numero : String){
        self.lista_recomendacion.removeAllObjects()
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        
        var pagodir = "N"
        let defaults = UserDefaults.standard
        let varpagodire : Bool = defaults.bool(forKey: "pagodirecto")
        
        if varpagodire{
            pagodir = "S"
            print("pago directo S")
        }else{
            pagodir = "N"
            print("pago directo N")
        }
        
        let url2 = URLFactory.obtenerRecomendaciones(tipo_doc: tipo, num_doc: numero, desconocido: pagodir)
        //println(url)
        print("la url que estoy enviando es : \(url2)")
        
        let url = NSURL(string: "\(url2)")
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            //println(NSString(data: data, encoding: NSUTF8StringEncoding))
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                    var err: NSError?
                    
                    //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    if (err != nil) {
                        print("JSON Error \(err!.localizedDescription)")
                    }
                    let info =  jsonResult as NSDictionary
                    let data = info["lista"] as! [[String : Any]]
                    print("data \(data)")
                    if data.count > 0 {
                        //self.lista_resul_img = NSMutableArray();
                        for jsonobj  in data {
                            let recomendacion = ObjRecomendacion()
                            
                            if (jsonobj["numeroorden"] is NSNull){
                                recomendacion.numeroorden = "0"
                            }else
                            {
                                recomendacion.numeroorden = jsonobj["numeroorden"] as! NSString
                            }
                            if (jsonobj["codigoempresa"] is NSNull){
                                recomendacion.codigoempresa = 0
                            }else
                            {
                                recomendacion.codigoempresa = jsonobj["codigoempresa"] as! NSInteger
                            }
                            if (jsonobj["lineadetalle"] is NSNull){
                                recomendacion.lineadetalle = "0"
                            }else
                            {
                                recomendacion.lineadetalle = jsonobj["lineadetalle"] as! NSString
                            }
                            if (jsonobj["eschequeo"] is NSNull){
                                recomendacion.eschequeo = ""
                            }else
                            {
                                recomendacion.eschequeo = jsonobj["eschequeo"] as! NSString
                            }
                            if (jsonobj["numeroidentificacionpaciente"] is NSNull){
                                recomendacion.numeroidentificacionpaciente = 0
                            }else
                            {
                                recomendacion.numeroidentificacionpaciente = jsonobj["numeroidentificacionpaciente"] as! NSInteger
                            }
                            if (jsonobj["codigotipoidentificacion"] is NSNull){
                                recomendacion.codigotipoidentificacion = ""
                            }else
                            {
                                recomendacion.codigotipoidentificacion = jsonobj["codigotipoidentificacion"] as! NSString
                            }
                            if (jsonobj["nombrepaciente"] is NSNull){
                                recomendacion.nombrepaciente = ""
                            }else
                            {
                                recomendacion.nombrepaciente = jsonobj["nombrepaciente"] as! NSString
                            }
                            if (jsonobj["codigomedico"] is NSNull){
                                recomendacion.codigomedico = ""
                            }else
                            {
                                recomendacion.codigomedico = jsonobj["codigomedico"] as! NSString
                            }
                            if (jsonobj["nombremedicointerno"] is NSNull){
                                recomendacion.nombremedicointerno = ""
                            }else
                            {
                                recomendacion.nombremedicointerno = jsonobj["nombremedicointerno"] as! NSString
                            }
                            
                            if (jsonobj["nombremedicoexterno"] is NSNull){
                                recomendacion.nombremedicoexterno = ""
                            }else
                            {
                                recomendacion.nombremedicoexterno = jsonobj["nombremedicoexterno"] as! NSString
                            }
                            if (jsonobj["codigoprestacion"] is NSNull){
                                recomendacion.codigoprestacion = "0"
                            }else
                            {
                                recomendacion.codigoprestacion = jsonobj["codigoprestacion"] as! NSString
                            }
                            if (jsonobj["nombreprestacion"] is NSNull){
                                recomendacion.nombreprestacion = ""
                            }else
                            {
                                recomendacion.nombreprestacion = jsonobj["nombreprestacion"] as! NSString
                            }
                            if (jsonobj["codigoservicio"] is NSNull){
                                recomendacion.codigoservicio = ""
                            }else
                            {
                                recomendacion.codigoservicio = jsonobj["codigoservicio"] as! NSString
                            }
                            if (jsonobj["nombreservicio"] is NSNull){
                                recomendacion.nombreservicio = ""
                            }else
                            {
                                recomendacion.nombreservicio = jsonobj["nombreservicio"] as! NSString
                            }
                            if (jsonobj["codigoestado"] is NSNull){
                                recomendacion.codigoestado = ""
                            }else
                            {
                                recomendacion.codigoestado = jsonobj["codigoestado"] as! NSString
                            }
                            if (jsonobj["requiereagendamientoprevio"] is NSNull){
                                recomendacion.requiereagendamientoprevio = ""
                            }else
                            {
                                recomendacion.requiereagendamientoprevio = jsonobj["requiereagendamientoprevio"] as! NSString
                            }
                            if (jsonobj["codigoespecialidad"] is NSNull){
                                recomendacion.codigoespecialidad = 0
                            }else
                            {
                                recomendacion.codigoespecialidad = jsonobj["codigoespecialidad"] as! NSInteger
                            }
                            if (jsonobj["esprocedimiento"] is NSNull){
                                recomendacion.esprocedimiento = ""
                            }else
                            {
                                recomendacion.esprocedimiento = jsonobj["esprocedimiento"] as! NSString
                            }
                            if (jsonobj["fechaorden"] is NSNull){
                                recomendacion.fechaorden = ""
                            }else
                            {
                                recomendacion.fechaorden = jsonobj["fechaorden"] as! NSString
                            }
                            if (jsonobj["codigopaquete"] is NSNull){
                                recomendacion.codigopaquete = 0
                            }else
                            {
                                recomendacion.codigopaquete = jsonobj["codigopaquete"] as! NSInteger
                            }
                            if (jsonobj["codigocpt"] is NSNull){
                                recomendacion.codigocpt = ""
                            }else
                            {
                                recomendacion.codigocpt = jsonobj["codigocpt"] as! NSString
                            }
                            if (jsonobj["codigoalterno"] is NSNull){
                                recomendacion.codigoalterno = ""
                            }else
                            {
                                recomendacion.codigoalterno = jsonobj["codigoalterno"] as! NSString
                            }
                            if (jsonobj["esagendable"] is NSNull){
                                recomendacion.esagendable = ""
                            }else
                            {
                                recomendacion.esagendable = jsonobj["esagendable"] as! NSString
                            }
                            if (jsonobj["nombreespecialidad"] is NSNull){
                                recomendacion.nombreespecialidad = ""
                            }else
                            {
                                recomendacion.nombreespecialidad = jsonobj["nombreespecialidad"] as! NSString
                            }
                            if (jsonobj["codigoespecialidadmedico"] is NSNull){
                                recomendacion.codigoespecialidadmedico = 0
                            }else
                            {
                                recomendacion.codigoespecialidadmedico = jsonobj["codigoespecialidadmedico"] as! NSInteger
                            }
                            if (jsonobj["esconsulta"] is NSNull){
                                recomendacion.esconsulta = ""
                            }else
                            {
                                recomendacion.esconsulta = jsonobj["esconsulta"] as! NSString
                            }
                            self.lista_recomendacion.add(recomendacion)
                            // self.getFamilia()
                        }
                    }
                    
                    //dispatch_async(dispatch_get_main_queue(), {
                    DispatchQueue.main.async(execute: {
                        //self.cargapantallarecomendacion()
                        self.show_resu_lab()
                        self.tableView.reloadData()
                        //self.getFamilia()
                        ViewUtilidades.hiddenLoader(controller: self)
                    })
                } catch {
                    print("ERROR")
                }
            }
            
            
        }
        //////////////////////////
        
        
    }
}
