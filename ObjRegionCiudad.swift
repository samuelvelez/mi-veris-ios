//
//  ObjRegionCiudad.swift
//  Mi Veris
//
//  Created by Andres Cantos on 2/6/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import UIKit

// MARK: Region: Clase región

class ObjRegionCiudad: NSObject {
    
    var codigopais : NSInteger = 0
    var codigoprovincia : NSInteger = 0
    var codigoregion : NSInteger = 0
    var codigociudad : NSInteger = 0
    var nombreciudad : String = ""
    
}
