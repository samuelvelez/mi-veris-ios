//
//  ViewController.swift
//  Mi Veris
//
//  Created by Jorge on 04/02/15.
//  Copyright (c) 2015 Firewall Soluciones All rights reserved.
//

import UIKit
import EventKit

class MisResultadosViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var not : Bool = false
    
    var tableView: UITableView  =   UITableView()
    var items:[String] = ["Resultados de Laboratorio", "Resultados de Imágenes", "Resultados de Procedimientos"];
    var imagenes : [String] = ["RESULTADOS LABORATORIO 2.png", "RESULTADOS IMG 2.png", "resultadoproc.png" ];
    var lista_recomendacion : NSMutableArray =  NSMutableArray()
    var setPush : Bool = false
    var selectedCell : NSInteger = 0
    
    @IBOutlet weak var btLogout: UIBarButtonItem!
    
    @IBOutlet weak var btacercade: UIBarButtonItem!
    
    var video : NSString = ""
    var contenidoNotificacion : NSString = ""
    var newNotificacion : Bool = false
    
    var nombreUser : UILabel!
    

    override func viewDidAppear(_ animated: Bool) {
        let user = Usuario.getEntity as Usuario
        
        Usuario.newcreateButtonFamilyTop(controlador: self, text: user.nombre as String)

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.style();
        if(not){
            let backButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(MisResultadosViewController.goBack))
            navigationItem.leftBarButtonItem = backButton
        }
        
        let barname : UIView = UIView(frame: CGRect(x:0, y: 64, width: self.view.frame.width,height: 30))
        barname.backgroundColor = UtilidadesColor.colordehexadecimal(hex: 0x000000, alpha: 0.1)
        
        nombreUser = UILabel(frame: CGRect(x: 10, y: 2, width: self.view.frame.width - 20, height: 30))
        nombreUser.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        nombreUser.font = UIFont(name: "helvetica-bold", size: 12)
        nombreUser.text = "MIS RESULTADOS"
        //barname.addSubview(nombreUser)
        //self.view.addSubview(barname)
        
        let barWhite : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width , height: 30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width / 2.6, height: 30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x: barBlue.frame.width, y: 64, width: 45, height: 30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x: 0, y: 64, width:self.view.frame.width - 20, height: 30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "Mis Resultados"
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)
        
        
        //btLogout.tintColor = UIColor(red: (0/256.0), green: (167/256.0), blue: (157/256.0), alpha: 1)
        
        // Configurar la tabla

        tableView.frame = CGRect(x: 0, y: 154, width:self.view.frame.width, height: self.view.frame.height - 94);
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.backgroundColor = UIColor.clear
        tableView.register(CellTableViewCell.self as AnyClass, forCellReuseIdentifier: "MenuCell");
        
        self.view.addSubview(tableView);
        
        
    }
    func goBack(){
        dismiss(animated: true, completion: nil)
    }
    
    func getFamilyService(){
        Usuario.getJsonFamilia(controlador: self)
        //setReload = true
    }

    
    // MARK: style: Estilo del controlador
    
    func style ()
    {
        
        self.navigationController?.navigationItem.backBarButtonItem?.title = ""
        
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        let fondo : UIImageView = UIImageView(frame: self.view.bounds)
        fondo.image = UIImage(named:"back.png")
        self.view.addSubview(fondo)
        
        
    }
    
    
    
    // MARK: Métodos de tableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count;
    }
    
    /*func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.separatorInset = UIEdgeInsetsZero
        cell.layoutMargins = UIEdgeInsetsZero
    }*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! CellTableViewCell
        
        
        cell.icono.image = UIImage(named: imagenes[indexPath.row])
        cell.title.text = items[indexPath.row]
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let usuario = Usuario.getEntity as Usuario
            if usuario.logueado {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "viewresulab") as! ResultadosLabController
                navigationController?.pushViewController(vc, animated: true)
            } else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "viewLogin") as! LoginViewController
                navigationController?.pushViewController(vc, animated: true)
            }
        } else if indexPath.row == 1 {
            let usuario = Usuario.getEntity as Usuario
            if usuario.logueado {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "viewresuimg") as! ResultadosImagController
                navigationController?.pushViewController(vc, animated: true)
            } else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "viewLogin") as! LoginViewController
                navigationController?.pushViewController(vc, animated: true)
            }
        } else if indexPath.row == 2 {
            let usuario = Usuario.getEntity as Usuario
            if usuario.logueado {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "viewResultadosProcedimientos") as! ResultadosProcController
                navigationController?.pushViewController(vc, animated: true)
            } else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "viewLogin") as! LoginViewController
                navigationController?.pushViewController(vc, animated: true)
            }
            
        }
        selectedCell = indexPath.row
        tableView.deselectRow(at: indexPath, animated: true);
    }
    
    func showAlertLogout(){
        let alert = UIAlertController(title: "Veris", message: "Usted ha cerrado sesión", preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
            UIAlertAction in
            
            
        }
        
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlert(){
        let alert = UIAlertController(title: "Veris", message: "¿Esta seguro de cerrar sesión?", preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
            UIAlertAction in
            
            Usuario.deleteLogin()
            
            //var parent = Usuario.getParent as Usuario
            //parent = Usuario()
            
           // let usuario = Usuario.getEntity as Usuario
            //usuario = Usuario()
            
            UserDefaults.standard.set("", forKey: "identificacion")
            //NSUserDefaults.standardUserDefaults().setObject(newValue as [NSString], forKey: "food")
            UserDefaults.standard.synchronize()
            
            UserDefaults.standard.set("", forKey: "nombre")
            //NSUserDefaults.standardUserDefaults().setObject(newValue as [NSString], forKey: "food")
            UserDefaults.standard.synchronize()
            
            UserDefaults.standard.set("", forKey: "tipo")
            UserDefaults.standard.synchronize()
            
            UserDefaults.standard.set(0, forKey: "ciudad")
            UserDefaults.standard.synchronize()
            
            UserDefaults.standard.set(0, forKey: "region")
            UserDefaults.standard.synchronize()
            
            UserDefaults.standard.set(0, forKey: "pais")
            UserDefaults.standard.synchronize()
            
            UserDefaults.standard.set(0, forKey: "provincia")
            UserDefaults.standard.synchronize()
            
            self.btLogout.isEnabled = false
            //self.btLogout.tintColor = UIColor.clearColor()
            //self.navigationItem.rightBarButtonItem.h
            //btLogout.enabled = false
            //btLogout.
            self.showAlertLogout()
            
            
        }
        let cancelAction = UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            
        }
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func cerrarSesion(sender: AnyObject) {
        self.showAlert()
    }
    
    
    
    @IBAction func verAcercaDe(sender: AnyObject) {
        print("Mostrar pantalla de acerca de ")
        /*let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("viewAcerca") as! AcercaDe
        navigationController?.pushViewController(vc, animated: true)
 */
    }
    
    
    
}

