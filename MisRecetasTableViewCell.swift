//
//  MisRecetasTableViewCell.swift
//  veris
//
//  Created by Felipe Lloret on 06/02/15.
//  Copyright (c) 2015 Felipe Lloret. All rights reserved.
//

import UIKit

class MisRecetasTableViewCell: UITableViewCell {
    
    var gradientView: UIView = UIView()
    var firstLineView: UIView = UIView()
    var secondLineView: UIView = UIView()
    var thirdLineView: UIView = UIView()
    
    var blueArrow : UIImageView = UIImageView()
    var shareImage: UIImageView = UIImageView()
    
    var sucursalLabel: UILabel = UILabel()
    var fechaLabel: UILabel = UILabel()
    var doctorLabel: UILabel = UILabel()
    var pacienteLabel: UILabel = UILabel()
    var prestacionLabel : UILabel = UILabel()

    var verRecetaButton: UIButton = UIButton()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width;
        
        gradientView.frame = CGRect(x:0.0, y:0.0, width:screenWidth, height:110.0)
        gradientView.backgroundColor = UIColor.lightGray
        gradientView.alpha = 0.3
        self.contentView.addSubview(gradientView)
        
        firstLineView.frame = CGRect(x:0.0, y:0.0, width:screenWidth, height:1.0)
        firstLineView.backgroundColor = UIColor.black
        self.contentView.addSubview(firstLineView)
        
        secondLineView.frame = CGRect(x:0.0, y:24.0,width: screenWidth, height:1.0)
        secondLineView.backgroundColor = UIColor.black
        self.contentView.addSubview(secondLineView)
        
        thirdLineView.frame = CGRect(x:0.0, y:109.0, width:screenWidth, height:1.0)
        thirdLineView.backgroundColor = UIColor.black
        self.contentView.addSubview(thirdLineView)
        
        blueArrow.frame = CGRect(x:0.0, y:0.0, width:17.0, height:26.0)
        blueArrow.image = UIImage(named: "BlueArrow")
        self.contentView.addSubview(blueArrow)
        
        pacienteLabel.frame = CGRect(x:21.0, y:-15.0, width:screenWidth, height:16.0)
        pacienteLabel.numberOfLines = 1
        pacienteLabel.font = UIFont(name: "helvetica-bold", size: 12.0)
        pacienteLabel.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        self.contentView.addSubview(pacienteLabel)
        
        
        sucursalLabel.frame = CGRect(x:21.0, y:5.0, width:140.0, height:16.0)
        sucursalLabel.numberOfLines = 1
        sucursalLabel.font = UIFont(name: "helvetica-bold", size: 12.0)
        sucursalLabel.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        self.contentView.addSubview(sucursalLabel)
        
        fechaLabel.frame = CGRect(x:screenWidth - 121.0 - 45.0, y:5.0, width:135.0, height:16.0)
        fechaLabel.numberOfLines = 1
        fechaLabel.font = UIFont(name: "helvetica", size: 12)
        self.contentView.addSubview(fechaLabel)
        
        shareImage.frame = CGRect(x:screenWidth - 40.0, y:-2.0, width:30.0, height:30.0)
        shareImage.image = UIImage(named: "ShareButton")
        self.contentView.addSubview(shareImage)
        
        doctorLabel.frame = CGRect(x:50.0, y:30.0, width:screenWidth, height:20)
        doctorLabel.numberOfLines = 1
        doctorLabel.font = UIFont(name: "helvetica-bold", size: 11.0)
        self.contentView.addSubview(doctorLabel)
        
        prestacionLabel.frame = CGRect(x:65.0, y:45.0, width:250.0, height:40.0)
        prestacionLabel.numberOfLines = 2
        prestacionLabel.lineBreakMode = .byWordWrapping        
        prestacionLabel.font = UIFont(name: "helvetica", size: 12.0)
        self.contentView.addSubview(prestacionLabel)
        
        verRecetaButton.frame = CGRect(x:200.0, y:70.0, width:105.0, height:39.0)
        verRecetaButton.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        verRecetaButton.setTitle("VER RECETA", for: UIControlState.normal)
        verRecetaButton.setTitleColor(UIColor.white, for: UIControlState.normal)
        verRecetaButton.titleLabel!.font = UIFont(name: "helvetica-bold", size: 12.0)
        self.contentView.addSubview(verRecetaButton)
        
        self.backgroundColor = UIColor.clear
        self.selectionStyle = UITableViewCellSelectionStyle.none;
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    //Con esta función haremos más simple la adjudicación a las labels desde MisRecetasViewController
    func setCell(sucursalLabelText: String, fechaLabelText: String, doctorLabelText: String, prestacionLabelText: String, pacienteText: String) {
        self.pacienteLabel.text = pacienteText
        self.sucursalLabel.text = sucursalLabelText
        self.fechaLabel.text = fechaLabelText
        self.doctorLabel.text = doctorLabelText
        self.prestacionLabel.text = prestacionLabelText
    }

}
