//
//  MisCitasViewController.swift
//  veris
//
//  Created by Felipe Lloret on 05/02/15.
//  Copyright (c) 2015 Felipe Lloret. All rights reserved.
//

import UIKit

class MisCitasViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate {
    
    var citas = [MisCitasModel]()
    var tableView : UITableView = UITableView()
    var setReload : Bool = false
    
    var nombreUser : UILabel = UILabel()
    var servProd : String = "http://miveris.com.ec/"
    var servdesa : String = "http://181.198.116.238:8080/"
    var servact: String = ""
    
    var sgm_forma : UISegmentedControl = UISegmentedControl()
    
    var scroll : UIScrollView = UIScrollView()
    
    var paciente : String = ""
    
    var boolea : Bool = true
    var btAceptar = UIButton()
    var global_esadmin : String  = "N"
    
    override func viewDidAppear(_ animated: Bool) {
        let user = Usuario.getEntity
        ViewUtilidades.showLoader(controller: self, title: "cargando")
        nombreUser.text = user.nombre as String
        self.citas = []
        btAceptar.removeFromSuperview()
        
        Usuario.citasButtonFamilyTop(controlador: self, text: user.nombre as String)
        
        self.paciente = ""
        self.servact = URLFactory.urldefinida()
        //if setReload {
            print("viewdidappear")
            /*mostrarCitas()
            self.tableView.reloadData()
            ViewUtilidades.hiddenLoader(self)
            */self.setReload = false
            
            if user.rol < 4 {
                self.citas = []
               print("entro al iffff")
                
                let log = UserDefaults.standard.object(forKey: "identificacion")
                let t_log = UserDefaults.standard.object(forKey: "tipo")
                
                let iden = "\(t_log!)-\(log!)"
                
                //let iden = "\(tipo_id_p)-\(identificacion)"
                let urldef = URLFactory.urldefinida()
                print(iden)
                let url = NSURL(string: "\(urldef)servicio/persona/grupo/\(iden)?arg0=2")
                let request = NSMutableURLRequest(url: url! as URL)
                var resultado = ""
                // var bool  = false
                NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                    
                    if (error != nil) {
                        print(error!.localizedDescription)
                        resultado = "N"
                    } else {
                        do{
                            var err: NSError?
                            
                            let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                            if (err != nil) {
                                print("JSON Error \(err!.localizedDescription)")
                            }
                            let usuario = Usuario.getEntity as Usuario
                            print(usuario.numeroidentificacion)
                            
                            let info =  jsonResult as NSDictionary
                            let data = info["lista"] as! [[String: Any]]//NSArray
                            // print(data)
                            if data.count > 0 {
                                for familia in data {
                                    if let res = familia["numeroIdentificacion"] as? String {
                                        if res == usuario.numeroidentificacion as String {
                                            resultado = (familia["esAdmin"] as? String)!
                                        }
                                    }
                                }
                            }
                            DispatchQueue.main.async(execute: {
                                
                                let usuario = Usuario.getEntity as Usuario
                                
                                let urlAsString = URLFactory.obtenercitasvigentes(tipoidentificacion: usuario.tipoid as String, identificacion: usuario.numeroidentificacion as String, esadmin: resultado, usua_logeado: log as! String)
                                print("did appear: \(urlAsString)")
                                
                                self.getJSONData(url: urlAsString.absoluteString!)
                                
                                
                            })
                        } catch {
                            print("ERROR")
                        }
                    }
                }
                self.tableView.reloadData()
                //self.revisarPermisos()
        }else{
               // self.mostrarCitas()
               // self.tableView.reloadData()

                
        }
        ViewUtilidades.hiddenLoader(controller: self)
        print("!Aqui!")
        var yacalifico : Bool  = true
        if let yaca = UserDefaults.standard.object(forKey: "califico"){
            yacalifico = yaca as! Bool
        }
        if let varaava  = UserDefaults.standard.object(forKey: "debecalificar"){
            
            print("hola soy \(varaava) - \(yacalifico)")
            
            let booleano : Bool = varaava as! Bool
            if booleano && boolea{
                var timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.llamarCalificar), userInfo: nil, repeats: false);
            
                
            }
        }
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        var frame: CGRect = self.btAceptar.frame
        frame.origin.y = scrollView.contentOffset.y + (UIScreen.main.bounds.height) - 280
        btAceptar.frame = frame
        
        view.bringSubview(toFront: btAceptar)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.servact = servdesa
        
        self.navigationItem.title = ""
        
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        let fondo : UIImageView = UIImageView(frame: self.view.bounds)
        fondo.image = UIImage(named:"fondocita.png")
        self.view.addSubview(fondo)
        
        let barWhite : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width, height: 30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x: 0, y: 64, width:self.view.frame.width / 1.8, height:30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x:barBlue.frame.width, y:64, width:45, height:30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x:0, y:64, width:self.view.frame.width - 20, height:30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "Mis Citas"
        
       
        
        let items = ["Mis Citas", "Historial de Citas"]
        sgm_forma = UISegmentedControl(items: items)
        sgm_forma.selectedSegmentIndex = 0
        sgm_forma.tintColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        sgm_forma.frame = CGRect(x: 0, y: 145,width: self.view.frame.width, height: 38)
        sgm_forma.addTarget(self, action: #selector(MisCitasViewController.verHistorial), for: .valueChanged);
        //sgm_forma.layer.cornerRadius = 0.0
        sgm_forma.layer.cornerRadius = 0.0;
        sgm_forma.layer.borderColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1).cgColor
        //sgm_forma.layer.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1).CGColor
        sgm_forma.layer.borderWidth = 1.5;
        sgm_forma.layer.masksToBounds = true;
        
        
        self.view.addSubview(sgm_forma)
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)
        
        let usuario = Usuario.getEntity as Usuario
        
        
        nombreUser = UILabel(frame: CGRect(x:10, y:94, width:self.view.frame.width - 20, height:30))
        nombreUser.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        nombreUser.font = UIFont(name: "helvetica-bold", size: 12)
        nombreUser.text = usuario.nombre as String
        
        //self.view.addSubview(nombreUser)
        print("entro a misCitasview con el usuario: \(usuario.nombre)")
       
        Usuario.createButtonFamily(controlador: self)
        
        
    }
    
    func getFamilyService(){
        Usuario.getJsonFamilia(controlador: self)
        print("getfamilyservice")
        setReload = true
    }
      func getJSONData(url: String) {
        
        print(url)
        let url = NSURL(string: url as String)
        
        let request = NSMutableURLRequest(url: url! as URL)
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Acetpys", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
//                self.present(alert, animated: true, completion: nil)
                
            } else {
                let json = JSON(data: data!)
                
                //Comprobamos si todo ha salido bien, en caso contrario se le muestra el mensaje de error al usuario
                if json["resultado"].string == "ok" {
                
                    print(json["lista"])
                    if let citasArray = json["lista"].array {
                        for citasDic in citasArray {
                            /*var npaciente = "\(citasDic["primerNombre"]) \(citasDic["segundoNombre"]) \(citasDic["primerApellido"]) \(citasDic["segundoApellido"])"
                            npaciente = npaciente.removingPercentEncoding!
                            npaciente = npaciente.replacingOccurrences(of: "null", with: "", options: NSString.CompareOptions.literal, range: nil)
                            npaciente = npaciente.replacingOccurrences(of: "+", with: " ", options: NSString.CompareOptions.literal, range: nil)
                            if (npaciente != self.paciente){
                                self.paciente = npaciente
                            }else{
                                self.paciente = ""
                            }
                            let newUser = Usuario()
                            newUser.numeroidentificacion = citasDic["numeroIdentificacion"].stringValue as NSString
                            newUser.nombre = npaciente as NSString
                            newUser.tipoid = citasDic["tipoIdentificacion"].stringValue as NSString
                            var user_go : Usuario
                            let user_log = Usuario.getEntity as Usuario
                            if (newUser.numeroidentificacion != user_log.numeroidentificacion){
                                user_go = newUser
                            }else{
                                user_go = user_log
                            }*/
                            //let arr : Array = citasDic["lista"].array!;
                            
                           // if(citasDic["parentesco"].stringValue == "TITULAR"){
                                
                            /*for citashow in arr {
                                let citaFecha: String? = citashow["fecha"].stringValue
                                let citaSucursal: String? = citashow["nombresucursal"].stringValue
                                let citaPrestacion: String? = citashow["nombreespecialidad"].stringValue
                                let citaMedico: String? = citashow["medico"].stringValue
                                let citaPuntuacion: String? = citashow["puntaje"].stringValue
                                let citaReserva: String? = citashow["codigoreserva"].stringValue
                                let hcodigoespecialidad : String? = citashow["codigoespecialidad"].stringValue
                                let hcodigomedico : String? = citashow["codigomedico"].stringValue
                                let hcodigoempresa : String? = citashow["codigoempresa"].stringValue
                                let hcodigosucursal : String? = citashow["codigosucursal"].stringValue
                                let hcodigoregion : String? = citashow["codigoregion"].stringValue
                                /*cambio 23 junio*/
                                let hcodigoprestacion :String? = citashow["codigoprestacion"].stringValue
                                let hcodigoempresaprestacion :String? = citashow["codigoempresaprestacion"].stringValue
                                let hnombreprestacion : String? = citashow["prestacion"].stringValue
                                let estadocita : String? = citashow["esPagada"].stringValue
                                
                                let cita = MisCitasModel(fecha: citaFecha, nombresucursal: citaSucursal, prestacion: citaPrestacion, medico: citaMedico, puntuacion: citaPuntuacion, reserva: citaReserva, codigoespecialidad: hcodigoespecialidad, codigomedico: hcodigomedico, codigoempresa: hcodigoempresa, codigosucursal: hcodigosucursal, codigoregion: hcodigoregion,codigoprestacion: hcodigoprestacion, codigoempresapres: hcodigoempresaprestacion, nombreprestacion: hnombreprestacion, nombrePaciente: "" , usuario: user_go, espagada : estadocita)
                                self.citas.append(cita)

                            }*/
                                
                           // }
                            let citaFecha: String? = citasDic["fecha"].stringValue
                            let citaSucursal: String? = citasDic["nombresucursal"].stringValue
                            let citaPrestacion: String? = citasDic["nombreespecialidad"].stringValue
                            let citaMedico: String? = citasDic["medico"].stringValue
                            let citaPuntuacion: String? = citasDic["puntaje"].stringValue
                            let citaReserva: String? = citasDic["codigoreserva"].stringValue
                            let hcodigoespecialidad : String? = citasDic["codigoespecialidad"].stringValue
                            let hcodigomedico : String? = citasDic["codigomedico"].stringValue
                            let hcodigoempresa : String? = citasDic["codigoempresa"].stringValue
                            let hcodigosucursal : String? = citasDic["codigosucursal"].stringValue
                            let hcodigoregion : String? = citasDic["codigoregion"].stringValue
                            /*cambio 23 junio*/
                            let hcodigoprestacion :String? = citasDic["codigoprestacion"].stringValue
                            let hcodigoempresaprestacion :String? = citasDic["codigoempresaprestacion"].stringValue
                            let hnombreprestacion : String? = citasDic["prestacion"].stringValue
                            let estadocita : String? = citasDic["esPagada"].stringValue
                            let newUser = Usuario()
                            newUser.numeroidentificacion = citasDic["numeroIdentificacion"].stringValue as NSString
                            newUser.nombre = "" 
                            newUser.tipoid = citasDic["tipoIdentificacion"].stringValue as NSString
                            var user_go : Usuario
                            let user_log = Usuario.getEntity as Usuario
                            if (newUser.numeroidentificacion != user_log.numeroidentificacion){
                                user_go = newUser
                            }else{
                                user_go = user_log
                            }
                            let cita = MisCitasModel(fecha: citaFecha, nombresucursal: citaSucursal, prestacion: citaPrestacion, medico: citaMedico, puntuacion: citaPuntuacion, reserva: citaReserva, codigoespecialidad: hcodigoespecialidad, codigomedico: hcodigomedico, codigoempresa: hcodigoempresa, codigosucursal: hcodigosucursal, codigoregion: hcodigoregion,codigoprestacion: hcodigoprestacion, codigoempresapres: hcodigoempresaprestacion, nombreprestacion: hnombreprestacion, nombrePaciente: "" , usuario: user_go, espagada : estadocita)
                            self.citas.append(cita)
                        }
                       
                    }
                } else {
                    let jsonError = json["resultado"].string
                    //print(jsonError!)
                }
                
                //dispatch_async(dispatch_get_main_queue(), {
                DispatchQueue.main.async(execute: {
                    self.mostrarCitas()
                    self.tableView.reloadData()
                    ViewUtilidades.hiddenLoader(controller: self)
                })
            }
            
            
        }
        
 
    }
    
    
    func mostrarCitas() {
        self.tableView.frame = CGRect(x: 0, y:190.0, width:self.view.frame.width, height:self.view.frame.height - 190 );
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.separatorColor = UIColor.clear
        self.tableView.register(MisCitasTableViewCell.self as AnyClass, forCellReuseIdentifier: "cell");
        
        print(self.tableView.frame.height)
        print(self.citas.count)
         btAceptar = UIButton(frame: CGRect( x: (UIScreen.main.bounds.width ) - 90 , y: (UIScreen.main.bounds.height) - 280, width: 70, height: 70))
        
        btAceptar.setTitle("+", for: UIControlState.normal)
        btAceptar.titleLabel!.font = UIFont(name: "helvetica", size: 35)
        btAceptar.addTarget(self, action: #selector(MisCitasViewController.agregarMasCitas), for: UIControlEvents.touchUpInside)
        btAceptar.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        btAceptar.layer.cornerRadius = 35
        btAceptar.layer.zPosition = 1
        btAceptar.isEnabled = true
        //.addSubview(btAceptar)
        self.tableView.addSubview(btAceptar)
        self.view.addSubview(self.tableView);
    }
    
    func verHistorial() {
       
        if(sgm_forma.selectedSegmentIndex == 1){
            sgm_forma.selectedSegmentIndex = 0
            
            let user = Usuario.getEntity
            if user.rol < 4 {
                self.setReload = true
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "viewHistorial") as! HistorialViewController
                navigationController?.pushViewController(vc, animated: true)
            } else {
                UtilidadesGeneral.mensaje(mensaje: "No tiene permisos para utilizar esta opción")
            }
        }
        
    }
    
    func cancelarCita(sender: UIButton) {
        let alert = UIAlertController(title: "Veris", message: "¿Está seguro de eliminar la cita?", preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
            UIAlertAction in
            
            ViewUtilidades.showLoader(controller: self, title: "Eliminando...")
            //Inicializamos la NSURL y la NSURLRequest
            let url = NSURL(string: "\(self.servact)servicio/agenda/eliminar?arg0=\(self.citas[sender.tag].reserva)")
            print("hola, el comienzo es: \(URLFactory.urldefinida())")
            print(url)
            let request = NSURLRequest(url: url! as URL)
            
            self.btAceptar.removeFromSuperview()
            //Cargamos la request de forma asíncrona
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                //Uso para debug
                //println(NSString(data: data, encoding: NSUTF8StringEncoding))
                
                if !((error) != nil) {
                      self.citas = []
                   let usuario = Usuario.getEntity as Usuario
                    let log = UserDefaults.standard.object(forKey: "identificacion")
                    let esadmin = self.global_esadmin
                    let urlAsString = "\(self.servact)servicio/v2/agenda/citasvigentes3?arg0=\(usuario.tipoid)&arg1=\(usuario.numeroidentificacion)&arg2=\(esadmin)&arg3=\(log!)";
                    
                    print(urlAsString)
                    self.getJSONData(url: urlAsString)
                } else {
                    print("Error al eliminar citar")
                }
            }
        }
        let cancelAction = UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            print("Acción cancelada")
        }
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        
        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
//        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.citas.count
    }
    
    //Devuelve número de filas en la sección
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! MisCitasTableViewCell
            
            //Hacemos que las celdas no sean seleccionables
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.cancelarCitaButton.tag = indexPath.section
        cell.cancelarCitaButton.addTarget(self, action: #selector(cancelarCita(sender:)), for: UIControlEvents.touchUpInside)
            
            let dr = "Dr(a) "
            let medicoString = dr + self.citas[indexPath.section].medico
        var txtpagado = ""
        if(self.citas[indexPath.section].espagada == "N"){
            txtpagado = "CITA NO PAGADA"
            
        }else if(self.citas[indexPath.section].espagada == "S"){
            txtpagado = "CITA PAGADA"
        }
        print("la cita esta pagada : \(txtpagado)")
            cell.setCell(sucursalLabelText: self.citas[indexPath.section].nombresucursal,
                fechaLabelText: self.citas[indexPath.section].fecha,
                doctorLabelText: medicoString,
                prestacionLabelText:self.citas[indexPath.section].prestacion, pacientetext: self.citas[indexPath.section].nombrePaciente, pagada: txtpagado)
        
            cell.reagendarButton.tag = indexPath.section
        cell.reagendarButton.addTarget(self, action: #selector(reagendarcita(sender:)), for: UIControlEvents.touchUpInside)
        cell.reagendarButton2.addTarget(self, action: #selector(reagendarcita(sender:)), for: UIControlEvents.touchUpInside)
            //cell.reagendarButton.addTarget(self, action: Selector(("reagendarcita")), for: UIControlEvents.touchUpInside)
            return cell
    }
    /*unc tocoreagendar(){
        print("tocoreagendar")
    }*/
    func reagendarcita(sender : UIButton){
        //sendAnalytics(categoria: "Opciones Menu MiVeris", action: "Abrir", label: "Familia y Amigos")
        Usuario.sendAnalytics(categoria: "Opciones Menu MiVeris", action: "Abrir", label: "Reagendar")
        let hcita : MisCitasModel = self.citas[sender.tag]
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "viewReservar") as! ReservaViewController
        
        print("es pagada la cita: \(hcita.espagada)")
        
        let espe : ObjEspecialidad = ObjEspecialidad()
        let establ : ObjEstablecimiento = ObjEstablecimiento()
        let servicio : ObjEspecialidad = ObjEspecialidad()
        var texto = hcita.prestacion.characters.split{$0 == "/"}.map { String($0) }
        
        espe.descripcion = texto[0].trimmingCharacters(in: NSCharacterSet.whitespaces)
        espe.codigo = (hcita.codigoespecialidad as NSString).integerValue
        
        establ.id = (hcita.codigosucursal as NSString).integerValue
        establ.idempresa = (hcita.codigoempresa as NSString).integerValue
        establ.descripcion = hcita.nombresucursal as NSString
        establ.codigoregion = (hcita.codigoregion as NSString).integerValue
        
        vc.tipodecarga = TipoCargaReserva.reagendar
        vc.especialidadsel = espe
        vc.establecimientosel = establ
        vc.codigoreservaactualizar = hcita.reserva as NSString
        //vc.nombredoctorsel = "Dr(a) "+hcita.medico
        //vc.codigodoctorsel = (hcita.codigomedico as NSString).integerValue
        vc.tipodecarga = TipoCargaReserva.reagendar
        /*camnbio 23 junio*/
        vc.codigoempresaprestacion = hcita.codigoempresaprestacion as NSString
        vc.codigoprestacion = hcita.codigoprestacion as NSString
        vc.nombreprestacion = hcita.nombreprestacion as NSString
        servicio.codigo = (hcita.codigoprestacion as NSString).integerValue
        servicio.descripcion = hcita.nombreprestacion
        vc.serviciosel = servicio
        vc.identificacion_recibida = hcita.usuario.numeroidentificacion as String
        vc.usuarioR = hcita.usuario
        //me falta el tipodocu y numeroidentificacion
        let us = Usuario.getEntity as Usuario
        vc.tipodocusuario = us.tipoid as String
        vc.documentousuario = us.numeroidentificacion as String
        
        vc.estapagada = hcita.espagada as String
        self.setReload = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func agregarMasCitas(){
        btAceptar.removeFromSuperview()
        print("debo de enviar a reservar citas")
       // sendAnalytics(categoria: "Opciones Menu MiVeris", action: "Abrir", label: "Reservar Cita")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //let vc = storyboard.instantiateViewController(withIdentifier: "viewRecomendaciones") as! RecomendacionesController
        let vc = storyboard.instantiateViewController(withIdentifier: "newReserva") as! NewReservaViewController
        vc.vienedemiscitas = "si"
        navigationController?.pushViewController(vc, animated: true)
        //self.consultarrecomendaciones()

    }
    
    func rateApp(appId: String, completion: @escaping ((_ success: Bool)->())) {
        guard let url = URL(string : "itms-apps://itunes.apple.com/app/" + appId) else {
            completion(false)
            return
        }
        guard #available(iOS 10, *) else {
            completion(UIApplication.shared.openURL(url))
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: completion)
    }
    func llamarCalificar(){
        boolea = false
        let alert = UIAlertController(title: "Veris", message: "Si esta aplicación te ha sido útil, por favor califícanos", preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "Calificar ahora", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            //let url = "itunes.apple.com/app/id967428498"
            UserDefaults.standard.set(false, forKey: "debecalificar")
            UserDefaults.standard.set(true, forKey: "califico")
            self.rateApp(appId: "id967428498") { success in
                print("RateApp \(success)")
            }
            
        }
        let cancelAction = UIAlertAction(title: "Recordar mas tarde", style: UIAlertActionStyle.default) {
            UIAlertAction in
            print("se debe de cambuar")
            //UserDefaults.standard.set(true, forKey: "debecalificar")
            self.boolea = false
            UserDefaults.standard.removeObject(forKey: "debecalificar")
            
            
        }
        
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        
        
            self.present(alert, animated: true, completion: nil)
        
    }
    
    func revisarPermisos(){
        
        let  usuario = Usuario.getEntity as Usuario
        let tipo_id_p = usuario.tipoid as String
        let identificacion = usuario.numeroidentificacion as String
        
        // let verif = Usuario.verificarEsadmin(identificacion: identificacion)
        
        let us = Usuario.getEntity as Usuario
        
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        
        let log = UserDefaults.standard.object(forKey: "identificacion")
        let t_log = UserDefaults.standard.object(forKey: "tipo")
        
        let iden = "\(t_log!)-\(log!)"
        
        //let iden = "\(tipo_id_p)-\(identificacion)"
        let urldef = URLFactory.urldefinida()
        
        let url = NSURL(string: "\(urldef)servicio/persona/grupo/\(iden)?arg0=2")
        let request = NSMutableURLRequest(url: url! as URL)
        var resultado = ""
        //var bool  = false
        //Verificar si es admin
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            if (error != nil) {
                print(error!.localizedDescription)
                resultado = "N"
            } else {
                do{
                    var err: NSError?
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    if (err != nil) {
                        print("JSON Error \(err!.localizedDescription)")
                    }
                    
                    let info =  jsonResult as NSDictionary
                    let data = info["lista"] as! [[String: Any]]//NSArray
                    print(data)
                    if data.count > 0 {
                        for familia in data {
                            if let res = familia["numeroIdentificacion"] as? String {
                                if res == us.numeroidentificacion as String {
                                    resultado = (familia["esAdmin"] as? String)!
                                    
                                }
                            }
                        }
                    }
                    DispatchQueue.main.async(execute: {
                        print("holis \(resultado)")
                        if(resultado != "N"){
                            let usuario = Usuario.getEntity as Usuario
                            
                            let urlAsString = URLFactory.obtenercitasvigentes(tipoidentificacion: usuario.tipoid as String, identificacion: usuario.numeroidentificacion as String, esadmin: resultado, usua_logeado: log as! String)
                            print("did appear: \(urlAsString)")
                            
                            self.getJSONData(url: urlAsString.absoluteString!)
    
                            
                        }
                        self.mostrarCitas()
                        self.tableView.reloadData()
                        ViewUtilidades.hiddenLoader(controller: self)
                        
                        ViewUtilidades.hiddenLoader(controller: self)
                        
                        
                    })
                } catch {
                    print("ERROR")
                }
            }
            
            
        }
        
        
    }
    
    
}
