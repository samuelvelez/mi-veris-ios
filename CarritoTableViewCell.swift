//
//  CarritoTableViewCell.swift
//  Mi Veris
//
//  Created by Samuel Velez on 9/3/17.
//  Copyright © 2017 Samuel Velez. All rights reserved.
//

import UIKit

class CarritoTableViewCell: UITableViewCell {

    var pacienteLabel : UILabel = UILabel()
    var totalPagar : UILabel = UILabel()
    
    
    var imgdetalle : UIImageView = UIImageView()
    
    
    //var controller = MisCitasViewController()
    
    var posY : CGFloat = 0.0
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width;
        
        
        pacienteLabel.frame = CGRect(x: 21.0, y: 5.0, width: screenWidth - 42.0, height: 16.0)
        pacienteLabel.numberOfLines = 1
        pacienteLabel.font = UIFont(name: "helvetica", size: 14.0)
        pacienteLabel.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        self.contentView.addSubview(pacienteLabel)
        
        totalPagar.frame = CGRect(x: 21.0, y: 25.0, width: screenWidth - 42.0, height: 16.0)
        totalPagar.numberOfLines = 1
        totalPagar.font = UIFont(name: "helvetica", size: 12.0)
        totalPagar.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        self.contentView.addSubview(totalPagar)

        let arrow = UIImageView(frame: CGRect(x: screenWidth-30, y: 20, width: 10, height:16))
        arrow.image = UIImage(named: "flecha.png")
        
        self.contentView.addSubview(arrow)
        
        self.backgroundColor = UIColor.clear
        //self.selectionStyle = UITableViewCellSelectionStyle.none;
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    //Con esta función haremos más simple la adjudicación a las labels desde MisCitasViewController
    func setCell(pacientetext: String, totalPagar : String) {
        self.pacienteLabel.text = pacientetext.capitalized
        self.totalPagar.text = "$ "+totalPagar
        
        //cell.imageView?.image = UIImage(named: imagenes[indexPath.row])
    }

}
