//
//  DetalleViewController.swift
//  Mi Veris
//
//  Created by Jorge on 04/02/15.
//  Copyright (c) 2015 Firewall Soluciones All rights reserved.
//

import UIKit

class TerminosViewController: UIViewController {
    
    var terminos : NSString!
    var texttelefonos : NSString!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.style();
        
        let webView = UIWebView(frame: CGRect(x:0, y:94, width:self.view.frame.width , height:self.view.frame.height - 94) )
        webView.loadHTMLString(terminos as String, baseURL: nil)
        self.view.addSubview(webView)
        
//        let lbDireccion = UILabel(frame: CGRectMake(10, 110, CGRectGetWidth(self.view.frame), 30))
//        lbDireccion.textColor = UIColor.blackColor()
//        lbDireccion.text = terminos
//        lbDireccion.font = UIFont(name: "helvetica", size: 14)
//        lbDireccion.numberOfLines = 0;
//        self.view.addSubview(lbDireccion)
        
    }
    
    
    // MARK: style: Estilo del controlador
    
    func style(){
        
        
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        self.navigationController?.navigationItem.backBarButtonItem?.title = ""
        
        let fondo : UIImageView = UIImageView(frame: self.view.bounds)
        fondo.image = UIImage(named:"back.png")
        self.view.addSubview(fondo)
        
        
        let barWhite : UIView = UIView(frame: CGRect(x:0, y:64, width: self.view.frame.width , height:30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x:0, y:64, width:self.view.frame.width / 4.2, height:30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x:barBlue.frame.width, y:64, width:45, height:30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x:0, y:64, width:self.view.frame.width - 20, height:30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "Terminos  condiciones"
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
