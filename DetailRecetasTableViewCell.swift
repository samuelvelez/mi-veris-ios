//
//  DetailRecetasTableViewCell.swift
//  veris
//
//  Created by Felipe Lloret on 07/02/15.
//  Copyright (c) 2015 Felipe Lloret. All rights reserved.
//

import UIKit

class DetailRecetasTableViewCell: UITableViewCell {
    
    var firstLineView: UIView = UIView()
    var denominacionLabel: UILabel = UILabel()
    var concentracionLabel: UILabel = UILabel()
    var formaLabel: UILabel = UILabel()
    var cantidadLabel : UILabel = UILabel()
    var indicacionesLabel : UILabel = UILabel()
    
    var addEventButton: UIButton = UIButton()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width;
        
        firstLineView.frame = CGRect(x:0.0, y:0.0, width:screenWidth - 60.0, height:1.0)
        firstLineView.backgroundColor = UIColor.black
        self.contentView.addSubview(firstLineView)
        
        denominacionLabel.frame = CGRect(x:8.0, y:8.0, width:267.0, height:26.0)
        denominacionLabel.numberOfLines = 1
        denominacionLabel.font = UIFont(name: "helvetica-bold", size: 13.0)
        self.contentView.addSubview(denominacionLabel)
        
        cantidadLabel.frame = CGRect(x:8.0, y:40.0, width:24.0, height:14.0)
        cantidadLabel.font = UIFont(name: "helvetica", size: 12.0)
        self.contentView.addSubview(cantidadLabel)
        
        formaLabel.frame = CGRect(x:37.0, y:40.0, width:175.0, height:14.0)
        formaLabel.numberOfLines = 1
        formaLabel.font = UIFont(name: "helvetica", size: 12.0)
        self.contentView.addSubview(formaLabel)
        
        concentracionLabel.frame = CGRect(x:190.0, y:40.0, width:67.0, height:14.0)
        concentracionLabel.numberOfLines = 1
        concentracionLabel.font = UIFont(name: "helvetica", size: 12.0)
        self.contentView.addSubview(concentracionLabel)
        
        addEventButton.frame = CGRect(x:220.0, y:1.0, width:40.0, height:40.0)
        addEventButton.setBackgroundImage(UIImage(named: "BellButton"), for: UIControlState.normal)//
        self.contentView.addSubview(addEventButton)

        indicacionesLabel.frame = CGRect(x:8.0, y:50.0, width:200.0, height:34.0)
        indicacionesLabel.numberOfLines = 2
        indicacionesLabel.lineBreakMode = .byWordWrapping
        indicacionesLabel.textColor = UIColor.black
        indicacionesLabel.font = UIFont(name: "helvetica", size: 12.0)
        self.contentView.addSubview(indicacionesLabel)
        
        self.backgroundColor = UIColor.clear
        self.selectionStyle = UITableViewCellSelectionStyle.none;
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    //Con esta función haremos más simple la adjudicación a las labels desde MisRecetasViewController
    func setCell(denominacionLabelText: String, concentracionLabelText: String, formaLabelText: String, cantidadLabelText: String, indicacionesLabelText: String) {
        self.denominacionLabel.text = denominacionLabelText
        self.concentracionLabel.text = concentracionLabelText
        self.formaLabel.text = formaLabelText
        self.cantidadLabel.text = cantidadLabelText
        self.indicacionesLabel.text = indicacionesLabelText
    }
    
}
