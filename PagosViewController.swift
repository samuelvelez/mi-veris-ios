//
//  FlebotomistaViewController.swift
//  Mi Veris
//
//  Created by Jorge on 17/06/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import UIKit

class PagosViewController : UIViewController, UIWebViewDelegate {
    
    var webV : UIWebView!
    var buttonBack : UIButton!
    var buttonNext : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        webV = UIWebView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.view.frame.height - 64 ) )
        webV.loadRequest(NSURLRequest(url: NSURL(string: "https://miveris.com.ec/MiverisWeb/Aplicaciones/Perfil/loginApp.zul")! as URL) as URLRequest)
        webV.delegate = self;
        webV.backgroundColor = UIColor.white
        
        webV.mediaPlaybackRequiresUserAction = false
        
        self.view.addSubview(webV)
        
        let view = UIView(frame: CGRect(x: 0, y: self.view.frame.height - 64, width: self.view.frame.width, height: 64))
        
        
        buttonBack   = UIButton(type: UIButtonType.system)
        buttonBack.frame = CGRect(x: 10, y: 10, width: 40, height: 40)
        //button.setTitle("Button", forState: UIControlState.Normal)
        buttonBack.setImage(UIImage(named: "arrowatras.png"), for: UIControlState.normal)
        buttonBack.addTarget(self, action: #selector(PagosViewController.back), for: UIControlEvents.touchUpInside)
        view.addSubview(buttonBack)
        
        buttonNext   = UIButton(type: UIButtonType.system)
        buttonNext.frame = CGRect(x: self.view.frame.width - 50, y: 10, width: 40, height: 40)
        //button.setTitle("Button", forState: UIControlState.Normal)
        buttonNext.setImage(UIImage(named: "arrowback.png"), for: UIControlState.normal)
        buttonNext.addTarget(self, action: #selector(PagosViewController.next as (PagosViewController) -> () -> ()), for: UIControlEvents.touchUpInside)
        view.addSubview(buttonNext)
        
        
        self.view.addSubview(view)
        
        ViewUtilidades.showLoader(controller: self, title: "Cargando")
        
        
    }
    
    func back(){
        webV.goBack()
    }
    
    func next(){
        webV.goForward()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print("Webview fail with error \(error)", terminator: "");
        buttonNext.isEnabled = webV.canGoForward;
        buttonBack.isEnabled = webV.canGoBack;
        ViewUtilidades.hiddenLoader(controller: self)
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return true;
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        print("Webview started Loading", terminator: "")
        buttonNext.isEnabled = webV.canGoForward;
        buttonBack.isEnabled = webV.canGoBack;
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        print("Webview did finish load", terminator: "")
        buttonNext.isEnabled = webV.canGoForward;
        buttonBack.isEnabled = webV.canGoBack;
        ViewUtilidades.hiddenLoader(controller: self)

    }
    /*
    func webViewDidStartLoad(webView: UIWebView!) {
        print("Webview started Loading", terminator: "")
        buttonNext.isEnabled = webV.canGoForward;
        buttonBack.isEnabled = webV.canGoBack;
    }
    
    func webViewDidFinishLoad(webView: UIWebView!) {
        print("Webview did finish load", terminator: "")
        buttonNext.isEnabled = webV.canGoForward;
        buttonBack.isEnabled = webV.canGoBack;
        ViewUtilidades.hiddenLoader(controller: self)
    }*/

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
