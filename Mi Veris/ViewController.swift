//
//  ViewController.swift
//  Mi Veris
//
//  Created by Samuel Velez on 21/9/16.
//  Copyright © 2016 Samuel Velez. All rights reserved.
//

import UIKit
//import JWT
//import JSONWebToken


var tipoNotificacionFlebotomista : NSString = ""

class ViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource, ENSideMenuDelegate {
   
    var previous = NSDecimalNumber.one
    var current = NSDecimalNumber.one
    var position: UInt = 1
    
    var updateTimer: Timer?
    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    
    var versionActual = "14 "
    
    @IBOutlet var collectionView: UICollectionView?
    static let reuseIdentifier = "Collection" // also enter this string as the cell identifier in the storyboard
    
    //"Reservar una Cita",
    var items:[String] = [ "Mis Citas", "Mis Doctores Favoritos", "Mis Resultados", "Mis Recetas", "Historia Clínica", "Paga tu Cita", "Laboratorio a domicilio gratis", "O-I"];// "¿Dónde Estamos?", "Contáctanos","Enlazar con HealthKit"];
    
    class func setTipoNotificacion( string : NSString) {
        tipoNotificacionFlebotomista = string
    }
    
    class func getTipoNotificacion() -> NSString {
        return tipoNotificacionFlebotomista
    }
    //"1_1.png",
    var imagenes : [String] = [ "2_1.png", "3_1.png", "4_1.png", "5_1.png", "6_1.png", "7_1.png", "8_1.png", "ic_launcher.png"];
    
    var lista_recomendacion : NSMutableArray =  NSMutableArray()
    var setPush : Bool = false
    var selectedCell : NSInteger = 0

    
    @IBOutlet weak var btacercade: UIBarButtonItem!
    
    var video : NSString = ""
    var contenidoNotificacion : NSString = ""
    var newNotificacion : Bool = false
    
    @IBAction func callM(_ sender: AnyObject) {
        print("showM")
        toggleSideMenuView()
    }
    
   /* @IBAction func callMenu(sender: UIButton) {
        print("showMenu")
        toggleSideMenuView()
    }*/
    func hideMenu(){
        print("hide menu")
        hideSideMenuView()
    }

    override func viewDidAppear(_ animated: Bool) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        //self.verificarToken()
        if newNotificacion {
            newNotificacion = false
            if video != "" {
               /* let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewControllerWithIdentifier("viewWebView") as! WebViewViewController
                vc.url = video
                vc.titulo = "Mi Veris"
                self.presentViewController(vc, animated: true, completion: nil)
 */
                print("agregar")
            } else {
               /* let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewControllerWithIdentifier("viewNotificacion") as! NotificacionViewController
                vc.texto = contenidoNotificacion
                vc.titulo = "Mi Veris"
                self.presentViewController(vc, animated: true, completion: nil)
 */
                print("agregar")
            }
        }
        
        
        
        if setPush {
            setPush = false
            if selectedCell == 0 {
                /*let storyboard = UIStoryboard(name: "Main", bundle: nil)
                 let vc = storyboard.instantiateViewControllerWithIdentifier("viewLogin") as LoginViewController
                 navigationController?.pushViewController(vc, animated: true)*/
            } else if selectedCell == 2 {
              /*  let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewControllerWithIdentifier("viewFavoritos") as! FavoritosViewController
                navigationController?.pushViewController(vc, animated: true)
                tasa capitalizable
                monto de ahorro + interes => monto que desea alcanzar
                 
                 wwww.dinersclub.com.ec
 */
                print("agregar")
            }
        }
        Usuario.getDefaultUser()
        
       
    }
    
    func verificarToken(){
        let usuario = Usuario.getEntity as Usuario
        
        
        var tok : String = ((UserDefaults.standard.object(forKey: "deviceToken") as AnyObject).description)!
        tok = tok.replacingOccurrences(of: "<", with: "")
        tok = tok.replacingOccurrences(of: ">", with: "")
        tok = tok.replacingOccurrences(of: " ", with: "")
        print("debe de ser el nuevo \(tok)")
        
        
        UserDefaults.standard.removeObject(forKey: "token")

        print("el token guardado en usuario es: \(usuario.codigoToken), y ahora tengo \(tok) ")
        if tok != usuario.codigoToken as String {
            usuario.codigoToken = tok as NSString
            self.enviarquitartoken(codigo: usuario.codigoToken)
            self.enviararegistrartoken(identificacion: usuario.numeroidentificacion as String, tipoid: usuario.tipoid as String)
            print("es diferente, debo de poner tok en usuario.codigotoken y actualizar el token en el ws")
        }else{
            print("es igual")
        }
        print("\(usuario.numeroidentificacion) el token que esta dando  es: \(tok)")
        
        /*if(token1 != token2){
            print("")
            print("son diferentes, debo de borrar token1 y registrar token2 y guardarlo como token")
            //UserDefaults.standard.set(token1, forKey: "token")
        }else{
            print("son igualess<!!!")
        }*/
        
        
        //NewLoginViewController?.enviararegistrartoken(self)
        
    }
    
    func verificaversion(){
        //comparar versionActaul con se4rvicio parametrosini
        
        let us = Usuario.getEntity as Usuario
        let tipoid = us.tipoid
        let identificacion = us.numeroidentificacion as String
        let urldef = URLFactory.urldefinida()

        let cadenaUrl = "\(urldef)servicio/catalogos/parametrosini?arg0=\(tipoid)&arg1=\(identificacion)"
        
        let url = NSURL(string: cadenaUrl )
        
        print(cadenaUrl)
        //let request = NSURLRequest(URL: url!)
        if url != nil {
            let request = NSMutableURLRequest(url: url! as URL)
            
            
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                
                if (error != nil) {
                    print(error!.localizedDescription)
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    do{
                        var err: NSError?
                        
                        let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        if (err != nil) {
                            print("JSON Error \(err!.localizedDescription)")
                        }
                        
                        let info =  jsonResult as NSDictionary
                        let resultado = info["resultado"] as! NSString
                        //arreglarnsarray
                        let data = info["lista"] as! [[String: Any]]
                        var numeroVersion  = "0"
                        if data.count > 0 {
                            for use in data {
                                numeroVersion = use["versionapp"] as! String
                            }
                        }
                        print(" el numero de version que trae es: \(numeroVersion) y el que debo de manejar siempre es \(self.versionActual)")
                        let numver = Int(numeroVersion)
                        let veract = Int(self.versionActual)
                        let vermin = numver! - 4
                        if(veract! < vermin){
                            print("entro al if y vermin es \(vermin)")
                            let alert = UIAlertController(title: "Veris", message: "Es necesario que actualices a la nueva versión", preferredStyle: UIAlertControllerStyle.alert)
                            let okAction = UIAlertAction(title: "Actualizar", style: UIAlertActionStyle.cancel) {
                                UIAlertAction in
                                self.rateApp(appId: "id967428498") { success in
                                    print("RateApp \(success)")
                                }
                                
                            }
                            alert.addAction(okAction)
                            self.present(alert, animated: true, completion: nil)
                        }else if(numver! > veract! && (veract! >= vermin)){
                            print("entro al else  y vermin es \(vermin)")
                            
                            let alert = UIAlertController(title: "Veris", message: "Hay una nueva versión disponible", preferredStyle: UIAlertControllerStyle.alert)
                            let okAction = UIAlertAction(title: "Actualizar", style: UIAlertActionStyle.cancel) {
                                UIAlertAction in
                                self.rateApp(appId: "id967428498") { success in
                                    print("RateApp \(success)")
                                }
                                
                            }
                            let cancelAction = UIAlertAction(title: "Ahora no", style: UIAlertActionStyle.default) {
                                UIAlertAction in
                                
                            }
                            
                            alert.addAction(cancelAction)
                            alert.addAction(okAction)
                            

                            

                            self.present(alert, animated: true, completion: nil)

                        }else{
                            print("ni gaver actual:\(veract)  numver:\(numver) minimi: \(vermin)")
                        }
                        /*if (self.versionActual != numeroVersion){
                            let alert = UIAlertController(title: "Veris", message: "Tenemos una nueva version disponible. Por favor descargala para poder tener las ultimas mejoras.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
*/

                        //dispatch_async(DispatchQueue.main, {
                        DispatchQueue.main.async {
                            ViewUtilidades.hiddenLoader(controller: self)
                            print(resultado)
                            print("obtener las citas")
                            
                            if resultado.isEqual(to: "ok") {
                                print("eas ok")
                                
                            } else {
                                print("no registro el token")
                            }
                            
                        }
                    } catch {
                        print("ERROR")
                        ViewUtilidades.hiddenLoader(controller: self)
                        let alert = UIAlertController(title: "Veris", message: "Revise su conexión a Datos o Internet", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
            
        } else {
            ViewUtilidades.hiddenLoader(controller: self)
            let alert = UIAlertController(title: "Veris", message: "Ha ocurrido un error. Intente nuevamente", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                UIAlertAction in
                
            }
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
        

        
    }
    
    func rateApp(appId: String, completion: @escaping ((_ success: Bool)->())) {
        guard let url = URL(string : "itms-apps://itunes.apple.com/app/" + appId) else {
            completion(false)
            return
        }
        guard #available(iOS 10, *) else {
            completion(UIApplication.shared.openURL(url))
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: completion)
    }
    
    func enviararegistrartoken(identificacion : String, tipoid : String){
        var tok : String = ((UserDefaults.standard.object(forKey: "deviceToken") as AnyObject).description)!
        tok = tok.replacingOccurrences(of: "<", with: "")
        tok = tok.replacingOccurrences(of: ">", with: "")
        tok = tok.replacingOccurrences(of: " ", with: "")
        
        print(tok)
        UserDefaults.standard.set(tok, forKey: "deviceToken")
        //var codigo_tok : NSString = "6"
        let urldef = URLFactory.urldefinida()

        let cadenaUrl = "\(urldef)servicio/login/registrargcm?arg0=\(identificacion)&arg1=\(tipoid)&arg2=\(tok)&arg3=iOS"
        
        let url = NSURL(string: cadenaUrl )
        
        print(cadenaUrl)
        //let request = NSURLRequest(URL: url!)
        if url != nil {
            let request = NSMutableURLRequest(url: url! as URL)
            
            
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                
                if (error != nil) {
                    print(error!.localizedDescription)
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    do{
                        var err: NSError?
                        
                        let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        if (err != nil) {
                            print("JSON Error \(err!.localizedDescription)")
                        }
                        
                        print(jsonResult)
                        let info =  jsonResult as NSDictionary
                        //let val = false
                        //let data = info["lista"] as! NSArray
                        let resultado = info["resultado"] as! NSString
                        let codigo_token = info["texto"] as! NSString
                        //dispatch_async(DispatchQueue.main, {
                        DispatchQueue.main.async {
                            ViewUtilidades.hiddenLoader(controller: self)
                            print(resultado)
                            print("obtener las citas")
                            
                            if resultado.isEqual(to: "ok") {
                                //self.token = codigo_token
                                let us = Usuario.getEntity as Usuario
                                us.codigoToken = codigo_token//tok as NSString
                                self.obtenerCitas(tipo: us.tipoid as String, identificacion: us.numeroidentificacion as String)
                                //codigo_tok = codigo_token
                                //return codigo_token
                                
                                
                            } else {
                                print("no registro el token")
                            }
                            
                        }
                    } catch {
                        print("ERROR")
                        ViewUtilidades.hiddenLoader(controller: self)
                        let alert = UIAlertController(title: "Veris", message: "Revise su conexión a Datos o Internet", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
            
        } else {
            ViewUtilidades.hiddenLoader(controller: self)
            let alert = UIAlertController(title: "Veris", message: "Ha ocurrido un error. Intente nuevamente", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                UIAlertAction in
                
            }
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }

    @IBAction func callmenu(sender: UIBarButtonItem) {
        print("showMenu")
        toggleSideMenuView()
    }
    func existUserDefault() {
        let userDefaults : UserDefaults = UserDefaults.standard
        if ( userDefaults.object(forKey: "nombre") != nil ){
            
        } else {
            let usuario = Usuario.getEntity as Usuario
            usuario.logueado = false
            usuario.nombre = ""
            usuario.numeroidentificacion = ""
            usuario.tipoid = ""
            
            usuario.codigoCiudad = 0
            usuario.codigoPais = 0
            usuario.codigoProvincia = 0
            usuario.codigoRegion = 0
            
            UserDefaults.standard.set("", forKey: "identificacion")
            UserDefaults.standard.synchronize()
            
            UserDefaults.standard.set("", forKey: "nombre")
            UserDefaults.standard.synchronize()
            
            UserDefaults.standard.set("", forKey: "tipo")
            UserDefaults.standard.synchronize()
            
            /*UserDefaults.standard.set("\(usuario.codigoCiudad)", forKey: "ciudad")
             UserDefaults.standard.synchronize()
             
             UserDefaults.standard.set("\(usuario.codigoPais)", forKey: "pais")
             UserDefaults.standard.synchronize()
             
             UserDefaults.standard.set("\(usuario.codigoProvincia)", forKey: "provincia")
             UserDefaults.standard.synchronize()
             
             UserDefaults.standard.set("\(usuario.codigoRegion)", forKey: "region")
             UserDefaults.standard.synchronize()*/
        }
        
        if ( userDefaults.object(forKey: "tipo") != nil ){
        } else {
            UserDefaults.standard.set("", forKey: "tipo")
            UserDefaults.standard.synchronize()
        }
        
        if ( userDefaults.object(forKey: "ciudad") != nil ){
        } else {
            UserDefaults.standard.set(0, forKey: "ciudad")
            UserDefaults.standard.synchronize()
        }
        if ( userDefaults.object(forKey: "pais") != nil ){
        } else {
            UserDefaults.standard.set(0, forKey: "pais")
            UserDefaults.standard.synchronize()
        }
        if ( userDefaults.object(forKey: "provincia") != nil ){
        } else {
            UserDefaults.standard.set(0, forKey: "provincia")
            UserDefaults.standard.synchronize()
        }
        if ( userDefaults.object(forKey: "region") != nil ){
        } else {
            UserDefaults.standard.set(0, forKey: "region")
            UserDefaults.standard.synchronize()
        }
        
    }
    
    func reinstateBackgroundTask() {
        if updateTimer != nil && (backgroundTask == UIBackgroundTaskInvalid) {
            registerBackgroundTask()
        }
    }
    
    func llamar() {
        updateTimer = Timer.scheduledTimer(timeInterval: 3600, target: self,
                                                             selector: #selector(ViewController.buscarCitas), userInfo: nil, repeats: true)
        //registerBackgroundTask()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let tracker = GAI.sharedInstance().defaultTracker
        tracker?.set(kGAIScreenName, value: "Inicio")
        
        //let builder = GAIDictionaryBuilder.createScreenView()
        tracker?.send(GAIDictionaryBuilder.createEvent(withCategory: "", action: "", label: "", value: nil).build() as [NSObject : AnyObject])
    }
    func sendAnalytics(categoria : String , action: String , label : String){
        let tracker = GAI.sharedInstance().defaultTracker
        tracker?.set(kGAIScreenName, value: "Inicio")
        
        //let builder = GAIDictionaryBuilder.createScreenView()
        tracker?.send(GAIDictionaryBuilder.createEvent(withCategory: categoria, action: action, label: label, value: nil).build() as [NSObject : AnyObject])
    }
    
    override func viewDidLoad() {
        // Do any additional setup after loading the view, typically from a nib.
        super.viewDidLoad()
        UIApplication.shared.applicationIconBadgeNumber = 0
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.reinstateBackgroundTask), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        
        print("viewdidload")
        self.llamar()
        
        //aqui iba
        self.style();
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        // collection view
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width;
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 70, left: 5, bottom: 5, right: 5)
        layout.itemSize = CGSize(width: screenWidth/2 - 10 , height: screenWidth/2 - 5)
        
        
        collectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        collectionView!.dataSource = self
        collectionView!.delegate = self
        collectionView!.register(CellCollectionViewCell.self, forCellWithReuseIdentifier: "Collection")
        collectionView!.backgroundColor = UIColor.clear
        self.view.addSubview(collectionView!)
        
        let usuario = Usuario.getEntity as Usuario
        
        self.existUserDefault()
        
        
        var returnValue: NSString = UserDefaults.standard.object(forKey: "nombre") as! NSString
        usuario.nombre = returnValue
        
        
        returnValue = UserDefaults.standard.object(forKey: "identificacion") as! NSString
        usuario.numeroidentificacion = returnValue
        
        returnValue = UserDefaults.standard.object(forKey: "tipo") as! NSString
        usuario.tipoid = returnValue
        
        var returnValue2 : NSInteger = UserDefaults.standard.object(forKey: "ciudad") as! NSInteger
        usuario.codigoCiudad = returnValue2
        
        returnValue2 = UserDefaults.standard.object(forKey: "pais") as! NSInteger
        usuario.codigoPais = returnValue2
        
        returnValue2 = UserDefaults.standard.object(forKey: "provincia") as! NSInteger
        usuario.codigoProvincia = returnValue2
        
        returnValue2 = UserDefaults.standard.object(forKey: "region") as! NSInteger
        usuario.codigoRegion = returnValue2
        
        if !usuario.nombre.isEqual(to: "") && !usuario.numeroidentificacion.isEqual(to: "")  {
            usuario.logueado = true
        }

        self.verificarToken()
        self.verificaversion()
    }

    func style()
    {
        print("viewcontroller - style ()")
        self.navigationController?.navigationItem.backBarButtonItem?.title = ""
        //self.navigationController?.navigationItem.hidesBackButton = true
        
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        let fondo : UIImageView = UIImageView(frame: self.view.bounds)
        fondo.image = UIImage(named:"back.png")
        self.view.addSubview(fondo)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    @available(iOS 6.0, *)
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ViewController.reuseIdentifier, for: indexPath as IndexPath) as! CellCollectionViewCell
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        cell.textLabel?.text = "\(items[indexPath.row])"
        cell.imageView?.image = UIImage(named: imagenes[indexPath.row])
        cell.layer.backgroundColor = UIColor.black.cgColor
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 1
        
        
        cell.backgroundColor = UIColor.clear // make cell more visible in our example project
 
        return cell

    }
    
    
    // MARK: Métodos de tableView
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("seleccionon \(indexPath)")
        self.hideMenu()
        /*if indexPath.row == 0 {
            let usuario = Usuario.getEntity as Usuario
            if usuario.logueado {
                
                 /*let storyboard = UIStoryboard(name: "Main", bundle: nil)
                 let vc = storyboard.instantiateViewController(withIdentifier: "viewRecomendaciones") as! RecomendacionesController
                 navigationController?.pushViewController(vc, animated: true)*/
                sendAnalytics(categoria: "Opciones Menu MiVeris", action: "Abrir", label: "Reservar Cita")
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                //let vc = storyboard.instantiateViewController(withIdentifier: "viewRecomendaciones") as! RecomendacionesController
                let vc = storyboard.instantiateViewController(withIdentifier: "newReserva") as! NewReservaViewController
                navigationController?.pushViewController(vc, animated: true)
                //self.consultarrecomendaciones()
                
            } else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "viewLogin") as! LoginViewController
                navigationController?.pushViewController(vc, animated: true)
            }
        } else*/
        if indexPath.row == 0 {
            let usuario = Usuario.getEntity as Usuario
            if usuario.logueado {
                sendAnalytics(categoria: "Opciones Menu MiVeris", action: "Abrir", label: "Citas")
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "viewCitas") as! MisCitasViewController
                navigationController?.pushViewController(vc, animated: true)
                

            } else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "viewLogin") as! LoginViewController
                navigationController?.pushViewController(vc, animated: true)
            }
            print("toco 1")
        } else if indexPath.row == 1 {
            let usuario = Usuario.getEntity as Usuario
            if usuario.logueado {
                sendAnalytics(categoria: "Opciones Menu MiVeris", action: "Abrir", label: "Doctores Favoritos")
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "viewFavoritos") as! FavoritosViewController
                navigationController?.pushViewController(vc, animated: true)
                
            } else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "viewLogin") as! LoginViewController
                navigationController?.pushViewController(vc, animated: true)
            }
            
            print("toco 2")
        }
        else if indexPath.row == 2 {
            let usuario = Usuario.getEntity as Usuario
            if usuario.logueado {
                sendAnalytics(categoria: "Opciones Menu MiVeris", action: "Abrir", label: "Mis resultados")
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "viewMisResultados") as! MisResultadosViewController
                navigationController?.pushViewController(vc, animated: true)
            } else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "viewLogin") as! LoginViewController
                navigationController?.pushViewController(vc, animated: true)
            }

        } else if indexPath.row == 3 {
            let usuario = Usuario.getEntity as Usuario
            if usuario.logueado {
                sendAnalytics(categoria: "Opciones Menu MiVeris", action: "Abrir", label: "Recetas")
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "viewRecetas") as! MisRecetasViewController
                navigationController?.pushViewController(vc, animated: true)
            } else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "viewLogin") as! LoginViewController
                navigationController?.pushViewController(vc, animated: true)
            }
        } else if indexPath.row == 4 {
            let usuario = Usuario.getEntity as Usuario
            if usuario.logueado {
                sendAnalytics(categoria: "Opciones Menu MiVeris", action: "Abrir", label: "Historia Clínica")
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "viewHistoriaClinica") as! HistoriaClinicaViewController
                navigationController?.pushViewController(vc, animated: true)
            } else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "viewLogin") as! LoginViewController
                navigationController?.pushViewController(vc, animated: true)
            }
        } else if indexPath.row == 5 {
            sendAnalytics(categoria: "Opciones Menu MiVeris", action: "Abrir", label: "Paga tu cita")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "viewPagos") as! PagosViewController
            navigationController?.pushViewController(vc, animated: true)
 
        } else if indexPath.row == 6 {
            let usuario = Usuario.getEntity as Usuario
            if usuario.logueado {
                //sendAnalytics(categoria: "Opciones Menu MiVeris", action: "Abrir", label: "Laboratorio a domicilio")
                
                //let vc = SolicitarServicioViewController()                navigationController?.pushViewController(vc, animated: true)
                let vc = storyboard?.instantiateViewController(withIdentifier: "LaboratorioView") as! LaboratorioViewController
                navigationController?.pushViewController(vc, animated: true)

 
            } else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "viewLogin") as! LoginViewController
                navigationController?.pushViewController(vc, animated: true)
            }
            
            //let storyboard = UIStoryboard(name: "Main", bundle: nil)
            //let vc = storyboard.instantiateViewControllerWithIdentifier("viewLogin") as LoginViewController
        
        }
        /*else if indexPath.row == 7 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "viewProfile") as! ProfileViewController
            navigationController?.pushViewController(vc, animated: true)
        }*/
        else if indexPath.row == 7 {
            
            let usuario = Usuario.getEntity as Usuario
            if usuario.logueado {
                sendAnalytics(categoria: "Opciones Menu MiVeris", action: "Abrir", label: "OI")
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "oiview") as! OiViewController
                navigationController?.pushViewController(vc, animated: true)
                
                
            } else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "viewLogin") as! LoginViewController
                navigationController?.pushViewController(vc, animated: true)
            }

            
        }
        else if indexPath.row == 8 {
            
            let usuario = Usuario.getEntity as Usuario
            if usuario.logueado {
                //sendAnalytics(categoria: "Opciones Menu MiVeris", action: "Abrir", label: "PAGO")
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "MisPagos") as! MisPagosViewController
                navigationController?.pushViewController(vc, animated: true)
                
                
            } else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "viewLogin") as! LoginViewController
                navigationController?.pushViewController(vc, animated: true)
            }
            
            
        }

        
        /* else if indexPath.row == 9 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier("viewContactos") as! ContactosViewController
            navigationController?.pushViewController(vc, animated: true)
        } else if indexPath.row == 10 {
            print("toco")
            let usuario = Usuario.getEntity as Usuario
            if usuario.logueado {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewControllerWithIdentifier("MasterHealth") as! MasterHealthViewController
                navigationController?.pushViewController(vc, animated: true)
            } else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewControllerWithIdentifier("viewLogin") as! LoginViewController
                navigationController?.pushViewController(vc, animated: true)
            }
        } else if indexPath.row == 11 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier("viewWebView") as! WebViewViewController
            vc.url = "http://miveris.com.ec/MiverisWsrest/empresa.html"
            vc.titulo = "La Empresa"
            navigationController?.pushViewController(vc, animated: true)
        }
        */
        selectedCell = indexPath.row
 
        collectionView.deselectItem(at: indexPath as IndexPath, animated: true)
        //tableView.deselectRowAtIndexPath(indexPath, animated: true);
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showAlertLogout(){
        let alert = UIAlertController(title: "Veris", message: "Usted ha cerrado sesión", preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
            UIAlertAction in
            
            
        }
        
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    func showAlert(){
        let alert = UIAlertController(title: "Veris", message: "¿Esta seguro de cerrar sesión?", preferredStyle: UIAlertControllerStyle.alert)
        
        let user = Usuario.getEntity as Usuario
        print("token: \(user.numeroidentificacion) token: \(user.codigoToken)")
        let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
            UIAlertAction in
            
            print("remover \(user.codigoToken)")
            self.enviarquitartoken(codigo: user.codigoToken)
            
            Usuario.deleteLogin()
            
            /*var parent = Usuario.getParent as Usuario
            parent = Usuario()
            */
            var usuario = Usuario.getEntity as Usuario
            
            usuario = Usuario()
            
            //let not : notificacion = notificacion()
            //not.borrarTodo()
            
            UserDefaults.standard.set("", forKey: "identificacion")
            //UserDefaults.standard.set(newValue as [NSString], forKey: "food")
            UserDefaults.standard.synchronize()
            
            UserDefaults.standard.set("", forKey: "nombre")
            //UserDefaults.standard.set(newValue as [NSString], forKey: "food")
            UserDefaults.standard.synchronize()
            
            UserDefaults.standard.set("", forKey: "tipo")
            UserDefaults.standard.synchronize()
            
            UserDefaults.standard.set(0, forKey: "ciudad")
            UserDefaults.standard.synchronize()
            
            UserDefaults.standard.set(0, forKey: "region")
            UserDefaults.standard.synchronize()
            
            UserDefaults.standard.set(0, forKey: "pais")
            UserDefaults.standard.synchronize()
            
            UserDefaults.standard.set(0, forKey: "provincia")
            UserDefaults.standard.synchronize()
            
            print("token: \(usuario.codigoToken)")
            //self.btLogout.enabled = false
            //self.btLogout.tintColor = UIColor.clearColor()
            //self.navigationItem.rightBarButtonItem.h
            //btLogout.enabled = false
            //btLogout.
            self.showAlertLogout()
            
            
            
        }
        let cancelAction = UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            
        }
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func cerrarSesion(sender: AnyObject) {
        self.showAlert()
    }
    
    func enviarquitartoken(codigo: NSString){
        let urldef = URLFactory.urldefinida()

        let cadenaUrl = "\(urldef)servicio/login/borrargcm?arg0=\(codigo)"
        
        let url = NSURL(string: cadenaUrl )
        
        print(cadenaUrl)
        //let request = NSURLRequest(URL: url!)
        if url != nil {
            let request = NSMutableURLRequest(url: url! as URL)
            
            let str = "wsappusuario:ZA$57@9b86@$2r5"
            
            //arreglarutf8str
            
            let base64Encoded = str.data(using: String.Encoding.utf8, allowLossyConversion: true)
            //let base64Encoded = str.dataUsingEncoding(NSUTF8StringEncoding)!.base64EncodedStringWithOptions([])
            print("Encoded: \(base64Encoded)")
            let base64DecodedData = NSData(base64Encoded: base64Encoded!, options: [])
           
            
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                
                if (error != nil) {
                    print(error!.localizedDescription)
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    do{
                        var err: NSError?
                        
                        let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        if (err != nil) {
                            print("JSON Error \(err!.localizedDescription)")
                        }
                        
                        print(jsonResult)
                        let info =  jsonResult as NSDictionary
                        //let val = false
                        //let data = info["lista"] as! NSArray
                        let resultado = info["resultado"] as! NSString
                        //let codigo_token = info["texto"] as! NSString
                        
                        ViewUtilidades.hiddenLoader(controller: self)
                        
                        DispatchQueue.main.async {
                            print(resultado)
                            if resultado.isEqual(to: "ok") {
                                //codigo_tok = codigo_token
                                //return codigo_token
                                
                                
                            } else {
                                /*let alert = UIAlertController(title: "Veris", message: "error", preferredStyle: UIAlertControllerStyle.alert)
                                let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                                    UIAlertAction in
                                    
                                }
                                alert.addAction(okAction)*/
                              //  self.present(alert, animated: true, completion: nil)
                            }                        }
                        /*
                        DispatchQueue.main.asynchronously(DispatchQueue.mainexecute: {
                            ViewUtilidades.hiddenLoader(self)
                            print(resultado)
                            if resultado.isEqualToString("ok") {
                                //codigo_tok = codigo_token
                                //return codigo_token
                                
                                
                            } else {
                                let alert = UIAlertController(title: "Veris", message: "error", preferredStyle: UIAlertControllerStyle.alert)
                                let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                                    UIAlertAction in
                                    
                                }
                                alert.addAction(okAction)
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                        })*/
                    } catch {
                        print("ERROR")
                        ViewUtilidades.hiddenLoader(controller: self)
                        let alert = UIAlertController(title: "Veris", message: "Revise su conexión a Datos o Internet", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
                
                
            }
            
        } else {
            ViewUtilidades.hiddenLoader(controller: self)
            let alert = UIAlertController(title: "Veris", message: "Ha ocurrido un error. Intente nuevamente", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                UIAlertAction in
                
            }
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
    
    
    
    @IBAction func verAcercaDe(sender: AnyObject) {
        print("Mostrar pantalla de acerca de ")
        /*let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("viewAcerca") as! AcercaDe
        navigationController?.pushViewController(vc, animated: true)*/
    }
    
    
    private func consultarrecomendaciones(){
        self.lista_recomendacion.removeAllObjects()
        let user = Usuario.getEntity as Usuario
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        
        var pagodir = "N"
        let defaults = UserDefaults.standard
        let varpagodire : Bool = defaults.bool(forKey: "pagodirecto")
        
        if varpagodire{
            pagodir = "S"
            print("pago directo S")
        }else{
            pagodir = "N"
            print("pago directo N")
        }
        
        let url2 = URLFactory.obtenerRecomendaciones(tipo_doc: user.tipoid as String, num_doc: user.numeroidentificacion as String, desconocido: pagodir)
        print(url2)
        
        let url = NSURL(string: "\(url2)")
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        let str = "wsappusuario:ZA$57@9b86@$2r5"
        
        let base64Encoded = str.data(using: String.Encoding.utf8, allowLossyConversion: true)
        //let base64Encoded = str.dataUsingEncoding(NSUTF8StringEncoding)!.base64EncodedStringWithOptions([])
        print("Encoded: \(base64Encoded)")

        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            //println(NSString(data: data, encoding: NSUTF8StringEncoding))
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                    var err: NSError?
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    if (err != nil) {
                        print("JSON Error \(err!.localizedDescription)")
                    }
                    
                    let info =  jsonResult as NSDictionary
                    let data = info["lista"] as! [[String: Any]]
                    
                    if data.count > 0 {
                        //self.lista_resul_img = NSMutableArray();
                        for jsonobj  in data {
                            let recomendacion = ObjRecomendacion()
                            if (jsonobj["numeroorden"] is NSNull){
                                recomendacion.numeroorden = "0"
                            }else
                            {
                                recomendacion.numeroorden = jsonobj["numeroorden"] as! NSString
                            }
                            if (jsonobj["codigoempresa"] is NSNull){
                                recomendacion.codigoempresa = 0
                            }else
                            {
                                recomendacion.codigoempresa = jsonobj["codigoempresa"] as! NSInteger
                            }
                            if (jsonobj["lineadetalle"] is NSNull){
                                recomendacion.lineadetalle = "0"
                            }else
                            {
                                recomendacion.lineadetalle = jsonobj["lineadetalle"] as! NSString
                            }
                            if (jsonobj["eschequeo"] is NSNull){
                                recomendacion.eschequeo = ""
                            }else
                            {
                                recomendacion.eschequeo = jsonobj["eschequeo"] as! NSString
                            }
                            if (jsonobj["numeroidentificacionpaciente"] is NSNull){
                                recomendacion.numeroidentificacionpaciente = 0
                            }else
                            {
                                recomendacion.numeroidentificacionpaciente = jsonobj["numeroidentificacionpaciente"] as! NSInteger
                            }
                            if (jsonobj["codigotipoidentificacion"] is NSNull){
                                recomendacion.codigotipoidentificacion = ""
                            }else
                            {
                                recomendacion.codigotipoidentificacion = jsonobj["codigotipoidentificacion"] as! NSString
                            }
                            if (jsonobj["nombrepaciente"] is NSNull){
                                recomendacion.nombrepaciente = ""
                            }else
                            {
                                recomendacion.nombrepaciente = jsonobj["nombrepaciente"] as! NSString
                            }
                            if (jsonobj["codigomedico"] is NSNull){
                                recomendacion.codigomedico = ""
                            }else
                            {
                                recomendacion.codigomedico = jsonobj["codigomedico"] as! NSString
                            }
                            if (jsonobj["nombremedicointerno"] is NSNull){
                                recomendacion.nombremedicointerno = ""
                            }else
                            {
                                recomendacion.nombremedicointerno = jsonobj["nombremedicointerno"] as! NSString
                            }
                            if (jsonobj["nombremedicoexterno"] is NSNull){
                                recomendacion.nombremedicoexterno = ""
                            }else
                            {
                                recomendacion.nombremedicoexterno = jsonobj["nombremedicoexterno"] as! NSString
                            }
                            if (jsonobj["codigoprestacion"] is NSNull){
                                recomendacion.codigoprestacion = "0"
                            }else
                            {
                                recomendacion.codigoprestacion = jsonobj["codigoprestacion"] as! NSString
                            }
                            if (jsonobj["nombreprestacion"] is NSNull){
                                recomendacion.nombreprestacion = ""
                            }else
                            {
                                recomendacion.nombreprestacion = jsonobj["nombreprestacion"] as! NSString
                            }
                            if (jsonobj["codigoservicio"] is NSNull){
                                recomendacion.codigoservicio = ""
                            }else
                            {
                                recomendacion.codigoservicio = jsonobj["codigoservicio"] as! NSString
                            }
                            if (jsonobj["nombreservicio"] is NSNull){
                                recomendacion.nombreservicio = ""
                            }else
                            {
                                recomendacion.nombreservicio = jsonobj["nombreservicio"] as! NSString
                            }
                            if (jsonobj["codigoestado"] is NSNull){
                                recomendacion.codigoestado = ""
                            }else
                            {
                                recomendacion.codigoestado = jsonobj["codigoestado"] as! NSString
                            }
                            if (jsonobj["requiereagendamientoprevio"] is NSNull){
                                recomendacion.requiereagendamientoprevio = ""
                            }else
                            {
                                recomendacion.requiereagendamientoprevio = jsonobj["requiereagendamientoprevio"] as! NSString
                            }
                            if (jsonobj["codigoespecialidad"] is NSNull){
                                recomendacion.codigoespecialidad = 0
                            }else
                            {
                                recomendacion.codigoespecialidad = jsonobj["codigoespecialidad"] as! NSInteger
                            }
                            if (jsonobj["esprocedimiento"] is NSNull){
                                recomendacion.esprocedimiento = ""
                            }else
                            {
                                recomendacion.esprocedimiento = jsonobj["esprocedimiento"] as! NSString
                            }
                            if (jsonobj["fechaorden"] is NSNull){
                                recomendacion.fechaorden = ""
                            }else
                            {
                                recomendacion.fechaorden = jsonobj["fechaorden"] as! NSString
                            }
                            if (jsonobj["codigopaquete"] is NSNull){
                                recomendacion.codigopaquete = 0
                            }else
                            {
                                recomendacion.codigopaquete = jsonobj["codigopaquete"] as! NSInteger
                            }
                            if (jsonobj["codigocpt"] is NSNull){
                                recomendacion.codigocpt = ""
                            }else
                            {
                                recomendacion.codigocpt = jsonobj["codigocpt"] as! NSString
                            }
                            if (jsonobj["codigoalterno"] is NSNull){
                                recomendacion.codigoalterno = ""
                            }else
                            {
                                recomendacion.codigoalterno = jsonobj["codigoalterno"] as! NSString
                            }
                            if (jsonobj["esagendable"] is NSNull){
                                recomendacion.esagendable = ""
                            }else
                            {
                                recomendacion.esagendable = jsonobj["esagendable"] as! NSString
                            }
                            if (jsonobj["nombreespecialidad"] is NSNull){
                                recomendacion.nombreespecialidad = ""
                            }else
                            {
                                recomendacion.nombreespecialidad = jsonobj["nombreespecialidad"] as! NSString
                            }
                            if (jsonobj["codigoespecialidadmedico"] is NSNull){
                                recomendacion.codigoespecialidadmedico = 0
                            }else
                            {
                                recomendacion.codigoespecialidadmedico = jsonobj["codigoespecialidadmedico"] as! NSInteger
                            }
                            if (jsonobj["esconsulta"] is NSNull){
                                recomendacion.esconsulta = ""
                            }else
                            {
                                recomendacion.esconsulta = jsonobj["esconsulta"] as! NSString
                            }
                            self.lista_recomendacion.add(recomendacion)
                        }
                    }
                    DispatchQueue.main.async {
                        self.cargapantallarecomendacion()
                        ViewUtilidades.hiddenLoader(controller: self)
                    }

                    /*DispatchQueue.main.asynchronously(DispatchQueue.mainexecute: {
                        self.cargapantallarecomendacion()
                        ViewUtilidades.hiddenLoader(self)
                    })*/
                } catch {
                    print("ERROR al cargar")
                    ViewUtilidades.hiddenLoader(controller: self)
                }
            }
        }
    }
    
    func cargapantallarecomendacion(){
        if self.lista_recomendacion.count < 0{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "viewReservar") as! ReservaViewController
            vc.tipodecarga = TipoCargaReserva.nueva
            navigationController?.pushViewController(vc, animated: true)
        }else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "viewRecomendaciones") as! RecomendacionesController
            vc.lista_recomendacion = self.lista_recomendacion
            navigationController?.pushViewController(vc, animated: true)
        }
        print("agregar carga pantalla recomentacion")
    }
    
    func consultarcitasdehoy(){
        var texto : String = ""
        let user = Usuario.getEntity as Usuario
        if(user.logueado){
            //ViewUtilidades.showLoader(self, title: "Cargando...")
            let url2 = URLFactory.obtenercitacercanadehoy(tipo_doc: user.tipoid as String, num_doc: user.numeroidentificacion as String)
            
            let url = NSURL(string: "\(url2)")
            print(url!)
            //let request = NSURLRequest(URL: url!)
            let request = NSMutableURLRequest(url: url! as URL)
            
            let str = "wsappusuario:ZA$57@9b86@$2r5"
            
            let base64Encoded = str.data(using: String.Encoding.utf8, allowLossyConversion: true)
            print("Encoded: \(base64Encoded)")
    //           let base64DecodedString = String(data: base64DecodedData as! Data, encoding: String.Encoding.utf8)!
   //         print("Decoded: \(base64DecodedString)")
            
            /*if let base64Encoded = utf8str?.base64EncodedStringWithOptions(NSData.Base64EncodingOptions(rawValue: 0)){
                
                print("Encoded:  \(base64Encoded)")
                //request.addValue("Authorization", forHTTPHeaderField: "Basic \(base64Encoded)")
                request.setValue("Basic \(base64Encoded)", forHTTPHeaderField: "Authorization")
                
                if let base64Decoded = NSData(base64EncodedString: base64Encoded, options:   NSDataBase64DecodingOptions(rawValue: 0))
                    .map({ NSString(data: $0, encoding: NSUTF8StringEncoding) })
                {
                    // Convert back to a string
                    print("Decoded:  \(base64Decoded)")
                }
            }*/
            
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                //println(NSString(data: data, encoding: NSUTF8StringEncoding))
                
                if (error != nil) {
                    print(error!.localizedDescription)
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    do{
                        var err: NSError?
                        
                        //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                        
                        let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        if (err != nil) {
                            print("JSON Error \(err!.localizedDescription)")
                        }
                        
                        let info =  jsonResult as NSDictionary
                        let data = info["lista"] as! NSArray
                        
                        if data.count > 0 {
                            let jsonobj : NSDictionary = data[0] as! NSDictionary
                            let fecha : String = jsonobj["fecha"] as! NSString as String
                            let medico : String = jsonobj["medico"] as! NSString as String
                            let nombresucursal : String = jsonobj["nombresucursal"] as! NSString as String
                            
                            //let texto1 = "Su próxima cita es el "+fecha+" con el Dr(a) "+medico.lowercased
                            //texto = texto1 +" EN "+nombresucursal;
                            texto = "Su próxima cita es el "
                            texto += fecha
                            texto += " con el Dr(a) "
                            texto += medico.lowercased()
                            texto += " EN "
                            texto += nombresucursal
                        }
                        
                        DispatchQueue.main.async {
                            if !texto.isEmpty{
                                UtilidadesGeneral.mensaje(mensaje: texto)
                            }
                        }
                        /*DispatchQueue.main.asynchronously(execute: {
                            if !texto.isEmpty{
                                UtilidadesGeneral.mensaje(mensaje: texto)
                            }
                        })*/
                    } catch {
                        print("ERROR")
                        ViewUtilidades.hiddenLoader(controller: self)
                        let alert = UIAlertController(title: "Veris", message: "Revise su conexión a Datos o Internet", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
                
            }
            //////////////////////////
        }
    }
    
    func registerBackgroundTask() {
        //UIApplication.sharedApplication().
        backgroundTask = UIApplication.shared.beginBackgroundTask {
            [unowned self] in
            self.endBackgroundTask()
        }
        assert(backgroundTask != UIBackgroundTaskInvalid)
    }
    
    func endBackgroundTask() {
        NSLog("Background task ended.")
        //UIApplication.sharedApplication().endBackgroundTask(backgroundTask)
        //backgroundTask = UIBackgroundTaskInvalid
    }
    func buscarCitas(){
        NSLog("nuevo buscando citas")
        let us = Usuario.getEntity as Usuario
        self.obtenerCitas(tipo: us.tipoid as String, identificacion: us.numeroidentificacion as String)
        
    }
    func calculateNextNumber() {
        print("buscar nuevas citas")
        let result = current.adding(previous)
        
        let bigNumber = NSDecimalNumber(mantissa: 1, exponent: 40, isNegative: false)
        if result.compare(bigNumber) == .orderedAscending {
            previous = current
            current = result
            position += 1
        }
        else {
            // This is just too much.... Start over.
            //resetCalculation()
        }
        
        let resultsMessage = "Position \(position) = \(current)"
        
        switch UIApplication.shared.applicationState {
        case .active:
            //resultsLabel.text = resultsMessage
            print(resultsMessage)
        case .background:
            NSLog("App is backgrounded. Next number = %@", resultsMessage)
            NSLog("Background time remaining = %.1f seconds", UIApplication.shared.backgroundTimeRemaining)
        case .inactive:
            break
        }
    }
    
    func obtenerCitas(tipo : String , identificacion : String) {
        let urlAsString = URLFactory.obtenercitasvigentes(tipoidentificacion: tipo, identificacion: identificacion, esadmin: "S", usua_logeado: identificacion)
        print(urlAsString.absoluteString!)
        self.getJSONData(url: urlAsString.absoluteString!)
    }
    
    @IBAction func shared(_ sender: UIButton) {
        print("clickeo")
        let message = "¡Hola! Te recomiendo usar Mi Veris, descárgala aquí"
        //Set the link to share. https://itunes.apple.com/ec/app/mi-veris/id967428498?mt=8
        if let link = NSURL(string: "http://www.veris.com.ec/aplicacion-miveris/")
        {
            let objectsToShare = [message,link] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
            self.present(activityVC, animated: true, completion: nil)
        }

    }
    func getJSONData(url: String) {
        
        let url = NSURL(string: url as String)
        //let request = NSURLRequest(URL: url!)
        print("ver   que url esta enviando")
        print(url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        let str = "wsappusuario:ZA$57@9b86@$2r5"
        
        let base64Encoded = str.data(using: String.Encoding.utf8, allowLossyConversion: true)
        //let base64Encoded = str.dataUsingEncoding(NSUTF8StringEncoding)!.base64EncodedStringWithOptions([])
        print("Encoded: \(base64Encoded)")
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                let json = JSON(data: data!)
                
                //Comprobamos si todo ha salido bien, en caso contrario se le muestra el mensaje de error al usuario
                if json["resultado"].string == "ok" {
                    if let citasArray = json["lista"].array {
                        
                        //Creamos nuestro objeto Citas con los datos obtenidos del JSON
                        for citasDic in citasArray {
                            let citaFecha: String? = citasDic["fecha"].stringValue
                            let citaSucursal: String? = citasDic["nombresucursal"].stringValue
                            let citaPrestacion: String? = citasDic["nombreespecialidad"].stringValue
                            let citaMedico: String? = citasDic["medico"].stringValue
                            let citaPuntuacion: String? = citasDic["puntaje"].stringValue
                            let citaReserva: String? = citasDic["codigoreserva"].stringValue
                            
                            let hcodigoespecialidad : String? = citasDic["codigoespecialidad"].stringValue
                            let hcodigomedico : String? = citasDic["codigomedico"].stringValue
                            let hcodigoempresa : String? = citasDic["codigoempresa"].stringValue
                            let hcodigosucursal : String? = citasDic["codigosucursal"].stringValue
                            let hcodigoregion : String? = citasDic["codigoregion"].stringValue
                            /*cambio 23 junio*/
                            let hcodigoprestacion :String? = citasDic["codigoprestacion"].stringValue
                            let hcodigoempresaprestacion :String? = citasDic["codigoempresaprestacion"].stringValue
                            let hnombreprestacion : String? = citasDic["prestacion"].stringValue
                            let estadocita : String? = citasDic["esPagada"].stringValue
                            
                            let usuario = Usuario.getEntity as Usuario
                            _ = MisCitasModel(fecha: citaFecha, nombresucursal: citaSucursal, prestacion: citaPrestacion, medico: citaMedico, puntuacion: citaPuntuacion, reserva: citaReserva, codigoespecialidad: hcodigoespecialidad, codigomedico: hcodigomedico, codigoempresa: hcodigoempresa, codigosucursal: hcodigosucursal, codigoregion: hcodigoregion,codigoprestacion: hcodigoprestacion, codigoempresapres: hcodigoempresaprestacion, nombreprestacion: hnombreprestacion, nombrePaciente: "", usuario: usuario, espagada: estadocita)
                         //   let not : notificacion = notificacion()
                            
                            //not.addItem(cita.fecha)
                            //self.citas.append(cita)
                            
                            /*Fin cambio 23 junio*/
                        }
                        //Uso para debug
                        //println(self.citas)
                        
                    }
                } else {
                    let jsonError = json["resultado"].string
                    //print(jsonError!)
                }
                
                ViewUtilidades.hiddenLoader(controller: self)
                /*DispatchQueue.main.asynchronously(DispatchQueue.mainexecute: {
                    ViewUtilidades.hiddenLoader(self)
                })*/
            }
            
            
        }
        
        
    }



}

