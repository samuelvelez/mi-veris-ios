//
//  AppDelegate.swift
//  Mi Veris
//
//  Created by Samuel Velez on 21/9/16.
//  Copyright © 2016 Samuel Velez. All rights reserved.
//

import UIKit
import EventKit
//#import <Google/Analytics.h>

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var eventStore: EKEventStore?
    var initialViewController : UIViewController?
    
    let iOS7 = floor(NSFoundationVersionNumber) <= floor(NSFoundationVersionNumber_iOS_7_1)
    let iOS8 = floor(NSFoundationVersionNumber) > floor(NSFoundationVersionNumber_iOS_7_1)
    
    var locationManager: CLLocationManager?
    let healthManager:HealthManager = HealthManager()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        
        
        //let usuario = Usuario.getEntity as Usuario
        
                if let myid = (UserDefaults.standard.object(forKey: "identificacion")) {
        
                    // EL USUARIO ESTA LOGEADO
                    print("logeado, manda datos healtKit")
                    self.mandarDatosHealthKit()
                   // self.verificarToken()
                    
        
                }
                else
                {
                    // EL USUARIO NO ESTA LOGEADO
                    print("no logeado, no manda datos healtKit")
        
                }
        
        // por ahora asumamos que es la primera vez que sale esta variable 
        //UserDefaults.standard.set(false, forKey: "debecalificar")
        //UserDefaults.standard.removeObject(forKey: "debecalificar")
        //UserDefaults.standard.removeObject(forKey: "califico")
       // self.mandarDatosHealthKit() // Usar solo esta funcion cuando se esta logueado, arriba se intento implementar pero el metodo logueo no funciona correctamente

        // Override point for customization after application launch.
        // Configure tracker from GoogleService-Info.plist.
        // Configure tracker from GoogleService-Info.plist.
        var configureError:NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        
        // Optional: configure GAI options.
        let gai = GAI.sharedInstance()
        gai?.trackUncaughtExceptions = true  // report uncaught exceptions
        //gai?.logger.logLevel = GAILogLevel.verbose  // remove before app release
        
        //notifications
        let notificationTypes: UIUserNotificationType = [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound]
        let pushNotificationSettings = UIUserNotificationSettings(types: notificationTypes, categories: nil)
        
        application.registerUserNotificationSettings(pushNotificationSettings)
        application.registerForRemoteNotifications()
        
        
        
        return true
    }
    func applicationDidFinishLaunching(_ application: UIApplication) {
        print("entroooooooo")
    }
    
    
    /*UNUserNotificationCenter.current().getNotificationSettings(){ (setttings) in
    
    switch setttings.soundSetting{
    case .enabled:
    
    print("enabled sound setting")
    
    case .disabled:
    
    print("setting has been disabled")
    
    case .notSupported:
    print("something vital went wrong here")
    }
    }
    */
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("DEVICE TOKEN = \(deviceToken)")
        let deviceTokenString = deviceToken.toHexString()//reduce("", {$0 + String(format: "%02X", $1)})
        print(deviceTokenString)

        let token =
            UserDefaults.standard.set(deviceToken, forKey: "deviceToken")
        UserDefaults.standard.synchronize()
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print(userInfo)
        var tipo : String = ""
        var tipoorden : String = ""
        if let aps = userInfo["aps"] as? NSDictionary {
            print("entro a 1: \(aps)")
           /* print(userInfo["secret"]!)
            print(userInfo["titulo"]!)
            print(userInfo["tipo"]!)
            print(userInfo["tiporden"]!)
            tipo = userInfo["tipo"]! as! String
            tipoorden = userInfo["tiporden"]! as! String*/
            
        }
        if(tipo == "C"){
            
            let alert = UIAlertController(title: "Veris", message: userInfo["secret"]! as? String, preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                UIAlertAction in
                UIApplication.shared.applicationIconBadgeNumber = 0
                
            }
            
            alert.addAction(okAction)
            //self.presentViewController(alert, animated: true, completion: nil)
            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
        else if(tipo == "R"){
            print("es de resultado")
            let navigationController = self.window?.rootViewController
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
            if(tipoorden == "L"){
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "viewresulab") as! ResultadosLabController
                viewController.not = true
                let navController = UINavigationController(rootViewController: viewController)
                navigationController?.present(navController, animated:true, completion: nil)
            }else if(tipoorden == "I"){
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "viewresuimg") as! ResultadosImagController
                
                viewController.not = true
                let navController = UINavigationController(rootViewController: viewController)
                navigationController?.present(navController, animated:true, completion: nil)
            }else if(tipoorden == "P"){
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "viewResultadosProcedimientos") as! ResultadosProcController
                viewController.not = true
                let navController = UINavigationController(rootViewController: viewController)
                navigationController?.present(navController, animated:true, completion: nil)
            }else{
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "viewMisResultados") as! MisResultadosViewController
                viewController.not = true
                let navController = UINavigationController(rootViewController: viewController)
                navigationController?.present(navController, animated:true, completion: nil)
            }
            
            
            
        }
        
    }
    
    func onTokenRefresh() {
        // A rotation of the registration tokens is happening, so the app needs to request a new token.
        print("The GCM registration token needs to be changed.")
        // GGLInstanceID.sharedInstance().tokenWithAuthorizedEntity(gcmSenderID, scope: kGGLInstanceIDScopeGCM, options: registrationOptions, handler: registrationHandler)
    }
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func mandarDatosHealthKit()
    {
        print("\n Consulta de los datos HealthKit, la app espera 6 segundos para poder recuperar todos los datos ya que se recuperan en forma asincrona \n")
        
        let objeto = ObjetoDatosHealthKit( sexo: "", sangre: "", fechaDeNacimiento: "", altura: "", peso: "", imc: "", contadorDePasos: "", distanciaRecorrida: "", distanciaPosición: "", energiaQuemadaReposo: "", energiaQuemadaActiva: "", frecuenciaCardiaca: "")
        ObjetoDatosHealthKit.getDatos().add(objeto)
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        backgroundThread(6.0,background: {
            // Your function here to run in the background
            
            
            let extensionesHealthKit = UIImage()
            extensionesHealthKit.reinciarContadorParaRefrescarObjeto()
            extensionesHealthKit.updateProfileInfo(healthManager: self.healthManager)
            extensionesHealthKit.updateWeight(healthManager: self.healthManager)
            extensionesHealthKit.updateHeight(healthManager: self.healthManager)
            extensionesHealthKit.updateIMC(healthManager: self.healthManager);
            extensionesHealthKit.updateContadorPasos(healthManager: self.healthManager)
            extensionesHealthKit.updateDistanciaRecorrida(healthManager: self.healthManager)
            extensionesHealthKit.updateEnergiaQuemadaReposo(healthManager: self.healthManager)
            extensionesHealthKit.updateEnergiaQuemadaActiva(healthManager: self.healthManager)
            extensionesHealthKit.updateHeartRate(healthManager: self.healthManager)
            
            
            },
                         completion: {
                            
                            print("\n Mandar JSON con Arreglo HealthKit")
                            
                            NSString().remplazarVaciosPorCero() //Remplaza en el objeto los campos nulos ("") por ceros
                            
                            let objeto = ObjetoDatosHealthKit.getDatos().object(at: 0) as! ObjetoDatosHealthKit
                            
                            let extensionMandarDatoPost = NSString()
                            let arregloIndicadores = [] as NSMutableArray
                            
                            //print(NSUserDefaults.standardUserDefaults().objectForKey("alturakit"))
                            //print("contador kit \(objeto.contadorDePasos)")
                            //print(NSUserDefaults.standardUserDefaults().objectForKey("contadorkit"))
                            
                            let objetoKitAltura  =  ["valor" : objeto.altura , "idindicador" : 1] as [String : Any]
                            arregloIndicadores.add(objetoKitAltura)
                            UserDefaults.standard.set(objeto.altura, forKey: "alturakit")
                            // LLENAR ALTURA 1  arg 1
                            
                            let objetoKitPeso  =  ["valor" : objeto.peso , "idindicador" : 2] as [String : Any]
                            arregloIndicadores.add(objetoKitPeso)
                            UserDefaults.standard.set(objeto.peso, forKey: "pesokit")
                            // LLENAR PESO 2 arg 2
                            
                            let objetoKitPasos  =  ["valor" : objeto.contadorDePasos , "idindicador" : 3] as [String : Any]
                            arregloIndicadores.add(objetoKitPasos)
                            UserDefaults.standard.set(objeto.contadorDePasos, forKey: "contadorkit")
                            // LLENAR CONTADOR PASOS 3 arg 3
                            
                            let objetoKitDistancia  =  ["valor" : objeto.distanciaRecorrida , "idindicador" : 4] as [String : Any]
                            arregloIndicadores.add(objetoKitDistancia)
                            UserDefaults.standard.set(objeto.distanciaRecorrida, forKey: "drecorridakit")
                            // LLENAR DISTACIA RECORRIDA4 arg 4
                            
                            let objetoKitFrecuenciaCardiaca  =  ["valor" : objeto.frecuenciaCardiaca , "idindicador" : 5] as [String : Any]
                            arregloIndicadores.add(objetoKitFrecuenciaCardiaca)
                            UserDefaults.standard.set(objeto.frecuenciaCardiaca, forKey: "frecuenciakit")
                            // LLENAR FRECUENCIA 5 arg 5
                            
                            let objetoKitEnergiaQuemadaActiva = ["valor" : objeto.energiaQuemadaActiva , "idindicador" : 9] as [String : Any]
                            arregloIndicadores.add(objetoKitEnergiaQuemadaActiva)
                            UserDefaults.standard.set(objeto.energiaQuemadaActiva, forKey: "eqakit")
                            // LLENAR EQA 6 arg 9
                            
                            let objetoKitEnergiaQuemadaReposo  =  ["valor" : objeto.energiaQuemadaReposo , "idindicador" : 10] as [String : Any]
                            arregloIndicadores.add(objetoKitEnergiaQuemadaReposo)
                            UserDefaults.standard.set(objeto.energiaQuemadaReposo, forKey: "eqrkit")
                            // LLENAR EQR 7 arg 10
                            
                            let objetoKitFechaNacimiento  =  ["valor" : objeto.fechaDeNacimiento , "idindicador" : 12] as [String : Any]
                            arregloIndicadores.add(objetoKitFechaNacimiento)
                            UserDefaults.standard.set(objeto.fechaDeNacimiento, forKey: "fechakit")
                            // LLENAR NACIMIENTO 8 arg 12
                            
                            let objetoKitImc  =  ["valor" : objeto.imc , "idindicador" : 13] as [String : Any]
                            arregloIndicadores.add(objetoKitImc)
                            UserDefaults.standard.set(objeto.imc, forKey: "imckit")
                            // LLENAR IMC 9 arg 13
                            
                            let objetoKitSexo  =  ["valor" : objeto.sexo , "idindicador" : 14] as [String : Any]
                            arregloIndicadores.add(objetoKitSexo)
                            UserDefaults.standard.set(objeto.sexo, forKey: "sexokit")
                            // LLENAR SEXO 10 arg 14
                            
                            let objetoKitSangre  =  ["valor" : objeto.sangre , "idindicador" : 15] as [String : Any]
                            arregloIndicadores.add(objetoKitSangre)
                            UserDefaults.standard.set(objeto.sangre, forKey: "sangrekit")
                            // LLENAR SANGRE 11 arg15
                            
                            UserDefaults.standard.synchronize()
                            //print(arregloIndicadores)
                            extensionMandarDatoPost.sendJSONHealthKit(arregloDatos: arregloIndicadores)
                            // A function to run in the foreground when the background thread is complete
        })
        
    }
    
    func backgroundThread(_ delay: Double = 0.0, background: (() -> Void)? = nil, completion: (() -> Void)? = nil) {
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
            if(background != nil){ background!(); }
            
            let popTime = DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: popTime) {
                if(completion != nil){ completion!(); }
            }
        }
    }


}

