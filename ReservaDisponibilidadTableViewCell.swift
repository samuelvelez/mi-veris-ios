//
//  ReservaDisponibilidadTableViewCell.swift
//  Mi Veris
//
//  Created by Andres Cantos on 2/8/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import UIKit

class ReservaDisponibilidadTableViewCell: UITableViewCell {

    var doctor : UILabel = UILabel()
    var fecha : UILabel = UILabel()
    var duracion : UILabel = UILabel()
    var izqnumdia : UILabel = UILabel()
    var izqmes : UILabel = UILabel()
    var izqhora : UILabel = UILabel()
    var barver : UIView = UIView(frame: CGRect(x:61, y:3, width:3, height:64))
    var izqrect : UIView = UIView(frame: CGRect(x:0, y:1, width:60, height:69))
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.clear
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width;
        
        /*
        let arrow = UIImageView(frame: CGRectMake(screenWidth-30, 17, 15, 24))
        arrow.image = UIImage(named: "flecha.png")
        imagen = UIImageView(frame: CGRectMake(10, 10, 70, 50))
        imagen.backgroundColor = UIColor.grayColor()
        fecha.numberOfLines = 0
        */
        
        //barver = UIView(frame: CGRectMake(61, 3, 3, 64))
        barver.backgroundColor = UtilidadesColor.colordehexadecimal(hex:0x65c2ff, alpha: 1.0)
        izqrect.backgroundColor = UtilidadesColor.colordehexadecimal(hex:0xfffca5, alpha: 1.0)
        
        doctor.frame = CGRect(x:70, y:6, width:screenWidth, height:30)
        doctor.font = UIFont(name: "helvetica", size: 12)
        doctor.textColor = UtilidadesColor.colordehexadecimal(hex:0x125c9a, alpha: 1.0)
        
        fecha.frame = CGRect(x:70, y:26, width:screenWidth, height:30)
        fecha.font = UIFont(name: "helvetica-light", size: 12)
        
        duracion.frame = CGRect(x:70, y:46, width:screenWidth, height:30)
        duracion.font = UIFont(name: "helvetica-light", size: 12)
        
        izqnumdia.frame = CGRect(x:0, y:6, width:60, height:21)
        izqnumdia.font = UIFont(name: "helvetica-light", size: 14)
        izqnumdia.textAlignment = NSTextAlignment.center
        
        izqmes.frame = CGRect(x:0, y:27, width:60, height:21)
        izqmes.font = UIFont(name: "helvetica", size: 15)
        izqmes.textAlignment = NSTextAlignment.center
        
        izqhora.frame = CGRect(x:0, y:48, width:60, height:21)
        izqhora.font = UIFont(name: "helvetica", size: 15)
        izqhora.textAlignment = NSTextAlignment.center
        
        self.contentView.addSubview(doctor)
        self.contentView.addSubview(fecha)
        self.contentView.addSubview(duracion)
        self.contentView.addSubview(barver)
        izqrect.addSubview(izqnumdia)
        izqrect.addSubview(izqmes)
        izqrect.addSubview(izqhora)
        self.contentView.addSubview(izqrect)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
