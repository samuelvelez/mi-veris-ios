//
//  MenuTableViewCell.swift
//  Mi Veris
//
//  Created by Samuel Velez on 27/7/16.
//  Copyright © 2016 Programadores-iOS.net. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    var icono : UIImageView = UIImageView()
    var separator : UIImageView = UIImageView();
    var title : UILabel = UILabel();
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.clear
        
        
        //let screenSize: CGRect = UIScreen.main.bounds
        //let screenWidth = 100.0;
        icono.frame = CGRect(x: 35, y: 5, width: 30, height: 30)
        //Make(35, 5, 30, 30)
        
        
        title.frame = CGRect(x: 0, y: 32, width: 100, height: 40) //Make(0, 32, 100, 40)
        title.textAlignment = NSTextAlignment.center
        title.lineBreakMode = NSLineBreakMode.byWordWrapping
        title.numberOfLines = 0
        title.font = UIFont(name: "helvetica", size: 12)
        title.textColor = UIColor(red: (48/256.0), green: (48/256.0), blue: (48/256.0), alpha: 1)
        
        
        
        self.contentView.addSubview(icono)
        self.contentView.addSubview(title)
        //self.contentView.addSubview(arrow)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

}
