//
//  DoctorFiltroTableViewCell.swift
//  Mi Veris
//
//  Created by Jorge on 05/02/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import UIKit

class DoctorFiltroTableViewCell: UITableViewCell {

    var imagen : UIImageView = UIImageView()
    var nombre : UILabel = UILabel()
    var especialidad : UILabel = UILabel()
    var doctor : Doctor!
    var verhoja : UIButton = UIButton()
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width;
        
        imagen.frame = CGRect(x: 10, y: 5, width: 60, height: 60)
        //imagen.self.backgroundColor = UIColor.grayColor();
        imagen.layer.cornerRadius = 30
        imagen.clipsToBounds = true
        imagen.image = UIImage(named: "medic.png")
        self.contentView.addSubview(imagen)
        
        nombre.frame = CGRect(x: 80, y: 5, width:screenWidth - 130 , height: 60)
        nombre.numberOfLines = 2
        nombre.font = UIFont(name: "helvetica-bold", size: 14)
        self.contentView.addSubview(nombre)
        
        especialidad.frame = CGRect(x: 80, y: 20, width: screenWidth - 80 - 80, height:60)
        especialidad.numberOfLines = 2
        especialidad.font = UIFont(name: "helvetica", size: 13)
        especialidad.font = UIFont.italicSystemFont(ofSize: 13)
        especialidad.textColor = UIColor.gray
        self.contentView.addSubview(especialidad)
        
        verhoja   = UIButton(type: UIButtonType.system)
        verhoja.frame = CGRect(x: 80, y: 60, width: screenWidth - 80 - 80, height: 20)
        verhoja.backgroundColor = UIColor.clear
        //verhoja.description
        verhoja.setTitleColor(UIColor.gray, for: UIControlState.normal)
        verhoja.setTitle("Hoja de Vida", for: UIControlState.normal)
        verhoja.titleLabel!.font =  UIFont(name: "helvetica-bold", size: 13)
        verhoja.contentHorizontalAlignment=UIControlContentHorizontalAlignment.left
        //reservar.addTarget(self, action: "action", forControlEvents: UIControlEvents.TouchUpInside)
        //verhoja.titleLabel!
        self.contentView.addSubview(verhoja)
        
        self.backgroundColor = UIColor.clear
        self.selectionStyle = UITableViewCellSelectionStyle.none;
        
        imagen.image = UIImage(named: "medic.png")
        //“/compartidos/portal/medicos/”+secuenciapersonal+".JPG
        
        
    }
    
    override func prepareForReuse() {
        imagen.removeFromSuperview()
        imagen.frame = CGRect(x: 10, y: 5, width: 60, height: 60)
        //imagen.self.backgroundColor = UIColor.grayColor();
        imagen.layer.cornerRadius = 30
        imagen.clipsToBounds = true
        imagen.image = UIImage(named: "medic.png")
        self.contentView.addSubview(imagen)
        self.accessoryType = UITableViewCellAccessoryType.none
    }
    
    func action(){
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }

}
