//
//  FavoritosViewController.swift
//  Mi Veris
//
//  Created by Jorge on 05/02/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import UIKit


class FavoritosViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate, UITextFieldDelegate{
    
    var tableView : UITableView = UITableView()
    var listaDoctores : NSMutableArray = NSMutableArray()
    
    var listaDoctoresFiltro : NSMutableArray = NSMutableArray()
    
    var resultadoBorrar : NSString = ""
    var setReload : Bool = false
    
    var txtBusqueda = UITextField()
    
    var nombreUser : UILabel = UILabel()
    
    var doctor : Doctor = Doctor()
    var espe : ObjEspecialidad = ObjEspecialidad()
    
    var servicio : ObjEspecialidad = ObjEspecialidad()
    
    var numeroOrden : String = ""
    var lineaDetalle : String = ""
    var codigoempresa : String = ""
    
    override func viewDidAppear(_ animated: Bool) {
        if setReload{
            setReload = false
            let usuario = Usuario.getEntity as Usuario
            if usuario.rol < 4 {
                ViewUtilidades.showLoader(controller: self, title: "Cargando...")
                
                let urldef = URLFactory.urldefinida()

                let url = NSURL(string: "\(urldef)servicio/v2/especialidad/obtenerdoctoresfavoritos?arg0=\(usuario.tipoid)&arg1=\(usuario.numeroidentificacion)")
                //let request = NSURLRequest(URL: url!)
                let request = NSMutableURLRequest(url: url! as URL)
                
                
                NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                    
                    if (error != nil) {
                        print(error!.localizedDescription)
                        ViewUtilidades.hiddenLoader(controller: self)
                        let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                    } else {
                        do{
                        var err: NSError?
                        
                        //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                        
                        let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        if (err != nil) {
                            print("JSON Error \(err!.localizedDescription)")
                        }
                        
                        //println(jsonResult);
                        print(jsonResult)
                        let info =  jsonResult as NSDictionary
                        let data = info["lista"] as! [[String: Any]]
                        if data.count > 0 {
                            self.listaDoctores.removeAllObjects()
                            self.listaDoctoresFiltro.removeAllObjects()
                            for doctor in data {
                                let doc = Doctor()
                                doc.idTipoID = doctor["id_tipodeidentificacion"] as! NSInteger
                                doc.idDoctor = doctor["id_doctor"] as! NSInteger
                                doc.idEspecialidad = doctor["id_especialidad"] as! NSInteger
                                doc.nombre = doctor["nombredoctor"] as! String
                                doc.identificacion = doctor["identificacion"] as! String
                                
                                //doc.nombreEspecialidad = doctor["nombreespecialidad"] as String
                                if (doctor["nombreespecialidad"] is NSNull){
                                    doc.nombreEspecialidad = ""
                                }else{
                                    doc.nombreEspecialidad = doctor["nombreespecialidad"] as! String
                                }
                                //doc.primerApellido = doctor["primerapellido"] as String
                                //doc.segundoApellido = doctor["segundoapellido"] as String
                                //doc.segundoNombre = doctor["segundonombre"] as String
                                doc.secuenciaPersonal = doctor["secuenciapersonal"] as! String
                                self.listaDoctores.add(doc)
                                self.listaDoctoresFiltro.add(doc)
                            }
                        }
                        
                        //dispatch_async(dispatch_get_main_queue(), {
                        DispatchQueue.main.async(execute: {
                            self.tableView.reloadData()
                            ViewUtilidades.hiddenLoader(controller: self)
                        })
                        } catch {
                            print("ERROR")
                        }
                    }
                    
                    
                }
                //////////////////////////
                
            } else {
                self.listaDoctores.removeAllObjects()
                self.listaDoctoresFiltro.removeAllObjects()
                self.tableView.reloadData()
                UtilidadesGeneral.mensaje(mensaje: "No tiene permisos para utilizar esta opción")
            }
            
        }
        print("listo para usars")
        let user = Usuario.getEntity
        nombreUser.text = user.nombre as String
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = ""

        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        let fondo : UIImageView = UIImageView(frame: self.view.bounds)
        fondo.image = UIImage(named:"fondodoctor copia.png")
        self.view.addSubview(fondo)
        
        let barWhite : UIView = UIView(frame: CGRect(x:0, y:64, width: self.view.frame.width , height:30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x:0, y:64, width:self.view.frame.width / 1.8, height:30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x:barBlue.frame.width, y:64, width:45, height:30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x:0, y:64, width:self.view.frame.width - 20, height:30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "Favoritos"
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)
                
        let usuario = Usuario.getEntity as Usuario
        
        nombreUser = UILabel(frame: CGRect(x:10, y:94, width:self.view.frame.width - 20, height: 30))
        nombreUser.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        nombreUser.font = UIFont(name: "helvetica-bold", size: 12)
        nombreUser.text = usuario.nombre as String
        
        self.view.addSubview(nombreUser)
        
        Usuario.createButtonFamily(controlador: self)
        
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        
        let urldef = URLFactory.urldefinida()

        let url = NSURL(string: "\(urldef)servicio/v2/especialidad/obtenerdoctoresfavoritos?arg0=\(usuario.tipoid)&arg1=\(usuario.numeroidentificacion)")
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do {
                var err: NSError?
                
                //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                if (err != nil) {
                    print("JSON Error \(err!.localizedDescription)")
                }
                
                //println(jsonResult);
                print(jsonResult)
                let info =  jsonResult as NSDictionary
                let data = info["lista"] as! [[String: Any]]
                if data.count > 0 {
                    for doctor in data {
                        let doc = Doctor()
                        doc.idTipoID = doctor["id_tipodeidentificacion"] as! NSInteger
                        doc.idDoctor = doctor["id_doctor"] as! NSInteger
                        doc.idEspecialidad = doctor["id_especialidad"] as! NSInteger
                        doc.nombre = doctor["nombredoctor"] as! String
                        doc.identificacion = doctor["identificacion"] as! String
                        //doc.nombreEspecialidad = doctor["nombreespecialidad"] as String
                        if (doctor["nombreespecialidad"] is NSNull){
                            doc.nombreEspecialidad = ""
                        }else{
                            doc.nombreEspecialidad = doctor["nombreespecialidad"] as! String
                        }
                        //doc.primerApellido = doctor["primerapellido"] as String
                        //doc.segundoApellido = doctor["segundoapellido"] as String
                        //doc.segundoNombre = doctor["segundonombre"] as String
                        doc.secuenciaPersonal = doctor["secuenciapersonal"] as! String
                        self.listaDoctores.add(doc)
                        self.listaDoctoresFiltro.add(doc)
                    }
                }
                
                //dispatch_async(dispatch_get_main_queue(), {
                    DispatchQueue.main.async(execute: {
                    self.mostrarDoctores()
                    ViewUtilidades.hiddenLoader(controller: self)
                })
                } catch {
                    print("ERROR")
                }
            }
            
            
        }
        //////////////////////////
        
    }
    
    func getFamilyService(){
        Usuario.getJsonFamilia(controlador: self)
        setReload = true
    }
    
    func mostrarDoctores(){
        
        let viewSearch = UIView()
        viewSearch.frame =  CGRect(x:0, y:124, width:self.view.frame.width, height:40);
        //viewSearch.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        self.view.addSubview(viewSearch)
        
        txtBusqueda.frame = CGRect(x:15, y:130, width:self.view.frame.width - 60, height:30)
        txtBusqueda.textColor = UIColor.black
        txtBusqueda.font = UIFont.systemFont(ofSize: 16)
        //txtBusqueda.layer.borderWidth = 1
        //txtBusqueda.layer.borderColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 0.7).CGColor
        txtBusqueda.attributedPlaceholder = NSAttributedString(string:"Nombre del Doctor...",
            attributes:[NSForegroundColorAttributeName: UIColor(red: (100.0/255.0), green: (100.0/255.0), blue: (100.0/255.0), alpha: 1)])
        txtBusqueda.delegate = self
        /*[textField addTarget:self
        action:@selector(textFieldDidChange:)
        forControlEvents:UIControlEventEditingChanged];*/
        //txtBusqueda.addTarget(self, action: "getDoctoresSearchEditing", forControlEvents: UIControlEvents.EditingChanged)
        self.view.addSubview(txtBusqueda)
        
        let bottomBar = UIView(frame: CGRect(x:13, y:158, width:self.view.frame.width - 62, height:2))
        bottomBar.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 0.7)
        self.view.addSubview(bottomBar)
        
        let imgOkSearch = UIImageView(frame: CGRect(x:self.view.frame.width - 40, y:124, width:35, height:35))
        imgOkSearch.image = UIImage(named: "btOkSearch.png" )
        self.view.addSubview(imgOkSearch)
        
        let btOkSeach = UIButton(frame: CGRect(x:self.view.frame.width - 60, y:130, width:60, height:40))
        //btOkSeach.setImage(UIImage(named: "btOkSearch.png"), forState: UIControlState.Normal)
        btOkSeach.addTarget(self, action: #selector(FavoritosViewController.getDoctoresSearch), for: UIControlEvents.touchDown)
        self.view.addSubview(btOkSeach)
        
        tableView.frame = CGRect(x:0, y:164, width:self.view.frame.width,height: self.view.frame.height - 164 - 60);
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.backgroundColor = UIColor.clear
        tableView.register(DoctorTableViewCell.self as AnyClass, forCellReuseIdentifier: "cell");
        tableView.backgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 0.6)
        
        self.view.addSubview(tableView);
        
        let btAdd = UIButton(type: UIButtonType.system)
        btAdd.frame = CGRect( x:50, y:self.view.frame.height - 50, width:self.view.frame.width - 100, height:40)
        btAdd.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        btAdd.setTitle("AGREGAR DOCTOR", for: UIControlState.normal)
        btAdd.layer.cornerRadius = 20
        btAdd.setTitleColor(UIColor.white, for: UIControlState.normal)
        //btAdd.titleLabel!.font =  UIFont(name: "helvetica", size: 10)
        btAdd.addTarget(self, action: #selector(FavoritosViewController.agregarDoctor), for: UIControlEvents.touchUpInside)
        self.view.addSubview(btAdd)
    }
    
    func getDoctoresSearch(){
        
        let usuario = Usuario.getEntity as Usuario
        if usuario.rol < 4 {
            if txtBusqueda.text == "" {
                UtilidadesGeneral.mensaje(mensaje: "No deje el campo de búsqueda vacío")
            } else {
                let usuario = Usuario.getEntity
                if usuario.rol < 4 {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "viewFiltroDoctores") as! FiltroDoctoresViewController
                    vc.txtBusqueda.text = txtBusqueda.text
                    navigationController?.pushViewController(vc, animated: true)
                } else {
                    UtilidadesGeneral.mensaje(mensaje: "No tiene permisos para utilizar esta opción")
                }
            }
        } else {
            UtilidadesGeneral.mensaje(mensaje: "No tiene permisos para utilizar esta opción")
        }
        
    }
    
    func getDoctoresSearchEditing(){
        
//        //txtBusqueda.resignFirstResponder()
//        listaDoctoresFiltro.removeAllObjects()
//        
//        for doc in listaDoctores {
//            let doctor = doc as Doctor
//            if txtBusqueda.text == "" {
//                listaDoctoresFiltro.addObject(doctor)
//            } else {
//                if doctor.nombre.lowercaseString.rangeOfString(txtBusqueda.text.lowercaseString) != nil {
//                    listaDoctoresFiltro.addObject(doctor)
//                }
//            }
//        }
//        self.tableView.reloadData()
    }
    
    func agregarDoctor(){
        let usuario = Usuario.getEntity
        if usuario.rol < 4 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "viewFiltroDoctores") as! FiltroDoctoresViewController
            navigationController?.pushViewController(vc, animated: true)
        } else {
            UtilidadesGeneral.mensaje(mensaje: "No tiene permisos para utilizar esta opción")
        }
        
    }
    
    // MARK: Métodos de tableView
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        //hola
        if editingStyle == .delete
        {
            
            self.listaDoctores.removeObject(at: indexPath.row)
            self.tableView.reloadData()
        }
    }
    
  
    
    func showAlert(){
        let alert = UIAlertController(title: "Veris", message: "Se ha eliminado el doctor de sus favoritos", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertError(){
        let alert = UIAlertController(title: "Veris", message: "Ha ocurrido un error, intente nuevamente", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDoctoresFiltro.count;
    }
   
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DoctorTableViewCell
        
        let doctor = listaDoctoresFiltro.object(at: indexPath.row) as! Doctor
        
        
        let directorio = "/compartidos/portal/medicos/\(doctor.secuenciaPersonal).JPG"
       
        let urldef = URLFactory.urldefinida()

        let urls = URL(string: "\(urldef)servicio/login/imagen?arg0=\(directorio)")
        getDataFromUrl(url: urls!) { (data, response, error)  in
            DispatchQueue.main.sync() { () -> Void in
                //print("entro")
                guard let data = data, error == nil else { print(error);return }
                
                cell.imagen.image  = UIImage(named: "medic.png")
                let varImg : UIImage?
                varImg = UIImage(data: data)
                if varImg?.size != nil {
                    cell.imagen.image = UIImage(data: data)
                    //self.view.addSubview(self.imagen)
                } else{
                    cell.imagen.image  = UIImage(named: "medic.png")
                }
            }
        }

        
        cell.nombre.text = doctor.nombre
        cell.nombre.sizeToFit();
        cell.nombre.frame = CGRect(x: 80, y: 5, width: self.view.frame.width - 130 , height: cell.nombre.frame.height)
        
        cell.especialidad.text = doctor.nombreEspecialidad

        
        /*Enlace con reserva*/
        cell.reservar.tag = indexPath.row
        cell.reservar.addTarget(self, action: #selector(beforereservar(sender:)), for: UIControlEvents.touchUpInside)
        //cell.icono.image = UIImage(named: imagenes[indexPath.row])
        //cell.title.text = items[indexPath.row]
        
        /*Enlace con hoja vida*/
        cell.verhoja.tag = indexPath.row
        // cell.verRecetaButton.addTarget(self, action: #selector(verDetalleReceta(sender:)), for: UIControlEvents.touchUpInside)
        cell.verhoja.addTarget(self, action: #selector(accionverhoja(sender:)), for: UIControlEvents.touchUpInside)
        //cell.icono.image = UIImage(named: imagenes[indexPath.row])
        //cell.title.text = items[indexPath.row]
        cell.setEditing(true, animated: true)
        return cell
    }
    func beforereservar(sender : UIButton){
        self.doctor = listaDoctoresFiltro.object(at: sender.tag) as! Doctor
        
        
        //var texto = split(doctor.nombreEspecialidad.characters){$0 == "/"}.map(String.init)
        var texto = doctor.nombreEspecialidad.characters.split{$0 == "/"}.map(String.init)
        self.espe.descripcion = texto[0].trimmingCharacters(in: NSCharacterSet.whitespaces)
        //stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        self.espe.codigo = doctor.idEspecialidad
        
        self.cargardatos(codigo: self.espe.codigo)
        //self.accionirareservar()
        
    }
    /*Funcion para enlazar con reserva*/
    func accionirareservar(){
       // var doctor = listaDoctoresFiltro.objectAtIndex(sender.tag) as! Doctor
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "viewReservar") as! ReservaViewController
        
       
        vc.serviciosel = self.servicio
        vc.tipodecarga=TipoCargaReserva.doctorfavorito
        vc.especialidadsel = self.espe
        vc.nombredoctorsel = "Doctor"//"Dr(a) "+self.doctor.nombre
        //vc.codigodoctorsel = self.doctor.idDoctor
        vc.numeroOrden = self.numeroOrden
        vc.lineaDetalle = self.lineaDetalle
        vc.codigoempresa = self.codigoempresa
        navigationController?.pushViewController(vc, animated: true)
    }
    
    /*Funcion para enlazar con hoja de vida*/
    func accionverhoja(sender : UIButton){
        let doctor = listaDoctoresFiltro.object(at: sender.tag) as! Doctor
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "viewHojaVida") as! HojaVidaViewController
        
        vc.codigomedico = doctor.idDoctor
        vc.nombremedico = "Dr(a) "+doctor.nombre
        vc.secuenciamedico = doctor.secuenciaPersonal
        vc.especialidadmedico = doctor.idEspecialidad
        vc.nombreespecialidadmedico = doctor.nombreEspecialidad
        navigationController?.pushViewController(vc, animated: true)
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    

     func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let button1 = UITableViewRowAction(style: .default, title: "Eliminar", handler: { (action, indexPath) in
        
            print("hola.. hola.. el muy codigo")
            let alert = UIAlertController(title: "Veris", message: "¿Esta seguro de eliminar el doctor de sus favoritos?", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                UIAlertAction in
                
                ViewUtilidades.showLoader(controller: self, title: "Cargando...")
                
                let usuario = Usuario.getEntity as Usuario
                let doctor = self.listaDoctoresFiltro.object(at: indexPath.row) as! Doctor
                
                let urldef = URLFactory.urldefinida()

                let url = NSURL(string: "\(urldef)servicio/especialidad/borrardoctordelistafavorito?arg0=\(usuario.tipoid)&arg1=\(usuario.numeroidentificacion)&arg2=\(doctor.idDoctor)")
                //let request = NSURLRequest(URL: url!)
                let request = NSMutableURLRequest(url: url! as URL)
                
                
                NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                    
                    if (error != nil) {
                        print(error!.localizedDescription)
                        ViewUtilidades.hiddenLoader(controller: self)
                        UtilidadesGeneral.mensaje( mensaje: error!.localizedDescription )
                        
                    } else {
                        do{
                        var err: NSError?
                        
                        //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                        
                        let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        if (err != nil) {
                            print("JSON Error \(err!.localizedDescription)")
                        }
                        
                        //println(jsonResult);
                        print(jsonResult)
                        let info =  jsonResult as NSDictionary
                        self.resultadoBorrar = info["resultado"] as! NSString//[[String: Any]]
                        
                            DispatchQueue.main.async(execute: {
                            if self.resultadoBorrar.isEqual(to: "ok") {
                                self.showAlert();
                                self.listaDoctores.remove(self.listaDoctoresFiltro.object(at: indexPath.row))
                                self.listaDoctoresFiltro.removeObject(at: indexPath.row)
                                self.tableView.deleteRows(at: [indexPath], with: .fade)
                            } else {
                                self.showAlertError();
                            }
                            
                            ViewUtilidades.hiddenLoader(controller: self)
                        })
                        } catch {
                            print("ERROR")
                        }
                    }
                    
                    
                }
                //////////////////////////
        
                
            }
            let cancelAction = UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.cancel) {
                UIAlertAction in
                
            }
            
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)

            
            
            
        })
        button1.backgroundColor =  UIColor(red: (0/256.0), green: (167/256.0), blue: (157/256.0), alpha: 1)
      
        return [button1]
    }
    
    
    /*func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let more = UITableViewRowAction(style: .normal, title: "More") { action, index in
            print("more button tapped")
        }
        more.backgroundColor = UIColor.lightGray
        
        let favorite = UITableViewRowAction(style: .normal, title: "Favorite") { action, index in
            print("favorite button tapped")
        }
        favorite.backgroundColor = UIColor.orange
        
        let share = UITableViewRowAction(style: .normal, title: "Share") { action, index in
            print("share button tapped")
        }
        share.backgroundColor = UIColor.blue
        
        return [share, favorite, more]
    }*/
    
   /* private func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: IndexPath) -> [AnyObject]? {
        let more = UITableViewRowAction(style: .normal, title: "More") { action, index in
            print("more button tapped")
        }
        more.backgroundColor = UIColor.lightGray
        
        let favorite = UITableViewRowAction(style: .normal, title: "Favorite") { action, index in
            print("favorite button tapped")
        }
        favorite.backgroundColor = UIColor.orange
        
        let share = UITableViewRowAction(style: .normal, title: "Share") { action, index in
            print("share button tapped")
        }
        share.backgroundColor = UIColor.blue
        
        return [share, favorite, more]
    }
    */
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.view.endEditing(true)
        getDoctoresSearch()
        return false
    }
    
    //implementacion de cargar servicio
    
    func cargardatos(codigo: Int!){
        print("entro a cargardatos -- favoritos")
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
         //let url2 = URLFactory.obtenerEspecialidades(self.recomendacionnumeroorden as String, codempresaorden: recomendacioncodempresa as String, lineadetalleorden: self.recomendacionlineadetalle as String)
        //http://200.31.28.222:8080/

        let us = Usuario.getEntity as Usuario
        print("----------QUIERO VER ESTOS VALORES--------------")
        print("\(us.numeroidentificacion) - \(us.tipoid)")
        let id_fam_sel = us.tipoid as String
        let nom_fam_sel = us.numeroidentificacion as String
        let identificacion_loggeado = UserDefaults.standard.object(forKey: "identificacion")
        let urldef = URLFactory.urldefinida()

        let url2 = "\(urldef)servicio/v2/agenda/consultarservicios?arg0=\(codigo!)&arg1=\(id_fam_sel)&arg2=\(nom_fam_sel)&arg3=\(identificacion_loggeado!)"
        
        
        let url = NSURL(string: url2)
        print(url!)
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                    print("llego")
                    var err: NSError?
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    if (err != nil) {
                        print("JSON Error \(err!.localizedDescription)")
                    }
                    //print(jsonResult)
                    let info =  jsonResult as NSDictionary
                    let data = info["lista"] as! [[String: Any]]
                    
                    if data.count == 1 {
                        //self.listaespecialidades = NSMutableArray();
                        print("entro a ==1")
                        //print(data)
                        for jsonobj  in data {
                            let esp = ObjEspecialidad()
                            esp.codigo = (jsonobj["codigoPrestacion"] as! NSString).integerValue
                            esp.descripcion = jsonobj["nombrePrestacion"] as! String
                            self.servicio.codigo = esp.codigo
                            self.servicio.descripcion = esp.descripcion
                            
                            if let ver = jsonobj["numeroOrden"] as? String {
                                self.numeroOrden = ver
                                self.lineaDetalle = jsonobj["lineaDetalle"] as! String
                                self.codigoempresa = jsonobj["codigoEmpresa"]! as! String
                            }else{
                                self.numeroOrden = "0"
                                self.lineaDetalle = "0"
                                self.codigoempresa = "0"
                            }

                            
                        }
                        //print(data.description)
                        //print(data.count)
                    }else{
                        self.servicio.codigo = 0
                        //self.servicio.descripcion = ""
                        print("no data")
                        ViewUtilidades.hiddenLoader(controller: self)
                    }
                    //dispatch_async(dispatch_get_main_queue(), {
                    DispatchQueue.main.async(execute: {
                        print("favoritos view numeroOrden: \(self.numeroOrden), lineaDetalle:  \(self.lineaDetalle), codigoEmp: \(self.codigoempresa)")
                        
                        ViewUtilidades.hiddenLoader(controller: self)
                        self.accionirareservar()
                    })
                } catch {
                    print("ERROR")
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: "Revise su conexión a Datos o Internet", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            
        }
        
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    


}
