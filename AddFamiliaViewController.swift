//
//  AddFamiliaViewController.swift
//  Mi Veris
//
//  Created by Samuel Velez on 16/8/16.
//  Copyright © 2016 Programadores-iOS.net. All rights reserved.
//

import UIKit

class AddFamiliaViewController: UIViewController {

    var scroll : UIScrollView = UIScrollView()
    var txt_cedula : UITextField = UITextField()
    var txt_nombres : UITextField = UITextField()
    
    var sgm_forma : UISegmentedControl = UISegmentedControl()
    var sgm_tipo : UISegmentedControl = UISegmentedControl()
    
    var ls_familiar : NSMutableArray?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        // Do any additional setup after loading the view.
        
        scroll.frame = CGRect(x: 0, y: -60, width: self.view.frame.width, height: self.view.frame.height+60)
        scroll.isUserInteractionEnabled = true
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(AddFamiliaViewController.hideKeyboard))
        scroll.addGestureRecognizer(singleTap)
        self.view.addSubview(scroll)
        
        self.navigationItem.title = ""
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        let barWhite : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width , height: 30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width / 2.8, height: 30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x: barBlue.frame.width, y:64, width: 45, height: 30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x: 0, y: 70, width: self.view.frame.width - 20, height: 30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "AÑADIR FAMILIAR"
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)
        
        let items = ["Identificación", "Nombres"]
        sgm_forma = UISegmentedControl(items: items)
        sgm_forma.selectedSegmentIndex = 0
        sgm_forma.tintColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        sgm_forma.frame = CGRect(x: 0, y: 90,width: self.view.frame.width, height: 40)
        sgm_forma.addTarget(self, action: #selector(AddFamiliaViewController.cambio), for: .valueChanged);
        //sgm_forma.layer.cornerRadius = 0.0
        sgm_forma.layer.cornerRadius = 0.0;
        sgm_forma.layer.borderColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1).cgColor
        //sgm_forma.layer.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1).CGColor
        sgm_forma.layer.borderWidth = 1.5;
        sgm_forma.layer.masksToBounds = true;
        

        scroll.addSubview(sgm_forma)
        
        
        let items2 = ["Cédula", "Pasaporte"]
        sgm_tipo = UISegmentedControl(items: items2)
        sgm_tipo.selectedSegmentIndex = 0
        sgm_tipo.tintColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        sgm_tipo.frame = CGRect(x:40, y: 155,width: self.view.frame.width - 80, height: 40)
        //sgm_tipo.addTarget(self, action: #selector(AddFamiliaViewController.cambio), forControlEvents: .ValueChanged);
        scroll.addSubview(sgm_tipo)


        
       
        
        txt_cedula = UITextField(frame: CGRect(x: 40, y: 205, width: self.view.frame.width - 80, height: 40))
        txt_cedula.layer.cornerRadius = 20
        txt_cedula.textAlignment = NSTextAlignment.center
        //txt_identificacion.delegate = self
        txt_cedula.placeholder="Identificación"
        txt_cedula.layer.borderColor = UIColor.lightGray.cgColor
        txt_cedula.layer.borderWidth = 1
        txt_cedula.autocorrectionType = UITextAutocorrectionType.no
        scroll.addSubview(txt_cedula)
        
        
        
        let btAceptar = UIButton(frame: CGRect(x: 30, y: 255, width: self.view.frame.width - 60, height: 40))
        btAceptar.setTitle("Buscar", for: UIControlState.normal)
        btAceptar.addTarget(self, action: #selector(AddFamiliaViewController.showFamiliares), for: UIControlEvents.touchUpInside)
        btAceptar.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        btAceptar.layer.cornerRadius = 20
        scroll.addSubview(btAceptar)
        
        scroll.contentSize = CGSize(width: 0, height: 460)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func cambio(){
        for v in view.subviews{
            if v is UITextField{
                v.removeFromSuperview()
            }
        }
        if(sgm_forma.selectedSegmentIndex == 1){
            sgm_forma.selectedSegmentIndex = 0
            //dismissViewControllerAnimated(false, completion: nil)
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
            
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "AddFamName") as! AddFamNameViewController
            
            viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Atrás", style: UIBarButtonItemStyle.plain, target: self, action: #selector(AddFamiliaViewController.goBack))
            // Creating a navigation controller with viewController at the root of the navigation stack.
            let navController = UINavigationController(rootViewController: viewController)
            self.present(navController, animated:false, completion: nil)
            
        }else{
            scroll.addSubview(txt_cedula)
            scroll.willRemoveSubview(txt_nombres)
        }
        
        
    }
    func goBack(){
        dismiss(animated: false, completion: nil)
        dismiss(animated: false, completion: nil)
    }
    
    func hideKeyboard(){
        self.txt_cedula.resignFirstResponder()
        
    }
    
        
    func showFamiliares(){
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        var tipo : String = ""
        if sgm_tipo.selectedSegmentIndex == 0{
            tipo = "2"
        }else{
            tipo = "3"
        }
        
        let identificacion = "\(tipo)-\(self.txt_cedula.text!)"
        if(self.txt_cedula.text!.isEmpty){
            print("esta vacio")
        }else{
            let us = Usuario.getEntity as Usuario
        let param = "arg0=\(us.tipoid)-\(us.numeroidentificacion)&arg1=\(identificacion)&arg2=&arg3=&arg4=&arg5=&arg6=&arg7=&arg8=&arg9=&arg10=0&arg11=10"
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SeleccionFamilia") as! SeleccionFamViewController
        
        viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Atrás", style: UIBarButtonItemStyle.plain, target: self, action: #selector(AddFamiliaViewController.goBack))
        viewController.parametros = param
        // Creating a navigation controller with viewController at the root of the navigation stack.
        let navController = UINavigationController(rootViewController: viewController)
        
        self.present(navController, animated:true, completion: nil)
        }
        
    }
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
