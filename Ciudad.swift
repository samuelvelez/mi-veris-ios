//
//  Ciudad.swift
//  Mi Veris
//
//  Created by Jorge on 29/04/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import UIKit

class Ciudad: NSObject {
   
    var codigoPais : NSString = ""
    var codigoProvincia : NSString = ""
    var codigoRegion : NSString = ""
    var codigoCiudad : NSString = ""
    var nombre : NSString = ""
    
}
