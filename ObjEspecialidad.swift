//
//  ObjEspecialidad.swift
//  Mi Veris
//
//  Created by Andres Cantos on 2/5/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import UIKit

// MARK: Region: Clase región

class ObjEspecialidad: NSObject {
    
    var codigo : NSInteger = 0
    var descripcion : String = ""
    
}
