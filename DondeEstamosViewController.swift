//
//  DondeEstamosViewController.swift
//  Mi Veris
//
//  Created by Christian Martinez M on 4/2/15.
//  Copyright (c) 2015 Firewall Soluciones All rights reserved.
//

import UIKit

class DondeEstamosViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, UIPickerViewDataSource,UIPickerViewDelegate {

    var tableView: UITableView  =   UITableView()
    var listaRegiones : NSMutableArray?
    var listaLugares : NSMutableArray = NSMutableArray()
    var isCreated : Bool = false
    var pickerData : NSMutableArray = NSMutableArray()
    var txtRegion : UILabel?
    var posicionSucursal : NSInteger = 0
    var selectedRegion : NSInteger = 0
    var fondoPick : UIView?

 
    @IBOutlet var myPicker: UIPickerView! = UIPickerView()
    @IBOutlet weak var myLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        self.navigationController?.navigationItem.backBarButtonItem?.title = ""
        //self.viewController.navigationItem.backBarButtonItem.title = "Custom Title";
        
        let fondo : UIImageView = UIImageView(frame: self.view.bounds)
        fondo.image = UIImage(named:"fondocita.png")
        self.view.addSubview(fondo)
        
        
        let barWhite : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width , height: 30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width / 2.6, height: 30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x: barBlue.frame.width, y: 64, width: 45, height: 30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x: 0, y: 64, width: self.view.frame.width - 20, height: 30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "¿Dónde Estamos?"
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)
        
        let usuario = Usuario.getEntity as Usuario
        if usuario.logueado {
            let nombreUser : UILabel = UILabel(frame: CGRect(x: 10, y: 94, width: self.view.frame.width - 20, height: 30))
            nombreUser.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
            nombreUser.font = UIFont(name: "helvetica-bold", size: 12)
            nombreUser.text = usuario.nombre as String
            
            self.view.addSubview(nombreUser)
        }
        
        
       
            ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        
        let urldef = URLFactory.urldefinida()

        let url = NSURL(string: "\(urldef)servicio/catalogos/regionciudades")
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        let str = "wsappusuario:ZA$57@9b86@$2r5"
        
        /*let utf8str = str.data(using: String.Encoding.utf8)
        
        if let base64Encoded = utf8str?.base64EncodedStringWithOptions(NSData.Base64EncodingOptions(rawValue: 0)){
            
            print("Encoded:  \(base64Encoded)")
            //request.addValue("Authorization", forHTTPHeaderField: "Basic \(base64Encoded)")
            request.setValue("Basic \(base64Encoded)", forHTTPHeaderField: "Authorization")
            
            if let base64Decoded = NSData(base64EncodedString: base64Encoded, options:   NSDataBase64DecodingOptions(rawValue: 0))
                .map({ NSString(data: $0, encoding: NSUTF8StringEncoding) })
            {
                // Convert back to a string
                print("Decoded:  \(base64Decoded)")
            }
        }*/
        let utf8str = str.data(using: String.Encoding.utf8)
        
        let base64Encoded = str.data(using: String.Encoding.utf8, allowLossyConversion: true)
        //let base64Encoded = str.dataUsingEncoding(NSUTF8StringEncoding)!.base64EncodedStringWithOptions([])
        print("Encoded: \(base64Encoded)")
        let base64DecodedData = NSData(base64Encoded: base64Encoded!, options: [])
        //NSData(base64EncodedString: base64Encoded, options: [])!
        //var base64DecodedString = String(data: base64DecodedData as! Data, encoding: String.Encoding.utf8)!
        //print("Decoded: \(base64DecodedString)")
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in

            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                var err: NSError?
                
                //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                
                var jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                if (err != nil) {
                    print("JSON Error \(err!.localizedDescription)")
                }
                
                //println(jsonResult);
                print(jsonResult)
                let info =  jsonResult as NSDictionary
                let data = info["lista"] as! [[String: Any]]
                if data.count > 0 {
                    self.listaRegiones = NSMutableArray();
                    for region in data {
                        let reg = Region()
                        reg.codigoCiudad = Int(region["codigociudad"] as! String)!
                        reg.codigoPais = Int(region["codigopais"] as! String)!
                        reg.codigoProvincia = Int(region["codigoprovincia"] as! String)!
                        reg.codigoRegion = Int(region["codigoregion"] as! String)!
                        reg.nombreCiudad = region["nombreciudad"] as! String
                        self.listaRegiones?.add(reg)
                        //println( region["nombreciudad"] )
                    }
                }
                
                self.showRegiones()
                ViewUtilidades.hiddenLoader(controller: self)
                
                } catch {
                    print("ERROR")
                }
            }
        
            
        }
        //////////////////////////
        
        
        
    }
    
    func showRegiones(){
        if listaRegiones!.count > 0 {
            txtRegion = UILabel(frame: CGRect(x: 0, y: 124, width: self.view.frame.width, height: 40))
            txtRegion?.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
            txtRegion?.textColor = UIColor.white
            
            //let usuario = Usuario.getEntity
            /*
            var defect : Region?
            for ciudad in listaRegiones! {
                if (ciudad.codigoCiudad == usuario.codigoCiudad) {
                    defect = ciudad as? Region
                    break
                }
            }*/
            
            var first : Region!
            first = listaRegiones?.object(at: 0) as! Region
            /*
            if defect != nil {
                first = defect
            } else {
                first = listaRegiones?.objectAtIndex(0) as! Region
            }*/
            
            txtRegion?.text = first.nombreCiudad
            txtRegion?.textAlignment = NSTextAlignment.center
            txtRegion?.textAlignment = .center
            txtRegion?.font = UIFont(name: "helvetica-bold", size: 16)
            txtRegion?.isUserInteractionEnabled = true
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(DondeEstamosViewController.showPicker))
            txtRegion?.addGestureRecognizer(tapGesture)
            self.view.addSubview(txtRegion!)
            
            let img : UIImageView = UIImageView(frame: CGRect(x: self.view.frame.width-30, y: 15, width: 20, height:12))
            img.image = UIImage(named: "arrow-down.png")
            
            txtRegion?.addSubview(img)

            
            
            pickerData.add( (listaRegiones?.object(at: 0) as! Region).nombreCiudad )
            pickerData.add( (listaRegiones?.object(at: 1) as! Region).nombreCiudad )
            pickerData.add( (listaRegiones?.object(at: 2) as! Region).nombreCiudad )
            
            ViewUtilidades.showLoader(controller: self, title: "Cargando...")
            
            let urldef = URLFactory.urldefinida()

            let url = NSURL(string: "\(urldef)servicio/establecimiento/datoscentros?arg0=2&arg1=quito")
            //let request = NSURLRequest(URL: url!)
            let request = NSMutableURLRequest(url: url! as URL)
            
            let str = "wsappusuario:ZA$57@9b86@$2r5"
            
            /*let utf8str = str.data(using: String.Encoding.utf8)
            
            if let base64Encoded = utf8str?.base64EncodedStringWithOptions(NSData.Base64EncodingOptions(rawValue: 0)){
                
                print("Encoded:  \(base64Encoded)")
                //request.addValue("Authorization", forHTTPHeaderField: "Basic \(base64Encoded)")
            request.setValue("Basic \(base64Encoded)", forHTTPHeaderField: "Authorization")
                
                if let base64Decoded = NSData(base64EncodedString: base64Encoded, options:   NSDataBase64DecodingOptions(rawValue: 0))
                    .map({ NSString(data: $0, encoding: NSUTF8StringEncoding) })
                {
                    // Convert back to a string
                    print("Decoded:  \(base64Decoded)")
                }
            }*/
            let utf8str = str.data(using: String.Encoding.utf8)
            
            let base64Encoded = str.data(using: String.Encoding.utf8, allowLossyConversion: true)
            //let base64Encoded = str.dataUsingEncoding(NSUTF8StringEncoding)!.base64EncodedStringWithOptions([])
            print("Encoded: \(base64Encoded)")
            let base64DecodedData = NSData(base64Encoded: base64Encoded!, options: [])
            //NSData(base64EncodedString: base64Encoded, options: [])!
            //var base64DecodedString = String(data: base64DecodedData as! Data, encoding: String.Encoding.utf8)!
            //print("Decoded: \(base64DecodedString)")
            
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                
                if (error != nil) {
                    print(error!.localizedDescription)
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    do{
                    var err: NSError?
                    
                    //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    if (err != nil) {
                        print("JSON Error \(err!.localizedDescription)")
                    }
                    
                    //println(jsonResult);
                    print(jsonResult)
                    let info =  jsonResult as NSDictionary
                    let data = info["lista"] as! [[String: Any]]
                    if data.count > 0 {
                        for sucursal in data {
                            let suc = Sucursal()
                            
                            if let idsuc = sucursal["id"] as? NSInteger {
                                suc.idSucursal = idsuc as NSInteger
                            }
                            if let descrip = sucursal["descripcion"] as? String {
                                suc.descripcion = descrip
                            }
                            if let direcc = sucursal["direccion"] as? String {
                                suc.direccion = direcc
                            }
                            if let tel = sucursal["telefono"] as? String {
                                suc.telefono = tel
                            }
                            if let hor = sucursal["horario"] as? String {
                                suc.horario = hor
                            }
                            if let reg = sucursal["codigoregion"] as? NSInteger  {
                                suc.region = reg
                            }
                            if let latitud = sucursal["latitud"] as? String {
                                suc.latitud = latitud
                            }
                            if let long = sucursal["longitud"] as? String {
                                suc.longitud = long
                            }
                            if let esp = sucursal["especialidades"] as? String {
                                suc.especialidades = esp
                            }
                            
                            self.listaLugares.add(suc)
                            
                        }
                    }
                    
                    self.showSucursales()
                    ViewUtilidades.hiddenLoader(controller: self)
                    
                    
                    } catch {
                            print("ERROR")
                    }
                }
                
                
            }
            //////////////////////////
            
            
        }
    }
    
    
    
    
    
    func showSucursales(){
        tableView.frame = CGRect(x: 0, y: 164, width: self.view.frame.width, height: self.view.frame.height - 164);
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.backgroundColor = UIColor.clear
        tableView.register(DondeEstamosTableViewCell.self as AnyClass, forCellReuseIdentifier: "SucursalesCell");
        
        self.view.addSubview(tableView);
    }
    
    func cancelPicker(){
        let transitionOptions = UIViewAnimationOptions.showHideTransitionViews//TransitionNone
        
        UIView.transition(with: self.view, duration: 0.3, options: transitionOptions, animations: {
            let back : UIView = self.view.viewWithTag(3001)!
            back.alpha = 0
            let view : UIView = self.view.viewWithTag(3000)!
            view.frame = CGRect(x: 0 , y: self.view.frame.height, width: self.view.frame.width, height: 230)
            }, completion: { finished in
                let back : UIView = self.view.viewWithTag(3001)!
                back.removeFromSuperview()
        })
    }
    
    func acceptPicker(){
        let transitionOptions = UIViewAnimationOptions.showHideTransitionViews //TransitionNone
        txtRegion?.text = pickerData[selectedRegion] as? String
        
        UIView.transition(with: self.view, duration: 0.3, options: transitionOptions, animations: {
            let back : UIView = self.view.viewWithTag(3001)!
            back.alpha = 0
            let view : UIView = self.view.viewWithTag(3000)!
            view.frame = CGRect(x: 0 , y: self.view.frame.height,width: self.view.frame.width, height: 230)
            }, completion: { finished in
                let back : UIView = self.view.viewWithTag(3001)!
                back.removeFromSuperview()
        })
        
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        let region = listaRegiones?.object(at: selectedRegion) as! Region
        //\((pickerData.object(at: selectedRegion) as AnyObject).lowercased)
        print("\(pickerData.object(at: selectedRegion))")
        var city_name = pickerData.object(at: selectedRegion) as! String
        city_name = city_name.lowercased()
        let urldef = URLFactory.urldefinida()

        let url = NSURL(string: "\(urldef)servicio/establecimiento/datoscentros?arg0=\(region.codigoRegion)&arg1=\(city_name)")
        print("url \(url)")
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        let str = "wsappusuario:ZA$57@9b86@$2r5"
        
        /*let utf8str = str.data(usingEncoding: String.Encoding.utf8)
        
        if let base64Encoded = utf8str?.base64EncodedStringWithOptions(NSData.Base64EncodingOptions(rawValue: 0)){
            
            print("Encoded:  \(base64Encoded)")
            //request.addValue("Authorization", forHTTPHeaderField: "Basic \(base64Encoded)")
            request.setValue("Basic \(base64Encoded)", forHTTPHeaderField: "Authorization")
            
            if let base64Decoded = NSData(base64EncodedString: base64Encoded, options:   NSDataBase64DecodingOptions(rawValue: 0))
                .map({ NSString(data: $0, encoding: NSUTF8StringEncoding) })
            {
                // Convert back to a string
                print("Decoded:  \(base64Decoded)")
            }
        }*/
        let utf8str = str.data(using: String.Encoding.utf8)
        
        let base64Encoded = str.data(using: String.Encoding.utf8, allowLossyConversion: true)
        //let base64Encoded = str.dataUsingEncoding(NSUTF8StringEncoding)!.base64EncodedStringWithOptions([])
        print("Encoded: \(base64Encoded)")
        let base64DecodedData = NSData(base64Encoded: base64Encoded!, options: [])
        //NSData(base64EncodedString: base64Encoded, options: [])!
        //var base64DecodedString = String(data: base64DecodedData as! Data, encoding: String.Encoding.utf8)!
        //print("Decoded: \(base64DecodedString)")
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                var err: NSError?
                
                //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                if (err != nil) {
                    print("JSON Error \(err!.localizedDescription)")
                }
                
                //println(jsonResult);
                let info =  jsonResult as NSDictionary
                let data = info["lista"] as! [[String: Any]]
                if data.count > 0 {
                    
                    self.listaLugares = NSMutableArray()
                    for sucursal in data {
                        let suc = Sucursal()
                        
                        if let idsuc = sucursal["id"] as? NSInteger {
                            suc.idSucursal = idsuc as NSInteger
                        }
                        if let descrip = sucursal["descripcion"] as? String {
                            suc.descripcion = descrip
                        }
                        if let direcc = sucursal["direccion"] as? String {
                            suc.direccion = direcc
                        }
                        if let tel = sucursal["telefono"] as? String {
                            suc.telefono = tel
                        }
                        if let hor = sucursal["horario"] as? String {
                            suc.horario = hor
                        }
                        if let reg = sucursal["codigoregion"] as? NSInteger  {
                            suc.region = reg
                        }
                        if let latitud = sucursal["latitud"] as? String {
                            suc.latitud = latitud
                        }
                        if let long = sucursal["longitud"] as? String {
                            suc.longitud = long
                        }
                        if let esp = sucursal["especialidades"] as? String {
                            suc.especialidades = esp
                        }
                        
                        
                        self.listaLugares.add(suc)
                        
                    }
                }
                
                ViewUtilidades.hiddenLoader(controller: self)
                self.tableView.reloadData()
                
                } catch {
                    print("ERROR")
                }
            }
        
            
        }
        //////////////////////////
        
    
    }
    func showPicker(){
        
        if !isCreated {
            isCreated = true
            fondoPick = UIView(frame: CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: 230))
            fondoPick?.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
            fondoPick?.tag = 3000
            self.view.addSubview(fondoPick!)
            
            let btCancel:UIButton = UIButton(frame: CGRect( x: 5 , y: 0, width: 70, height: 40))
            btCancel.backgroundColor = UIColor.clear
            btCancel.setTitle("Cancelar", for: UIControlState.normal)
            btCancel.addTarget(self, action: #selector(DondeEstamosViewController.cancelPicker), for: UIControlEvents.touchUpInside)
            btCancel.titleLabel!.font = UIFont(name: "helvetica-bold", size: 15)
            btCancel.titleLabel?.textColor = UIColor.white
            btCancel.tag = 22;
            
            fondoPick?.addSubview(btCancel)
            
            let btAceptar:UIButton = UIButton(frame: CGRect( x: self.view.frame.width - 75 , y: 0, width: 70, height: 40))
            btAceptar.backgroundColor = UIColor.clear
            btAceptar.setTitle("Aceptar", for: UIControlState.normal)
            btAceptar.addTarget(self, action: #selector(DondeEstamosViewController.acceptPicker), for: UIControlEvents.touchUpInside)
            btAceptar.titleLabel!.font = UIFont(name: "helvetica-bold", size: 15)
            btAceptar.titleLabel?.textColor = UIColor.white
            btAceptar.tag = 22;
            
            fondoPick?.addSubview(btAceptar)
            
            //myPicker = UIPickerView()
            myPicker.dataSource = self
            myPicker.delegate = self
            myPicker.frame = CGRect(x: 0, y: 40, width: self.view.frame.width, height: 300)
            myPicker.backgroundColor = UIColor.white
            fondoPick!.addSubview(myPicker)
        }
        let transitionOptions = UIViewAnimationOptions.showHideTransitionViews //TransitionNone
        
        let back = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height - 230))
        back.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
        back.alpha = 0
        back.tag = 3001
        self.view.addSubview(back)
        
        UIView.transition(with: self.view, duration: 0.4, options: transitionOptions, animations: {
            
            back.alpha = 1
            let view : UIView = self.view.viewWithTag(3000)!
            view.frame = CGRect(x: 0 , y: self.view.frame.height - 230, width: self.view.frame.width, height:230)
            }, completion: { finished in
               
        })
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row] as? String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //let transitionOptions = UIViewAnimationOptions.showHideTransitionViews//TransitionNone
    
        selectedRegion = row
        print( pickerData[row] )
        //txtRegion?.text = pickerData[row] as? String
        //var posicion = row
        
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaLugares.count;
    }
    
    /*func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.separatorInset = UIEdgeInsetsZero
        cell.layoutMargins = UIEdgeInsetsZero
    }*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SucursalesCell", for: indexPath as IndexPath) as! DondeEstamosTableViewCell
        
        let sucursal = listaLugares.object(at: indexPath.row) as! Sucursal
        cell.title.text = sucursal.descripcion
        //cell.title.sizeToFit()
        cell.title.adjustsFontSizeToFitWidth = true
        cell.descripcion.text = sucursal.direccion
        cell.descripcion.sizeToFit()
        cell.descripcion.adjustsFontSizeToFitWidth = true
        cell.descripcion.frame =  CGRect(x: 90, y: 10, width: self.view.frame.width - 130, height: cell.frame.height)
        
        
        let urldef = URLFactory.urldefinida()

        let url = URL(string: "\(urldef)sucursal/\(sucursal.region)/\(sucursal.idSucursal).jpg")
        getDataFromUrl(url: url!) { (data, response, error)  in
            DispatchQueue.main.sync() { () -> Void in
                print("entro")
                guard let data = data, error == nil else { print(error);return }
                print(response?.suggestedFilename ?? url?.lastPathComponent)
                print("Download Finished")
                //self.imageView.image = UIImage(data: data)
                cell.imagen.image = UIImage(data: data)
                print("se ejecuto")
            }
        }
 
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "viewSucursal") as! SucursalViewController

        vc.sucursal = listaLugares.object(at: indexPath.row) as! Sucursal
        navigationController?.pushViewController(vc, animated: true)
        
    }
    //funcion samuel
   /* func downloadImage(url: URL) {
        print("Download Started")
        getDataFromUrl(url: url) { (data, response, error)  in
            DispatchQueue.main.sync() { () -> Void in
                guard let data = data, error == nil else { return }
                print(response?.suggestedFilename ?? url.lastPathComponent)
                print("Download Finished")
                self.imageView.image = UIImage(data: data)
            }
        }
    }*/
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }

}
