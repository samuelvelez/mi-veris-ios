//
//  IngresoCodigoViewController.swift
//  Mi Veris
//
//  Created by Samuel Velez on 1/5/16.
//  Copyright © 2016 Programadores-iOS.net. All rights reserved.
//

import UIKit

class IngresoCodigoViewController: UIViewController, UITextFieldDelegate  {

    var scroll : UIScrollView = UIScrollView()
    
    var sgm_tipo : UISegmentedControl = UISegmentedControl()
    var txtCodigo : UITextField!
    
    var listaUsuarios : NSMutableArray = NSMutableArray()
    var resultado : NSString = ""
    
    var usuario : Usuario!
    
    var identificacion : NSString!
    var tipo_id: NSString!
    var pass : NSString!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(IngresoCodigoViewController.hideKeyboard))
        scroll.addGestureRecognizer(singleTap)
        self.view.addSubview(scroll)
        self.navigationItem.title = ""
        
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        scroll.frame = CGRect(x: 0, y: 0, width: self.view.frame.width , height: self.view.frame.height )
        scroll.contentSize = CGSize(width: 0, height: 568)
        scroll.backgroundColor = UIColor.white
        self.view.addSubview(scroll)
        
        let barWhite : UIView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width , height: 30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width / 1.6, height: 30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x: barBlue.frame.width, y: 64, width: 45, height: 30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 20, height: 30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "LOGIN"
        
        scroll.addSubview(barWhite)
        scroll.addSubview(barBlue)
        scroll.addSubview(img)
        scroll.addSubview(title)
        
        let image = UIImageView(frame: CGRect(x: 0, y:30, width: self.view.frame.width, height: 135))
        image.backgroundColor = UIColor.gray
        image.image = UIImage(named: "loginbanner.png")
        scroll.addSubview(image)
        
        let etiqueta = UILabel(frame: CGRect(x: 20, y: 166, width: self.view.frame.width - 40, height: 60))
        etiqueta.font = UIFont(name: "helvetica-bold", size: 14)
        etiqueta.text = "Ingrese el código de activación que recibió en su correo."
        etiqueta.lineBreakMode = NSLineBreakMode.byWordWrapping
        etiqueta.numberOfLines = 0
        
        scroll.addSubview(etiqueta)
        
        let items = ["Cédula", "Pasaporte"]
        sgm_tipo = UISegmentedControl(items: items)
        //sgm_tipo.selectedSegmentIndex = 0
        sgm_tipo.tintColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        sgm_tipo.frame = CGRect(x:40, y: 200,width: self.view.frame.width - 80, height: 40)
        //scroll.addSubview(sgm_tipo)
        
        let lbPass = UILabel(frame: CGRect(x: 30, y: 271, width: self.view.frame.width - 40, height: 20))
        lbPass.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        lbPass.text = "Codigo"
        //scroll.addSubview(lbPass)
        
        txtCodigo = UITextField(frame: CGRect(x: 10, y: 296, width: self.view.frame.width - 20, height: 40))
        txtCodigo.layer.cornerRadius = 20
        txtCodigo.textAlignment = NSTextAlignment.center
        txtCodigo.delegate = self
        txtCodigo.autocorrectionType = UITextAutocorrectionType.no
        txtCodigo.layer.borderColor = UIColor.lightGray.cgColor
        txtCodigo.layer.borderWidth = 1
        txtCodigo.addTarget(self, action: #selector(IngresoCodigoViewController.upView), for: UIControlEvents.editingDidBegin)
        txtCodigo.addTarget(self, action: #selector(IngresoCodigoViewController.downView), for: UIControlEvents.editingDidEndOnExit)
        scroll.addSubview(txtCodigo)
        
        let btAceptar = UIButton(frame: CGRect( x: (self.view.frame.width / 2) - 85 , y: 360, width: 170, height: 50))
        btAceptar.setTitle("Ingresar", for: UIControlState.normal)
        btAceptar.addTarget(self, action: #selector(IngresoCodigoViewController.login), for: UIControlEvents.touchUpInside)
        btAceptar.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        btAceptar.layer.cornerRadius = 25
        scroll.addSubview(btAceptar)
        
        
    }
    func upView(){
        let transitionOptions = UIViewAnimationOptions.showHideTransitionViews
        UIView.transition(with: self.view, duration: 0.2, options: transitionOptions, animations: {
            self.view.frame = CGRect(x: 0, y: -120, width: self.view.frame.width, height: self.view.frame.height)
            }, completion: { finished in
                
        })
    }
    
    func downView(){
        let transitionOptions = UIViewAnimationOptions.showHideTransitionViews
        UIView.transition(with: self.view, duration: 0.2, options: transitionOptions, animations: {
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            }, completion: { finished in
                
        })
    }
    
    func login(){
        //txtUsuario.resignFirstResponder()
        txtCodigo.resignFirstResponder()
        let transitionOptions = UIViewAnimationOptions.showHideTransitionViews
        UIView.transition(with: self.view, duration: 0.2, options: transitionOptions, animations: {
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            }, completion: { finished in
                
        })
        
        if txtCodigo.text!.isEmpty {
            let alert = UIAlertController(title: "Veris", message: "Datos Incorrectos", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            ViewUtilidades.showLoader(controller: self, title: "Cargando...")
            
            
            let codigo = txtCodigo.text!
            let urldef = URLFactory.urldefinida()

            let url = NSURL(string: "\(urldef)servicio/login/validarcodigo?arg0=\(tipo_id)&arg1=\(identificacion)&arg2=\(codigo)")
           // print ("url consultar:  \(url!)")
            //let request = NSURLRequest(URL: url!)
            let request = NSMutableURLRequest(url: url! as URL)
            
           
            
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                
                
                if (error != nil) {
                    print(error!.localizedDescription)
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    do {
                        var err: NSError?
                        
                        //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                        
                        let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        if (err != nil) {
                            print("JSON Error \(err!.localizedDescription)")
                        }
                        
                        //println(jsonResult);
                        print(jsonResult)
                        let info =  jsonResult as NSDictionary
                       // self.status = info["texto"] as! String
                        let resultado = info["resultado"] as! NSString

                        //dispatch_async(dispatch_get_main_queue(), {
                        DispatchQueue.main.async(execute: {
                            ViewUtilidades.hiddenLoader(controller: self)
                            if resultado.isEqual(to: "ok") {//self.status.isEqualToString("S") {
                                print("codigo correcto")
                                self.llamaraLogear()
                                
                            } else {
                                let alert = UIAlertController(title: "Veris", message: "Código Incorrecto", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            //self.navigationController?.popToRootViewControllerAnimated(false)
                        })
                    } catch {
                        print("ERROR")
                        ViewUtilidades.hiddenLoader(controller: self)
                        let alert = UIAlertController(title: "Veris", message: "Revise su conexión a Datos o Internet", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
                
            }
            
        }
    }

    func hideKeyboard(){
       self.txtCodigo.resignFirstResponder()
        self.downView()
    }
    
    func llamaraLogear(){
        let pass = self.pass
        let usr = self.identificacion
        var bool_codigo = true
        var idS : NSString!
        var tipoS : NSString!
        
        let urldef = URLFactory.urldefinida()

        let url = NSURL(string: "\(urldef)servicio/login/login?arg0=\(usr)&arg1=\(pass)")
        print ("url consultar:  \(url!)")
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar",  style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                    //print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue))
                    
                    var err: NSError?
                    
                    //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    if (err != nil) {
                        print("JSON Error \(err!.localizedDescription)")
                    }
                    
                    //println(jsonResult); Skyfall
                    print(jsonResult)
                    let info =  jsonResult as NSDictionary
                    let data = info["lista"] as! [[String: Any]]
                    self.resultado = info["resultado"] as! NSString
                    if data.count > 0 {
                        let usuario = Usuario.getEntity as Usuario
                        for doctor in data {
                            
                            usuario.nombre = doctor["nombre"] as! NSString
                            usuario.numeroidentificacion = doctor["numeroidentificacion"] as! NSString
                            usuario.tipoid = doctor["tipoid"] as! NSString
                            //usuario.primeravez = doctor["primeravez"] as! NSString
                            let codCiudad = doctor["codigo_ciudad"] as! NSInteger
                            usuario.codigoCiudad = codCiudad
                            let codPais = doctor["codigo_pais"] as! NSInteger
                            usuario.codigoPais = codPais
                            let codProv = doctor["codigo_provincia"] as! NSInteger
                            usuario.codigoProvincia = codProv
                            let codReg = doctor["codigo_region"] as! NSInteger
                            usuario.codigoRegion = codReg
                            //usuario.aceptatermino = doctor["aceptatermino"] as NSString
                            if (doctor["aceptatermino"] is NSNull){
                                usuario.aceptatermino = "N"
                            }else{
                                usuario.aceptatermino = doctor["aceptatermino"] as! NSString
                            }
                            if(doctor["codigoActivacion"] is NSNull){
                                print("no tiene codigo de activacion")
                                bool_codigo = false
                            }else{
                                print(doctor["codigoActivacion"]!)
                                bool_codigo = true
                            }
                            tipoS = usuario.tipoid
                            idS = usuario.numeroidentificacion
                            self.listaUsuarios.add(usuario)
                        }
                    }
                    
                    //dispatch_async(dispatch_get_main_queue(), {
                    DispatchQueue.main.async(execute: {
                        ViewUtilidades.hiddenLoader(controller: self)
                        if self.resultado.isEqual(to: "ok") {
                            
                            //let usuario = self.listaUsuarios.objectAtIndex(0) as! Usuario
                            
                            // let primeravez = usuario.primeravez
                            //let aceptatermino = usuario.aceptatermino
                            if(bool_codigo){
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "viewCodigo") as! IngresoCodigoViewController
                                //vc.usuarios = usuario
                                vc.tipo_id = tipoS
                                vc.identificacion = idS
                                self.navigationController?.pushViewController(vc, animated: true)
                                
                                
                            }else{
                                print("LOGUEADO")
                                let usuario = Usuario.getEntity as Usuario
                                usuario.logueado = true
                                let root = self.navigationController?.viewControllers[0] as! ViewController
                                root.setPush = true
                                self.navigationController?.popToRootViewController(animated: false)
                                
                                UserDefaults.standard.set(usuario.numeroidentificacion, forKey: "identificacion")
                                UserDefaults.standard.synchronize()
                                
                                UserDefaults.standard.set(usuario.nombre, forKey: "nombre")
                                UserDefaults.standard.synchronize()
                                
                                UserDefaults.standard.set(usuario.tipoid, forKey: "tipo")
                                UserDefaults.standard.synchronize()
                                
                                UserDefaults.standard.set(usuario.codigoCiudad, forKey: "ciudad")
                                UserDefaults.standard.synchronize()
                                
                                UserDefaults.standard.set(usuario.codigoPais, forKey: "pais")
                                UserDefaults.standard.synchronize()
                                
                                UserDefaults.standard.set(usuario.codigoProvincia, forKey: "provincia")
                                UserDefaults.standard.synchronize()
                                
                                UserDefaults.standard.set(usuario.codigoRegion, forKey: "region")
                                UserDefaults.standard.synchronize()
                            }
                            /*
                             if primeravez.isEqualToString("S") || !aceptatermino.isEqualToString("S")
                             
                             {
                             
                             
                             
                             let storyboard = UIStoryboard(name: "Main", bundle: nil)
                             let vc = storyboard.instantiateViewControllerWithIdentifier("viewCambiarPass") as! CambiarPassViewController
                             vc.usuarios = usuario
                             
                             self.navigationController?.pushViewController(vc, animated: true)
                             
                             
                             
                             } else {
                             let storyboard = UIStoryboard(name: "Main", bundle: nil)
                             let vc = storyboard.instantiateViewControllerWithIdentifier("viewLoginImagenes") as! LoginImagenesViewController
                             self.navigationController?.pushViewController(vc, animated: true)
                             
                             }*/
                            
                        } else {
                            
                            UtilidadesGeneral.mensaje(mensaje: self.resultado as String)
                            
                            
                            
                        }
                    })
                } catch {
                    print("ERROR")
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: "Revise su conexión a Datos o Internet", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }

    }

    
}
