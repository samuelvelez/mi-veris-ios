//
//  ListaFamiliarTableViewCell.swift
//  Mi Veris
//
//  Created by Samuel Velez on 27/12/16.
//  Copyright © 2016 Samuel Velez. All rights reserved.
//

import UIKit

class ListaFamiliarTableViewCell: UITableViewCell {

    var parentesco : UILabel = UILabel();
    var nombre : UILabel = UILabel();
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.clear
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width;
        
        
        nombre.frame = CGRect(x: 20, y: 0, width: self.frame.width, height: 30)
        nombre.font = UIFont(name: "helvetica", size: 14)
        //nombre.textColor = UIColor(red: (48/256.0), green: (48/256.0), blue: (48/256.0), alpha: 1)
        
        parentesco.frame = CGRect(x: 20, y: 20, width: self.frame.width, height: 30)
        parentesco.font = UIFont(name: "helvetica", size: 12)

        
        let arrow = UIImageView(frame: CGRect(x: screenWidth-30, y: 17, width: 15, height:24))
        arrow.image = UIImage(named: "flecha.png")
        
        self.contentView.addSubview(nombre)
        self.contentView.addSubview(parentesco)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

}
