//
//  CompleteViewController.swift
//  Mi Veris
//
//  Created by Samuel Velez on 27/4/16.
//  Copyright © 2016 Programadores-iOS.net. All rights reserved.
//

import UIKit
import CoreLocation

class CompleteViewController: UIViewController, UITextFieldDelegate,UINavigationControllerDelegate, CLLocationManagerDelegate{
    
    var scroll : UIScrollView = UIScrollView()
    var sgm_genero : UISegmentedControl = UISegmentedControl()
    var txt_primerNombre : UITextField = UITextField()
    var txt_segundoNombre : UITextField = UITextField()
    var txt_primerApellido : UITextField = UITextField()
    var txt_segundoApellido : UITextField = UITextField()
    var txtProvincia : UITextField = UITextField()
    var txtCiudad : UITextField = UITextField()
    var bool_provi : Bool = false
    
    var pickerProvincia : PickerList!
    var dataProvincia = [""]
    var provincias : NSMutableArray = NSMutableArray()
    
    
    var locationManager : CLLocationManager!
    var coord : CLLocationCoordinate2D!
    
    var pickerCiudad : PickerList!
    var dataCiudad = [""]
    var ciudades : NSMutableArray = NSMutableArray()
    
    var pickerSelected : PickerList!
    var downKeyboard : Bool  = true
    
    var tipo : NSString!
    var identificacion : NSString!
    var clave : NSString!
    var email : NSString!
    var celular : NSString!
    var fecha : NSDate!
    
    var vieneFamilia2 : NSString!
    
    let iOS8 = floor(NSFoundationVersionNumber) > floor(NSFoundationVersionNumber_iOS_7_1)

    var lat :Double = 0
    var lon :Double = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scroll.frame = CGRect(x: 0, y: -60, width: self.view.frame.width, height: self.view.frame.height+60)
        scroll.isUserInteractionEnabled = true
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(CompleteViewController.hideKeyboard))
        scroll.addGestureRecognizer(singleTap)
        self.view.addSubview(scroll)
        
        self.navigationItem.title = ""
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        let barWhite : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width , height: 30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width / 2.8, height: 30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x: barBlue.frame.width, y: 64, width: 45, height: 30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        let title : UILabel = UILabel(frame: CGRect(x: 0, y: 70, width: self.view.frame.width - 20, height: 30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "CREAR CUENTA"
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)
        
        let items = ["Masculino", "Femenino"]
        sgm_genero = UISegmentedControl(items: items)
        sgm_genero.selectedSegmentIndex = 0
        sgm_genero.tintColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        sgm_genero.frame = CGRect(x: 40, y: 105, width: self.view.frame.width - 80, height:  40)
        scroll.addSubview(sgm_genero)
        
        txt_primerNombre = UITextField(frame: CGRect(x: 40, y: 155, width: self.view.frame.width - 80, height: 40))
        txt_primerNombre.layer.cornerRadius = 20
        txt_primerNombre.textAlignment = NSTextAlignment.center
        txt_primerNombre.placeholder="Primer Nombre"
        txt_primerNombre.layer.borderColor = UIColor.lightGray.cgColor
        txt_primerNombre.layer.borderWidth = 1
        txt_primerNombre.autocorrectionType = UITextAutocorrectionType.no
        txt_primerNombre.addTarget(self, action: #selector(upView(view:)), for: UIControlEvents.editingDidBegin)
        txt_primerNombre.addTarget(self, action: #selector(downView(view:)), for: UIControlEvents.editingDidEndOnExit)
        scroll.addSubview(txt_primerNombre)
        
        txt_primerApellido = UITextField(frame: CGRect(x: 40, y: 205, width: self.view.frame.width - 80, height: 40))
        txt_primerApellido.layer.cornerRadius = 20
        txt_primerApellido.textAlignment = NSTextAlignment.center
        txt_primerApellido.delegate = self
        txt_primerApellido.addTarget(self, action: #selector(upView(view:)), for: UIControlEvents.editingDidBegin)
        txt_primerApellido.addTarget(self, action:  #selector(downView(view:)), for: UIControlEvents.editingDidEndOnExit)
        txt_primerApellido.placeholder="Primer Apellido"
        txt_primerApellido.layer.borderColor = UIColor.lightGray.cgColor
        txt_primerApellido.layer.borderWidth = 1
        txt_primerApellido.autocorrectionType = UITextAutocorrectionType.no
        scroll.addSubview(txt_primerApellido)
        
        txt_segundoApellido = UITextField(frame: CGRect(x: 40, y: 255, width: self.view.frame.width - 80, height: 40))
        txt_segundoApellido.layer.cornerRadius = 20
        txt_segundoApellido.textAlignment = NSTextAlignment.center
        txt_segundoApellido.delegate = self
        //txt_segundoApellido.enabled = false
        txt_segundoApellido.addTarget(self, action:  #selector(upView(view:)), for: UIControlEvents.editingDidBegin)
        txt_segundoApellido.addTarget(self, action:  #selector(downView(view:)), for: UIControlEvents.editingDidEndOnExit)
        txt_segundoApellido.placeholder="Segundo Apellido"
        txt_segundoApellido.layer.borderColor = UIColor.lightGray.cgColor
        txt_segundoApellido.layer.borderWidth = 1
        txt_segundoApellido.autocorrectionType = UITextAutocorrectionType.no
        scroll.addSubview(txt_segundoApellido)
        
        let lbProvincia = UILabel(frame: CGRect(x: 30, y: 355, width: self.view.frame.width - 40, height:  25))
        lbProvincia.text = "Provincia: *"
        lbProvincia.font = UIFont.systemFont(ofSize: 14)
        //scroll.addSubview(lbProvincia)
        
        
        txtProvincia = UITextField(frame: CGRect(x: 20, y: 305, width: self.view.frame.width - 40, height: 40))
        txtProvincia.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        txtProvincia.textColor = UIColor.white
        txtProvincia.textAlignment = NSTextAlignment.center
        txtProvincia.delegate = self
        txtProvincia.layer.borderColor = UIColor.lightGray.cgColor
        txtProvincia.layer.borderWidth = 1
        txtProvincia.text = "Provincia"
        txtProvincia.addTarget(self, action:  #selector(upView(view:)), for: UIControlEvents.editingDidBegin)
        txtProvincia.addTarget(self, action:  #selector(downView(view:)), for: UIControlEvents.editingDidEndOnExit)
        txtProvincia.autocorrectionType = UITextAutocorrectionType.no
        //txtProvincia.enabled = false
        scroll.addSubview(txtProvincia)

        
        
        txtCiudad = UITextField(frame: CGRect(x: 20,y: 355, width: self.view.frame.width - 40, height: 40))
        txtCiudad.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        txtCiudad.textColor = UIColor.white
        txtCiudad.textAlignment = NSTextAlignment.center
        txtCiudad.delegate = self
        txtCiudad.layer.borderColor = UIColor.lightGray.cgColor
        txtCiudad.layer.borderWidth = 1
        txtCiudad.addTarget(self, action:  #selector(upView(view:)), for: UIControlEvents.editingDidBegin)
        txtCiudad.addTarget(self, action:  #selector(downView(view:)), for: UIControlEvents.editingDidEndOnExit)
        txtCiudad.text = "Ciudad"
        txtCiudad.autocorrectionType = UITextAutocorrectionType.no
        scroll.addSubview(txtCiudad)
        
        let btAceptar = UIButton(frame: CGRect(x: 30, y: 405, width: self.view.frame.width - 60, height: 40))
        btAceptar.setTitle("Siguiente", for: UIControlState.normal)
        btAceptar.addTarget(self, action: #selector(CompleteViewController.siguiente), for: UIControlEvents.touchUpInside)
        btAceptar.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        btAceptar.layer.cornerRadius = 20
        scroll.addSubview(btAceptar)
        
        pickerCiudad = PickerList(datos: dataCiudad as NSArray)
        
        scroll.contentSize = CGSize(width: 0, height: 455)
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        if iOS8 {
            locationManager.requestAlwaysAuthorization()
        }
        locationManager.startUpdatingLocation()
        
        getProvincias()
    }
    
    func locationManager(_ manager:CLLocationManager, didUpdateLocations locations:[CLLocation]) {
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        coord = locationObj.coordinate
        
        lat = coord.latitude
        lon = coord.longitude
    }

   
    
    func siguiente(){
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        var valido = true
        var genero = ""
        var mensaje = "Datos Incorrectos"
        if self.sgm_genero.selectedSegmentIndex==0{
            genero = "M"
        }else if self.sgm_genero.selectedSegmentIndex==1{
            genero = "F"
        }else {
            valido = false
            mensaje = "Debe seleccionar genero"
        }
        
        
        //let codPais = "1"
        
        let provincia = provincias.object(at: pickerProvincia.eleccion) as! Provincia
        let ciudad = ciudades.object(at: pickerCiudad.eleccion) as! Ciudad

        
        
        valido = valido && !(self.txt_primerNombre.text?.isEmpty)!
        valido = valido && !(self.txt_primerApellido.text?.isEmpty)!
        valido = valido && !(self.txt_segundoApellido.text?.isEmpty)!
        valido = valido && !(self.txtProvincia.text?.isEmpty)!
        valido = valido && !(self.txtCiudad.text?.isEmpty)!
        
        if valido{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/YYYY" //format style. Browse online to get a format that fits your needs.
            let dateString = dateFormatter.string(from: fecha as Date)
           
            let urldef = URLFactory.urldefinida()

            let cadenaUrl = "\(urldef)servicio/login/crearcuenta?arg0=\(tipo.description)&arg1=\(identificacion.description)&arg2=\((self.txt_primerApellido.text)!)&arg3=\((self.txt_segundoApellido.text)!)&arg4=\((self.txt_primerNombre.text)!)&arg5=\(email.description)&arg6=\(dateString)&arg7=\(genero)&arg8=\(celular.description)&arg9=\(provincia.codigoPais)&arg10=\((provincia.codigoProvincia))&arg11=\(ciudad.codigoCiudad)&arg12=\(self.lat)&arg13=\(self.lon)&arg14=\(clave.description)"
            
            
            let url = NSURL(string: cadenaUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!  )
            
            print(cadenaUrl)

            if url != nil {
                let request = NSMutableURLRequest(url: url! as URL)
                let str = "wsappusuario:ZA$57@9b86@$2r5"
                let utf8str = str.data(using: String.Encoding.utf8)
                let base64DecodedData = utf8str!.base64EncodedString()
                print(base64DecodedData)
                request.addValue(base64DecodedData, forHTTPHeaderField: "Authorization")
                NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                    if (error != nil) {
                        print(error!.localizedDescription)
                        ViewUtilidades.hiddenLoader(controller: self)
                        let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        do{
                            var err: NSError?
                            
                            let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                            if (err != nil) {
                                print("JSON Error \(err!.localizedDescription)")
                            }
                             let info =  jsonResult as NSDictionary
                            _ = info["lista"] as! NSArray
                            let resultado = info["resultado"] as! NSString
                            
                            DispatchQueue.main.async(execute: {
                                ViewUtilidades.hiddenLoader(controller: self)
                                print(resultado)
                                if resultado.isEqual(to: "ok") {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = storyboard.instantiateViewController(withIdentifier: "FinRegistro") as! RegistroExitViewController
                                    vc.email = self.email
                                    vc.vieneFamilia3 = self.vieneFamilia2
                                    self.navigationController?.pushViewController(vc, animated: true)
                                } else {
                                    let alert = UIAlertController(title: "Veris", message: resultado as String, preferredStyle: UIAlertControllerStyle.alert)
                                    let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                                        UIAlertAction in
                                    }
                                    alert.addAction(okAction)
                                    self.present(alert, animated: true, completion: nil)
                                }
                                
                            })
                        } catch {
                            print("ERROR")
                            ViewUtilidades.hiddenLoader(controller: self)
                            let alert = UIAlertController(title: "Veris", message: "Revise su conexión a Datos o Internet", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }
            } else {
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: "Ha ocurrido un error. Intente nuevamente", preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    
                }
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            }

            
            
            
            
            
            
            /////////////////////////
            
        }else{
            ViewUtilidades.hiddenLoader(controller: self)
            let alert = UIAlertController(title: "Veris", message: mensaje, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        

        
    }
    
    
    
    
    func hideKeyboard(){
        print("hidekeyboard")
        self.txt_primerNombre.resignFirstResponder()
        self.txt_primerApellido.resignFirstResponder()
        self.txt_segundoApellido.resignFirstResponder()
        self.txtProvincia.resignFirstResponder()
        self.txtCiudad.resignFirstResponder()
        
    }
    func cancelPicker(){
        pickerSelected.hidePicker()
        txtProvincia.resignFirstResponder()
        txtCiudad.resignFirstResponder()
    }
    
    
    //
    func acceptPicker(){
        pickerSelected.hidePicker()
        if pickerSelected == pickerProvincia {
            txtProvincia.text = dataProvincia[pickerProvincia.eleccion]
            txtProvincia.resignFirstResponder()
            self.getCiudades()
        } else if pickerSelected == pickerCiudad {
            txtCiudad.text = dataCiudad[pickerCiudad.eleccion]
            txtCiudad.resignFirstResponder()
        }
    }
    
    func upView( view : UITextField ){
        print("entro al upView")
        if (view == txtProvincia || view == txtCiudad){
            print("entro al ifview")
            print("wawawawaw")
            //view.resignFirstResponder()
            //self.hideKeyboard()
        } else {
            print("no entro al if view")
            //txt_primerApellido.becomeFirstResponder()
            //UIApplication.sharedApplication().sendAction("resignFirstResponder", to:nil, from:nil, forEvent:nil)
            
            if self.downKeyboard {
                self.downKeyboard = false
                //self.scroll.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.scroll.frame) - 230)
            }
            /*let transitionOptions = UIViewAnimationOptions.TransitionNone
            UIView.transitionWithView(self.view, duration: 0.2, options: transitionOptions, animations: {
                //self.view.frame = CGRectMake(0, -120, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))
                if self.downKeyboard {
                    self.downKeyboard = false
                    //self.scroll.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.scroll.frame) - 230)
                }
                
               // let point = CGPointMake( 0, view.frame.origin.y - 150)
               // self.scroll.setContentOffset(point, animated: true)
                }, completion: { finished in
                    
            })*/
        }
        
        
    }
    /*(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return NO;
    }*/
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if(textField == txtProvincia){
            //bool_provi = true
            /*pickerProvincia.showPicker(self)
            pickerSelected = pickerProvincia
            txtProvincia.resignFirstResponder()*/
            self.hideKeyboard()
            //return false
            
            
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //print("entro a editar" + textField.description)
        
        if (textField == txtProvincia){
            //self.hideKeyboard()
            //self.dismissKeyboard()
            print("txtprovincia")
            //UIApplication.sharedApplication().sendAction(#selector(UIResponder.resignFirstResponder), to:nil, from:nil, forEvent:nil)
            //self.view.endEditing(true)
            
            //self.txt_primerNombre.resignFirstResponder()
            //self.txt_primerApellido.resignFirstResponder()
            //self.txt_segundoApellido.resignFirstResponder()
        //self.txtProvincia.setena
            pickerProvincia.showPicker(controller: self)
            pickerSelected = pickerProvincia
            txtProvincia.resignFirstResponder()
        } else if (textField == txtCiudad){
            //self.hideKeyboard()
            pickerCiudad.showPicker(controller: self)
            pickerSelected = pickerCiudad
            txtCiudad.resignFirstResponder()
        }
    }
    
    func downView( view : UITextField ){
        print("downview")
        //view.endEditing(true)
        self.dismissKeyboard()
        //self.hideKeyboard()
        self.txt_primerNombre.resignFirstResponder()
        self.txt_primerApellido.resignFirstResponder()
        self.txt_segundoApellido.resignFirstResponder()
        print("valor downkeyboard" + self.downKeyboard.description)
        if !self.downKeyboard {
            print("entro a la negacion")
            self.downKeyboard = true
            //self.scroll.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.scroll.frame) + 230)
        }
       /* let transitionOptions = UIViewAnimationOptions.TransitionNone
        UIView.transitionWithView(self.view, duration: 0.2, options: transitionOptions, animations: {
            //self.view.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))
            if !self.downKeyboard {
                print("entro a la negacion")
                self.downKeyboard = true
                //self.scroll.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.scroll.frame) + 230)
            }
            }, completion: { finished in
                
        })*/
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        print("dismiss")
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
       
        let transitionOptions = UIViewAnimationOptions.showHideTransitionViews//TransitionNone
        UIView.transition(with: self.view, duration: 0.2, options: transitionOptions, animations: {
            if !self.downKeyboard {
                self.downKeyboard = true
                //self.scroll.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.scroll.frame) + 230)
            }
            
            }, completion: { finished in
                
        })
        return true;
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getProvincias() {
        
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        
        
        let urldef = URLFactory.urldefinida()

        let url = NSURL(string: "\(urldef)servicio/login/obtenerprovincias?arg0=1")
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        let str = "wsappusuario:ZA$57@9b86@$2r5"
        
        /*let utf8str = str.dataUsingEncoding(NSUTF8StringEncoding)
        
        if let base64Encoded = utf8str?.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0)){
            
            //print("Encoded:  \(base64Encoded)")
            //request.addValue("Authorization", forHTTPHeaderField: "Basic \(base64Encoded)")
            request.setValue("Basic \(base64Encoded)", forHTTPHeaderField: "Authorization")
            
            
            if let base64Decoded = NSData(base64EncodedString: base64Encoded, options:   NSDataBase64DecodingOptions(rawValue: 0))
                .map({ NSString(data: $0, encoding: NSUTF8StringEncoding) })
            {
                // Convert back to a string
                print("Decoded:  \(base64Decoded)")
            }
        }*/
        let utf8str = str.data(using: String.Encoding.utf8)
        
        //let base64Encoded = str.data(using: String.Encoding.utf8, allowLossyConversion: true)
        //let base64Encoded = str.dataUsingEncoding(NSUTF8StringEncoding)!.base64EncodedStringWithOptions([])
        //print("Encoded: \(base64Encoded), \(utf8str!.base64EncodedString())")
        let base64DecodedData = utf8str!.base64EncodedString()
        print(base64DecodedData)
        //NSData(base64EncodedString: base64Encoded, options: [])!
        // let base64DecodedString = String(data: base64DecodedData as! Data, encoding: String.Encoding.utf8)!
        //print("Decoded: \(base64DecodedString)")
        
        //Note : Add the corresponding "Content-Type" and "Accept" header. In this example I had used the application/json.
        request.addValue(base64DecodedData, forHTTPHeaderField: "Authorization")

        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                    var err: NSError?
                    
                    //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    if (err != nil) {
                        print("JSON Error \(err!.localizedDescription)")
                    }
                    
                    //println(jsonResult);
                    //print(jsonResult)
                    let info =  jsonResult as NSDictionary
                    let data = info["lista"] as! [[String: Any]]
                    if data.count > 0 {
                        for item in data {
                            let provincia = Provincia()
                            if let pais = item["codigo_pais"] as? String {
                                provincia.codigoPais = pais as NSString
                            }
                            
                            if let prov = item["codigo_provincia"] as? String {
                                provincia.codigoProvincia = prov as NSString
                            }
                            
                            if let reg = item["codigo_region"] as? String {
                                provincia.codigoRegion = reg as NSString
                            }
                            
                            if let nombre = item["nombre_provincia"] as? String {
                                provincia.nombre = nombre as NSString
                            }
                            
                            self.provincias.add(provincia)
                        }
                    }
                    
                    
                    var lista = [""]
                    /*for provi in self.provincias {
                        lista.append(provi.valueForKey("nombre")as! String)
                        //lista.addObject(provi.nombre)
                    }*/
                    for  i in 0..<self.provincias.count{
                        print("encontro 1 \((self.provincias[i] as AnyObject).value(forKey: "nombre"))")
                        let nombre = (self.provincias[i] as AnyObject).value(forKey: "nombre")
                        lista.append(nombre as! String)
                    }

                    lista.remove(at: 0)
                    
                    self.dataProvincia = lista
                    self.txtProvincia.text = self.dataProvincia[0]
                    //
                    //
                    self.pickerProvincia = PickerList(datos: self.dataProvincia as NSArray)
                    
                    ViewUtilidades.hiddenLoader(controller: self)
                    self.getCiudades()
                } catch {
                    print("ERROR")
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: "Revise su conexión a Datos o Internet", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            
            
        }
    }
    
    func getCiudades() {
        
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        
        let provincia = provincias.object(at: pickerProvincia.eleccion) as! Provincia
        
        let urldef = URLFactory.urldefinida()

        let url = NSURL(string: "\(urldef)servicio/login/obtenerciudades?arg0=1&arg1=\(provincia.codigoProvincia)")
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        let str = "wsappusuario:ZA$57@9b86@$2r5"
        
        /*let utf8str = str.dataUsingEncoding(NSUTF8StringEncoding)
        
        if let base64Encoded = utf8str?.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0)){
            
           // print("Encoded:  \(base64Encoded)")
           // request.addValue("Authorization", forHTTPHeaderField: "Basic \(base64Encoded)")
            request.setValue("Basic \(base64Encoded)", forHTTPHeaderField: "Authorization")
            
            
            if let base64Decoded = NSData(base64EncodedString: base64Encoded, options:   NSDataBase64DecodingOptions(rawValue: 0))
                .map({ NSString(data: $0, encoding: NSUTF8StringEncoding) })
            {
                // Convert back to a string
                print("Decoded:  \(base64Decoded)")
            }
        }*/
        let utf8str = str.data(using: String.Encoding.utf8)
        
       // let base64Encoded = str.data(using: String.Encoding.utf8, allowLossyConversion: true)
        //let base64Encoded = str.dataUsingEncoding(NSUTF8StringEncoding)!.base64EncodedStringWithOptions([])
        //print("Encoded: \(base64Encoded), \(utf8str!.base64EncodedString())")
        let base64DecodedData = utf8str!.base64EncodedString()
        print(base64DecodedData)
        //NSData(base64EncodedString: base64Encoded, options: [])!
        // let base64DecodedString = String(data: base64DecodedData as! Data, encoding: String.Encoding.utf8)!
        //print("Decoded: \(base64DecodedString)")
        
        //Note : Add the corresponding "Content-Type" and "Accept" header. In this example I had used the application/json.
        request.addValue(base64DecodedData, forHTTPHeaderField: "Authorization")

        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                    var err: NSError?
                    
                    //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    if (err != nil) {
                        print("JSON Error \(err!.localizedDescription)")
                    }
                    
                    //println(jsonResult);
                    //print(jsonResult)
                    let info =  jsonResult as NSDictionary
                    let data = info["lista"] as! [[String: Any]]
                    if data.count > 0 {
                        self.ciudades.removeAllObjects()
                        for item in data {
                            let ciudad = Ciudad()
                            ciudad.codigoPais = item["codigopais"] as! NSString
                            ciudad.codigoProvincia = item["codigoprovincia"] as! NSString
                            //ciudad.codigoRegion = item["codigoregion"] as String
                            ciudad.codigoCiudad = item["codigociudad"] as! NSString
                            ciudad.nombre = item["nombreciudad"] as! NSString
                            self.ciudades.add(ciudad)
                        }
                    }
                    
                    var lista = [""]
                    /*for ciud in self.ciudades {
                        lista.append(ciud.valueForKey("nombre") as! String)
                    }*/
                    for  i in 0..<self.ciudades.count{
                        print("encontro 1 \((self.ciudades[i] as AnyObject).value(forKey: "nombre"))")
                        let nombre = (self.ciudades[i] as AnyObject).value(forKey: "nombre")
                        lista.append(nombre as! String)
                    }
                    lista.remove(at: 0)
                    
                    self.dataCiudad = lista as AnyObject as! [String]
                    self.txtCiudad.text = self.dataCiudad[0]
                    
                    
                    self.pickerCiudad = PickerList(datos: self.dataCiudad as NSArray)
                    
                    ViewUtilidades.hiddenLoader(controller: self)
                } catch {
                    print("ERROR")
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: "Revise su conexión a Datos o Internet", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            
            
        }
    }
    
}
