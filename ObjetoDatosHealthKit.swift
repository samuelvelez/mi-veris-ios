//
//  ObjetoDatosHealthKit.swift
//  Mi Veris
//
//  Created by Jorge on 04/07/16.
//  Copyright © 2016 Programadores-iOS.net. All rights reserved.
//

import UIKit

private var listaHealthKit : NSMutableArray = NSMutableArray()


class ObjetoDatosHealthKit: NSObject {
    
    init(sexo : String,sangre : String , fechaDeNacimiento : String, altura : String , peso : String, imc : String, contadorDePasos : String, distanciaRecorrida : String, distanciaPosición : String ,energiaQuemadaReposo : String ,energiaQuemadaActiva : String , frecuenciaCardiaca : String ) {
        
        self.sexo = sexo as String
        self.sangre = sangre as String
        self.fechaDeNacimiento = fechaDeNacimiento as String
        self.altura = altura as String
        
        self.peso = peso as String
        self.imc = imc as String
        self.contadorDePasos = contadorDePasos as String
        self.distanciaRecorrida = distanciaRecorrida as String
        self.distanciaPosición = distanciaPosición as String
        
        self.energiaQuemadaReposo = energiaQuemadaReposo as String
        self.energiaQuemadaActiva = energiaQuemadaActiva as String
        self.frecuenciaCardiaca = frecuenciaCardiaca as String
        
        
        
    }
    
    
    var sexo : String
    var sangre : String
    var fechaDeNacimiento :String
    var altura : String
    
    var peso : String
    var imc : String
    var contadorDePasos : String
    var distanciaRecorrida : String
    var distanciaPosición : String
    
    var energiaQuemadaReposo : String
    var energiaQuemadaActiva : String
    var frecuenciaCardiaca : String
    
    
    class func getDatos() -> NSMutableArray {
        return listaHealthKit
    }
    
    class func setDatos( lista : NSMutableArray) {
        listaHealthKit = lista
    }
    
    
}
