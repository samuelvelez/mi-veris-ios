//
//  ObjVerDetalle.swift
//  Mi Veris
//
//  Created by Jorge Merchan on 13/2/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//


import Foundation
class ObjVerDetalle
{
    var numeroorden : NSString = ""
    var codigoempresa : NSInteger = 0
    var lineadetalle : NSInteger = 0
    var codigoprestacion : NSString = ""
    var nombreprestacion : NSString = ""
}