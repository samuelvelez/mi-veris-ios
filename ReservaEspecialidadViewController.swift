//
//  ReservaEspecialidadViewController.swift
//  Mi Veris
//
//  Created by Andres Cantos on 2/6/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import UIKit

class ReservaEspecialidadViewController: UIViewController,UITableViewDelegate, UITableViewDataSource{

    var tableView: UITableView  =   UITableView() /*Table view que muestra la lista de especailidades*/
    var listaespecialidades : NSMutableArray?
    var delegate:ReservaEspecialidadDelegate! = nil
    var especialidadsel : ObjEspecialidad?
    var recomendacionnumeroorden : NSString = "0"
    var recomendacioncodempresa : NSString = "0"
    var recomendacionlineadetalle : NSString = "0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationItem.backBarButtonItem?.title=""
        //self.navigationController?.navigationItem.title = "Seleccione una Especialidad"
        navigationItem.backBarButtonItem?.title = ""
        navigationItem.title = "Seleccione una Especialidad"
    
        let us = Usuario.getEntity as Usuario
        cargardatos()
    }
    private func cargardatos(){
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        //let url2 = URLFactory.obtenerEspecialidades(numeroorden: self.recomendacionnumeroorden as String, codempresaorden: recomendacioncodempresa as String, lineadetalleorden: self.recomendacionlineadetalle as String)
        
        let us = Usuario.getEntity as Usuario
        let url2 = URLFactory.obtenerEspecialidades(tipoIdentificacion: us.tipoid as String, numeroIdentificacion: us.numeroidentificacion as String)
        
        
        let url = NSURL(string: "\(url2)")
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                var err: NSError?
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                if (err != nil) {
                    print("JSON Error \(err!.localizedDescription)")
                }
                
                
                let info =  jsonResult as NSDictionary
                    let data = info["lista"] as! [[String: Any]]

                
                if data.count > 0 {
                    self.listaespecialidades = NSMutableArray();
                    
                    for jsonobj  in data {
                        let esp = ObjEspecialidad()
                        esp.codigo = jsonobj["codigo"] as! NSInteger
                        esp.descripcion = jsonobj["descripcion"] as! String 
                        self.listaespecialidades?.add(esp)
                    }
                }else{
                    self.listaespecialidades = NSMutableArray();
                    }
                
                //dispatch_async(dispatch_get_main_queue(), {
                    DispatchQueue.main.async(execute: {
                    self.showEspecialidades()
                    ViewUtilidades.hiddenLoader(controller: self)
                })
                } catch {
                    print("ERROR")
                }
            }
            
        }
        
        ///////////////////////
        

        
    }
    func showEspecialidades(){
        tableView.frame = CGRect(x:0, y:60, width:self.view.frame.width, height:self.view.frame.height - 60);
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.backgroundColor = UIColor.clear
        //tableView.registerClass(DondeEstamosTableViewCell.self as AnyClass, forCellReuseIdentifier: "SucursalesCell");
        
        self.view.addSubview(tableView);
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaespecialidades!.count;
    }
    
    /*func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.separatorInset = UIEdgeInsetsZero
        cell.layoutMargins = UIEdgeInsetsZero
    }*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "") as UITableViewCell!
             ?? UITableViewCell(style: .default, reuseIdentifier: ""),
        especialidad = listaespecialidades!.object(at: indexPath.row) as! ObjEspecialidad
        
        cell.textLabel?.text = especialidad.descripcion
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        especialidadsel = listaespecialidades!.object(at: indexPath.row) as? ObjEspecialidad
        print("selecciono: \(especialidadsel!.codigo)")
        delegate.retornadereservaespecialidad(controller: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

protocol ReservaEspecialidadDelegate{
    func retornadereservaespecialidad(controller:ReservaEspecialidadViewController)
}
