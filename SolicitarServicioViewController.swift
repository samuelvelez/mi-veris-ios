//
//  SolicitarServicioViewController.swift
//  Mi Veris
//
//  Created by Jorge on 17/06/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import UIKit
import CoreLocation

class SolicitarServicioViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate {
    
    
    let galeriaController = UIImagePickerController()
    var chosenImage : UIImage!
    var listaImagenes : NSMutableArray = NSMutableArray()
    var cantidadFotos = 0
    
    
    var txtCedula : TextField = TextField()
    var txtIdentificacion : TextField = TextField()
    
    var txtNombre : TextField = TextField()
    var txtCiudad : TextField = TextField()
    var txtCelular : TextField = TextField()
    
    var pickerTipo : PickerList!
    var dataTipo = ["Cédula", "Pasaporte"]
    var pickerSelected : PickerList!
    
    var pickerCiudad : PickerList!
    var dataCiudad = [""]
    var arrayCiudad : NSMutableArray = NSMutableArray()
    
    var locationManager : CLLocationManager!
    var coord : CLLocationCoordinate2D!
    
    var posicionY : CGFloat = 15
    
    var downKeyboard : Bool  = true
    
    var scroll : UIScrollView = UIScrollView()
    
    let iOS7 = floor(NSFoundationVersionNumber) <= floor(NSFoundationVersionNumber_iOS_7_1)
    let iOS8 = floor(NSFoundationVersionNumber) > floor(NSFoundationVersionNumber_iOS_7_1)

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = ""
        galeriaController.delegate = self
        
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        let fondo : UIImageView = UIImageView(frame: self.view.bounds)
        fondo.image = UIImage(named:"fondocita.png")
        self.view.addSubview(fondo)
        
        let barWhite : UIView = UIView(frame: CGRect(x:0, y:64, width:self.view.frame.width , height: 30))
        
       // barWhite.backgroundColor = UIColor.black - backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x:0, y:64, width:self.view.frame.width / 1.8, height:30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x:barBlue.frame.width, y:64, width:45, height:30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x: 0, y: 64, width: self.view.frame.width - 15, height: 30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = " "
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)
        
       
        scroll = UIScrollView(frame: CGRect(x: 0, y: 94, width: self.view.frame.width, height: self.view.frame.height - 94) )
        scroll.isUserInteractionEnabled = true
        self.view.addSubview(scroll)
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(SolicitarServicioViewController.hideKeyboard))
        scroll.addGestureRecognizer(singleTap)
        self.view.addSubview(scroll)
        let screenWidth = UIScreen.main.bounds.width
        txtCedula.frame = CGRect(x: 13.0, y: posicionY, width: screenWidth - 26, height: 35)
        txtCedula.delegate = self
        txtCedula.text = "Cédula"
        txtCedula.layer.borderWidth = 1;
        txtCedula.layer.borderColor = UIColor.gray.cgColor
        txtCedula.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.2)
        txtCedula.layer.cornerRadius = 5
        txtCedula.font = UIFont.systemFont(ofSize: 14)
        scroll.addSubview(txtCedula)
        obtenerCiudades()
        self.pickerTipo = PickerList(datos: self.dataTipo as NSArray)
        
        posicionY += 45
        txtIdentificacion.frame = CGRect(x: 13.0, y: posicionY, width: screenWidth - 26, height: 35)
        txtIdentificacion.layer.borderWidth = 1;
        txtIdentificacion.delegate = self
        txtIdentificacion.placeholder = "Número de Identificación"
        txtIdentificacion.addTarget(self, action: #selector(upView(view:)), for: UIControlEvents.editingDidBegin)
        txtIdentificacion.addTarget(self, action: #selector(downView(view:)), for: UIControlEvents.editingDidEndOnExit)
        txtIdentificacion.layer.borderColor = UIColor.gray.cgColor
        txtIdentificacion.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.2)
        txtIdentificacion.layer.cornerRadius = 5
        txtIdentificacion.font = UIFont.systemFont(ofSize: 14)
        scroll.addSubview(txtIdentificacion)
        
        posicionY += 45
        txtNombre.frame = CGRect(x: 13.0, y: posicionY, width: screenWidth - 26, height: 35)
        txtNombre.layer.borderWidth = 1;
       // txtNombre.delegate = self
        txtNombre.addTarget(self, action: #selector(upView(view:)), for: UIControlEvents.editingDidBegin)
        txtNombre.addTarget(self, action: #selector(downView(view:)), for: UIControlEvents.editingDidEndOnExit)
        txtNombre.placeholder = "Nombre"
        txtNombre.layer.borderColor = UIColor.gray.cgColor
        txtNombre.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.2)
        txtNombre.layer.cornerRadius = 5
        txtNombre.font = UIFont.systemFont(ofSize: 14)
        scroll.addSubview(txtNombre)
        
        posicionY += 45
        txtCiudad.frame = CGRect(x: 13.0, y: posicionY, width: screenWidth - 26, height: 35)
        txtCiudad.layer.borderWidth = 1;
        txtCiudad.delegate = self
        txtCiudad.text = "Ciudad"
        txtCiudad.addTarget(self, action: #selector(upView(view:)), for: UIControlEvents.editingDidBegin)
        txtCiudad.addTarget(self, action: #selector(downView(view:)), for: UIControlEvents.editingDidEndOnExit)
        txtCiudad.layer.borderColor = UIColor.gray.cgColor
        txtCiudad.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.2)
        txtCiudad.layer.cornerRadius = 5
        txtCiudad.font = UIFont.systemFont(ofSize: 14)
        scroll.addSubview(txtCiudad)
        
        posicionY += 45
        let lbCelular = UILabel(frame: CGRect(x: 18.0, y: posicionY, width: 290.0, height: 35) )
        lbCelular.text = "Mi celular"
        

        lbCelular.textColor = UIColor.gray
        lbCelular.font = UIFont.systemFont(ofSize: 14)
        scroll.addSubview(lbCelular)
        
        txtCelular.frame = CGRect(x: 100, y: posicionY, width: screenWidth - 126, height: 35)
        txtCelular.layer.borderWidth = 1;
        txtCelular.delegate = self
        txtCelular.placeholder = "Teléfono móvil 0999999999"
        txtCelular.layer.borderColor = UIColor.gray.cgColor
        txtCelular.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.2)
        
        txtCelular.addTarget(self, action: #selector(upView(view:)), for: UIControlEvents.editingDidBegin)
        txtCelular.addTarget(self, action: #selector(downView(view:)), for: UIControlEvents.editingDidEndOnExit)
        txtCelular.layer.cornerRadius = 5
        txtCelular.font = UIFont.systemFont(ofSize: 14)
        txtCelular.keyboardType = UIKeyboardType.numbersAndPunctuation
        scroll.addSubview(txtCelular)
        
        posicionY += 45

        let verFlobotomista = UIButton(type: UIButtonType.system)
        verFlobotomista.frame = CGRect(x: 13.0, y: posicionY, width: screenWidth - 26, height: 40.0)
        verFlobotomista.layer.cornerRadius = 20
        verFlobotomista.layer.masksToBounds = true
        verFlobotomista.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        verFlobotomista.setTitle("Deseo adjuntar foto de orden de exámenes (máx 4)", for: UIControlState.normal)
        verFlobotomista.setTitleColor(UIColor.white, for: UIControlState.normal)
        verFlobotomista.titleLabel!.font =  UIFont(name: "helvetica-bold", size: 11)
        //verFlobotomista.addTarget(self, action: #selector(SolicitarServicioViewController.addFoto), for: UIControlEvents.touchUpInside)
        scroll.addSubview(verFlobotomista)
        
        posicionY += 45
        let solicitar = UIButton(type: UIButtonType.system)
        solicitar.frame = CGRect(x: 13.0, y: posicionY, width: screenWidth - 26, height: 40.0)
        solicitar.layer.cornerRadius = 20
        solicitar.layer.masksToBounds = true
        solicitar.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        solicitar.setTitle("Solicitar", for: UIControlState.normal)
        solicitar.setTitleColor(UIColor.white, for: UIControlState.normal)
        solicitar.titleLabel!.font =  UIFont(name: "helvetica-bold", size: 13)
        //solicitar.addTarget(self, action: #selector(SolicitarServicioViewController.solicitar), for: UIControlEvents.touchUpInside)
        scroll.addSubview(solicitar)
        
        
        let usuario = Usuario.getEntity
        txtIdentificacion.text = usuario.numeroidentificacion as String
        //txtIdentificacion.enabled = false
        txtNombre.text = usuario.nombre as String
        //txtNombre.enabled = false
        
        pickerCiudad = PickerList(datos: dataCiudad as NSArray)
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        if iOS8 {
            locationManager.requestAlwaysAuthorization()
        }
        locationManager.startUpdatingLocation()
        
        scroll.contentSize = CGSize(width:self.view.frame.width,height: 550)
        
        obtenerCiudades()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        print("entro a didappperar")
     //   pickerCiudad.showPicker(controller: self)
    }
    func obtenerCiudades(){
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        let url = NSURL(string: "http://10.20.90.64:8080/MiverisWsrest/servicio/laboratorio/ciudades")
        let request = NSMutableURLRequest(url: url! as URL)
        print("http://10.20.90.64:8080/MiverisWsrest/servicio/laboratorio/ciudades")
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                    var err: NSError?
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    if (err != nil) {
                        print("JSON Error \(err!.localizedDescription)")
                    }
                    
                    let info =  jsonResult as NSDictionary
                    let data = info["lista"] as! [[String: Any]]//NSArray
                    print(data)
                    if data.count > 0{
                        self.arrayCiudad.removeAllObjects()
                        for item in data{
                            let ciudad = Ciudad()
                            ciudad.codigoPais = item["codigopais"] as! NSString
                            ciudad.codigoProvincia = item["codigoprovincia"] as! NSString
                            ciudad.codigoRegion = item["codigoregion"] as! NSString
                            ciudad.codigoCiudad = item["codigociudad"] as! NSString
                            ciudad.nombre = item["nombreciudad"] as! NSString
                            self.arrayCiudad.add(ciudad)
                        }
                    }
                    var lista = [""]
                    for  i in 0..<self.arrayCiudad.count{
                        let nombre = (self.arrayCiudad[i] as AnyObject).value(forKey: "nombre")
                        lista.append(nombre as! String)
                    }
                    lista.remove(at: 0)
                    
                    self.dataCiudad = ((lista as AnyObject as! [NSString] as [String]))
                    self.txtCiudad.text = self.dataCiudad[0] as String
                    
                    
                    self.pickerCiudad = PickerList(datos: self.dataCiudad as NSArray)
                    print("contrador de ciudades \(self.dataCiudad.count)")

                    DispatchQueue.main.async {
                     //   self.showSucursales()
                        
                     //   self.tableView.reloadData()
                        ViewUtilidades.hiddenLoader(controller: self)
                    }
                } catch {
                    print("ERROR")
                }
            }

        }
    }
  
    func hideKeyboard(){
        print("hidekeyboard")
        self.txtIdentificacion.resignFirstResponder()
        //self.txtCedula.resignFirstResponder()
        self.txtNombre.resignFirstResponder()
        //self.txtCiudad.resignFirstResponder()
        self.txtCelular.resignFirstResponder()
    }
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        print("dismiss")
        self.view.endEditing(true)
    }
    func downView( view : UITextField ){
        print("downview")
        //view.endEditing(true)
        //self.dismissKeyboard()
        //self.hideKeyboard()
        self.txtIdentificacion.resignFirstResponder()
        //self.txtCedula.resignFirstResponder()
        self.txtNombre.resignFirstResponder()
        self.txtCelular.resignFirstResponder()
        print("valor downkeyboard" + self.downKeyboard.description)
        if !self.downKeyboard {
            print(" downview - entro a la negacion")
            self.downKeyboard = true
        }else{
            print("downview - no entraaa")
        }
    }
    
    func upView( view : UITextField ){
        print("entro al upView")
        if (view == txtCiudad || view == txtCedula){
            print("entro al ifview")
            print("wawawawaw")
            pickerCiudad.showPicker(controller: self)
            //view.resignFirstResponder()
            //self.hideKeyboard()
        } else {
            print("no entro al if view")
            if self.downKeyboard {
                self.downKeyboard = false
                //self.scroll.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.scroll.frame) - 230)
            }
        }
    }
    
    func locationManager(_ manager:CLLocationManager, didUpdateLocations locations:[CLLocation]) {
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        coord = locationObj.coordinate
        
        print(coord.latitude)
        print(coord.longitude)
    }
    
    
    let alertSolicitar: UIAlertView = UIAlertView()
    func solicitar(){
        if ( pickerCiudad.eleccion == 0) {
            UtilidadesGeneral.mensaje(mensaje: "Seleccione su ciudad")
        } else {
            if ( txtCelular.text!.trimmingCharacters(in: NSCharacterSet.whitespaces) == "") {//stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()) == "" ){
                UtilidadesGeneral.mensaje(mensaje: "Ingrese su número de celular")
            } else {/*
                if ( listaImagenes.count == 0 ){
                    UtilidadesGeneral.mensaje("Adjunte al menos una fotografía")
                } else {*/
                    if ( coord == nil ){
                        UtilidadesGeneral.mensaje(mensaje: "No se puede obtener su posición actual. Revise su configuración")
                    } else {
                        alertSolicitar.delegate = self
                        
                        alertSolicitar.title = "Veris"
                        alertSolicitar.message = "¿Esta seguro de enviar la solicitud?"
                        alertSolicitar.addButton(withTitle: "Aceptar")
                        alertSolicitar.addButton(withTitle: "Cancelar")
                        
                        alertSolicitar.show()
                        
                    }
                /*}*/
            }
        }
    }
    
    func consultarFlebo(){
        
        /*var userDefaults : NSUserDefaults = NSUserDefaults.standardUserDefaults()
        if ( userDefaults.integerForKey("FlebotomistaSolicitado") == 1 && userDefaults.valueForKey("telefono") != nil ){
            
            var soap = SOAPEngine()
            soap.userAgent = "SOAPEngine"
            soap.licenseKey = "43J2WGZZBbuKdZYr2RZr2NqgaS1KODsGmZRm2JDR6iT+WkOwEewhuWNL7mfjJZBUi3P41Whqn/0dTLNnybeWJQ=="
            soap.actionNamespaceSlash = true
            soap.version = VERSION_1_1
            soap.responseHeader = true // use only for non standard MS-SOAP service
            
            soap.setValue( userDefaults.stringForKey("telefono"), forKey: "arg0")
            
            //self.getUbicacionFlebo();
            
            soap.requestURL("http://181.198.116.237:8080/WS_Muestras/services/Procesos?wsdl",
                soapAction: "http://181.198.116.237:8080/WS_Muestras/services/obtenerUbicacionFlebo",
                completeWithDictionary: { (statusCode : Int,
                    dict : [NSObject : AnyObject]!) -> Void in
                    
                    var result : Dictionary = dict as Dictionary
                    var result2 : NSMutableDictionary = result["Body"] as! NSMutableDictionary
                    var result3 : NSMutableDictionary = result2["obtenerUbicacionFleboResponse"] as! NSMutableDictionary
                    
                    print( result3["obtenerUbicacionFleboReturn"] )
                    
                    var cadena = result3["obtenerUbicacionFleboReturn"] as! NSString
                    var valores = cadena.componentsSeparatedByString("\t") as [NSString]
                    
                    if valores.count > 2 {
                        
                        var codigoProfesional : NSString = valores[0]
                        MapFlebotomistaViewController.setCodigo(codigoProfesional)
                        
                        var latitud : NSString = valores[1]
                        MapFlebotomistaViewController.setLatitud(latitud)
                        
                        var longitud : NSString = valores[2]
                        MapFlebotomistaViewController.setLongitud(longitud)
                        
                        var tipo : NSString = valores[3]
                        
                        if (  ViewController.getTipoNotificacion() != tipo ){
                            ViewController.setTipoNotificacion( tipo )
                            if tipo == "2" {
                                var localNotification: UILocalNotification = UILocalNotification()
                                localNotification.alertAction = "Flebotomista"
                                localNotification.alertBody = "Flebotomista se encuentra en camino"
                                localNotification.fireDate = NSDate()
                                localNotification.applicationIconBadgeNumber = 2
                                UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
                            } else if tipo == "3" {
                                var localNotification: UILocalNotification = UILocalNotification()
                                localNotification.alertAction = "Flebotomista"
                                localNotification.alertBody = "Flebotomista ha llegado"
                                localNotification.fireDate = NSDate()
                                localNotification.applicationIconBadgeNumber = 3
                                UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
                            } else if tipo == "4" {
                                var userDefaults : NSUserDefaults = NSUserDefaults.standardUserDefaults()
                                userDefaults.removeObjectForKey("FlebotomistaSolicitado")
                                userDefaults.removeObjectForKey("telefono")
                            }
                        }
                    } else {
                        MapFlebotomistaViewController.setCodigo("")
                        MapFlebotomistaViewController.setLatitud("")
                        MapFlebotomistaViewController.setLongitud("")
                    }
                    
                    //UtilidadesGeneral.mensaje( result3["obtenerUbicacionFlebotomistaReturn"] as NSString )
                    
                }) { (error : NSError!) -> Void in
                    //UtilidadesGeneral.mensaje("Ha ocurrido un error")
                    NSLog("%@", error)
            }
            
        } else {
            // UtilidadesGeneral.mensaje("Ha ocurrido un error")
        }*/
        
    }
    
    func soapSolicitar(){
       
        var stringImagenes = ""
        
        for imagen in self.listaImagenes {
            if stringImagenes != "" {
                stringImagenes += ";"
            }
            //var base64String = imagen.base64EncodedStringWithOptions(.allZeros)
            var imageData = UIImagePNGRepresentation(imagen as! UIImage)
            //--let base64String = imageData!.base64EncodedStringWithOptions([ ])
            
            //--stringImagenes += base64String
            
        }

        
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        
    }
    
    func addFoto(){
        
        let createAccountErrorAlert: UIAlertView = UIAlertView()
        
        createAccountErrorAlert.delegate = self
        createAccountErrorAlert.title = "¿Que desea hacer?"
        createAccountErrorAlert.message = "Eliga una opción"
        createAccountErrorAlert.addButton(withTitle: "Cancelar")
        createAccountErrorAlert.addButton(withTitle: "Tomar foto")
        createAccountErrorAlert.addButton(withTitle: "Ir a galeria")
        createAccountErrorAlert.show()
    }
    
    func alertView(View: UIAlertView!, clickedButtonAtIndex buttonIndex: Int) {
        
        
        if View == alertSolicitar {
            switch buttonIndex{
                
            case 1:
                print("Cancelar");
                
            default:
                self.soapSolicitar()
            }
            
        } else {
            switch buttonIndex {
                
            case 1:
                if UIImagePickerController.availableCaptureModes(for: .rear) != nil {
                    galeriaController.allowsEditing = false
                    galeriaController.sourceType = UIImagePickerControllerSourceType.camera
                    galeriaController.cameraCaptureMode = .photo
                    present(galeriaController, animated: true, completion: nil)
                } else {
                    let alertVC = UIAlertController(title: "No se encontro el dispositivo", message: "Este dispositivo no cuenta con cámara", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Ok", style:.default, handler: nil)
                    alertVC.addAction(okAction)
                    present(alertVC, animated: true, completion: nil)
                }
            case 2:
                galeriaController.allowsEditing = false
                galeriaController.sourceType = .photoLibrary
                galeriaController.modalPresentationStyle = .popover
                present(galeriaController, animated: true, completion: nil)//4
                
            default:
                print("alertView \(buttonIndex) clicked")
                
            }
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        //let imgAux = Toucan(image: chosenImage!).resize(CGSize(width: 250, height: 250), fitMode: Toucan.Resize.FitMode.Clip).image
        
        let image = UIImageView()
        if cantidadFotos == 0 {
            posicionY += 50
            image.frame = CGRect(x: 10, y: posicionY, width: self.view.frame.width / 2 - 20, height: 180)
            posicionY += 300
        } else if cantidadFotos == 1 {
            image.frame = CGRect(x: 10 + self.view.frame.width / 2, y: posicionY - 300, width: self.view.frame.width / 2 - 20, height: 180)
        } else if cantidadFotos == 2 {
            image.frame = CGRect(x: 10, y: posicionY - 100, width: self.view.frame.width / 2 - 20, height: 180)
            posicionY += 300
        } else if cantidadFotos == 3 {
            image.frame = CGRect(x: 10 + self.view.frame.width / 2, y: posicionY - 400, width: self.view.frame.width / 2 - 20, height: 180)
        }
        
        //image.image = imgAux
        scroll.addSubview(image)
    
        scroll.contentSize = CGSize(width: self.view.frame.width, height: posicionY)
        
        //listaImagenes.addObject(imgAux)
        cantidadFotos += 1
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
        print("sale")
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if(textField == txtCiudad || textField == txtCedula){
            
            self.hideKeyboard()
          }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        if(textField == txtCelular){
            return newLength <= 10
        }else{
            return newLength <= 50
        }
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if (textField == txtCedula){
            pickerTipo.showPicker(controller: self)
            pickerSelected = pickerTipo
            txtCedula.resignFirstResponder()
            //txtCelular.resignFirstResponder()
        } else if (textField == txtCiudad){
            print("entro al else txtciudad")
            pickerCiudad.showPicker(controller: self)
            pickerSelected = pickerCiudad
            txtCiudad.resignFirstResponder()
        }
    }
    
    /*func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }*/
    
    func cancelPicker(){
        pickerSelected.hidePicker()
        txtCedula.resignFirstResponder()
        txtCiudad.resignFirstResponder()
    }
    
    
    //
    func acceptPicker(){
        pickerSelected.hidePicker()
        if pickerSelected == pickerTipo {
            txtCedula.text = dataTipo[pickerTipo.eleccion]
            txtCedula.resignFirstResponder()
        } else if (pickerSelected == pickerCiudad){
            txtCiudad.text = dataCiudad[pickerCiudad.eleccion] as String
            txtCiudad.resignFirstResponder()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
         textField.resignFirstResponder()
        let transitionOptions = UIViewAnimationOptions.showHideTransitionViews
        UIView.transition(with: self.view, duration: 0.2, options: transitionOptions, animations: {
            if !self.downKeyboard {
                self.downKeyboard = true
                
            }
            
            }, completion: { finished in
                
        })
        return true;

    }


}
