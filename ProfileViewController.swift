//
//  ProfileViewController.swift
//  Mi Veris
//
//  Created by Samuel Velez on 1/8/16.
//  Copyright © 2016 Programadores-iOS.net. All rights reserved.
//

import UIKit
import HealthKit

var contadorDatosHealthKit = 0

class ProfileViewController: UIViewController, UITableViewDelegate , UITableViewDataSource {
    
    let UpdateProfileInfoSection = 0
    let SaveBMISection = 1
    let kUnknownString   = "-"
    
    var tableView = UITableView()
    
    
    var objeto : ObjetoDatosHealthKit?
    
    
    var healthManager:HealthManager?
    
    
    var tipoDatos = ["Sexo","Sangre","Fecha de Nacimiento","Altura","Peso","IMC","Contador de Pasos","Distancia Recorrida","Energia quemada reposo","Energia quemada Activa","Frecuencia Cardiaca"]
    
    var DatosHealthKit = ["","","","","","","","","","",""]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        objeto = ObjetoDatosHealthKit( sexo: "", sangre: "", fechaDeNacimiento: "", altura: "", peso: "", imc: "", contadorDePasos: "", distanciaRecorrida: "", distanciaPosición: "", energiaQuemadaReposo: "", energiaQuemadaActiva: "", frecuenciaCardiaca: "")
        ObjetoDatosHealthKit.getDatos().add(objeto!)
        
        let identificacion: String? = UserDefaults.standard.object(forKey: "identificacion") as? String
        print(identificacion!)
        let nombre3: String? = UserDefaults.standard.object(forKey: "nombre") as? String
        print(nombre3!)
        
        
        let qualityOfServiceClass = QOS_CLASS_BACKGROUND
        //  let backgroundQueue = DispatchQueue.global(qos: QualityOfService)//Int(UInt32(qualityOfServiceClass.rawValue)), 0)
        //dispatch_async(backgroundQueue, {
        print("This is run on the background queue", terminator: "")
        
        
        //dispatch_async(dispatch_get_main_queue(), { () -> Void in
        DispatchQueue.main.async(execute: {
            ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        })
        //   });
        
        //comentado
        //self.updateHealthInfo()
        
        //dispatch_async(dispatch_get_main_queue(), { () -> Void in
        DispatchQueue.main.async(execute: {
            print("This is run on the main queue, after the previous code in outer block", terminator: "")
            
            
            
            while true
            {
                //println(self.banderaContadorNotificaciones)
                if contadorDatosHealthKit >= 7
                {
                    sleep(1)
                    ViewUtilidades.hiddenLoader(controller: self)
                    self.tableView.reloadData()
                    break
                }
            }
            
            
            
        })
        //})
        
        createTable()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func updateHealthInfo() {
        
        let extensionesHealthKit = UIImage()
        
        extensionesHealthKit.reinciarContadorParaRefrescarObjeto()
        //updateProfileInfo();
        extensionesHealthKit.updateProfileInfo(healthManager: healthManager!)
        //updateWeight();
        extensionesHealthKit.updateWeight(healthManager: healthManager!)
        //updateHeight();
        extensionesHealthKit.updateHeight(healthManager: healthManager!)
        //updateIMC();
        extensionesHealthKit.updateIMC(healthManager: healthManager!);
        //updateContadorPasos()
        extensionesHealthKit.updateContadorPasos(healthManager: healthManager!)
        //updateDistanciaRecorrida()
        extensionesHealthKit.updateDistanciaRecorrida(healthManager: healthManager!)
        //updateEnergiaQuemadaReposo()
        extensionesHealthKit.updateEnergiaQuemadaReposo(healthManager: healthManager!)
        //updateEnergiaQuemadaActiva()
        extensionesHealthKit.updateEnergiaQuemadaActiva(healthManager: healthManager!)
        //updateHeartRate()
        extensionesHealthKit.updateHeartRate(healthManager: healthManager!)
        
    }
    
    
    
    // MARK: - utility methods
    func calculateBMIWithWeightInKilograms(weightInKilograms:Double, heightInMeters:Double) -> Double?
    {
        if heightInMeters == 0 {
            return nil;
        }
        return (weightInKilograms/(heightInMeters*heightInMeters));
    }
    
    
    
    
    func bloodTypeLiteral(bloodType:HKBloodType?)->String
    {
        
        var bloodTypeText = kUnknownString;
        
        if bloodType != nil {
            
            switch( bloodType! ) {
            case .aPositive:
                bloodTypeText = "A+"
            case .aNegative:
                bloodTypeText = "A-"
            case .bPositive:
                bloodTypeText = "B+"
            case .bNegative:
                bloodTypeText = "B-"
            case .abPositive:
                bloodTypeText = "AB+"
            case .abNegative:
                bloodTypeText = "AB-"
            case .oPositive:
                bloodTypeText = "O+"
            case .oNegative:
                bloodTypeText = "O-"
            default:
                break;
            }
            
        }
        return bloodTypeText;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath , animated: true)
        
        switch (indexPath.row)
        {
        case (UpdateProfileInfoSection):
           // updateHealthInfo()
            print(DatosHealthKit)
            tableView.reloadData()
            //    case (SaveBMISection):
        //      saveBMI()
        case 1:
            let objeto = ObjetoDatosHealthKit.getDatos().object(at: 0) as! ObjetoDatosHealthKit
            
            print(objeto.sexo)
            print(objeto.sangre)
            print(objeto.fechaDeNacimiento)
            print(objeto.altura)
            print(objeto.peso)
            print(objeto.imc)
            print(objeto.contadorDePasos)
            print(objeto.distanciaRecorrida)
            print(objeto.energiaQuemadaReposo)
            print(objeto.energiaQuemadaActiva)
            print(objeto.frecuenciaCardiaca)
            
            
        default:
            break;
        }
        
        
    }
    
    
    
    
    func createTable()
    {
        
        tableView.frame = CGRect(x:0, y:64, width:self.view.frame.width, height:self.view.frame.height - 64);
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.backgroundColor = UIColor.clear
        
        let nib1 = UINib(nibName: "HealthKitTableViewCell", bundle: nil)
        tableView.register(nib1,forCellReuseIdentifier: "cellhealth")
        
        self.view.addSubview(tableView);
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tipoDatos.count;
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellhealth : HealthKitTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "cellhealth", for: indexPath) as! HealthKitTableViewCell
        
        let objeto : ObjetoDatosHealthKit = ObjetoDatosHealthKit.getDatos().object(at: 0) as! ObjetoDatosHealthKit
        
        
        //  var tipoDatos = ["Sexo","Sangre","Fecha de Nacimiento","Altura","Peso","IMC","Contador de Pasos","Distancia Recorrida","Distancia Posición","Energia quemada reposo","Energia quemada Activa","Frecuencia Cardiaca"]
        
        
        switch indexPath.row
        {
        case 0:
            cellhealth.resultado.text = objeto.sexo
            cellhealth.tipo.text = tipoDatos[indexPath.row]
            cellhealth.resultado.adjustsFontSizeToFitWidth = true
            cellhealth.tipo.adjustsFontSizeToFitWidth = true
            cellhealth.resultado.textColor = UIColor.black
            if objeto.sexo == ""
            {
                cellhealth.resultado.textColor = UIColor.lightGray
                cellhealth.resultado.text = "-"
            }
            return cellhealth
        case 1:
            cellhealth.resultado.text = objeto.sangre
            cellhealth.tipo.text = tipoDatos[indexPath.row]
            cellhealth.resultado.adjustsFontSizeToFitWidth = true
            cellhealth.tipo.adjustsFontSizeToFitWidth = true
            cellhealth.resultado.textColor = UIColor.black
            if objeto.sangre == ""
            {
                cellhealth.resultado.textColor = UIColor.lightGray
                cellhealth.resultado.text = "-"
            }
            return cellhealth
        case 2:
            cellhealth.resultado.text = objeto.fechaDeNacimiento
            cellhealth.tipo.text = tipoDatos[indexPath.row]
            cellhealth.resultado.adjustsFontSizeToFitWidth = true
            cellhealth.tipo.adjustsFontSizeToFitWidth = true
            cellhealth.resultado.textColor = UIColor.black
            if objeto.fechaDeNacimiento == ""
            {
                cellhealth.resultado.textColor = UIColor.lightGray
                cellhealth.resultado.text = "-"
            }
            return cellhealth
        case 3:
            cellhealth.resultado.text = "\(objeto.altura) m."
            cellhealth.tipo.text = tipoDatos[indexPath.row]
            cellhealth.resultado.adjustsFontSizeToFitWidth = true
            cellhealth.tipo.adjustsFontSizeToFitWidth = true
            cellhealth.resultado.textColor = UIColor.black
            if objeto.altura == ""
            {
                cellhealth.resultado.textColor = UIColor.lightGray
                //cellhealth.resultado.text = "-"
            }
            return cellhealth
        case 4:
            cellhealth.resultado.text = "\(objeto.peso) Kg."
            cellhealth.tipo.text = tipoDatos[indexPath.row]
            cellhealth.resultado.adjustsFontSizeToFitWidth = true
            cellhealth.tipo.adjustsFontSizeToFitWidth = true
            cellhealth.resultado.textColor = UIColor.black
            if objeto.peso == ""
            {
                cellhealth.resultado.textColor = UIColor.lightGray
            }
            return cellhealth
        case 5:
            cellhealth.resultado.text = objeto.imc
            cellhealth.tipo.text = tipoDatos[indexPath.row]
            cellhealth.resultado.adjustsFontSizeToFitWidth = true
            cellhealth.tipo.adjustsFontSizeToFitWidth = true
            cellhealth.resultado.textColor = UIColor.black
            if objeto.imc == ""
            {
                cellhealth.resultado.textColor = UIColor.lightGray
                cellhealth.resultado.text = "-"
            }
            return cellhealth
        case 6:
            cellhealth.resultado.text = "\(objeto.contadorDePasos) pasos"
            cellhealth.tipo.text = tipoDatos[indexPath.row]
            cellhealth.resultado.adjustsFontSizeToFitWidth = true
            cellhealth.tipo.adjustsFontSizeToFitWidth = true
            cellhealth.resultado.textColor = UIColor.black
            if objeto.contadorDePasos == ""
            {
                cellhealth.resultado.textColor = UIColor.lightGray
            }
            return cellhealth
        case 7:
            cellhealth.resultado.text = "\(objeto.distanciaRecorrida) m."
            cellhealth.tipo.text = tipoDatos[indexPath.row]
            cellhealth.resultado.adjustsFontSizeToFitWidth = true
            cellhealth.tipo.adjustsFontSizeToFitWidth = true
            cellhealth.resultado.textColor = UIColor.black
            if objeto.distanciaRecorrida == ""
            {
                cellhealth.resultado.textColor = UIColor.lightGray
            }
            return cellhealth
        case 8:
            cellhealth.resultado.text = "\(objeto.energiaQuemadaReposo) Kcal"
            cellhealth.tipo.text = tipoDatos[indexPath.row]
            cellhealth.resultado.adjustsFontSizeToFitWidth = true
            cellhealth.tipo.adjustsFontSizeToFitWidth = true
            cellhealth.resultado.textColor = UIColor.black
            if objeto.energiaQuemadaReposo == ""
            {
                cellhealth.resultado.textColor = UIColor.lightGray
            }
            return cellhealth
        case 9:
            cellhealth.resultado.text = "\(objeto.energiaQuemadaActiva) Kcal"
            cellhealth.tipo.text = tipoDatos[indexPath.row]
            cellhealth.resultado.adjustsFontSizeToFitWidth = true
            cellhealth.tipo.adjustsFontSizeToFitWidth = true
            cellhealth.resultado.textColor = UIColor.black
            if objeto.energiaQuemadaActiva == ""
            {
                cellhealth.resultado.textColor = UIColor.lightGray
            }
            return cellhealth
        case 10:
            cellhealth.resultado.text = "\(objeto.frecuenciaCardiaca) lat/s"
            cellhealth.tipo.text = tipoDatos[indexPath.row]
            cellhealth.resultado.adjustsFontSizeToFitWidth = true
            cellhealth.tipo.adjustsFontSizeToFitWidth = true
            cellhealth.resultado.textColor = UIColor.black
            if objeto.frecuenciaCardiaca == ""
            {
                cellhealth.resultado.textColor = UIColor.lightGray
            }
            return cellhealth
        default:
            print("error")
            return cellhealth
        }
        
        //cellhealth.tipo.text = tipoDatos[indexPath.row]
        
        
        
        //cellhealth.resultado.text = DatosHealthKit[indexPath.row] as! String
        
        //cell.icono.image = UIImage(named: imagenes[indexPath.row])
        //cell.title.text = items[indexPath.row]
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
    
    
}


extension UIImage  // Extensiones de funciones de HealthKit sobre una imagen
{
    func reinciarContadorParaRefrescarObjeto()
    {
        contadorDatosHealthKit = 0
    }
    
    func updateProfileInfo(healthManager : HealthManager)
    {
        let profile = healthManager.readProfile()
        contadorDatosHealthKit += 1
        
        
        //print(profile)
        
        let objeto : ObjetoDatosHealthKit = ObjetoDatosHealthKit.getDatos().object(at: 0) as! ObjetoDatosHealthKit
        
        let biologicalSex = profile.biologicalsex?.biologicalSex
        let kUnknownString   = "-"
        var biologicalSexText = kUnknownString;
        
        if  biologicalSex != nil {
            
            switch( biologicalSex! )
            {
            case .female:
                biologicalSexText = "Female"
                objeto.sexo = biologicalSexText
            case .male:
                biologicalSexText = "Male"
                objeto.sexo = biologicalSexText
            default:
                break;
            }
            
        }
        
        
        let bloodType = profile.bloodtype?.bloodType
        var bloodTypeText = kUnknownString;
        if bloodType != nil {
            
            switch( bloodType! ) {
            case .aPositive:
                bloodTypeText = "A+"
                objeto.sangre = bloodTypeText
            case .aNegative:
                bloodTypeText = "A-"
                objeto.sangre = bloodTypeText
            case .bPositive:
                bloodTypeText = "B+"
                objeto.sangre = bloodTypeText
            case .bNegative:
                bloodTypeText = "B-"
                objeto.sangre = bloodTypeText
            case .abPositive:
                bloodTypeText = "AB+"
                objeto.sangre = bloodTypeText
            case .abNegative:
                bloodTypeText = "AB-"
                objeto.sangre = bloodTypeText
            case .oPositive:
                bloodTypeText = "O+"
                objeto.sangre = bloodTypeText
            case .oNegative:
                bloodTypeText = "O-"
                objeto.sangre = bloodTypeText
            default:
                break;
            }
            
            
            
            // 1. Request birthday and calculate age
            var birthDay: NSDate
            var age: Int?
            var fechaNacimiento : NSDate?
            
            do {
                try birthDay = (healthManager.healthKitStore.dateOfBirth() as NSDate)
                fechaNacimiento = birthDay
                let today = NSDate()
                
                //let differenceComponents = NSCalendar.currentCalendar.components(NSCalendar.Unit.Year, fromDate: birthDay, toDate: today, options: NSCalendar.Options(rawValue:0))
                
                //age = differenceComponents.year
                
                
                let cadenaRuido = String(describing: fechaNacimiento!)
                let extraerValor : NSString = cadenaRuido.components(separatedBy: " ")[0] as NSString
                
                //print(age)
                //print(fechaNacimiento)
                
                objeto.fechaDeNacimiento = String (extraerValor)
                
                
            } catch let error as NSError {
                print("Error: \(error)")
            }
            
            
        }
    }
    
    func updateWeight ( healthManager : HealthManager )
    {
        
        // 1. Construct an HKSampleType for weight
        var weight : HKQuantitySample?
        let sampleType = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMass)
        
        // 2. Call the method to read the most recent weight sample
        healthManager.readMostRecentSample(sampleType: sampleType!, completion: { (mostRecentWeight, error) -> Void in
            
            if( error != nil )
            {
                print("Error reading weight from HealthKit Store: \(error?.localizedDescription)")
                return;
            }
            //var weightLocalizedString = self.kUnknownString;
            // 3. Format the weight to display it on the screen
            weight = mostRecentWeight as? HKQuantitySample;
            //print(weight)
            
            DispatchQueue.main.async {
                //self.weightLabel.text = weightLocalizedString
                //self.DatosHealthKit.addObject("Peso : \(weightLocalizedString)")
                if weight != nil
                {
                    let cadenaRuido = String(describing: weight!)
                    //let extraerValor : NSString = cadenaRuido.componentsSeparatedByString(" ")[0] as NSString
                    let extraerValor : NSString = cadenaRuido.components(separatedBy: " ")[0] as NSString
                    let objeto : ObjetoDatosHealthKit = ObjetoDatosHealthKit.getDatos().object(at: 0) as! ObjetoDatosHealthKit
                    objeto.peso = extraerValor as String
                    
                    //print(weight)
                    //print(extraerValor)
                }
                //self.updateBMI()
            }
        });
    }
    
    
    func updateHeight(healthManager : HealthManager)
    {
        // 1. Construct an HKSampleType for Height
        var height : HKQuantitySample?
        let sampleType = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.height)
        
        // 2. Call the method to read the most recent Height sample
        healthManager.readMostRecentSample(sampleType: sampleType!, completion: { (mostRecentHeight, error) -> Void in
            
            if( error != nil )
            {
                print("Error reading height from HealthKit Store: \(error?.localizedDescription)")
                return;
            }
            
            
            //      var heightLocalizedString = self.kUnknownString;
            height = mostRecentHeight as? HKQuantitySample;
            
            //      // 3. Format the height to display it on the screen
            //      if let meters = self.height?.quantity.doubleValueForUnit(HKUnit.meterUnit()) {
            //        let heightFormatter = NSLengthFormatter()
            //        heightFormatter.forPersonHeightUse = true;
            //        heightLocalizedString = heightFormatter.stringFromMeters(meters);
            //      }
            
            
            // 4. Update UI. HealthKit use an internal queue. We make sure that we interact with the UI in the main thread
            DispatchQueue.main.async {
                
                //self.heightLabel.text = heightLocalizedString
                
                if height != nil
                {
                    let cadenaRuido = String(describing: height!)
                    let extraerValor : NSString = cadenaRuido.components(separatedBy: " ")[0] as NSString
                    
                    let objeto : ObjetoDatosHealthKit = ObjetoDatosHealthKit.getDatos().object(at: 0) as! ObjetoDatosHealthKit
                    objeto.altura = extraerValor as String
                    //print(extraerValor)
                }
                
                //self.updateBMI()
            }
        })
        
    }
    
    
    func updateIMC(healthManager : HealthManager)
    {
        var imc : HKQuantitySample?
        let sampleType = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMassIndex)
        
        healthManager.readMostRecentSample(sampleType: sampleType!, completion: { (mostRecentEnergy, error) -> Void in
            contadorDatosHealthKit += 1
            if( error != nil )
            {
                print("Error reading weight from HealthKit Store: \(error?.localizedDescription)")
                return;
            }
            
            //var frecuenciaCorazonLocalizedString = self.kUnknownString;
            imc = mostRecentEnergy as? HKQuantitySample;
            
            
            //print(imc)
            if imc != nil
            {
                DispatchQueue.main.async {
                    
                    let objeto : ObjetoDatosHealthKit = ObjetoDatosHealthKit.getDatos().object(at: 0) as! ObjetoDatosHealthKit
                    
                    
                    let cadenaRuido = String(describing: imc!)
                    let extraerValor : NSString = cadenaRuido.components(separatedBy: " ")[0] as NSString
                    
                    
                    objeto.imc = (extraerValor) as String
                    //print(extraerValor)
                    
                }
            }
        });
        
    }
    
    
    func updateContadorPasos(healthManager : HealthManager)
    {
        print("si esta llamando al contador de passos")
        var contadorPasos : HKQuantitySample?
        let sampleType = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)
        
        healthManager.readMostRecentSample(sampleType: sampleType!, completion: { (mostRecentEnergy, error) -> Void in
            contadorDatosHealthKit += 1
            if( error != nil )
            {
                print("Error reading weight from HealthKit Store: \(error?.localizedDescription)")
                return;
            }
            
            //var frecuenciaCorazonLocalizedString = self.kUnknownString;
            contadorPasos = mostRecentEnergy as? HKQuantitySample;
            
            print("los pasos que ha dado \(contadorPasos)")
            //print(contadorPasos)
            if contadorPasos != nil
            {
                DispatchQueue.main.async {
                    
                    let objeto : ObjetoDatosHealthKit = ObjetoDatosHealthKit.getDatos().object(at: 0) as! ObjetoDatosHealthKit
                    
                    
                    let cadenaRuido = String(describing: contadorPasos!)
                    let extraerValor : NSString = cadenaRuido.components(separatedBy: " ")[0] as NSString
                    
                    
                    objeto.contadorDePasos = (extraerValor) as String
                    
                }
            }
        });
        
    }
    
    
    func updateDistanciaRecorrida(healthManager : HealthManager)
    {
        var distanciaRecorrida : HKQuantitySample?
        let sampleType = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.distanceWalkingRunning)
        
        healthManager.readMostRecentSample(sampleType: sampleType!, completion: {
            (mostRecentEnergy, error) -> Void in
            contadorDatosHealthKit += 1
            if( error != nil )
            {
                print("Error reading weight from HealthKit Store: \(error?.localizedDescription)")
                return;
            }
            
            //var frecuenciaCorazonLocalizedString = self.kUnknownString;
            distanciaRecorrida = mostRecentEnergy as? HKQuantitySample;
            
            
            //print(distanciaRecorrida)
            if distanciaRecorrida != nil
            {
                DispatchQueue.main.async{
                    let objeto : ObjetoDatosHealthKit = ObjetoDatosHealthKit.getDatos().object(at: 0) as! ObjetoDatosHealthKit
                    
                    
                    let cadenaRuido = String(describing: distanciaRecorrida!)
                    let extraerValor : NSString = cadenaRuido.components(separatedBy: " ")[0] as NSString
                    
                    
                    objeto.distanciaRecorrida = (extraerValor) as String
                    
                }
            }
        });
    }
    
    func updateEnergiaQuemadaReposo(healthManager : HealthManager)
    {
        var EnergiaQuemada : HKQuantitySample?
        let sampleType = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.basalEnergyBurned)
        
        healthManager.readMostRecentSample(sampleType: sampleType!, completion: { (mostRecentEnergy, error) -> Void in
            contadorDatosHealthKit += 1
            if( error != nil )
            {
                print("Error reading weight from HealthKit Store: \(error?.localizedDescription)")
                return;
            }
            
            //var frecuenciaCorazonLocalizedString = self.kUnknownString;
            EnergiaQuemada = mostRecentEnergy as? HKQuantitySample;
            
            
            //print(EnergiaQuemada)
            DispatchQueue.main.async{
                if EnergiaQuemada != nil
                {
                    let objeto : ObjetoDatosHealthKit = ObjetoDatosHealthKit.getDatos().object(at: 0) as! ObjetoDatosHealthKit
                    
                    let cadenaRuido = String(describing: EnergiaQuemada!)
                    let extraerValor : NSString = cadenaRuido.components(separatedBy: " ")[0] as NSString
                    
                    objeto.energiaQuemadaReposo = (extraerValor) as String
                }
                
            }
        });
    }
    
    
    func updateEnergiaQuemadaActiva(healthManager : HealthManager)
    {
        var EnergiaQuemada : HKQuantitySample?
        let sampleType = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.activeEnergyBurned)
        
        healthManager.readMostRecentSample(sampleType: sampleType!, completion: { (mostRecentEnergy, error) -> Void in
            contadorDatosHealthKit += 1
            if( error != nil )
            {
                print("Error reading weight from HealthKit Store: \(error?.localizedDescription)")
                return;
            }
            
            //var frecuenciaCorazonLocalizedString = self.kUnknownString;
            EnergiaQuemada = mostRecentEnergy as? HKQuantitySample;
            
            
            //print(EnergiaQuemada)
            //dispatch_async(dispatch_get_main_queue(), { () -> Void in
            DispatchQueue.main.async(execute: {
                if EnergiaQuemada != nil
                {
                    let objeto : ObjetoDatosHealthKit = ObjetoDatosHealthKit.getDatos().object(at: 0) as! ObjetoDatosHealthKit
                    
                    let cadenaRuido = String(describing: EnergiaQuemada!)
                    let extraerValor : NSString = cadenaRuido.components(separatedBy: " ")[0] as NSString
                    
                    objeto.energiaQuemadaActiva = (extraerValor) as String
                }
                
            });
        });
    }
    
    func updateHeartRate(healthManager : HealthManager)
    {
        var heartRate : HKQuantitySample?
        let sampleType = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)
        
        healthManager.readMostRecentSample(sampleType: sampleType!, completion: { (mostRecentRate, error) -> Void in
            contadorDatosHealthKit += 1
            if( error != nil )
            {
                print("Error reading weight from HealthKit Store: \(error?.localizedDescription)")
                return;
            }
            
            //var frecuenciaCorazonLocalizedString = self.kUnknownString;
            heartRate = mostRecentRate as? HKQuantitySample;
            
            
            //print(heartRate)
            DispatchQueue.main.async(execute: {
                if heartRate != nil
                {
                    let objeto : ObjetoDatosHealthKit = ObjetoDatosHealthKit.getDatos().object(at: 0) as! ObjetoDatosHealthKit
                    let cadenaRuido = String(describing: heartRate!)
                    let extraerValor : NSString = cadenaRuido.components(separatedBy: " ")[0] as NSString
                    
                    objeto.frecuenciaCardiaca = (extraerValor) as String
                    //print(String(heartRate!))
                }
            });
        });
    }
    
}

extension NSString // Extension, mandar datos de HealthKit por POST
{
    func sendJSONHealthKit(arregloDatos: NSMutableArray )
    {
        
        print("arreglo de datos")
        //print(arregloDatos)
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://miveris.com.ec/WebServiceFit/servicio/indicadores/ingresarmuestratodas")! as URL)
        let str = "wsappusuario:ZA$57@9b86@$2r5" //AUTENTICACION
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        request.httpMethod = "POST"
        
        let utf8str = str.data(using: String.Encoding.utf8)
        
        //let base64Encoded = str.data(using: String.Encoding.utf8, allowLossyConversion: true)
        
        //if let base64Encoded = utf8str?.base64EncodedStringWithOptions(NSData.Base64EncodingOptions(rawValue: 0)) {
        if let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0)) {
            print("Encoded:  \(base64Encoded)")
            //request.addValue("Authorization", forHTTPHeaderField: "Basic \(base64Encoded)")
            request.setValue("Basic \(base64Encoded)", forHTTPHeaderField: "Authorization")
            if let base64Decoded = NSData(base64Encoded: base64Encoded, options:   NSData.Base64DecodingOptions(rawValue: 0))
                .map({ NSString(data: $0 as Data, encoding: String.Encoding.utf8.rawValue) })
            {
                // Convert back to a string
                print("Decoded:  \(base64Decoded)")
            }
        }
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        var indicadores: [String: String] = [:]
        indicadores["10"] = "1"
        
        let device = "IOS"
        
        let identificacion: String? = UserDefaults.standard.object(forKey: "identificacion") as? String
        print(identificacion)
        
        let nombre3: String? = UserDefaults.standard.object(forKey: "nombre") as? String
        
        let modelName : String = UIDevice.current.modelName
        print(modelName)
        
        //let nombreHTML = nombre3!.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLHostAllowedCharacterSet())
        let nombreHTML = nombre3?.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlHostAllowed)
        print("escapedString: \(nombreHTML)")
        
        let payload : [String : AnyObject] = ["nombre" : nombreHTML! as AnyObject, "cedula" : identificacion! as AnyObject,"dispositivo" : device as AnyObject,"versiondispositivo" : modelName as AnyObject,"indicadores" : arregloDatos]
        //let any : [AnyObject]  = payload
        
        let body = try! JSONSerialization.data(withJSONObject: payload, options: [])
        request.httpBody = body
        
        
        print("\(request)?\(payload)")
        
        //request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                
                //dispatch_async(dispatch_get_main_queue(), {
                DispatchQueue.main.async(execute: {
                    //self.controldecarga.hideActivityIndicator(self.view)
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    
                })
                //self.refreshControl?.endRefreshing()
                print("error=\(error)")
                
                return
            }
            print("response = \(response)")
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(responseString)")
            
            let nsdata:NSData = NSData(data: data!)
            let jsonCompleto : AnyObject! = try? JSONSerialization.jsonObject(with: nsdata as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject!
            
            
            if jsonCompleto != nil
            {
                if let items = jsonCompleto["resultado"] as? NSArray
                {
                    //                    for item in items
                    //                    {
                    //                        if let address = item["res"] as? String {
                    //                            //apellidos =  item["apellidos"] as! String
                    //                        }
                    //
                    //                    }
                }
                
                
            }
            //dispatch_async(dispatch_get_main_queue(), {
            DispatchQueue.main.async(execute: {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                //self.controldecarga.hideActivityIndicator(self.view)
            })
        }
        task.resume()
    }
    
    func remplazarVaciosPorCero()
    {
        let objeto = ObjetoDatosHealthKit.getDatos().object(at: 0) as! ObjetoDatosHealthKit
        
        if objeto.altura == ""
        {
            objeto.altura = "0"
        }
        if objeto.peso == ""
        {
            objeto.peso = "0"
        }
        if objeto.contadorDePasos == ""
        {
            objeto.contadorDePasos = "0"
        }
        if objeto.distanciaRecorrida == ""
        {
            objeto.distanciaRecorrida = "0"
        }
        if objeto.frecuenciaCardiaca == ""
        {
            objeto.frecuenciaCardiaca = "0"
        }
        if objeto.energiaQuemadaActiva == ""
        {
            objeto.energiaQuemadaActiva = "0"
        }
        if objeto.energiaQuemadaReposo == ""
        {
            objeto.energiaQuemadaReposo = "0"
        }
        if objeto.fechaDeNacimiento == ""
        {
            objeto.fechaDeNacimiento = "0"
        }
        if objeto.imc == ""
        {
            objeto.imc = "0"
        }
        if objeto.sexo == ""
        {
            objeto.sexo = "0"
        }
        if objeto.sangre == ""
        {
            objeto.sangre = "0"
        }
        
    }
    
    
}
