//
//  HistorialViewController.swift
//  veris
//
//  Created by Felipe Lloret on 06/02/15.
//  Copyright (c) 2015 Felipe Lloret. All rights reserved.
//

import UIKit

class HistorialViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var historial = [HistorialCitasModel]()
    var tableView : UITableView = UITableView()
    var setReload : Bool = false
    var servact: String = URLFactory.urldefinida()
    
    var espe_gl : ObjEspecialidad = ObjEspecialidad()
    
    var establ_gl : ObjEstablecimiento = ObjEstablecimiento()
    var servicio : ObjEspecialidad = ObjEspecialidad()
    var medico_gl : String = ""
    var codigo_medico : Int = 0
    var sgm_forma : UISegmentedControl = UISegmentedControl()
    var identificacion : String = ""
    
    var tipo_id_p : String = ""
    var identificacion_p : String  = ""
    var verif : String = ""

    var numeroOrden : String = ""
    var lineaDetalle : String = ""
    var codigoempresa : String = ""
    
    override func viewDidAppear(_ animated: Bool) {
        let usuario = Usuario.getEntity as Usuario
        Usuario.citasButtonFamilyTop(controlador: self, text: usuario.nombre as String)
        
        if setReload {
            self.setReload = false
            self.historial = [HistorialCitasModel]()
            //self.servact = servdesa
            
            tipo_id_p = usuario.tipoid as String
            identificacion = usuario.numeroidentificacion as String

            let urlAsString = "\(self.servact)servicio/v2/agenda/citasatendidaspagin2?arg0=\(tipo_id_p)&arg1=\(identificacion)&arg2=0&arg3=10&arg4=\(verif)&arg5=\(identificacion_p)"
            
            self.getJSONData(url: urlAsString)
        }
       // Usuario.getDefaultUser()
        
        //let us = Usuario.getEntity as Usuario
        
        let log = UserDefaults.standard.object(forKey: "identificacion")
        let t_log = UserDefaults.standard.object(forKey: "tipo")
        
        let iden = "\(t_log!)-\(log!)"
        
        //let iden = "\(tipo_id_p)-\(identificacion)"
        let urldef = URLFactory.urldefinida()

        let url = NSURL(string: "\(urldef)servicio/persona/grupo/\(iden)?arg0=2")
        let request = NSMutableURLRequest(url: url! as URL)
        var resultado = ""
       // var bool  = false
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            if (error != nil) {
                print(error!.localizedDescription)
                resultado = "N"
            } else {
                do{
                    var err: NSError?
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    if (err != nil) {
                        print("JSON Error \(err!.localizedDescription)")
                    }
                    
                    let info =  jsonResult as NSDictionary
                    let data = info["lista"] as! [[String: Any]]//NSArray
                    // print(data)
                    if data.count > 0 {
                        for familia in data {
                            if let res = familia["numeroIdentificacion"] as? String {
                                if res == self.identificacion as String {
                                    resultado = (familia["esAdmin"] as? String)!
                                }
                            }
                        }
                    }
                    DispatchQueue.main.async(execute: {
                        
                        let urlAsString = "\(self.servact)servicio/v2/agenda/citasatendidaspagin2?arg0=\(self.tipo_id_p)&arg1=\(self.identificacion)&arg2=0&arg3=10&arg4=\(resultado)&arg5=\(self.identificacion_p)"
                        self.getJSONData(url: urlAsString)
                        
                    })
                } catch {
                    print("ERROR")
                }
            }
        }
        ViewUtilidades.hiddenLoader(controller: self)
        
    }
    
    override func viewDidLoad() {
        
        let idid = UserDefaults.standard.object(forKey: "identificacion")
        identificacion_p = idid as! String
        super.viewDidLoad()
        
        self.navigationItem.title = ""
        
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        let fondo : UIImageView = UIImageView(frame: self.view.bounds)
        fondo.image = UIImage(named:"fondocita.png")
        self.view.addSubview(fondo)
        
        let barWhite : UIView = UIView(frame: CGRect(x:0, y:64, width:self.view.frame.width, height:30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x:0, y:64, width:self.view.frame.width / 1.8, height:30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x:barBlue.frame.width, y:64, width:45, height: 30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x:0, y:64, width:self.view.frame.width - 20, height:30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "Mis Citas"
       
        let items = ["Mis Citas", "Historial de Citas"]
        sgm_forma = UISegmentedControl(items: items)
        sgm_forma.selectedSegmentIndex = 1
        sgm_forma.tintColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        sgm_forma.frame = CGRect(x: 0, y: 145,width: self.view.frame.width, height: 38)
        sgm_forma.addTarget(self, action: #selector(HistorialViewController.verCitas), for: .valueChanged);
        sgm_forma.layer.cornerRadius = 0.0;
        sgm_forma.layer.borderColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1).cgColor
        sgm_forma.layer.borderWidth = 1.5;
        sgm_forma.layer.masksToBounds = true;
        
        
        self.view.addSubview(sgm_forma)

        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)
        
        let usuario = Usuario.getEntity as Usuario
        
        let nombreUser : UILabel = UILabel(frame: CGRect(x:10, y:94, width:self.view.frame.width - 20, height:30))
        nombreUser.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        nombreUser.font = UIFont(name: "helvetica-bold", size: 12)
        nombreUser.text = usuario.nombre as String
        

        tipo_id_p = usuario.tipoid as String
        identificacion = usuario.numeroidentificacion as String
        
        verif = Usuario.verificarEsadmin(identificacion: identificacion)
        
        let us = Usuario.getEntity as Usuario
        
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        
        let log = UserDefaults.standard.object(forKey: "identificacion")
        let t_log = UserDefaults.standard.object(forKey: "tipo")
        
        let iden = "\(t_log!)-\(log!)"
        
        //let iden = "\(tipo_id_p)-\(identificacion)"
        let urldef = URLFactory.urldefinida()

        let url = NSURL(string: "\(urldef)servicio/persona/grupo/\(iden)?arg0=2")
        let request = NSMutableURLRequest(url: url! as URL)
        var resultado = ""
        //var bool  = false
        //Verificar si es admin
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            if (error != nil) {
                print(error!.localizedDescription)
                resultado = "N"
            } else {
                do{
                    var err: NSError?
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    if (err != nil) {
                        print("JSON Error \(err!.localizedDescription)")
                    }
                    
                    let info =  jsonResult as NSDictionary
                    let data = info["lista"] as! [[String: Any]]//NSArray
                     print(data)
                    if data.count > 0 {
                        for familia in data {
                            if let res = familia["numeroIdentificacion"] as? String {
                                if res == us.numeroidentificacion as String {
                                    resultado = (familia["esAdmin"] as? String)!
                                    
                                }
                            }
                        }
                    }
                    DispatchQueue.main.async(execute: {
                        print("holis \(resultado)")
                        
                        let urlAsString = "\(self.servact)servicio/v2/agenda/citasatendidaspagin2?arg0=\(self.tipo_id_p)&arg1=\(self.identificacion)&arg2=0&arg3=10&arg4=\(resultado)&arg5=\(self.identificacion_p)"
                        print("esta url \(urlAsString)")
                        
                        self.getJSONData(url: urlAsString)

                        
                        
                        
                    })
                } catch {
                    print("ERROR")
                }
            }
            
            
        }
    }
    
    func getFamilyService(){
        Usuario.getJsonFamilia(controlador: self)
        setReload = true
    }

    func getJSONData(url: String) {
        
        let url = NSURL(string: url as String)
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        let str = "wsappusuario:ZA$57@9b86@$2r5"
        
        _ = str.data(using: String.Encoding.utf8)
        
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                let json = JSON(data: data!)
                
                //Comprobamos si todo ha salido bien, en caso contrario se le muestra el mensaje de error al usuario
                if json["resultado"].string == "ok" {
                    self.historial.removeAll()
                    if let historialArray = json["lista"].array {
                        print(json)
                        //Creamos nuestro objeto Citas con los datos obtenidos del JSON
                        for historialDic in historialArray {
                            let historialFecha: String? = historialDic["fecha"].stringValue
                            let historialSucursal: String? = historialDic["nombresucursal"].stringValue
                            let historialPrestacion: String? = historialDic["prestacion"].stringValue
                            let historialMedico: String? = historialDic["medico"].stringValue
                            let historialPuntuacion: String? = historialDic["puntaje"].stringValue
                            let historialReserva: String? = historialDic["codigoreserva"].stringValue
                            let hcodigoespecialidad : String? = historialDic["codigoespecialidad"].stringValue
                            let hcodigomedico : String? = historialDic["codigomedico"].stringValue
                            let hcodigoempresa : String? = historialDic["codigoempresa"].stringValue
                            let hcodigosucursal : String? = historialDic["codigosucursal"].stringValue
                            let hcodigoregion : String? = historialDic["codigoregion"].stringValue
                            
                            let hcodigoservicio : String? = historialDic["puntaje"].stringValue
                            let _ : String?  = historialDic["medico"].stringValue
                            
                            let _ :String? = historialDic["codigoprestacion"].stringValue
                            let _ :String? = historialDic["codigoempresaprestacion"].stringValue
                            let hnombreprestacion : String? = historialDic["prestacion"].stringValue
                            
                            //print("\(hcodigoprestacion) : \(hnombreservicio) : \(hnombreprestacion)")
                            let hist = HistorialCitasModel(fecha: historialFecha, nombresucursal: historialSucursal, prestacion: historialPrestacion, medico: historialMedico, puntuacion: historialPuntuacion, reserva: historialReserva, codigoespecialidad: hcodigoespecialidad, codigomedico: hcodigomedico, codigoempresa: hcodigoempresa, codigosucursal: hcodigosucursal, codigoregion : hcodigoregion, codigoservicio: hcodigoservicio, nombreservicio: hnombreprestacion)
                            
                            
                            self.historial.append(hist)
                        }
                         ViewUtilidades.hiddenLoader(controller: self)
                        
                    }
                } else {
                    let jsonError = json["resultado"].string
                    print(jsonError!)
                }
                
                //dispatch_async(dispatch_get_main_queue(), {
                DispatchQueue.main.async(execute: {
                    self.mostrarHistorial()
                    self.tableView.reloadData()
                    ViewUtilidades.hiddenLoader(controller: self)
                })
            }
            
            
        }
        
        
    }
    
    func mostrarHistorial() {
        self.tableView.frame = CGRect(x:0, y:190.0, width:self.view.frame.width, height:self.view.frame.height - 190);
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.separatorColor = UIColor.clear
        self.tableView.register(HistorialTableViewCell.self as AnyClass, forCellReuseIdentifier: "cell");
        
        self.view.addSubview(self.tableView);
    }
    
    func verCitas() {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }

    func puntuacionCita(sender: UIButton) {
       self.modalPresentationStyle = UIModalPresentationStyle.currentContext
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let calificarCitaViewController : CalificarCitaViewController = storyboard.instantiateViewController(withIdentifier: "viewCalificar") as! CalificarCitaViewController

        calificarCitaViewController.codigoRecetaString = self.historial[sender.tag].reserva
        calificarCitaViewController.puntuacionString = self.historial[sender.tag].puntuacion
        
        self.setReload = true
        
        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController?.present(calificarCitaViewController, animated: true, completion: nil)
    }
    
    //TableViewDataSource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.historial.count
    }
    
    
    //Devuelve número de filas en la sección
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
            headerView.backgroundColor = UIColor.clear
            return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! HistorialTableViewCell
            
            //Hacemos que las celdas no tenga estilo al ser seleccionadas
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.puntuarCitaButton.tag = indexPath.section
        cell.puntuarCitaButton.addTarget(self, action: #selector(puntuacionCita(sender:)), for: UIControlEvents.touchUpInside)
            
            let dr = "Dr(a) "
            let medicoString = dr + self.historial[indexPath.section].medico
            
            if (self.historial[indexPath.section].puntuacion != "0") {
                switch self.historial[indexPath.section].puntuacion {
                case "1", "2", "3", "4", "5", "6":
                    cell.setCell(sucursalLabelText: self.historial[indexPath.section].nombresucursal,
                        fechaLabelText: self.historial[indexPath.section].fecha,
                        doctorLabelText: medicoString,
                        prestacionLabelText:self.historial[indexPath.section].prestacion,
                        calificarCitaButtonImage: UIImage(named: "Votacion0.png"))
                case "7", "8":
                    cell.setCell(sucursalLabelText: self.historial[indexPath.section].nombresucursal,
                        fechaLabelText: self.historial[indexPath.section].fecha,
                        doctorLabelText: medicoString,
                        prestacionLabelText:self.historial[indexPath.section].prestacion,
                        calificarCitaButtonImage: UIImage(named: "Votacion1.png"))
                case "9":
                    cell.setCell(sucursalLabelText: self.historial[indexPath.section].nombresucursal,
                        fechaLabelText: self.historial[indexPath.section].fecha,
                        doctorLabelText: medicoString,
                        prestacionLabelText:self.historial[indexPath.section].prestacion,
                        calificarCitaButtonImage: UIImage(named: "Votacion2.png"))
                default:
                    cell.setCell(sucursalLabelText: self.historial[indexPath.section].nombresucursal,
                        fechaLabelText: self.historial[indexPath.section].fecha,
                        doctorLabelText: medicoString,
                        prestacionLabelText:self.historial[indexPath.section].prestacion,
                        calificarCitaButtonImage: UIImage(named: "Votacion3.png"))
                }
                
                //Mostramos la imagen y escondemos el texto
                cell.puntuarCitaButton.isHidden = true
                cell.calificarImageView.isHidden = false
            } else {
                cell.setCell(sucursalLabelText: self.historial[indexPath.section].nombresucursal,
                    fechaLabelText: self.historial[indexPath.section].fecha,
                    doctorLabelText: medicoString,
                    prestacionLabelText:self.historial[indexPath.section].prestacion,
                    calificarCitaButtonImage: nil)
                
                //Mostramos el texto y escondemos la imagen
                cell.puntuarCitaButton.isHidden = false
                cell.calificarImageView.isHidden = true
            }
            /**/
            cell.nuevaCitaButton.tag = indexPath.section
        cell.nuevaCitaButton.addTarget(self, action: #selector(reutilizarcita(sender:)), for: UIControlEvents.touchUpInside)
            return cell
    }
    func reutilizarcita(sender : UIButton){
        let hcita : HistorialCitasModel = self.historial[sender.tag]
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "viewReservar") as! ReservaViewController
        
        vc.vienedemiscitas = "si"
        _ = Usuario.getEntity as Usuario
        let espe : ObjEspecialidad = ObjEspecialidad()
        let establ : ObjEstablecimiento = ObjEstablecimiento()
        
        //enviar servicio
        let servicio : ObjEspecialidad = ObjEspecialidad()
        servicio.codigo = Int(hcita.codigoservicio as String)! 
        servicio.descripcion = hcita.nombreservicio
        
        var texto = hcita.prestacion.characters.split{$0 == "/"}.map { String($0) }
        espe.descripcion = texto[0].trimmingCharacters(in: NSCharacterSet.whitespaces)
        espe.codigo = (hcita.codigoespecialidad as NSString).integerValue
        espe_gl = espe
        //  var txt_especialidad = hcita.description
        self.obtenerServicios(value: espe.codigo)
        

        establ.id = (hcita.codigosucursal as NSString).integerValue
        establ.idempresa = (hcita.codigoempresa as NSString).integerValue
        establ.descripcion = hcita.nombresucursal as NSString
        establ.codigoregion = (hcita.codigoregion as NSString).integerValue
 
        medico_gl = "Dr(a) "+hcita.medico
        codigo_medico = (hcita.codigomedico as NSString).integerValue
       // print("hola : \(hcita.nombreprestacion) y codio : \(hcita.codigoprestacion)")
        establ_gl = establ
        
    }
    
    func obtenerServicios(value : Int){
        _ = ObjEspecialidad()
        let us = Usuario.getEntity as Usuario
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        let urldef = URLFactory.urldefinida()

        let url2 = "\(urldef)servicio/v2/agenda/consultarservicios?arg0=\(value)&arg1=\(us.tipoid)&arg2=\(us.numeroidentificacion)&arg3=\(us.numeroidentificacion)"
            
            
        let url = NSURL(string: url2)
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
            
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                
                if (error != nil) {
                    print(error!.localizedDescription)
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    do{
                        print("llego")
                        var err: NSError?
                        
                        let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        if (err != nil) {
                            print("JSON Error \(err!.localizedDescription)")
                        }
                        
                        print(jsonResult)
                        let info =  jsonResult as NSDictionary
                        let data = info["lista"] as! [[String: Any]]

                        
                        if data.count > 0 {
                            for jsonobj  in data {
                                print("\(jsonobj)")
                                let esp = ObjEspecialidad()
                                esp.codigo = (jsonobj["codigoPrestacion"] as! NSString).integerValue
                                esp.descripcion = jsonobj["nombrePrestacion"] as! String
                                self.servicio = esp
                                print(jsonobj["lineaDetalle"]!)
                                print(jsonobj["codigoPrestacion"]!)
                                print(" hola ho la hola empresa \(jsonobj["codigoEmpresa"]!)")
                                
                                if let ver = jsonobj["numeroOrden"] as? String {
                                    self.numeroOrden = ver
                                    self.lineaDetalle = jsonobj["lineaDetalle"] as! String
                                    self.codigoempresa = jsonobj["codigoEmpresa"]! as! String
                                }else{
                                    self.numeroOrden = "0"
                                    self.lineaDetalle = "0"
                                    self.codigoempresa = "0"
                                }
                                
                            }
                        }else{
                            let espnull = ObjEspecialidad()
                            self.servicio = espnull
                             return
                        }
                        //dispatch_async(dispatch_get_main_queue(), {
                        DispatchQueue.main.async(execute: {
                            ViewUtilidades.hiddenLoader(controller: self)
                            self.goto()
                            
                        })
                    } catch {
                        print("ERROR")
                        ViewUtilidades.hiddenLoader(controller: self)
                        let alert = UIAlertController(title: "Veris", message: "Revise su conexión a Datos o Internet", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
        }
    }
    func goto(){
        //let hcita : HistorialCitasModel = self.historial[sender.tag]
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "viewReservar") as! ReservaViewController
        
        //let establ : ObjEstablecimiento = ObjEstablecimiento()
        
        /*establ_gl.id = (hcita.codigosucursal as NSString).integerValue
        establ.idempresa = (hcita.codigoempresa as NSString).integerValue
        establ.descripcion = hcita.nombresucursal as NSString
        establ.codigoregion = (hcita.codigoregion as NSString).integerValue
        */
       // let servicio : ObjEspecialidad = ObjEspecialidad()
        servicio.codigo = self.servicio.codigo//Int(hcita.codigoservicio as String)!
        servicio.descripcion = self.servicio.descripcion

        vc.serviciosel = servicio
        vc.vienedemiscitas = "si" 
        //vc.codigoempresaprestacion = (self.servicio.codigo) as NSString
        vc.nombreprestacion = self.servicio.descripcion as NSString
        vc.tipodecarga=TipoCargaReserva.reutilizarcita
        vc.especialidadsel = espe_gl
        vc.establecimientosel = establ_gl
        vc.nombredoctorsel = "Doctor"//medico_gl
        //vc.codigodoctorsel = //codigo_medico
        vc.numeroOrden = self.numeroOrden
        vc.lineaDetalle = self.lineaDetalle
        vc.codigoempresa = self.codigoempresa

        
        navigationController?.pushViewController(vc, animated: true)
    }
//    func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!) {
//        if (self.historial[indexPath.section].puntuacion == "0") {
//            self.modalPresentationStyle = UIModalPresentationStyle.CurrentContext
//            
//            var calificarCitaViewController: CalificarCitaViewController = self.storyboard?.instantiateViewControllerWithIdentifier("viewCalificar") as CalificarCitaViewController
//            
//            calificarCitaViewController.codigoRecetaString = self.historial[indexPath.section].reserva
//            calificarCitaViewController.puntuacionString = self.historial[indexPath.section].puntuacion
//            
//            self.presentViewController(calificarCitaViewController, animated: true, completion: nil)
//        }
//    }
}
