//
//  Objdisponibilidad.swift
//  Mi Veris
//
//  Created by Andres Cantos on 2/8/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//
import UIKit

// MARK: Region: Clase región

class Objdisponibilidad: NSObject {
    
    var codigomedico : NSInteger = 0
    var nombredoctor : String = ""
    var horainicio : String = ""
    var horafin : String = ""
    var duracion : String = ""
    var diasemana : String = ""
    var idsintervalos : String = ""
    
    
    init(data : NSDictionary) {
        codigomedico = data["codigomedico"] as! NSInteger
        nombredoctor = data["nombredoctor"] as! NSString as String
        horainicio = data["horainicio"] as! NSString as String
        horafin = data["horafin"] as! NSString as String
        duracion = data["duracion"] as! NSString as String
        diasemana = data["diasemana"] as! NSString as String
        idsintervalos = data["idsintervalos"] as! NSString as String
    }
}

