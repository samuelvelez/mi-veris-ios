//
//  Provincia.swift
//  Mi Veris
//
//  Created by Jorge on 29/04/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import UIKit

class Provincia: NSObject {
    var codigoPais : NSString = ""
    var codigoProvincia : NSString = ""
    var codigoRegion : NSString = ""
    var nombre : NSString = ""  
}
