//
//  ReservaDisponibilidadViewController.swift
//  Mi Veris
//
//  Created by Andres Cantos on 2/8/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import UIKit

class ReservaDisponibilidadViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    var tipodocusuario : String?
    var documentousuario : String?
    var tableView: UITableView  =   UITableView()
    var listadisponibildad : NSMutableArray = NSMutableArray()
    var iniciopagina : NSInteger = 0
    var tamaniopagina : NSInteger = 30
    var controlpagina : NSInteger = 0
    var dateFormatter = DateFormatter()
    var meses : [String] = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"]
    var mesesabrev : [String] = ["ENE","FEB","MAR","ABR","MAY","JUN","JUL","AGO","SEP","OCT","NOV","DIC"]
    var delegate : ReservaDisponibilidadDelegate! = nil
    var fechaconsulta : NSString = ""
    var btnfecha : UITextField!
    var btnbuscar : UIButton!
    var datePicker: UIDatePicker = UIDatePicker()
    
    var disponibilidadsel : Objdisponibilidad!
    var establecimientosel : ObjEstablecimiento!
    var idespecialidadsel : String = ""
    var codigomedico : NSInteger = 0
    var nombremedico : NSString = ""
    
    var codigoprestacion : NSString = "0"
    var codigoempresaprestacion : NSString = "0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dateFormatter.locale = NSLocale(localeIdentifier: "es_EC") as Locale!
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.s"
        
        setearfechaconsulta(fechad: NSDate())
        agregarbarratitulo()
        cargardisponibilidad(iniciopagin: iniciopagina,encerar: false)
    }
    
    func setearfechaconsulta(fechad : NSDate){
        let myCalendar : NSCalendar = NSCalendar.current as NSCalendar
        let myComponents = myCalendar.components([NSCalendar.Unit.year, NSCalendar.Unit.day, NSCalendar.Unit.month, NSCalendar.Unit.hour, NSCalendar.Unit.minute], from: fechad as Date)
        //fechaconsulta = String(format: "%02d", myComponents.day) + "/" + String(format: "%02d", myComponents.month) + "/\(myComponents.year)"
        var mifecha = String(format: "%02d", myComponents.day!)
        mifecha.append("/")
        mifecha.append(String(format: "%02d", myComponents.month!))
        mifecha.append("/")
        mifecha.append(String(format: "%02d", myComponents.year!))
        //fechaconsulta = String(format: "%02d", myComponents.day!) as NSString
        //fechaconsulta.appending("/")
        //fechaconsulta.appending((String(format: "%02d", myComponents.month!)))
        //fechaconsulta.appending("/")
        //fechaconsulta.appending((String(format: "%02d", myComponents.year!)))
        print(mifecha)
        fechaconsulta = mifecha as NSString
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func agregarbarratitulo(){
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        self.navigationController?.navigationItem.backBarButtonItem?.title = ""
        
        let barBlue : UIView = UIView(frame: CGRect(x:0, y:64, width:self.view.frame.width, height:30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let title : UILabel = UILabel(frame: CGRect(x:0, y:64, width:self.view.frame.width, height:30))
        title.textAlignment = NSTextAlignment.center
        title.textColor = UIColor.white
        title.font = UIFont(name: "helvetica-bold", size: 12)
        title.text = "Seleccione el Doctor(a)"
        
        self.view.addSubview(barBlue)
        self.view.addSubview(title)
        
        datePicker.addTarget(self, action: "updateDatePickerLabel", for: .valueChanged)
        self.datePicker.datePickerMode = UIDatePickerMode.date
        
        
        btnfecha = UITextField(frame: CGRect( x:0, y:94, width:self.view.frame.width - 41, height:41))
        btnfecha.backgroundColor = UtilidadesColor.colordehexadecimal(hex: 0x89c3e8, alpha: 1.0)
        //btnfecha.setTitle("A partir de: \(self.fechaconsulta)", forState: UIControlState.Normal)
        //btnfecha.addTarget(self, action: "seleccionarfecha", forControlEvents: UIControlEvents.TouchUpInside)
        
        btnfecha.text = "A partir de: \(self.fechaconsulta)"
        btnfecha.font = UIFont(name: "helvetica-bold", size: 13)
        btnfecha.textAlignment = NSTextAlignment.center
        //btnfecha.titleLabel!.font = UIFont(name: "helvetica-bold", size: 12)
        //btnfecha.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        btnfecha.layer.borderWidth = CGFloat(0.99)
        btnfecha.layer.borderColor = UtilidadesColor.colordehexadecimal(hex: 0x3498db, alpha: 1.0).cgColor
        btnfecha.inputView = datePicker
        btnfecha.delegate = self
        
        btnbuscar = UIButton(frame: CGRect(x:self.view.frame.width - 41, y: 94,width: 41, height:41))
        btnbuscar.backgroundColor = UtilidadesColor.colordehexadecimal(hex: 0x023145, alpha: 1.0)
        //btnbuscar.setTitle("A partir de: ", forState: UIControlState.Normal)
        btnbuscar.addTarget(self, action: "buscardisponibilidad", for: UIControlEvents.touchUpInside)
        btnbuscar.titleLabel!.font = UIFont(name: "helvetica-bold", size: 12)
        //btnbuscar.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        btnbuscar.setImage(UIImage(named: "search.png"), for: UIControlState.normal)
        
        //btnbuscar.addSubview(icono)
        self.view.addSubview(btnfecha)
        self.view.addSubview(btnbuscar)
        
        tableView.frame = CGRect(x:0, y:135, width:self.view.frame.width, height:self.view.frame.height - 135);
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.backgroundColor = UIColor.clear
        tableView.register(ReservaDisponibilidadTableViewCell.self as AnyClass, forCellReuseIdentifier: "disponibilidadcell");
        
        self.view.addSubview(tableView);
    }
    
    
    func updateDatePickerLabel() {
        setearfechaconsulta(fechad: datePicker.date as NSDate)
        btnfecha.text = "A partir de: \(self.fechaconsulta)"
    }
    func buscardisponibilidad(){
        self.controlpagina = 0
        self.iniciopagina = 0
        self.btnfecha.resignFirstResponder()
        cargardisponibilidad(iniciopagin: iniciopagina,encerar: true)
    }
    
    func cargardisponibilidad(iniciopagin : NSInteger, encerar : Bool){
        print("llamando --------------------------- cargardisponibilidad")
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        if(encerar){
            self.listadisponibildad.removeAllObjects()
        }
        let url2 = URLFactory.obtenerdisponibilidadpaginada(codigoregion: "\(establecimientosel.codigoregion)", codigoempresa: "\(establecimientosel.idempresa)",
            codigosucursal: "\(establecimientosel.id)", idespecialidad: idespecialidadsel, tipoidentificacion: tipodocusuario!,
            identificacion: documentousuario!,fecha: self.fechaconsulta as String, codigoempresaprestacion: self.codigoempresaprestacion as String, codigoprestacion: self.codigoprestacion as String, codigomedico: "\(codigomedico)", iniciopagina: "\(iniciopagin)", tamaniopagina: "\(tamaniopagina)")
        
        
        let url = NSURL(string: "\(url2)")
        //let request = NSURLRequest(URL: url!)
        print(url!)
        print("el tipo doc es: \(tipodocusuario), \(documentousuario)")
        let request = NSMutableURLRequest(url: url! as URL)
        
        let str = "wsappusuario:ZA$57@9b86@$2r5"
        
        /*let utf8str = str.dataUsingEncoding(NSUTF8StringEncoding)
        
        if let base64Encoded = utf8str?.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0)){
            
            print("Encoded:  \(base64Encoded)")
            //request.addValue("Authorization", forHTTPHeaderField: "Basic \(base64Encoded)")
            request.setValue("Basic \(base64Encoded)", forHTTPHeaderField: "Authorization")
            
            if let base64Decoded = NSData(base64EncodedString: base64Encoded, options:   NSDataBase64DecodingOptions(rawValue: 0))
                .map({ NSString(data: $0, encoding: NSUTF8StringEncoding) })
            {
                // Convert back to a string
                print("Decoded:  \(base64Decoded)")
            }
        }*/
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            if (error != nil) {
                print(error!.localizedDescription)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                var err: NSError?
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                if (err != nil) {
                    print("JSON Error \(err!.localizedDescription)")
                }
                let info =  jsonResult as NSDictionary
                let data = info["lista"] as! NSArray
                if data.count > 0 {
                    print("aeaeae ******************** ************************** *******************")
                    for jsonobj in data {
                        let di = Objdisponibilidad(data: jsonobj as! NSDictionary)
                        self.listadisponibildad.add(di)
                        
                    }
                    self.showdisponibilidad()
                    // ViewUtilidades.hiddenLoader(self)
                }else{
                    print("nodata: \(data.count)")
                }
                //dispatch_async(dispatch_get_main_queue(), {
                    DispatchQueue.main.async(execute: {
                    //self.showdisponibilidad()
                    ViewUtilidades.hiddenLoader(controller: self)
                })
                } catch {
                    print("ERROR")
                }
            }
            
        }
        
        ///////////////////////
        
    }
    
    func showdisponibilidad(){
        if(self.listadisponibildad.count == 0){
            UtilidadesGeneral.mensaje(mensaje: "No hay Disponibilidad para el \(self.nombremedico)\(self.fechaconsulta), por favor intente con otra sucursal")
        }
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listadisponibildad.count;
    }
    /*
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    cell.separatorInset = UIEdgeInsetsZero
    cell.layoutMargins = UIEdgeInsetsZero
    }*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "disponibilidadcell", for: indexPath) as! ReservaDisponibilidadTableViewCell
        
        let disp : Objdisponibilidad = listadisponibildad.object(at: indexPath.row) as! Objdisponibilidad
        
        let horainicio = dateFormatter.date(from: disp.horainicio)!

        /*NSDateFormatter * df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [df setTimeZone:[NSTimeZone systemTimeZone]];
        [df setFormatterBehavior:NSDateFormatterBehaviorDefault];
        
        NSDate *theDate = [df dateFromString:@"2011-04-12 10:00:00"];
        NSLog(@"date: %@", theDate);*/
        
        //if horainicio != nil {
            cell.doctor.text = disp.nombredoctor
            let myCalendar : NSCalendar = NSCalendar.current as NSCalendar
            let myComponents = myCalendar.components([NSCalendar.Unit.year, NSCalendar.Unit.day, NSCalendar.Unit.month, NSCalendar.Unit.hour, NSCalendar.Unit.minute], from: horainicio)
            
            cell.fecha.text = disp.diasemana + ", \(myComponents.day!) de \(meses[myComponents.month! - 1]) del \(myComponents.year!)"
            cell.duracion.text = String(format: "%02d", myComponents.hour!) + "H" + String(format: "%02d", myComponents.minute!) + " Duración: " + disp.duracion
            cell.izqnumdia.text = String(format: "%02d", myComponents.day!)
            cell.izqmes.text =   "\(mesesabrev[myComponents.month! - 1])"
            cell.izqhora.text = String(format: "%02d", myComponents.hour!) + "H" + String(format: "%02d", myComponents.minute!)
        //}
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        disponibilidadsel = listadisponibildad.object(at: indexPath.row) as? Objdisponibilidad
        delegate.retornareservadisponibilidad(controller: self)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        controlpagina = listadisponibildad.count-1
        print("tableview: willdiplaycell")
        print(indexPath.row)
        print(controlpagina)
        if indexPath.row == controlpagina {
            iniciopagina = controlpagina+1
            //println("nueva pagina: \(iniciopagina)")
            cargardisponibilidad(iniciopagin: iniciopagina, encerar : false)
        }else{
            ViewUtilidades.hiddenLoader(controller: self)
        }
        
    }
    
}

protocol ReservaDisponibilidadDelegate{
    func retornareservadisponibilidad(controller:ReservaDisponibilidadViewController)
}
