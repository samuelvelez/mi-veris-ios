//
//  FamiliaTableViewController.swift
//  
//
//  Created by Samuel Velez on 15/8/16.
//
//

import UIKit

class FamiliaTableViewController: UITableViewController {

    
    //var scroll : UIScrollView = UIScrollView()
    var items : [String] = ["Samuel Velez", "Kleber Velez"]
    var listaFamiliares : NSMutableArray?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        self.navigationController?.navigationItem.backBarButtonItem?.title = ""
        //self.viewController.navigationItem.backBarButtonItem.title = "Custom Title";
        
        let fondo : UIImageView = UIImageView(frame: self.view.bounds)
        fondo.image = UIImage(named:"fondocita.png")
        self.view.addSubview(fondo)
        
        
        let barWhite : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width , height: 30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width / 2.6, height: 30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x: barBlue.frame.width, y: 64, width:  45, height: 30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x: 0, y: 64, width: self.view.frame.width - 20, height: 30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "Mi Grupo"
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)
        
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        let us = Usuario.getEntity as Usuario
        print(us.numeroidentificacion)
        print(us.tipoid)
        let urldef = URLFactory.urldefinida()

        let url = NSURL(string: "\(urldef)servicio/persona/grupo/2-0919446328")
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        let str = "wsappusuario:ZA$57@9b86@$2r5"
        
        let utf8str = str.data(using: String.Encoding.utf8)
        
        let base64Encoded = str.data(using: String.Encoding.utf8, allowLossyConversion: true)
        //let base64Encoded = str.dataUsingEncoding(NSUTF8StringEncoding)!.base64EncodedStringWithOptions([])
        print("Encoded: \(base64Encoded)")
        let base64DecodedData = NSData(base64Encoded: base64Encoded!, options: [])
        //NSData(base64EncodedString: base64Encoded, options: [])!
        let base64DecodedString = String(data: base64DecodedData as! Data, encoding: String.Encoding.utf8)!
        print("Decoded: \(base64DecodedString)")
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                    var err: NSError?
                    
                    //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    if (err != nil) {
                        print("JSON Error \(err!.localizedDescription)")
                    }
                    
                   
                    print(jsonResult)
                    let info =  jsonResult as NSDictionary
                    let data = info["lista"] as! [[String: Any]]
                    if data.count > 0 {
                        print(data)
                        //self.listaRegiones = NSMutableArray();
                        for familiar in data {
                            let reg = Region()
                            reg.codigoCiudad = Int(familiar["tipoIdentifiacion"] as! String)!
                            reg.nombreCiudad = familiar["primerNombre"] as! String
                            self.listaFamiliares?.add(reg)
                            //println( region["nombreciudad"] )
 
                        }
                    }
                    
                    self.showFamiliares()
                    ViewUtilidades.hiddenLoader(controller: self)
                    
                } catch {
                    print("ERROR")
                }
            }
            
            
        }
        //////////////////////////
        
        
        
    
    }
    
    func showFamiliares(){
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    /*override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }*/

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return items.count
    }

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("MenuCell", forIndexPath: indexPath) as! CellTableViewCell

        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.icono.image = UIImage.init(named: "ic_room_3x.png")
        cell.title.text = items[indexPath.row]
        
        // Configure the cell...
        //cell.textLabel?.text = items[indexPath.row]
        //cell.layer.backgroundColor = UIColor.whiteColor().CGColor
        return cell
    }
    */
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
    //}
    //override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SucursalesCell", for: indexPath) as! DondeEstamosTableViewCell
        
        let sucursal = listaFamiliares!.object(at: indexPath.row) as! Sucursal
        cell.title.text = sucursal.descripcion
        //cell.title.sizeToFit()
        cell.title.adjustsFontSizeToFitWidth = true
        cell.descripcion.text = sucursal.direccion
        cell.descripcion.sizeToFit()
        cell.descripcion.adjustsFontSizeToFitWidth = true
        cell.descripcion.frame =  CGRect(x: 90, y: 10, width: self.view.frame.width - 130, height: cell.frame.height)
        
        let urldef = URLFactory.urldefinida()

        ImageLoader.sharedLoader.imageForUrl(urlString: "\(urldef)sucursal/\(sucursal.region)/\(sucursal.idSucursal).jpg", completionHandler:{(image: UIImage?, url: String) in
            
            cell.imagen.image = image
        })
        
        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
