//
//  MisSolicitudesViewController.swift
//  Mi Veris
//
//  Created by Jorge on 28/05/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import UIKit

class MisSolicitudesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var nombreUser : UILabel!
    var setReload : Bool = false
    
    var tabla : UITableView = UITableView()
    var listaSolicitudes : NSMutableArray = NSMutableArray()
    
    override func viewDidAppear(_ animated: Bool) {
        if setReload {
            self.setReload = false
            
            let usuario = Usuario.getEntity as Usuario
            if usuario.rol < 3 {
                tabla.removeFromSuperview()
                getSolicitudes()
            } else {
                listaSolicitudes.removeAllObjects()
                tabla.reloadData()
                UtilidadesGeneral.mensaje(mensaje: "No tiene permisos para utilizar esta opción")
            }
            
        }
        let user = Usuario.getEntity
        nombreUser.text = user.nombre as String
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationItem.backBarButtonItem?.title = ""
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        let fondo : UIImageView = UIImageView(frame: self.view.bounds)
        fondo.image = UIImage(named:"fondocita.png")
        self.view.addSubview(fondo)
        
        let barWhite : UIView = UIView(frame: CGRect(x:0, y: 64, width: self.view.frame.width, height:30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x:0, y:64, width:self.view.frame.width / 2.8, height:30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x:barBlue.frame.width, y:64, width:45, height:30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x:0, y:64, width:self.view.frame.width - 20, height:30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "Mis solicitudes"
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)
        
        let usuario = Usuario.getEntity as Usuario
        
        
        nombreUser = UILabel(frame: CGRect(x: 10, y: 94, width: self.view.frame.width - 20, height:30))
        nombreUser.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        nombreUser.font = UIFont(name: "helvetica-bold", size: 12)
        nombreUser.text = usuario.nombre as String
        
        self.view.addSubview(nombreUser)
        Usuario.createButtonFamily(controlador: self)
        
        getSolicitudes()
    }
    
    func showSolicitudes(){
        tabla = UITableView()
        tabla.frame = CGRect(x: 0, y: 130, width: self.view.frame.width, height: self.view.frame.height - 130);
        tabla.delegate = self;
        tabla.dataSource = self;
        tabla.backgroundColor = UIColor.clear
        tabla.separatorColor = UIColor.clear
        tabla.register(SolicitudTableViewCell.self as AnyClass, forCellReuseIdentifier: "cell");
        
        self.view.addSubview(self.tabla);
    }
    
    func getSolicitudes(){
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        let usuario = Usuario.getEntity
        let urldef = URLFactory.urldefinida()

        let url = NSURL(string: "\(urldef)servicio/doctor/obtenersolicitudeshc?arg0=\(usuario.numeroidentificacion)&arg1=\(usuario.tipoid)")
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
       
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                if (error != nil) {
                    print(error!.localizedDescription)
                }
                var err: NSError?
                
                //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                if (err != nil) {
                    print("JSON Error \(err!.localizedDescription)")
                }
                
                
                //println(jsonResult);
                print(jsonResult)
                let info =  jsonResult as NSDictionary
                    let data = info["lista"] as! [[String : Any]]
                if data.count > 0 {
                    for especialidad in data {
                        let objeto = Solicitud()
                        objeto.codigoSolicitud = especialidad["codigo_solicitud"] as! NSInteger
                        objeto.tipo = especialidad["tipo"] as! String as NSString
                        objeto.fechaSolicitud = especialidad["fecha_solicitud"] as! String as NSString
                        objeto.fechaCierre = especialidad["fecha_cierre"] as! String
                        objeto.motivo = especialidad["motivo"] as! String
                        objeto.fechaDesde = especialidad["fecha_desde"] as! String
                        objeto.fechaHasta = especialidad["fecha_hasta"] as! String
                        objeto.estado = especialidad["estado"] as! String
                        self.listaSolicitudes.add(objeto)
                    }
                }
                
                //dispatch_async(dispatch_get_main_queue(), {
                    DispatchQueue.main.async(execute: {
                    ViewUtilidades.hiddenLoader(controller: self)
                    self.showSolicitudes()
                })
                } catch {
                    print("ERROR")
                }
            }
            
            
            
        }
    }
    
    // MARK: Métodos de tableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaSolicitudes.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SolicitudTableViewCell
        
        let solicitud = listaSolicitudes.object(at: indexPath.row) as! Solicitud
        
        cell.sucursalLabel.text = "SOLICITUD: \(solicitud.tipo)"
        cell.sucursalLabel.adjustsFontSizeToFitWidth = true
        //Importante: Si tu sefuro médico no aparece en el cuadro superior, comunícate al 6009600
        cell.fechaLabel.text = "\(solicitud.estado)"

        let desde = solicitud.fechaDesde.components(separatedBy: " ")//componentsSeparatedByString(" ")
        let hasta = solicitud.fechaHasta.components(separatedBy: " ")
        
        
        cell.doctorLabel.text = "Fecha de Solicitud: \(solicitud.fechaSolicitud)"
        cell.prestacionLabel.text = "Desde: \(desde[0]) hasta: \(hasta[0])"
        cell.motivo.text = "Motivo: \(solicitud.motivo)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //var cell : UITableViewCell = tableView.cellForRow(at: indexPath as IndexPath)! as UITableViewCell
        tableView.deselectRow(at: indexPath, animated: true);
    }
    
    func getFamilyService(){
        Usuario.getJsonFamilia(controlador: self)
        setReload = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
