//
//  ViewController.swift
//  Mi Veris
//
//  Created by Jorge on 04/02/15.
//  Copyright (c) 2015 Firewall Soluciones All rights reserved.
//

import UIKit

// MARK: Configuraciçon de las celdas del menú

class CellTableViewCell: UITableViewCell {
    
        
    var icono : UIImageView = UIImageView()
    var separator : UIImageView = UIImageView();
    var title : UILabel = UILabel();
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
     
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.clear

        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width;
        icono.frame = CGRect(x: 20, y: 15, width:25, height: 25)
        
        
        title.frame = CGRect(x: 60, y: 0, width: self.frame.width, height: 60)
        title.font = UIFont(name: "helvetica", size: 16)
        title.textColor = UIColor(red: (48/256.0), green: (48/256.0), blue: (48/256.0), alpha: 1)
        
        
        let arrow = UIImageView(frame: CGRect(x: screenWidth-30, y: 17, width: 15, height:24))
        arrow.image = UIImage(named: "flecha.png")
        
        self.contentView.addSubview(icono)
        self.contentView.addSubview(title)
        self.contentView.addSubview(arrow)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    
}
