//
//  PickerList.swift
//  luxlite
//
//  Created by Jorge on 10/04/15.
//  Copyright (c) 2015 FirewallSoluciones. All rights reserved.
//

import UIKit


class ViewControllerPick : UIViewController, UIPickerViewDataSource, UIPickerViewDelegate{
    
    /*func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }*/
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 0
    }
    /*
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return "LALALA"
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }*/
}

class PickerList: NSObject, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var fondoGris : UIView!
    var fondoPick : UIView!
    var myPicker: UIPickerView = UIPickerView()
    var posicion : NSInteger = 0
    var controller : UIViewController!
    var data : NSArray!
    var eleccion : Int = 0
    init( datos : NSArray) {
        self.data = datos
    }

    func showPicker( controller : UIViewController ){
        
        print("showpicker")
        if ( self.controller == nil) {
            print("showpicker -. entro al if")
            self.controller = controller
            
            fondoGris = UIView(frame: CGRect(x: 0, y: 64, width: controller.view.frame.width, height: controller.view.frame.height - 64))
                //(0, 64, controller.view.frame.width, controller.view.frame.height - 64 ))
            fondoGris.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
            fondoGris.alpha = 0
            fondoGris.layer.zPosition = -1
            fondoGris.tag = 3001
            controller.view.addSubview(fondoGris)
            
            
            fondoPick = UIView(frame: CGRect(x: 0, y: fondoGris.frame.height, width: fondoGris.frame.width, height: 230))
                //(0, fondoGris.frame.height, fondoGris.frame.width, 230))
            fondoPick.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
            fondoGris.layer.zPosition = 10
            fondoGris.addSubview(fondoPick)
            
            let btCancel:UIButton = UIButton(frame: CGRect(x: 5, y: 0, width: 70, height: 40))
                //Make( 5 , 0, 70, 40))
            btCancel.backgroundColor = UIColor.clear
            btCancel.setTitle("Cancelar", for: UIControlState.normal)
            btCancel.addTarget(controller, action: "cancelPicker", for: UIControlEvents.touchUpInside)
            btCancel.titleLabel!.font = UIFont(name: "helvetica-bold", size: 15)
            btCancel.titleLabel?.textColor = UIColor.white
            btCancel.tag = 22;
            
            fondoPick.addSubview(btCancel)
            
            let btAceptar:UIButton = UIButton(frame: CGRect(x: controller.view.frame.width - 75, y: 0, width: 70, height: 40))
                //Make( controller.view.frame.width - 75 , 0, 70, 40))
            btAceptar.backgroundColor = UIColor.clear
            btAceptar.setTitle("Aceptar", for: UIControlState.normal)
            btAceptar.addTarget(controller, action: "acceptPicker", for: UIControlEvents.touchUpInside)
            btAceptar.titleLabel!.font = UIFont(name: "helvetica-bold", size: 15)
            btAceptar.titleLabel?.textColor = UIColor.white
            btAceptar.tag = 22;
            
            fondoPick.addSubview(btAceptar)
            
            myPicker.dataSource = self
            myPicker.delegate = self
            myPicker.selectRow(posicion, inComponent: 0, animated: true)
            myPicker.frame = CGRect(x: 0, y: 40, width: controller.view.frame.width,height:  216)
            myPicker.backgroundColor = UIColor.white
            
            fondoPick.addSubview(myPicker)
        }else{
            print("showpicker - vino al else")
        }
        
        self.mostrarPicker()
        
    }
    
    func mostrarPicker(){
        let transitionOptions = UIViewAnimationOptions.showHideTransitionViews//transitionFlipFromLeft //none
        UIView.transition(with: controller.view, duration: 0.4, options: transitionOptions, animations: {
            
            self.fondoGris.alpha = 1
            self.fondoPick.frame = CGRect(x: 0 , y: self.fondoGris.frame.height - 230, width: self.fondoGris.frame.width, height: 230)
            
            }, completion: { finished in
                
        })
    }
    
    func hidePicker(){
        let transitionOptions = UIViewAnimationOptions.showHideTransitionViews//transitionCrossDissolve // none
        UIView.transition(with: controller.view, duration: 0.4, options: transitionOptions, animations: {
            
            self.fondoGris.alpha = 0
            self.fondoPick.frame = CGRect(x: 0 , y: self.fondoGris.frame.height, width: self.fondoGris.frame.width, height: 230)
            
            }, completion: { finished in
                
        })
    }
    
    /*func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }*/
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return data[row] as? String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        eleccion = row
    }
   
}
