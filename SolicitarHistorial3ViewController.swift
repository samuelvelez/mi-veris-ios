//
//  SolicitarHistorial3ViewController.swift
//  Mi Veris
//
//  Created by Jorge on 27/05/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import UIKit

class SolicitarHistorial3ViewController: UIViewController, UITextViewDelegate {
    
    var titleView : NSString!
    
    var listaEspecialidades : NSMutableArray!
    var especialidadesSeleccionadas : NSMutableArray!
    var listaDoctores : NSMutableArray!
    var doctoresSeleccionados : NSMutableArray!
    
    var nombreUser : UILabel!
    
    var firstStep : UILabel!
    var secondStep : UILabel!
    var thirdStep : UILabel!
    
    var scroll : UIScrollView = UIScrollView()
    var pickerDesde : UIDatePicker = UIDatePicker()
    var pickerHasta : UIDatePicker = UIDatePicker()
    var txtMotivo : UITextView = UITextView()
    
    var btCheckCondiciones : UICheckBox!
    var btcheck : UIButton? //CheckBox?
    
    var test_check : UICheckBox? //= UICheckBox()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationItem.backBarButtonItem?.title = ""
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(CompleteViewController.hideKeyboard))
        scroll.addGestureRecognizer(singleTap)
        self.view.addSubview(scroll)

        let fondo : UIImageView = UIImageView(frame: self.view.bounds)
        fondo.image = UIImage(named:"fondocita.png")
        self.view.addSubview(fondo)
        
        let barWhite : UIView = UIView(frame: CGRect(x:0, y:64, width:self.view.frame.width , height: 30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x:0, y:64, width:self.view.frame.width / 2.8, height:30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x:barBlue.frame.width, y:64, width:45, height:30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x:0, y:64, width:self.view.frame.width - 20, height:30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = titleView as String
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)
        
        let usuario = Usuario.getEntity as Usuario
        
        nombreUser = UILabel(frame: CGRect(x:10, y:94, width:self.view.frame.width - 20, height:30))
        nombreUser.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        nombreUser.font = UIFont(name: "helvetica-bold", size: 12)
        nombreUser.text = usuario.nombre as String
        
        self.view.addSubview(nombreUser)
        
        let anchoStep = self.view.frame.width / 3
        
        firstStep = UILabel(frame: CGRect(x:0, y:125, width:anchoStep, height:30))
        firstStep.text = "Especialidad"
        firstStep.textAlignment = NSTextAlignment.center
        firstStep.textColor = UIColor.white
        firstStep.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        firstStep.font = UIFont.boldSystemFont(ofSize: 13)
        self.view.addSubview(firstStep)
        
        secondStep = UILabel(frame: CGRect( x:anchoStep, y:125, width:anchoStep, height:30))
        secondStep.text = "Doctor"
        secondStep.textAlignment = NSTextAlignment.center
        secondStep.textColor = UIColor.white
        secondStep.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        secondStep.font = UIFont.boldSystemFont(ofSize: 13)
        self.view.addSubview(secondStep)
        
        thirdStep = UILabel(frame: CGRect( x:anchoStep * 2, y:125, width:anchoStep, height:30))
        thirdStep.text = "Fechas"
        thirdStep.textAlignment = NSTextAlignment.center
        thirdStep.textColor = UIColor.white
        thirdStep.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        thirdStep.font = UIFont.boldSystemFont(ofSize: 13)
        self.view.addSubview(thirdStep)
        
        scroll.frame = CGRect(x:0, y:155, width:self.view.frame.width, height:self.view.frame.height)
        self.view.addSubview(scroll)
        
        let lbDesde = UILabel(frame: CGRect(x:10, y:15, width:self.view.frame.width - 20, height:30))
        lbDesde.text = "Consultar Desde: "
        lbDesde.font = UIFont.systemFont(ofSize: 14)
        scroll.addSubview(lbDesde)
        
        pickerDesde.frame = CGRect(x:20, y:45, width:self.view.frame.width - 40, height:170)
        pickerDesde.datePickerMode = UIDatePickerMode.date
        scroll.addSubview(pickerDesde)
        
        let lbHasta = UILabel(frame: CGRect(x:10, y:230, width:self.view.frame.width - 20, height:30))
        lbHasta.text = "Consultar Hasta: "
        lbHasta.font = UIFont.systemFont(ofSize: 14)
        scroll.addSubview(lbHasta)
        
        pickerHasta.frame = CGRect(x:20, y:260, width:self.view.frame.width - 40, height:170)
        pickerHasta.datePickerMode = UIDatePickerMode.date
        scroll.addSubview(pickerHasta)
        
        let lbMotivo = UILabel(frame: CGRect(x:10, y:440, width:self.view.frame.width - 20, height:30))
        lbMotivo.text = "Motivo: "
        lbMotivo.font = UIFont.systemFont(ofSize: 14)
        scroll.addSubview(lbMotivo)
        
        txtMotivo.frame = CGRect(x:15,y: 470, width:self.view.frame.width - 30, height:100)
        txtMotivo.layer.cornerRadius = 10;
        txtMotivo.layer.borderWidth = 1;
        txtMotivo.layer.borderColor = UIColor.gray.cgColor
        txtMotivo.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.7)
        txtMotivo.delegate = self;
        scroll.addSubview(txtMotivo)
        
        let separador = UIView(frame: CGRect(x:5, y:580, width:self.view.frame.width - 10, height:1))
        separador.backgroundColor = UIColor.gray
        scroll.addSubview(separador)
        
        let lbCondiciones = UILabel(frame: CGRect(x:10, y:585, width:self.view.frame.width - 20, height:30))
        lbCondiciones.text = "Términos y Condiciones: "
        lbCondiciones.font = UIFont.systemFont(ofSize: 14)
        scroll.addSubview(lbCondiciones)
        
        let lbCondiciones2 = UILabel(frame: CGRect(x:55, y:610, width:self.view.frame.width - 20, height:30))
        lbCondiciones2.text = "YO ESTOY DE ACUERDO CON LOS"
        lbCondiciones2.font = UIFont.systemFont(ofSize: 12)
        scroll.addSubview(lbCondiciones2)
        
        let btCondiciones = UIButton(frame: CGRect( x:55, y:630 , width:self.view.frame.width - 45 , height:25))
        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
        
        let myMutableString : NSMutableAttributedString = NSMutableAttributedString(string: "TÉRMINOS Y CONDICIONES.", attributes: underlineAttribute)
        myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.blue , range: NSMakeRange(0, myMutableString.length))
        
        //var underlineAttributedString : NSAttributedString = NSAttributedString(string: "TÉRMINOS Y CONDICIONES.", attributes: underlineAttribute )
        
        
        
        
        
        btCondiciones.setAttributedTitle( myMutableString, for: UIControlState.normal)
        btCondiciones.titleLabel?.numberOfLines = 0
        btCondiciones.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        btCondiciones.addTarget(self, action: #selector(SolicitarHistorial3ViewController.showCondiciones), for: UIControlEvents.touchUpInside)
        btCondiciones.setTitleColor(UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1), for: UIControlState.normal)
        btCondiciones.titleLabel!.font =  UIFont.boldSystemFont(ofSize: 12)
        scroll.addSubview(btCondiciones)
        
        btCheckCondiciones = UICheckBox(posX: 15, posY: 618)
        btCheckCondiciones.isChecked = true
        btCheckCondiciones.clicked(sender: UIButton.init())
        
        //btCheckCondiciones.addTarget(self, action: #selector(toco(sender:)), for: UIControlEvents.touchUpInside)
        //scroll.addSubview(btCheckCondiciones)
        /*
        btcheck = UICheckBox(posX: 15, posY: 658)
        scroll.addSubview(btcheck!)
 */
        let btcheck = UIButton(frame: CGRect(x: 15, y:618 , width:32, height:32))
        btcheck.setBackgroundImage(UIImage(named:"Checkbox-Full.png"), for: UIControlState.normal)
        btcheck.addTarget(self, action: ("entro"), for: UIControlEvents.touchUpInside)
        //btcheck.addTarget(self, action: ("finalizar"), for: UIControlEvents.touchUpInside)
        //btcheck.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        //btcheck.layer.cornerRadius = 15
        //scroll.addSubview(btcheck)
        
        let separador2 = UIView(frame: CGRect(x:5, y:655, width:self.view.frame.width - 10, height: 1))
        separador2.backgroundColor = UIColor.gray
        scroll.addSubview(separador2)
        
        let btPrev = UIButton(frame: CGRect( x:20, y:670 ,width: 100, height:40))
        btPrev.setTitle("Anterior", for: UIControlState.normal)
        btPrev.addTarget(self, action: #selector(SolicitarHistorial3ViewController.prevSolicitud), for: UIControlEvents.touchUpInside)
        btPrev.backgroundColor = UIColor.lightGray
        btPrev.layer.cornerRadius = 15
        scroll.addSubview(btPrev)
        
        let btNext = UIButton(frame: CGRect(x: self.view.frame.width - 120, y:670 , width:100, height:40))
        btNext.setTitle("Finalizar", for: UIControlState.normal)
        btNext.addTarget(self, action: (#selector(SolicitarHistorial3ViewController.finalizar)), for: UIControlEvents.touchUpInside)
        btNext.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        btNext.layer.cornerRadius = 15
        scroll.addSubview(btNext)
        
        test_check = UICheckBox(posX: 15, posY: 618)
        
        
        scroll.addSubview(test_check!)
        
        scroll.contentSize = CGSize(width: 0, height:1000)
        
        
    }
    func clicked(sender : AnyObject){
        print("hola hola hola")
        
    }
    
    func prevSolicitud(){
        print(test_check?.isChecked)
        self.navigationController?.popViewController(animated: true)
    }
    
    func entro(){
        print("entro a entro")
        btcheck?.setBackgroundImage(UIImage(named:""), for: UIControlState.normal)
    }
    func finalizar(){
        if(test_check!.isChecked){
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "es_EC") as Locale!
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        //let fechaDesde = dateFormatter.string(from: pickerDesde.date)
        //let fechaHasta = dateFormatter.string(from: pickerHasta.date)
        
        let currentDate = NSDate();
        
        dateFormatter.dateStyle = DateFormatter.Style.medium
        //dateFormatter.timeZone = NSTimeZone() as TimeZone! //NSTimeZone() as TimeZone!
        
        //let stringDate = dateFormatter.string(from: currentDate as Date)
        //let localDate = dateFormatter.date(from: stringDate)
        print("llego 1")
        if pickerDesde.date.compare( pickerHasta.date ) == ComparisonResult.orderedDescending {
            let alert = UIAlertController(title: "Veris", message: "La fecha Desde no puede ser mayor que la fecha Hasta", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                UIAlertAction in
                
            }
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        } else if pickerHasta.date.compare( currentDate as Date ) == ComparisonResult.orderedDescending {
            let alert = UIAlertController(title: "Veris", message: "La fecha Hasta no puede ser mayor que la fecha Actual", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                UIAlertAction in
                
            }
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        } else {
                self.enviarSolicitud()
                print("enviando...")
           
        }
        }else{
            let alert = UIAlertController(title: "Veris", message: "Debe de aceptar los terminos y condiciones", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                UIAlertAction in
                
            }
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func enviarSolicitud(){
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        
        let usuario = Usuario.getEntity
        let parent = Usuario.getParent
        let motivo = txtMotivo.text.addingPercentEscapes(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        //stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "es_EC") as Locale!
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        let fechaDesde = dateFormatter.string(from: pickerDesde.date)
        let fechaHasta = dateFormatter.string(from: pickerHasta.date)
        
        var stringDoctores : NSString = "";
        var doctor : Doctor!
        for index in doctoresSeleccionados {
            doctor = listaDoctores.object( at: index as! NSInteger ) as! Doctor
            if doctor.idDoctor != -1 {
                if stringDoctores == ""{
                    stringDoctores = "\(doctor.idDoctor),\(doctor.idEspecialidad)" as NSString
                } else {
                    stringDoctores = "\(stringDoctores)@\(doctor.idDoctor),\(doctor.idEspecialidad)" as NSString
                }
                
            }
        }
        let doctores = stringDoctores.addingPercentEscapes(using: String.Encoding.utf8.rawValue)
        //stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding.rawValue)!
        
        var tipoDocumento : NSString = ""
        if titleView == "Historia Clínica" {
            tipoDocumento = "HC"
        } else {
            tipoDocumento = "CA"
        }
        var stringUrl : NSString = ""
        let urldef = URLFactory.urldefinida()

        if parent.numeroidentificacion != "" {
            stringUrl = "\(urldef)servicio/doctor/solicitarhc?arg0=\(usuario.numeroidentificacion)&arg1=\(usuario.tipoid)&arg2=\(parent.numeroidentificacion)&arg3=\(parent.tipoid)&arg4=\(motivo!)&arg5=\(fechaDesde)&arg6=\(fechaHasta)&arg7=\(doctores!)&arg8=HC&arg9=1" as NSString
        } else {
            stringUrl = "\(urldef)servicio/doctor/solicitarhc?arg0=\(usuario.numeroidentificacion)&arg1=\(usuario.tipoid)&arg2=\(usuario.numeroidentificacion)&arg3=\(usuario.tipoid)&arg4=\(motivo!)&arg5=\(fechaDesde)&arg6=\(fechaHasta)&arg7=\(doctores!)&arg8=\(tipoDocumento)&arg9=1" as NSString
        }
        
        let url = NSURL(string: stringUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)! as String)
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        let str = "wsappusuario:ZA$57@9b86@$2r5"
        
        let utf8str = str.data(using: String.Encoding.utf8)
        
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                var err: NSError?
                
                //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                if (err != nil) {
                    print("JSON Error \(err!.localizedDescription)")
                }
                
                let info =  jsonResult as NSDictionary
                
                let resultado = info["resultado"] as! NSString
                
                    DispatchQueue.main.async(execute: {
                    ViewUtilidades.hiddenLoader(controller: self)
                    if resultado == "ok" {
                        
                        let alert = UIAlertController(title: "Veris", message: "La solicitud ha sido enviada a nuestros auditories médicos, dentro de las próximas 48 horas laborables será procesada", preferredStyle: UIAlertControllerStyle.alert)
                        
                        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) {
                            UIAlertAction in
                            
                            let root = self.navigationController?.viewControllers[1] as UIViewController!
                            self.navigationController?.popToViewController(root!, animated: true)
                            
                        }
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    } else {
                        let alert = UIAlertController(title: "Veris", message: resultado as String, preferredStyle: UIAlertControllerStyle.alert)
                        
                        let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                            UIAlertAction in
                        }
                        
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                })
                } catch {
                    print("ERROR")
                }
            }
            
            
        }
    }
    
    func showCondiciones(){

        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        let urldef = URLFactory.urldefinida()

        let url = NSURL(string: "\(urldef)servicio/catalogos/obtenerparametroindividual?arg0=terminoshc")
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        let str = "wsappusuario:ZA$57@9b86@$2r5"
        
        let utf8str = str.data(using: String.Encoding.utf8)
        
                
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
        
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                var err: NSError?
                
                //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                if (err != nil) {
                    print("JSON Error \(err!.localizedDescription)")
                }
                
                
                //println(jsonResult);
                print(jsonResult)
                let info =  jsonResult as NSDictionary
                let data = info["texto"] as! NSString
                
                let resultado = info["resultado"] as! NSString
                
                
                //dispatch_async(dispatch_get_main_queue(), {
                DispatchQueue.main.async(execute: {
                    ViewUtilidades.hiddenLoader(controller: self)
                    if resultado == "ok" {
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "viewTerminos") as! TerminosViewController
                        vc.terminos = data
                        
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    } else {
                        let alert = UIAlertController(title: "Veris", message: resultado as String, preferredStyle: UIAlertControllerStyle.alert)
                        
                        let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                            UIAlertAction in
                        }
                        
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                })
                } catch {
                    print("ERROR")
                }
            }
            
            
        }
    }
    
    @IBAction func hideKeyboard(sender: AnyObject) {
        txtMotivo.resignFirstResponder()
    }
    
    func hideKeyboard() {
        txtMotivo.resignFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
