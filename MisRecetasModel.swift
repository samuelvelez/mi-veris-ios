//
//  MisRecetasModel.swift
//  veris
//
//  Created by Felipe Lloret on 05/02/15.
//  Copyright (c) 2015 Felipe Lloret. All rights reserved.
//

import Foundation

class MisRecetasModel: NSObject {
    let fecha: String
    let doctor: String
    let centromedico: String
    let especialidad: String
    let receta: String
    let paciente : String
    override var description: String {
        return "fecha: \(fecha), doctor: \(doctor)\n, centromedico: \(centromedico)\n, especialidad: \(especialidad)\n, receta: \(receta)\n, paciente: \(paciente)"
    }
    
    init(fecha: String?, doctor: String?, centromedico: String?, especialidad: String?, receta: String?, paciente: String?) {
        self.fecha = fecha ?? ""
        self.doctor = doctor ?? ""
        self.centromedico = centromedico ?? ""
        self.especialidad = especialidad ?? ""
        self.receta = receta ?? ""
        self.paciente = paciente ?? ""
    }
}