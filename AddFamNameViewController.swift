//
//  AddFamNameViewController.swift
//  Mi Veris
//
//  Created by Samuel Velez on 16/8/16.
//  Copyright © 2016 Programadores-iOS.net. All rights reserved.
//

import UIKit

class AddFamNameViewController: UIViewController, UITextFieldDelegate {
    
    var scroll : UIScrollView = UIScrollView()
    var txt_cedula : UITextField = UITextField()
    var txt_nombres : UITextField = UITextField()
    var txt_papellido : UITextField = UITextField()
    var txt_sapellido : UITextField = UITextField()
    var txt_minedad : UITextField = UITextField()
    var txt_maxedad : UITextField = UITextField()
    var sgm_tipo : UISegmentedControl = UISegmentedControl()
    var sgm_genero : UISegmentedControl = UISegmentedControl()
    
    var txtProvincia : UITextField = UITextField()
    var pickerProvincia : PickerList!
    var dataProvincia = [""]
    var provincias : NSMutableArray = NSMutableArray()
    
    var pickerSelected : PickerList!
    var downKeyboard : Bool  = true
    


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        scroll.frame = CGRect(x: 0, y: -60, width: self.view.frame.width, height: self.view.frame.height + 60)
        scroll.isUserInteractionEnabled = true
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(AddFamNameViewController.hideKeyboard))
        scroll.addGestureRecognizer(singleTap)
        self.view.addSubview(scroll)
        
        self.navigationItem.title = ""
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        let barWhite : UIView = UIView(frame: CGRect(x:0, y:64, width: self.view.frame.width , height: 30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x: 0, y:64, width: self.view.frame.width / 2.8, height: 30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x: barBlue.frame.width, y: 64, width: 45, height: 30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x: 0, y: 70,width: self.view.frame.width - 20, height: 30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "AÑADIR FAMILIAR"
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)
        
        let items = ["Identificación", "Nombres"]
        sgm_tipo = UISegmentedControl(items: items)
        sgm_tipo.selectedSegmentIndex = 1
        sgm_tipo.tintColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        sgm_tipo.frame = CGRect(x: 0, y: 90,width: self.view.frame.width, height: 40)
        sgm_tipo.addTarget(self, action: #selector(AddFamNameViewController.cambio), for: .valueChanged);
        scroll.addSubview(sgm_tipo)
        
        
        txt_nombres = UITextField(frame: CGRect(x: 40, y: 155, width: self.view.frame.width - 80, height: 40))
        txt_nombres.layer.cornerRadius = 20
        txt_nombres.textAlignment = NSTextAlignment.center
        //txt_identificacion.delegate = self
        txt_nombres.placeholder="Primer Nombre"
        txt_nombres.layer.borderColor = UIColor.lightGray.cgColor
        txt_nombres.layer.borderWidth = 1
        txt_nombres.autocorrectionType = UITextAutocorrectionType.no
        scroll.addSubview(txt_nombres)
        
        txt_papellido = UITextField(frame: CGRect(x: 40, y: 205, width: self.view.frame.width - 80, height: 40))
        txt_papellido.layer.cornerRadius = 20
        txt_papellido.textAlignment = NSTextAlignment.center
        //txt_identificacion.delegate = self
        txt_papellido.placeholder="Primer Apellido"
        txt_papellido.layer.borderColor = UIColor.lightGray.cgColor
        txt_papellido.layer.borderWidth = 1
        txt_papellido.autocorrectionType = UITextAutocorrectionType.no
        scroll.addSubview(txt_papellido)
        
        txt_sapellido = UITextField(frame: CGRect(x: 40, y: 255, width: self.view.frame.width - 80, height:40))
        txt_sapellido.layer.cornerRadius = 20
        txt_sapellido.textAlignment = NSTextAlignment.center
        //txt_identificacion.delegate = self
        txt_sapellido.placeholder="Segundo Apellido"
        txt_sapellido.layer.borderColor = UIColor.lightGray.cgColor
        txt_sapellido.layer.borderWidth = 1
        txt_sapellido.autocorrectionType = UITextAutocorrectionType.no
        scroll.addSubview(txt_sapellido)
        
        let genero = ["Masculino", "Femenino"]
        sgm_genero = UISegmentedControl(items: genero)
        sgm_genero.selectedSegmentIndex = 1
        sgm_genero.tintColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        sgm_genero.frame = CGRect(x: 40, y: 305,width: self.view.frame.width - 80, height:40)
       // sgm_tipo.addTarget(self, action: #selector(AddFamNameViewController.cambio), forControlEvents: .ValueChanged);
        scroll.addSubview(sgm_genero)
        
        txt_minedad = UITextField(frame: CGRect(x: 40,y: 355, width: self.view.frame.width - 80, height: 40))
        txt_minedad.layer.cornerRadius = 20
        txt_minedad.textAlignment = NSTextAlignment.center
        //txt_identificacion.delegate = self
        txt_minedad.placeholder="Edad Inicio"
        txt_minedad.keyboardType = UIKeyboardType.numberPad
        txt_minedad.layer.borderColor = UIColor.lightGray.cgColor
        txt_minedad.layer.borderWidth = 1
        txt_minedad.autocorrectionType = UITextAutocorrectionType.no
        scroll.addSubview(txt_minedad)
        
        txt_maxedad = UITextField(frame: CGRect(x: 40, y: 405, width: self.view.frame.width - 80, height: 40))
        txt_maxedad.layer.cornerRadius = 20
        txt_maxedad.textAlignment = NSTextAlignment.center
        //txt_identificacion.delegate = self
        txt_maxedad.placeholder="Edad Fin"
        txt_maxedad.keyboardType = UIKeyboardType.numberPad
        
        txt_maxedad.layer.borderColor = UIColor.lightGray.cgColor
        txt_maxedad.layer.borderWidth = 1
        txt_maxedad.autocorrectionType = UITextAutocorrectionType.no
        scroll.addSubview(txt_maxedad)
        
        txtProvincia = UITextField(frame: CGRect(x: 20, y: 455, width: self.view.frame.width - 40, height: 40))
        txtProvincia.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        txtProvincia.textColor = UIColor.white
        txtProvincia.textAlignment = NSTextAlignment.center
        txtProvincia.delegate = self
        txtProvincia.layer.borderColor = UIColor.lightGray.cgColor
        txtProvincia.layer.borderWidth = 1
        txtProvincia.text = "Provincia"
        txtProvincia.addTarget(self, action: #selector(upView(view:)), for: UIControlEvents.editingDidBegin)
        txtProvincia.addTarget(self, action: #selector(downView(view:)), for: UIControlEvents.editingDidEndOnExit)
        txtProvincia.autocorrectionType = UITextAutocorrectionType.no
        //txtProvincia.enabled = false
        scroll.addSubview(txtProvincia)
        



        
        
        let btAceptar = UIButton(frame: CGRect(x: 30, y: 505, width: self.view.frame.width - 60, height: 40))
        btAceptar.setTitle("Buscar", for: UIControlState.normal)
        btAceptar.addTarget(self, action: #selector(AddFamNameViewController.buscar), for: UIControlEvents.touchUpInside)
        btAceptar.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        btAceptar.layer.cornerRadius = 20
        scroll.addSubview(btAceptar)
        
        scroll.contentSize = CGSize(width: 0, height: 460)
        
        getProvincias()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func cambio(){
        
        if(sgm_tipo.selectedSegmentIndex == 0){
            dismiss(animated: false, completion: nil)
        }else{
            
        }
        
        
    }
    func buscar(){
        var genero : String = ""
        if(sgm_genero.selectedSegmentIndex == 0){
            genero = "M"
        }else{
            genero = "F"
        }
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        
            let us = Usuario.getEntity as Usuario
        
            let provincia = provincias.object(at: pickerProvincia.eleccion) as! Provincia
            _ = txt_nombres.text!
        
            var appelido_cod = txt_papellido.text!
            appelido_cod = appelido_cod.uppercased()
            appelido_cod = appelido_cod.replacingOccurrences(of: "Ñ", with: "%C3%91")
        
            var apellido_cod = txt_sapellido.text!
            apellido_cod = apellido_cod.uppercased()
            apellido_cod = apellido_cod.replacingOccurrences(of: "Ñ", with: "%C3%91")
        
            let param = "arg0=\(us.tipoid)-\(us.numeroidentificacion)&arg1=&arg2=\(txt_nombres.text!)&arg3=\(appelido_cod)&arg4=\(apellido_cod)&arg5=\(genero)&arg6=\(txt_minedad.text!)&arg7=\(txt_maxedad.text!)&arg8=1&arg9=\(provincia.codigoProvincia)&arg10=0&arg11=80"
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SeleccionFamilia") as! SeleccionFamViewController
            
            viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Atrás", style: UIBarButtonItemStyle.plain, target: self, action: #selector(AddFamNameViewController.goBack))
            viewController.parametros = param
            // Creating a navigation controller with viewController at the root of the navigation stack.
            let navController = UINavigationController(rootViewController: viewController)
            
            self.present(navController, animated:true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func goBack(){
        dismiss(animated: false, completion: nil)
        dismiss(animated: false, completion: nil)
    }
    
    func cancelPicker(){
        pickerSelected.hidePicker()
        txtProvincia.resignFirstResponder()
        
    }
    
    
    //
    func acceptPicker(){
        pickerSelected.hidePicker()
            txtProvincia.text = dataProvincia[pickerProvincia.eleccion]
            txtProvincia.resignFirstResponder()
    }
    
    func upView( view : UITextField ){
        if (view == txtProvincia){
            //view.resignFirstResponder()
            //self.hideKeyboard()
        } else {
            
            if self.downKeyboard {
                self.downKeyboard = false
            }
        }
        
        
    }
    /*(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
     return NO;
     }*/
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if(textField == txtProvincia){
            self.hideKeyboard()
            //return false
            
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
            pickerProvincia.showPicker(controller: self)
            pickerSelected = pickerProvincia
            txtProvincia.resignFirstResponder()
        
    }
    
    func downView( view : UITextField ){
        //view.endEditing(true)
        self.dismissKeyboard()
        //self.hideKeyboard()
        self.txt_nombres.resignFirstResponder()
        self.txt_papellido.resignFirstResponder()
        self.txt_sapellido.resignFirstResponder()
        
        self.txt_minedad.resignFirstResponder()
        self.txt_maxedad.resignFirstResponder()
        if !self.downKeyboard {
            self.downKeyboard = true
            //self.scroll.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.scroll.frame) + 230)
        }
        
    }
    
    func hideKeyboard(){
        self.txt_nombres.resignFirstResponder()
        self.txt_papellido.resignFirstResponder()
        self.txt_sapellido.resignFirstResponder()
        self.txtProvincia.resignFirstResponder()
        self.txt_minedad.resignFirstResponder()
        self.txt_maxedad.resignFirstResponder()
        
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        self.view.endEditing(true)
    }
    
    func getProvincias() {
        
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        
        let urldef = URLFactory.urldefinida()

        let url = NSURL(string: "\(urldef)servicio/login/obtenerprovincias?arg0=1")
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                    var err: NSError?
                    
                    //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    if (err != nil) {
                        print("JSON Error \(err!.localizedDescription)")
                    }
                    
                    //println(jsonResult);
                    //print(jsonResult)
                    let info =  jsonResult as NSDictionary
                    let data = info["lista"] as! [[String : Any]]
                    if data.count > 0 {
                        for item in data {
                            let provincia = Provincia()
                            if let pais = item["codigo_pais"] as? String {
                                provincia.codigoPais = pais as NSString
                            }
                            
                            if let prov = item["codigo_provincia"] as? String {
                                provincia.codigoProvincia = prov as NSString
                            }
                            
                            if let reg = item["codigo_region"] as? String {
                                provincia.codigoRegion = reg as NSString
                            }
                            
                            if let nombre = item["nombre_provincia"] as? String {
                                provincia.nombre = nombre as NSString
                            }
                            
                            self.provincias.add(provincia)
                        }
                    }
                    
                    
                    var lista = [""]
                    for  i in 0..<self.provincias.count{
                        let nombre = (self.provincias[i] as AnyObject).value(forKey: "nombre")
                        lista.append(nombre as! String)
                    }

                  /*  for provi in self.provincias {
                        lista.append(provi.valueForKey("nombre")as! String)
                        //lista.addObject(provi.nombre)
                    }*/
                    lista.remove(at: 0)
                    
                    self.dataProvincia = lista
                    self.txtProvincia.text = self.dataProvincia[0]
                    //
                    //
                    self.pickerProvincia = PickerList(datos: self.dataProvincia as NSArray)
                    
                    ViewUtilidades.hiddenLoader(controller: self)
                    // self.getCiudades()
                } catch {
                    print("ERROR")
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: "Revise su conexión a Datos o Internet", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            
            
        }
    }
    

}
