//
//  ContactosViewController.swift
//  Mi Veris
//
//  Created by Jorge on 04/02/15.
//  Copyright (c) 2015 Firewall Soluciones All rights reserved.
//

import UIKit
import AddressBook
import AddressBookUI


class ContactosViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var tableViewService:UITableView?
    @IBOutlet var tableViewReclame:UITableView?
    @IBOutlet var tableViewSugerencia:UITableView?
    @IBOutlet var scroll:UIScrollView?
    var correo : String = ""
    var whatsapp : String = ""
    var fonReclamo : String = ""
    var fontServicio : String = ""
    var authDone : Bool = false
    @IBOutlet var adbk : ABAddressBook?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.style()
        //var backButton = UIBarButtonItem(title: "Volver", style: UIBarButtonItemStyle.Plain, target: self, action: "goBack")
        //backButton = self.navigationItem.backBarButtonItem!
        //navigationItem.leftBarButtonItem = backButton
        
        scroll = UIScrollView(frame: CGRect(x: 0, y: 94, width: self.view.frame.width, height: self.view.frame.height - 0))
        scroll?.backgroundColor = UIColor.clear
        self.view.addSubview(scroll!)
        
        if correo.isEmpty {
            ViewUtilidades.showLoader(controller: self, title: "Cargando...")
            
            let urldef = URLFactory.urldefinida()

            
            let url = NSURL(string: "\(urldef)servicio/catalogos/obtenerparametrosapp")
            //let request = NSURLRequest(URL: url!)
            let request = NSMutableURLRequest(url: url! as URL)
            
            _ = "wsappusuario:ZA$57@9b86@$2r5"
            
           
            //let utf8str = str.data(using: String.Encoding.utf8)
            
            //let base64Encoded = str.data(using: String.Encoding.utf8, allowLossyConversion: true)
            //let base64Encoded = str.dataUsingEncoding(NSUTF8StringEncoding)!.base64EncodedStringWithOptions([])
            //print("Encoded: \(base64Encoded)")
            //let base64DecodedData = NSData(base64Encoded: base64Encoded!, options: [])
            //NSData(base64EncodedString: base64Encoded, options: [])!
            //let base64DecodedString = String(data: base64DecodedData as! Data, encoding: String.Encoding.utf8)!
            //print("Decoded: \(base64DecodedString)")
            
            
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                
                if (error != nil) {
                    print(error!.localizedDescription)
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    do{
                    var err: NSError?
                    
                    //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    if (err != nil) {
                        print("JSON Error \(err!.localizedDescription)")
                    }
                    
                    //println(jsonResult);
                    print(jsonResult)
                    let info =  jsonResult as NSDictionary
                    let data = info["lista"] as! [[String: Any]]
                    self.correo = data[0]["correocontacto"] as! String
                    self.fonReclamo = data[0]["fonocontactoreclamo"] as! String
                    self.fontServicio = data[0]["fonocontactoservicios"] as! String
                    self.whatsapp = data[0]["numerowhatsapp"] as! String
                    if self.whatsapp.isEmpty {
                        self.whatsapp = "593994108730"
                    }
                    
                    DispatchQueue.main.async {
                        //println(jsonDate + jsonTime);
                        self.crearTablas()
                        ViewUtilidades.hiddenLoader(controller: self)
                    }
                    /*DispatchQueue.main.asynchronously(execute: {
                        //println(jsonDate + jsonTime);
                        self.crearTablas()
                        ViewUtilidades.hiddenLoader(controller: self)
                    })*/
                    } catch {
                        print("ERROR")
                    }
                }
                
            }
            
            ///////////////////

            
        }
        
      
    }
    func goBack(){
        dismiss(animated: true, completion: nil)
    }
    
    func style() {
    
    
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        self.navigationController?.navigationItem.backBarButtonItem?.title = ""
        
        let fondo : UIImageView = UIImageView(frame: self.view.bounds)
        fondo.image = UIImage(named:"fondocita .png")
        self.view.addSubview(fondo)
        
        let barWhite : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width , height: 30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width / 1.8, height: 30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x: barBlue.frame.width, y: 64, width: 45, height: 30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x: 0, y: 64, width: self.view.frame.width - 20, height: 30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "Contactos"
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)

        let usuario = Usuario.getEntity as Usuario
        if usuario.logueado {
            let nombreUser : UILabel = UILabel(frame: CGRect(x: 10, y: 94, width: self.view.frame.width - 20, height: 30))
            nombreUser.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
            nombreUser.font = UIFont(name: "helvetica-bold", size: 12)
            nombreUser.text = usuario.nombre as String
            
            self.view.addSubview(nombreUser)
        }
    
    }
    
    // MARK: crearTablas: Método para crear las tablas de servicios,reclamos y sugerencias
    
    func crearTablas(){
        
        let lbServices = UILabel(frame: CGRect(x: 15, y: 20, width: self.view.frame.width - 15, height: 30))
        lbServices.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        lbServices.font = UIFont(name: "helvetica-bold", size: 16)
        lbServices.text = "Servicios"
        
        scroll!.addSubview(lbServices)
        
        tableViewService = UITableView(frame: CGRect(x: 10, y: 50, width: self.view.frame.width - 20, height: 80))
        tableViewService?.layer.cornerRadius = 10;
        tableViewService?.layer.borderWidth = 1;
        let borderColor = UIColor.gray
        tableViewService?.layer.borderColor = borderColor.cgColor;
        tableViewService?.backgroundColor = UIColor.clear;
        tableViewService?.isScrollEnabled = false
        tableViewService?.rowHeight = 40
        tableViewService?.delegate = self;
        tableViewService?.dataSource = self;
        tableViewService?.separatorColor = UIColor.clear
        tableViewService?.register(ContactoTableViewCell.self, forCellReuseIdentifier: "cell");
        
        scroll?.addSubview(tableViewService!)
        
        let lbReclame = UILabel(frame: CGRect(x: 15, y: 160, width: self.view.frame.width - 15, height: 30))
        lbReclame.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        lbReclame.font = UIFont(name: "helvetica-bold", size: 16)
        lbReclame.text = "Reclamos"
        
        scroll!.addSubview(lbReclame)
        
        tableViewReclame = UITableView(frame: CGRect(x: 10, y: 190, width: self.view.frame.width - 20, height: 80))
        tableViewReclame?.layer.cornerRadius = 10;
        tableViewReclame?.layer.borderWidth = 1;
        tableViewReclame?.layer.borderColor = borderColor.cgColor;
        tableViewReclame?.backgroundColor = UIColor.clear;
        tableViewReclame?.isScrollEnabled = false
        tableViewReclame?.rowHeight = 40
        tableViewReclame?.delegate = self;
        tableViewReclame?.dataSource = self
        tableViewReclame?.separatorColor = UIColor.clear
        tableViewReclame?.register(ContactoTableViewCell.self, forCellReuseIdentifier: "cell");
        
        scroll?.addSubview(tableViewReclame!)
        
        
        let lbSugerencia = UILabel(frame: CGRect(x: 15, y: 300, width: self.view.frame.width - 15, height: 30))
        lbSugerencia.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        lbSugerencia.font = UIFont(name: "helvetica-bold", size: 16)
        lbSugerencia.text = "Ayúdanos a servirte mejor"
        
        scroll!.addSubview(lbSugerencia)
        
        tableViewSugerencia = UITableView(frame: CGRect(x: 10, y: 330, width: self.view.frame.width - 20, height: 80))
        tableViewSugerencia?.layer.cornerRadius = 10;
        tableViewSugerencia?.layer.borderWidth = 1;
        tableViewSugerencia?.layer.borderColor = borderColor.cgColor;
        tableViewSugerencia?.backgroundColor = UIColor.clear;
        tableViewSugerencia?.isScrollEnabled = false
        tableViewSugerencia?.rowHeight = 40
        tableViewSugerencia?.delegate = self;
        tableViewSugerencia?.dataSource = self
        tableViewSugerencia?.separatorColor = UIColor.clear
        tableViewSugerencia?.register(ContactoTableViewCell.self, forCellReuseIdentifier: "cell");
        
        scroll?.addSubview(tableViewSugerencia!)
        
        scroll?.contentSize = CGSize(width: 0, height: 500)//CGSizeMake(0, 500)
        
    }
    
    
    // MARK: Métodos de tableView
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3;
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ContactoTableViewCell
        if tableView == tableViewService {
            if indexPath.row == 0 {
                cell.title.text = "Tlfo: \(fontServicio)"
                cell.icono.image = UIImage(named: "contacto_llamada.png")
            } else if indexPath.row == 1 {
                cell.title.text = "Email: \(correo)"
                cell.icono.image = UIImage(named: "contacto_correo.png")
            } /*else {
                cell.title.text = "Wsp: \(whatsapp)"
               cell.icono.image = UIImage(named: "contacto_whatsapp.png")
            }*/
        } else if tableView == tableViewReclame {
            if indexPath.row == 0 {
                cell.title.text = "Tlfo: \(fonReclamo)"
                cell.icono.image = UIImage(named: "contacto_llamada.png")
            } else if indexPath.row == 1 {
                cell.title.text = "Redactar un reclamo "
                cell.icono.image = UIImage(named: "contacto_formulario.png")
            } /*else {
                cell.title.text = "Wsp: \(whatsapp)"
                cell.icono.image = UIImage(named: "contacto_whatsapp.png")
            }*/
        } else if tableView == tableViewSugerencia {
            if indexPath.row == 0 {
                cell.title.text = "Tlfo: \(fonReclamo)"
                cell.icono.image = UIImage(named: "contacto_llamada.png")
            } else if indexPath.row == 1 {
                cell.title.text = "Danos tu sugerencia "
                cell.icono.image = UIImage(named: "contacto_formulario.png")
            } /*else {
                cell.title.text = "Wsp: \(whatsapp)"
                cell.icono.image = UIImage(named: "contacto_whatsapp.png")
            }*/
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("toqueteo uno")
        if tableView == tableViewService {
            print("entro a tableservice \(tableViewService)")
            if indexPath.row == 0 {
                
                // Guardar en contactos
              
                let newContact:ABRecord! = ABPersonCreate().takeRetainedValue()
                //var success:Bool = false
                var error: Unmanaged<CFError>? = nil
              
               ABRecordSetValue(newContact, kABPersonFirstNameProperty, "Veris" as CFTypeRef!, &error)
                ABRecordSetValue(newContact, kABPersonLastNameProperty, "Servicios" as CFTypeRef!, &error)
                
                let phones: ABMutableMultiValue = ABMultiValueCreateMutable(UInt32(kABMultiStringPropertyType)).takeUnretainedValue()
                ABMultiValueAddValueAndLabel(phones, fontServicio as CFTypeRef!, kABPersonPhoneMobileLabel, nil);
                ABRecordSetValue(newContact, kABPersonPhoneProperty, phones, nil)

                ABAddressBookAddRecord(adbk, newContact, &error)
                ABAddressBookSave(adbk, &error)
                
                let url:NSURL = NSURL(string: "tel://"+fontServicio)!
                UIApplication.shared.openURL(url as URL)
              
          
            } else if indexPath.row == 1 {
                let url = NSURL(string: "mailto:\(correo)")
                UIApplication.shared.openURL(url! as URL)
            } else {
                
                // Abrir Whatsapp
                
               let whatsappURL = NSURL(string: "whatsapp:send?text=Test")
                if (UIApplication.shared.canOpenURL(whatsappURL! as URL)) {
                    UIApplication.shared.openURL(whatsappURL! as URL)
                }
            }
        } else if tableView == tableViewReclame {
            if indexPath.row == 0 {
                
                
                // Guardar en contactos
                
                
                let newContact:ABRecord! = ABPersonCreate().takeRetainedValue()
                //var success:Bool = false
                var error: Unmanaged<CFError>? = nil
                
                ABRecordSetValue(newContact, kABPersonFirstNameProperty, "Veris" as CFTypeRef!, &error)
                ABRecordSetValue(newContact, kABPersonLastNameProperty, "Reclamos" as CFTypeRef!, &error)
                
                let phones: ABMutableMultiValue = ABMultiValueCreateMutable(UInt32(kABMultiStringPropertyType)).takeUnretainedValue()
                ABMultiValueAddValueAndLabel(phones, fonReclamo as CFTypeRef!, kABPersonPhoneMobileLabel, nil);
                ABRecordSetValue(newContact, kABPersonPhoneProperty, phones, nil)
                
                ABAddressBookAddRecord(adbk, newContact, &error)
                ABAddressBookSave(adbk, &error)

                let url:NSURL = NSURL(string: "tel://"+fonReclamo)!
                UIApplication.shared.openURL(url as URL)
                
                
            } else if indexPath.row == 1 {
                
                
                // Abrir viewcontroller de reclamo
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "viewReclamo") as! ReclamoViewController
                    navigationController?.pushViewController(vc, animated: true)
                
                tableView.deselectRow(at: indexPath as IndexPath, animated: true);
            } else {
                
                // Abrir Whatsapp
                
                
                let whatsappURL = NSURL(string: "whatsapp:send?text=Test")
                if (UIApplication.shared.canOpenURL(whatsappURL! as URL)) {
                    UIApplication.shared.openURL(whatsappURL! as URL)
                }
                
            }
        } else if tableView == tableViewSugerencia {
            if indexPath.row == 0 {
                
                
                // Guardar en contactos
                
                
                let newContact:ABRecord! = ABPersonCreate().takeRetainedValue()
               // _ :Bool = false
                var error: Unmanaged<CFError>? = nil
                
                ABRecordSetValue(newContact, kABPersonFirstNameProperty, "Veris" as CFTypeRef!, &error)
                ABRecordSetValue(newContact, kABPersonLastNameProperty, "Sugerencias" as CFTypeRef!, &error)
                
                let phones: ABMutableMultiValue = ABMultiValueCreateMutable(UInt32(kABMultiStringPropertyType)).takeUnretainedValue()
                ABMultiValueAddValueAndLabel(phones, fonReclamo as CFTypeRef!, kABPersonPhoneMobileLabel, nil);
                ABRecordSetValue(newContact, kABPersonPhoneProperty, phones, nil)
                
                ABAddressBookAddRecord(adbk, newContact, &error)
                ABAddressBookSave(adbk, &error)
                
                
                let url:NSURL = NSURL(string: "tel://"+fonReclamo)!
                UIApplication.shared.openURL(url as URL)

                
            } else if indexPath.row == 1 {
                
                
                  // Abrir viewcontroller de sugerencia
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "viewSugerencia") as! SugerenciaViewController
                //self.presentViewController(vc, animated: true, completion: nil)
                navigationController?.pushViewController(vc, animated: true)
                
                tableView.deselectRow(at: indexPath as IndexPath, animated: true);
            } else {
                
                // Abrir Whatsapp
                
                
                let whatsappURL = NSURL(string: "whatsapp:send?text=Test")
                if (UIApplication.shared.canOpenURL(whatsappURL! as URL)) {
                    UIApplication.shared.openURL(whatsappURL! as URL)
                }
                
            }
        }
        tableView.deselectRow(at: indexPath as IndexPath, animated: true);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
/*
    func createMultiStringRef() -> ABMutableMultiValue {
        let propertyType: NSNumber = NSNumber(kABMultiStringPropertyType)
        return Unmanaged.fromOpaque(ABMultiValueCreateMutable(propertyType.uint32Value).toOpaque()).takeUnretainedValue() as NSObject as ABMultiValue
    }
  */
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Autorización para acceder a los contactos
        
        
        if !self.authDone {
          //  self.authDone = true
            let stat = ABAddressBookGetAuthorizationStatus()
            switch stat {
            case .denied, .restricted:
                print("no access")
            case .authorized, .notDetermined:
                var err : Unmanaged<CFError>? = nil
                let adbk : ABAddressBook? = ABAddressBookCreateWithOptions(nil, &err).takeRetainedValue()
                if adbk == nil {
                    print(err)
                    return
                }
                /*ABAddressBookRequestAccessWithCompletion(adbk) {
                    (granted:Bool, err:CFError!) in
                    if granted {
                        self.adbk = adbk
                    } else {
                        print(err)
                    }//if
                }//ABAddressBookReqeustAccessWithCompletion*/
            }//case
        }//if
    }

}
