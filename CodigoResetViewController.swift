//
//  CodigoResetViewController.swift
//  Mi Veris
//
//  Created by Samuel Velez on 1/5/16.
//  Copyright © 2016 Programadores-iOS.net. All rights reserved.
//

import UIKit

class CodigoResetViewController: UIViewController {

    var scroll : UIScrollView = UIScrollView()
   // var sgm_tipo : UISegmentedControl = UISegmentedControl()
    var txt_codigo : UITextField = UITextField()
    var txt_clave : UITextField = UITextField()
    var confirma_clave : UITextField = UITextField()
    
    var identificacion : NSString!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scroll.frame = CGRect(x: 0, y: -60, width: self.view.frame.width, height: self.view.frame.height + 60)
        scroll.isUserInteractionEnabled = true
        
        let singleTap = UITapGestureRecognizer(target: self, action: "hideKeyboard")
        scroll.addGestureRecognizer(singleTap)
        self.view.addSubview(scroll)
        
        self.navigationItem.title = ""
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        let barWhite : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width , height:  30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width / 2.8, height:30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x: barBlue.frame.width, y: 64, width: 45, height: 30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x: 0, y: 70, width: self.view.frame.width - 20, height: 30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "CONFIRMACIÓN"
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)
        
        
        let lbtexto = UILabel(frame: CGRect(x: 20, y: 85, width: self.view.frame.width - 40, height:100))
        lbtexto.text = "Hemos enviado el código de autorización a la cuenta de correo que tienes registrada."
        lbtexto.font = UIFont.systemFont(ofSize: 16)
        lbtexto.lineBreakMode = NSLineBreakMode.byWordWrapping
        lbtexto.numberOfLines = 0
        scroll.addSubview(lbtexto)
        
        txt_codigo = UITextField(frame: CGRect(x: 40, y: 275, width: self.view.frame.width - 80, height:40))
        txt_codigo.layer.cornerRadius = 20
        txt_codigo.textAlignment = NSTextAlignment.center
        //txt_identificacion.delegate = self
        txt_codigo.placeholder="Código de Autorización"
        txt_codigo.layer.borderColor = UIColor.lightGray.cgColor
        txt_codigo.layer.borderWidth = 1
        txt_codigo.autocorrectionType = UITextAutocorrectionType.no
        scroll.addSubview(txt_codigo)
        
        txt_clave = UITextField(frame: CGRect(x: 40, y: 175, width: self.view.frame.width - 80, height:40))
        txt_clave.layer.cornerRadius = 20
        txt_clave.textAlignment = NSTextAlignment.center
        //txt_identificacion.delegate = self
        txt_clave.placeholder="Clave nueva (8 dígitos mínimo)"
        txt_clave.isSecureTextEntry = true
        txt_clave.layer.borderColor = UIColor.lightGray.cgColor
        txt_clave.layer.borderWidth = 1
        txt_clave.autocorrectionType = UITextAutocorrectionType.no
        scroll.addSubview(txt_clave)
        
        confirma_clave = UITextField(frame: CGRect(x: 40, y: 225, width: self.view.frame.width - 80, height: 40))
        confirma_clave.layer.cornerRadius = 20
        confirma_clave.textAlignment = NSTextAlignment.center
        //txt_identificacion.delegate = self
        confirma_clave.placeholder="Confirmar Clave"
        confirma_clave.isSecureTextEntry = true
        confirma_clave.layer.borderColor = UIColor.lightGray.cgColor
        confirma_clave.layer.borderWidth = 1
        confirma_clave.autocorrectionType = UITextAutocorrectionType.no
        scroll.addSubview(confirma_clave)
        
        let lb_fin = UILabel(frame: CGRect(x: 20, y: 325, width: self.view.frame.width - 40, height: 100))
        lb_fin.text = "Nota: Si no has recibido el correo con el código de autorización, por favor revise su bandeja de correo no deseado (spam) o comunícate con nosotros al 6009600."
        lb_fin.font = UIFont.systemFont(ofSize: 16)
        lb_fin.lineBreakMode = NSLineBreakMode.byWordWrapping
        lb_fin.numberOfLines = 0
        scroll.addSubview(lb_fin)
        
        let btAceptar = UIButton(frame: CGRect(x: 30, y: 440, width:  self.view.frame.width - 60, height: 40))
        btAceptar.setTitle("Resetear Clave", for: UIControlState.normal)
        btAceptar.addTarget(self, action: "siguiente", for: UIControlEvents.touchUpInside)
        btAceptar.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        btAceptar.layer.cornerRadius = 20
        scroll.addSubview(btAceptar)
        
        scroll.contentSize = CGSize(width: 0, height: 460)

        // Do any additional setup after loading the view.
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func siguiente(){
        var band = false
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        let clave = txt_clave.text!.sha1().lowercased()
        let codigo = txt_codigo.text!
        print(identificacion.description)
        print(txt_clave.text!)
        print(txt_codigo.text!)
        var mensaje = "Ha ocurrido un error. Intente nuevamente"
        
        let urldef = URLFactory.urldefinida()

        let cadenaUrl = "\(urldef)servicio/login/reset?arg0=\(identificacion!)&arg1=\(clave)&arg2=\(codigo)"
        
        if(clave != confirma_clave.text!.sha1().lowercased()){
            mensaje = "Contraseña no coincide"
            band = false
        }else{
            band = true
        }
        
        let url = NSURL(string: cadenaUrl )
        
        print(cadenaUrl)
        //let request = NSURLRequest(URL: url!)
        if url != nil && band {
            let request = NSMutableURLRequest(url: url! as URL)
            
            let str = "wsappusuario:ZA$57@9b86@$2r5"
            let utf8str = str.data(using: String.Encoding.utf8)
            
            let base64Encoded = str.data(using: String.Encoding.utf8, allowLossyConversion: true)
            //let base64Encoded = str.dataUsingEncoding(NSUTF8StringEncoding)!.base64EncodedStringWithOptions([])
            print("Encoded: \(base64Encoded), \(utf8str!.base64EncodedString())")
            let base64DecodedData = utf8str!.base64EncodedString()
            print(base64DecodedData)
            //NSData(base64EncodedString: base64Encoded, options: [])!
            // let base64DecodedString = String(data: base64DecodedData as! Data, encoding: String.Encoding.utf8)!
            //print("Decoded: \(base64DecodedString)")
            
            //Note : Add the corresponding "Content-Type" and "Accept" header. In this example I had used the application/json.
            request.addValue(base64DecodedData, forHTTPHeaderField: "Authorization")
            /*
            let utf8str = str.dataUsingEncoding(NSUTF8StringEncoding)
            
            if let base64Encoded = utf8str?.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0)){
                
                print("Encoded:  \(base64Encoded)")
                //request.addValue("Authorization", forHTTPHeaderField: "Basic \(base64Encoded)")
                request.setValue("Basic \(base64Encoded)", forHTTPHeaderField: "Authorization")
                
                
                if let base64Decoded = NSData(base64EncodedString: base64Encoded, options:   NSDataBase64DecodingOptions(rawValue: 0))
                    .map({ NSString(data: $0, encoding: NSUTF8StringEncoding) })
                {
                    // Convert back to a string
                    print("Decoded:  \(base64Decoded)")
                }
            }
            */
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                request.timeoutInterval = 10
                if (error != nil) {
                    print(error!.localizedDescription)
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    do{
                        var err: NSError?
                        
                        let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        if (err != nil) {
                            print("JSON Error \(err!.localizedDescription)")
                        }
                        
                        print(jsonResult)
                        let info =  jsonResult as NSDictionary
                        //let data = info["lista"] as! NSArray
                        let resultado = info["resultado"] as! NSString
                        
                        
                        //dispatch_async(dispatch_get_main_queue(), {
                        DispatchQueue.main.async(execute: {
                            ViewUtilidades.hiddenLoader(controller: self)
                            print(resultado)
                            if resultado.isEqual(to: "ok") {
                                //let p_identificacion = self.txt_identificacion.text! as String
                                let alert = UIAlertController(title: "Veris", message: "Tu clave ha sido cambiada con exito!" as String, preferredStyle: UIAlertControllerStyle.alert)
                                let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                                    UIAlertAction in
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = storyboard.instantiateViewController(withIdentifier: "viewControllerMain") as! ViewController
                                    
                                    vc.navigationItem.hidesBackButton = true
                                    
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                                alert.addAction(okAction)
                                self.present(alert, animated: true, completion: nil)
                                
                            } else {
                                let alert = UIAlertController(title: "Veris", message: resultado as String, preferredStyle: UIAlertControllerStyle.alert)
                                let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                                    UIAlertAction in
                                    
                                }
                                alert.addAction(okAction)
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                        })
                    } catch {
                        print("ERROR")
                        ViewUtilidades.hiddenLoader(controller: self)
                        let alert = UIAlertController(title: "Veris", message: "Revise su conexión a Datos o Internet", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
                
                
            }
            
        } else {
            ViewUtilidades.hiddenLoader(controller: self)
            let alert = UIAlertController(title: "Veris", message: mensaje, preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                UIAlertAction in
                
            }
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func hideKeyboard(){
        //self.txt_identificacion.resignFirstResponder()
        self.txt_codigo.resignFirstResponder()
        self.txt_clave.resignFirstResponder()
        self.confirma_clave.resignFirstResponder()
        
    }

}
