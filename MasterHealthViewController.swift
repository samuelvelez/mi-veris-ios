//
//  MasterHealthViewController.swift
//  Mi Veris
//
//  Created by Samuel Velez on 1/8/16.
//  Copyright © 2016 Programadores-iOS.net. All rights reserved.
//

import UIKit

class MasterHealthViewController: UIViewController {
    
    let kAuthorizeHealthKitSection = 2
    let kProfileSegueIdentifier = "profileSegue"
    let kWorkoutSegueIdentifier = "workoutsSeque"
    
    let healthManager:HealthManager = HealthManager()
    var tableView = UITableView()
    var btnConcederPermiso = UIButton()
    var btnLeerMisDatos = UIButton()
    var btnswitch = UISwitch()
    
    
    var datosACapturar = ["Sexo","Sangre","Fecha de Nacimiento","Altura","Peso","IMC","Contador de Pasos","Distancia Recorrida","Distancia Posición","Energia quemada reposo","Energia quemada Activa","Frecuencia Cardiaca"]
    
    var imgCheck = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.navigationItem.title = ""
        
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        let fondo : UIImageView = UIImageView(frame: self.view.bounds)
        fondo.image = UIImage(named:"MisCitasBackground")
        self.view.addSubview(fondo)
        
        let barWhite : UIView = UIView(frame: CGRect(x:0, y:64, width:self.view.frame.width , height:30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x:0, y:64, width:self.view.frame.width / 1.8, height:30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x:barBlue.frame.width, y: 64, width:45, height:30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x: 5, y: 64, width: self.view.frame.width - 20, height:30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 10)
        title.text = "CONFIGURACIÓN"
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)
        
        
        btnLeerMisDatos.frame =  CGRect(x: 20, y: 120, width: self.view.frame.width - 40, height:45);
        btnLeerMisDatos.addTarget(self, action: #selector(MasterHealthViewController.irPantallaMisDatos), for: UIControlEvents.touchUpInside)
        btnLeerMisDatos.backgroundColor = UIColor.blue
        btnLeerMisDatos.setTitle("Tus Datos", for: UIControlState.normal)
        btnLeerMisDatos.setTitleColor(UIColor.white, for: UIControlState.normal)
        //self.view.addSubview(btnLeerMisDatos)
        
        
        btnConcederPermiso.frame =  CGRect(x: 20, y: 280, width: self.view.frame.width - 40,height: 100);
        btnConcederPermiso.addTarget(self, action: #selector(MasterHealthViewController.authorizeHealthKit), for: UIControlEvents.touchUpInside)
        let lbtexto = UILabel(frame: CGRect(x: 20, y: 80, width: self.view.frame.width - 40, height:100))
        lbtexto.text = "¡Nos importa tu salud!"
        lbtexto.font = UIFont.systemFont(ofSize: 18)
        lbtexto.lineBreakMode = NSLineBreakMode.byWordWrapping
        lbtexto.numberOfLines = 0
        self.view.addSubview(lbtexto)
        
        
        let lbtexto2 = UILabel(frame: CGRect(x: 20, y: 150, width: self.view.frame.width - 40, height:100))
        lbtexto2.text = "Por eso queremos sincronizar nuestra aplicación con HealthKit, así podremos brindarte consejos personalizados para que mejores tu estilo de vida."
        lbtexto2.font = UIFont.systemFont(ofSize: 15)
        lbtexto2.lineBreakMode = NSLineBreakMode.byWordWrapping
        lbtexto2.numberOfLines = 0
        self.view.addSubview(lbtexto2)
        
        self.view.addSubview(btnConcederPermiso)
        
        print("switch creado")
        btnswitch.frame = CGRect(x: 20, y: 280, width: self.view.frame.width - 40, height: 100)
        btnswitch.addTarget(self, action: #selector(MasterHealthViewController.authorizeHealthKit), for: UIControlEvents.touchUpInside)
        
        self.view.addSubview(btnswitch)
        
        imgCheck = UIImageView(frame: CGRect(x: self.view.frame.width * 0.13, y: 280, width: 35, height:100))
        imgCheck.image = UIImage(named: "errorHealthKit.png")
        imgCheck.contentMode = .scaleAspectFit
        //imgCheck.backgroundColor = UIColor.redColor()
        //self.view.addSubview(imgCheck)
        
        if (((UserDefaults.standard.object(forKey: "banderaPrimeraVezHealthKit"))) == nil) // true
        {
            imgCheck.image = UIImage(named: "errorHealthKit.png")
            //btnswitch.on = false
            self.btnswitch.setOn(false, animated: false)
            
        }
        else
        {
            imgCheck.image = UIImage(named: "iconHealthKit.png")
            //btnswitch.on = true
            self.btnswitch.setOn(true, animated: false)
            
            
            
        }
        
        let restarAncho = self.view.frame.width * 0.18
        
        let leyenda : UILabel = UILabel(frame: CGRect(x: restarAncho + 45, y: 230, width: self.view.frame.width - (restarAncho * 2), height: 100))
        leyenda.textAlignment = NSTextAlignment.left
        leyenda.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        //leyenda.font = UIFont(name: "helvetica-bold", size: 11)
        leyenda.numberOfLines  = 0
        //leyenda.backgroundColor = UIColor.redColor()
        self.view.addSubview(leyenda)
        
        
        //var myString:NSString = "Permitir sincronizar con HealthKit"
        
        
        let myString = "Permitir sincronizar con HealthKit"
        
        var myMutableString = NSMutableAttributedString()
        
        myMutableString = NSMutableAttributedString(
            string: myString,
            attributes: [NSFontAttributeName:
                UIFont(name: "Georgia", size: 18.0)!])
        
        myMutableString.addAttribute(NSFontAttributeName,
                                     value: UIFont(
                                        name: "helvetica-light",
                                        size: 20.0)!,
                                     range: NSRange(
                                        location: 0,
                                        length: 24))
        
        myMutableString.addAttribute(NSFontAttributeName,
                                     value: UIFont(
                                        name: "helvetica",
                                        size: 21.0)!,
                                     range: NSRange(
                                        location:25,
                                        length:9))
        
        
        myMutableString.addAttribute(NSForegroundColorAttributeName,
                                     value: UIColor.black,
                                     range: NSRange(
                                        location:0,
                                        length:34))
        
        
        leyenda.attributedText = myMutableString
        
        
        
        //createTable()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func authorizeHealthKit()
    {
        print("entroa autorizacion")
        healthManager.authorizeHealthKit { (authorized,  error) -> Void in
            if authorized {
                print("HealthKit authorization received.")
                print(UserDefaults.standard.object(forKey: "banderaPrimeraVezHealthKit"))
                
                if (((UserDefaults.standard.object(forKey: "banderaPrimeraVezHealthKit"))) == nil)
                {
                    //self.imgCheck.image = UIImage(named: "errorHealthKit.png")
                    print("true tiene permisos")
                    UserDefaults.standard.set("true", forKey: "banderaPrimeraVezHealthKit") // Cambia la bandera a true para que solo se ejecute una vez el mandado de datos, al aceptar los permisos para HealthKit
                    self.mandarDatosHealthKit()
                    
                }
                else
                {
                    //self.imgCheck.image = UIImage(named: "iconHealthKit.png")
                    self.btnswitch.setOn(true, animated: false)
                    print("false")
                    print("mandemos por segunda vez... ok")
                    self.mandarDatosHealthKit()
                }
                
                
                
                //dispatch_async(dispatch_get_main_queue(), { () -> Void in
                DispatchQueue.main.async {
                    
                    self.imgCheck.image = UIImage(named: "iconHealthKit.png")
                    self.btnswitch.setOn(true, animated: false)
                    
                    let alert = UIAlertController(title: "Mi Veris", message: "La autorización a HealthKit se encuentra activa, si deseas editar un permiso, por favor házlo directamente en la configuración de Salud de Apple (AppleHealth)", preferredStyle: UIAlertControllerStyle.alert)
                    let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                        UIAlertAction in
                        self.btnswitch.setOn(true, animated: false)
                        
                    }
                    alert.addAction(okAction)
                    self.present(alert, animated: true, completion: nil)
                    print("fasle")
                    
                }
            }
            else
            {
                self.imgCheck.image = UIImage(named: "errorHealthKit.png")
                print("HealthKit authorization denied!")
                let alert = UIAlertView()
                alert.title = "Mi Veris"
                alert.message = "La autorización a HealthKit está denegada"
                alert.addButton(withTitle: "Aceptar")
                
                alert.show()
                if error != nil {
                    print("\(error)")
                }
            }
        }
    }
    
    func irPantallaMisDatos()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "viewProfile") as! ProfileViewController
        vc.healthManager = healthManager
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func mandarDatosHealthKit()
    {
        print("\n Consulta de los datos HealthKit, la app espera 6 segundos para poder recuperar todos los datos ya que se recuperan en forma asincrona \n")
        
        let objeto = ObjetoDatosHealthKit( sexo: "", sangre: "", fechaDeNacimiento: "", altura: "", peso: "", imc: "", contadorDePasos: "", distanciaRecorrida: "", distanciaPosición: "", energiaQuemadaReposo: "", energiaQuemadaActiva: "", frecuenciaCardiaca: "")
        ObjetoDatosHealthKit.getDatos().add(objeto)
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        backgroundThread(6.0,background: {
            // Your function here to run in the background
            
            
            let extensionesHealthKit = UIImage()
            extensionesHealthKit.reinciarContadorParaRefrescarObjeto()
            extensionesHealthKit.updateProfileInfo(healthManager: self.healthManager)
            extensionesHealthKit.updateWeight(healthManager: self.healthManager)
            extensionesHealthKit.updateHeight(healthManager: self.healthManager)
            extensionesHealthKit.updateIMC(healthManager: self.healthManager);
            extensionesHealthKit.updateContadorPasos(healthManager: self.healthManager)
            extensionesHealthKit.updateDistanciaRecorrida(healthManager: self.healthManager)
            extensionesHealthKit.updateEnergiaQuemadaReposo(healthManager: self.healthManager)
            extensionesHealthKit.updateEnergiaQuemadaActiva(healthManager: self.healthManager)
            extensionesHealthKit.updateHeartRate(healthManager: self.healthManager)
            
            
            },
                         completion: {
                            
                            print("\n Mandar JSON con Arreglo HealthKit")
                            
                            NSString().remplazarVaciosPorCero() //Remplaza en el objeto los campos nulos ("") por ceros
                            
                            let objeto = ObjetoDatosHealthKit.getDatos().object(at: 0) as! ObjetoDatosHealthKit
                            
                            let extensionMandarDatoPost = NSString()
                            let arregloIndicadores = [] as NSMutableArray
                            
                            //print(NSUserDefaults.standardUserDefaults().objectForKey("alturakit"))
                            //print("contador kit \(objeto.contadorDePasos)")
                            //print(NSUserDefaults.standardUserDefaults().objectForKey("contadorkit"))
                            
                            let objetoKitAltura  =  ["valor" : objeto.altura , "idindicador" : 1] as [String : Any]
                            arregloIndicadores.add(objetoKitAltura)
                            UserDefaults.standard.set(objeto.altura, forKey: "alturakit")
                            // LLENAR ALTURA 1  arg 1
                            
                            let objetoKitPeso  =  ["valor" : objeto.peso , "idindicador" : 2] as [String : Any]
                            arregloIndicadores.add(objetoKitPeso)
                            UserDefaults.standard.set(objeto.peso, forKey: "pesokit")
                            // LLENAR PESO 2 arg 2
                            
                            let objetoKitPasos  =  ["valor" : objeto.contadorDePasos , "idindicador" : 3] as [String : Any]
                            arregloIndicadores.add(objetoKitPasos)
                            UserDefaults.standard.set(objeto.contadorDePasos, forKey: "contadorkit")
                            // LLENAR CONTADOR PASOS 3 arg 3
                            
                            let objetoKitDistancia  =  ["valor" : objeto.distanciaRecorrida , "idindicador" : 4] as [String : Any]
                            arregloIndicadores.add(objetoKitDistancia)
                            UserDefaults.standard.set(objeto.distanciaRecorrida, forKey: "drecorridakit")
                            // LLENAR DISTACIA RECORRIDA4 arg 4
                            
                            let objetoKitFrecuenciaCardiaca  =  ["valor" : objeto.frecuenciaCardiaca , "idindicador" : 5] as [String : Any]
                            arregloIndicadores.add(objetoKitFrecuenciaCardiaca)
                            UserDefaults.standard.set(objeto.frecuenciaCardiaca, forKey: "frecuenciakit")
                            // LLENAR FRECUENCIA 5 arg 5
                            
                            let objetoKitEnergiaQuemadaActiva = ["valor" : objeto.energiaQuemadaActiva , "idindicador" : 9] as [String : Any]
                            arregloIndicadores.add(objetoKitEnergiaQuemadaActiva)
                            UserDefaults.standard.set(objeto.energiaQuemadaActiva, forKey: "eqakit")
                            // LLENAR EQA 6 arg 9
                            
                            let objetoKitEnergiaQuemadaReposo  =  ["valor" : objeto.energiaQuemadaReposo , "idindicador" : 10] as [String : Any]
                            arregloIndicadores.add(objetoKitEnergiaQuemadaReposo)
                            UserDefaults.standard.set(objeto.energiaQuemadaReposo, forKey: "eqrkit")
                            // LLENAR EQR 7 arg 10
                            
                            let objetoKitFechaNacimiento  =  ["valor" : objeto.fechaDeNacimiento , "idindicador" : 12] as [String : Any]
                            arregloIndicadores.add(objetoKitFechaNacimiento)
                            UserDefaults.standard.set(objeto.fechaDeNacimiento, forKey: "fechakit")
                            // LLENAR NACIMIENTO 8 arg 12
                            
                            let objetoKitImc  =  ["valor" : objeto.imc , "idindicador" : 13] as [String : Any]
                            arregloIndicadores.add(objetoKitImc)
                            UserDefaults.standard.set(objeto.imc, forKey: "imckit")
                            // LLENAR IMC 9 arg 13
                            
                            let objetoKitSexo  =  ["valor" : objeto.sexo , "idindicador" : 14] as [String : Any]
                            arregloIndicadores.add(objetoKitSexo)
                            UserDefaults.standard.set(objeto.sexo, forKey: "sexokit")
                            // LLENAR SEXO 10 arg 14
                            
                            let objetoKitSangre  =  ["valor" : objeto.sangre , "idindicador" : 15] as [String : Any]
                            arregloIndicadores.add(objetoKitSangre)
                            UserDefaults.standard.set(objeto.sangre, forKey: "sangrekit")
                            // LLENAR SANGRE 11 arg15
                            
                            UserDefaults.standard.synchronize()
                            //print(arregloIndicadores)
                            extensionMandarDatoPost.sendJSONHealthKit(arregloDatos: arregloIndicadores)
                            // A function to run in the foreground when the background thread is complete
        })
        
    }
    
    func backgroundThread(_ delay: Double = 0.0, background: (() -> Void)? = nil, completion: (() -> Void)? = nil) {
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
            if(background != nil){ background!(); }
            
            let popTime = DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: popTime) {
                if(completion != nil){ completion!(); }
            }
        }
    }
}


public extension UIDevice
{
    
    var modelName: String
    {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8 , value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier
        {
        case "iPod5,1":                                 return "iPod_Touch_5"
        case "iPod7,1":                                 return "iPod_Touch_6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone-4"
        case "iPhone4,1":                               return "iPhone_4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone_5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone_5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone_5s"
        case "iPhone7,2":                               return "iPhone_6"
        case "iPhone7,1":                               return "iPhone_6_Plus"
        case "iPhone8,1":                               return "iPhone_6s"
        case "iPhone8,2":                               return "iPhone_6s_Plus"
        case "iPhone8,4":                               return "iPhone_SE"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad_2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad_3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad_4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad_Air"
        case "iPad5,3", "iPad5,4":                      return "iPad_Air_2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad_Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad_Mini_2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad_Mini_3"
        case "iPad5,1", "iPad5,2":                      return "iPad_Mini_4"
        case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8":return "iPad_Pro"
        case "AppleTV5,3":                              return "Apple_TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
