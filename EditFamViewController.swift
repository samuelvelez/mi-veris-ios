//
//  EditFamViewController.swift
//  Mi Veris
//
//  Created by Samuel Velez on 16/8/16.
//  Copyright © 2016 Programadores-iOS.net. All rights reserved.
//

import UIKit

class EditFamViewController: UIViewController, UITextFieldDelegate {

    var scroll : UIScrollView = UIScrollView()
    var txt_cedula : UITextField = UITextField()
    var txt_nombres : UITextField = UITextField()
    var txt_parentesco : UITextField = UITextField()
    var swi_admin : UISwitch = UISwitch()
    var sgm_tipo : UISegmentedControl = UISegmentedControl()
    var fam_selected : Familiar!
    
    var ls_parentesco : NSMutableArray = NSMutableArray()
    
    var pickerParentesco : PickerList!
    var dataParentesco = [""]
    
    var pickerSelected : PickerList!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        print("\(self.fam_selected?.numeroIdentificacion) - \(self.fam_selected?.edad) ")

        // Do any additional setup after loading the view.
        scroll.frame = CGRect(x: 0, y: -60, width: self.view.frame.width, height:self.view.frame.height + 60)
        scroll.isUserInteractionEnabled = true
        
       // let singleTap = UITapGestureRecognizer(target: self, action: "hideKeyboard")
       // scroll.addGestureRecognizer(singleTap)
        self.view.addSubview(scroll)
        
        self.navigationItem.title = ""
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        let barWhite : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width , height:30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width / 2.8, height: 30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x:barBlue.frame.width, y: 64, width: 45, height: 30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x: 0, y: 70, width: self.view.frame.width - 20, height: 30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "ASIGNAR ROL"
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)
        
       
        var nom_completo : String = "\(self.fam_selected!.primerNombre) \(self.fam_selected!.segundoNombre) \(self.fam_selected!.primerApellido) \(self.fam_selected!.segundoApellido)"
        nom_completo = nom_completo.removingPercentEncoding! //stringByRemovingPercentEncoding!
        nom_completo = nom_completo.replacingOccurrences(of: "+", with: " ", options: String.CompareOptions.literal, range: nil)
        
        //stringByReplacingOccurrencesOfString("+", withString: " ", options: NSStringCompareOptions.LiteralSearch, range: nil)
        nom_completo = nom_completo.replacingOccurrences(of: "  ", with: " ", options: String.CompareOptions.literal, range: nil)
        print("el nombre es: \(nom_completo)")
        
        let lbnombre = UILabel(frame: CGRect(x: 20, y: 105, width: self.view.frame.width - 40, height: 50))
        lbnombre.text = nom_completo
        lbnombre.font = UIFont.systemFont(ofSize: 16)
        lbnombre.lineBreakMode = NSLineBreakMode.byWordWrapping
        lbnombre.numberOfLines = 0
        
        lbnombre.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        lbnombre.textAlignment = NSTextAlignment.left
        
        scroll.addSubview(lbnombre)
        
        var edad = fam_selected!.edad as String
        edad = edad.replacingOccurrences(of: "ANIOS", with: "AÑOS", options: String.CompareOptions.literal, range: nil)
        //stringByReplacingOccurrencesOfString("ANIOS", withString: "AÑOS", options: NSStringCompareOptions.LiteralSearch, range: nil)
        
        let lbedad = UILabel(frame: CGRect(x: 20, y: 155, width: (self.view.frame.width) - 40, height: 25))
        lbedad.text = edad
        lbedad.font = UIFont.systemFont(ofSize: 16)
        lbedad.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        lbedad.textAlignment = NSTextAlignment.left
        scroll.addSubview(lbedad)
        
        let lbseleccion = UILabel(frame: CGRect(x: 20, y: 195, width: self.view.frame.width - 40, height: 60))
        lbseleccion.text = "Seleccionar el tipo de relación que tiene esta persona contigo: "
        lbseleccion.font = UIFont.systemFont(ofSize: 16)
        lbseleccion.lineBreakMode = NSLineBreakMode.byWordWrapping
        lbseleccion.numberOfLines = 0
        lbseleccion.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        lbseleccion.textAlignment = NSTextAlignment.left
        
        scroll.addSubview(lbseleccion)
        
        txt_parentesco = UITextField(frame: CGRect(x: 20, y: 255, width: self.view.frame.width - 40, height: 40))
        txt_parentesco.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        txt_parentesco.layer.cornerRadius = 20
        txt_parentesco.layer.borderColor = UIColor.lightGray.cgColor
        txt_parentesco.layer.borderWidth = 1
        txt_parentesco.textAlignment = NSTextAlignment.center
        txt_parentesco.delegate = self
        txt_parentesco.layer.borderColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1).cgColor
        txt_parentesco.layer.borderWidth = 1
        txt_parentesco.text = "Parentesco"
        
        txt_parentesco.autocorrectionType = UITextAutocorrectionType.no
        scroll.addSubview(txt_parentesco)
        
                
        let btAceptar = UIButton(frame: CGRect(x:30, y: 315, width: self.view.frame.width - 60, height: 40))
        btAceptar.setTitle("Guardar", for: UIControlState.normal)
        btAceptar.addTarget(self, action: #selector(EditFamViewController.enviar2), for: UIControlEvents.touchUpInside)
        btAceptar.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        btAceptar.layer.cornerRadius = 20
        scroll.addSubview(btAceptar)
        
        scroll.contentSize = CGSize(width: 0, height: 460)
        getParentesco()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func asignar(){
        print("listo para asignar")
        
    }
    
    func getParentesco(){
        
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        let urldef = URLFactory.urldefinida()

        let url = NSURL(string: "\(urldef)servicio/persona/tiposparentesco")
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
       
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                    var err: NSError?
                    
                    //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    if (err != nil) {
                        print("JSON Error \(err!.localizedDescription)")
                    }
                    
                    //println(jsonResult);
                    //print(jsonResult)
                    let info =  jsonResult as NSDictionary
                    let data = info["lista"] as! [[String : Any]]
                    if data.count > 0 {
                        for item in data {
                            let parentesco = TipoParentesco()
                            if let cod = item["codigoParentesco"] as? String {
                                parentesco.codigoParentesco = cod as NSString
                            }
                            
                            if let des = item["descripcion"] as? String {
                                parentesco.descripcion = des as NSString
                            }
                            self.ls_parentesco.add(parentesco)
                        }
                    }
                    
                    
                    var lista = [""]
                    /*for provi in self.ls_parentesco {
                        lista.append(provi.valueForKey("descripcion")as! String)
                        //lista.addObject(provi.nombre)
                    }*/
                    for  i in 0..<self.ls_parentesco.count{
                       // print("encontro 1 \((self.ls_usuario[i] as AnyObject).value(forKey: "descripcion"))")
                        let nombre = (self.ls_parentesco[i] as AnyObject).value(forKey: "descripcion")
                        lista.append(nombre as! String)
                    }

                    lista.remove(at: 0)
                    
                    self.dataParentesco = lista
                    self.txt_parentesco.text = self.dataParentesco[0]
                    //
                    //
                    self.pickerParentesco = PickerList(datos: self.dataParentesco as NSArray)
                    
                    ViewUtilidades.hiddenLoader(controller: self)
                    //self.getCiudades()
                } catch {
                    print("ERROR")
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: "Revise su conexión a Datos o Internet", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    func enviar2(){
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        
        let fmaa = ls_parentesco.object(at: pickerParentesco.eleccion) as! TipoParentesco
        
        let parameters = ["tipoIdentificacion":"\(fam_selected!.tipoIdentificacion)", "numeroIdentificacion": "\(fam_selected!.numeroIdentificacion)", "primerNombre": "\(fam_selected!.primerNombre)", "segundoNombre": "\(fam_selected!.segundoNombre)", "primerApellido": "\(fam_selected!.primerApellido)", "segundoApellido": "\(fam_selected!.segundoApellido)", "edad": "\(fam_selected!.edad)", "codigoParentesco": "\(fmaa.codigoParentesco)", "parentesco": "\(fmaa.descripcion)"] as Dictionary<String, String>
        let us = Usuario.getEntity as Usuario
        let urldef = URLFactory.urldefinida()

        
        
        let  myurl = "\(urldef)servicio/persona/grupo/\(us.tipoid)-\(us.numeroidentificacion)"
        let request = NSMutableURLRequest(url: NSURL(string:
            myurl)! as URL)
        
       
        let session = URLSession.shared
        request.httpMethod = "POST"
        
        let str = "wsappusuario:ZA$57@9b86@$2r5"
        
        /*let utf8str = str.data(using: String.Encoding.utf8)
        
        if let base64Encoded = utf8str?.base64EncodedStringWithOptions(NSData.Base64EncodingOptions(rawValue: 0)){
            
            
            request.setValue("Basic \(base64Encoded)", forHTTPHeaderField: "Authorization")
            
            
            if let base64Decoded = NSData(base64EncodedString: base64Encoded, options:   NSDataBase64DecodingOptions(rawValue: 0))
                .map({ NSString(data: $0, encoding: NSUTF8StringEncoding) })
            {
                // Convert back to a string
                print("Decoded:  \(base64Decoded)")
            }
        }*/
        let utf8str = str.data(using: String.Encoding.utf8)
        
        let base64Encoded = str.data(using: String.Encoding.utf8, allowLossyConversion: true)
        //let base64Encoded = str.dataUsingEncoding(NSUTF8StringEncoding)!.base64EncodedStringWithOptions([])
        print("Encoded: \(base64Encoded), \(utf8str!.base64EncodedString())")
        let base64DecodedData = utf8str!.base64EncodedString()
        print(base64DecodedData)
        //NSData(base64EncodedString: base64Encoded, options: [])!
       // let base64DecodedString = String(data: base64DecodedData as! Data, encoding: String.Encoding.utf8)!
        //print("Decoded: \(base64DecodedString)")

        //Note : Add the corresponding "Content-Type" and "Accept" header. In this example I had used the application/json.
        request.addValue(base64DecodedData, forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        request.httpBody = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        
        let task = session.dataTask(with: request as URLRequest) { data, response, error in
            guard data != nil else {
                print("no data found: \(error)")
                return
            }
            
            do {
                print("entro al do.....")
                if let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    print("Response: \(json)")
                    ViewUtilidades.hiddenLoader(controller: self)
                    
                    let alert = UIAlertController(title: "Veris", message: "Guardado con exito!", preferredStyle: UIAlertControllerStyle.alert)
                    let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                        UIAlertAction in
                        self.dismiss(animated: false, completion: nil)
                        self.dismiss(animated: false, completion: nil)
                        self.dismiss(animated: false, completion: nil)
                    }
                    alert.addAction(okAction)
                    self.present(alert, animated: true, completion: nil)
                } else {
                    let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)// No error thrown, but not NSDictionary
                    ViewUtilidades.hiddenLoader(controller: self)
                    
                    print("Error could not parse JSON: \(jsonStr)")
                }
            } catch let parseError {
                print(parseError)// Log the error thrown by `JSONObjectWithData`
                let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("Error could not parse JSON: '\(jsonStr)'")
            }
        }
        
        task.resume()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if(textField == txt_parentesco){
            self.hideKeyboard()
            
        }
        return true
    }
    
    func hideKeyboard(){
        print("hidekeyboard")
        self.txt_parentesco.resignFirstResponder()
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //print("entro a editar" + textField.description)
        
        if (textField == txt_parentesco){
            pickerParentesco.showPicker(controller: self)
            pickerSelected = pickerParentesco
            txt_parentesco.resignFirstResponder()
        }
    }

    func cancelPicker(){
        pickerSelected.hidePicker()
        txt_parentesco.resignFirstResponder()
        
    }
    
    
    //
    func acceptPicker(){
        pickerSelected.hidePicker()
            txt_parentesco.text = dataParentesco[pickerParentesco.eleccion]
            txt_parentesco.resignFirstResponder()
            //self.getCiudades()
        
    }
    
    func goInit(){
        dismiss(animated: false, completion: {print("entro3")})
    }
}
