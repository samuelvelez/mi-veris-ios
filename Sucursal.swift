//
//  Sucursal.swift
//  Mi Veris
//
//  Created by Jorge on 04/02/15.
//  Copyright (c) 2015 Firewall Soluciones All rights reserved.
//

import UIKit

// MARK: Sucursal: Clase Sucursal

class Sucursal: NSObject {
   
    var idSucursal : NSInteger = 0
    var idEmpresa : NSInteger = 0
    var descripcion : String = ""
    var direccion : String = ""
    var telefono : String = ""
    var horario : String = ""
    var horarioLaboratorio : String = ""
    var region : NSInteger = 0
    var descripcion2 : String = ""
    var idciudad : String = ""
    var ciudad : String = ""
    var latitud : String = ""
    var longitud : String = ""
    var especialidades : String = ""
    var complementarios : String = ""
    var mailcontacto : String = ""
    var telefono2 : String = ""
    var codigoregion : String = ""
    var urlMapa : String = ""
    
    
}
