//
//  CalificarCitaViewController.swift
//  veris
//
//  Created by Felipe Lloret on 07/02/15.
//  Copyright (c) 2015 Felipe Lloret. All rights reserved.
//

import UIKit

class CalificarCitaViewController: UIViewController, UITextFieldDelegate {
    
    var contentView: UIView = UIView()
    
    var calificarImage: UIImageView = UIImageView()
    var puntuacionLabel: UILabel = UILabel()
    var mensajeLabel: UILabel = UILabel()
    var calificarSlider: UISlider = UISlider()
    var observacionTextField: UITextField = UITextField()
    
    var calificarButton: UIButton = UIButton()
    var salirButton: UIButton = UIButton()
    
    var codigoRecetaString: String?
    var puntuacionString: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width;
        
        let dimBackground: UIImageView = UIImageView()
        dimBackground.frame = CGRect(x:0.0, y:0.0, width:400.0, height:600.0)
        dimBackground.image = UIImage(named: "DimBackground")
        dimBackground.backgroundColor = UIColor.clear
        dimBackground.alpha = 0.3
        self.view.addSubview(dimBackground)
        
        contentView.frame = CGRect(x:30.0, y:45.0, width:screenWidth - 60.0,height: 510.0)
        contentView.layer.cornerRadius = 15
        contentView.layer.masksToBounds = true
        contentView.backgroundColor = UIColor.white
        self.view.addSubview(contentView)
        
        let headerLabel: UILabel = UILabel()
        headerLabel.frame = CGRect(x:0.0, y:0.0, width:self.contentView.frame.width,height: 65.0)
        headerLabel.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        headerLabel.text = "CALIFICA LA CITA"
        headerLabel.textAlignment = NSTextAlignment.center
        headerLabel.textColor = UIColor.white
        headerLabel.font = UIFont(name: "helvetica-bold", size: 16.0)
        self.contentView.addSubview(headerLabel)
        
        let backButton: UIButton = UIButton()
        backButton.frame = CGRect(x:0.0, y:15.0, width:40.0, height:40.0)
        backButton.setImage(UIImage(named: "ArrowBackWhite"), for: UIControlState.normal)
        backButton.addTarget(self, action: #selector(CalificarCitaViewController.closeModalWindow), for: UIControlEvents.touchUpInside)
        self.contentView.addSubview(backButton)
        
        let faceImageOne: UIImageView = UIImageView()
        faceImageOne.frame = CGRect(x:45.0, y:67.0, width:40.0, height:40.0)
        faceImageOne.image = UIImage(named: "Votacion0")
        self.contentView.addSubview(faceImageOne)
        
        let faceImageTwo: UIImageView = UIImageView()
        faceImageTwo.frame = CGRect(x:120.0, y:67.0, width:40.0, height:40.0)
        faceImageTwo.image = UIImage(named: "Votacion1")
        self.contentView.addSubview(faceImageTwo)
        
        let faceImageThree: UIImageView = UIImageView()
        faceImageThree.frame = CGRect(x:195.0, y:67.0, width:40.0, height:40.0)
        faceImageThree.image = UIImage(named: "Votacion3")
        self.contentView.addSubview(faceImageThree)
        
        calificarImage.frame = CGRect(x:70.0, y:130.0, width:120.0, height:120.0)
        calificarImage.image = UIImage(named: "Votacion3")
        self.contentView.addSubview(calificarImage)
        
        puntuacionLabel.frame = CGRect(x:30.0, y:250.0, width:45.0, height:45.0)
        puntuacionLabel.font = UIFont(name: "helvetica-bold", size: 30.0)
        puntuacionLabel.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        self.contentView.addSubview(puntuacionLabel)
        
        mensajeLabel.frame = CGRect(x:65.0, y:263.0, width:280.0, height:20.0)
        mensajeLabel.font = UIFont(name: "helvetica-bold", size: 18.0)
        self.contentView.addSubview(mensajeLabel)
        
        let calificarNumbers: UIImageView = UIImageView()
        calificarNumbers.frame = CGRect(x:15.0, y:320.0, width:self.contentView.frame.width - 20.0, height:48.0)
        calificarNumbers.image = UIImage(named: "CalificarNumbers")
        self.contentView.addSubview(calificarNumbers)
        
        calificarSlider.frame = CGRect(x:15.0, y:284.0, width:self.contentView.frame.width - 20.0, height: 31.0)
        calificarSlider.minimumValue = 1
        calificarSlider.maximumValue = 10
        calificarSlider.addTarget(self, action: #selector(calificarSliderChange(sender:)), for: UIControlEvents.valueChanged)
        self.contentView.addSubview(calificarSlider)
        
        observacionTextField.frame = CGRect(x:13.0, y:365.0, width:self.contentView.frame.width - 25.0, height:85.0)
        observacionTextField.placeholder = "Agregar Observación"
        observacionTextField.delegate = self
        observacionTextField.textAlignment = NSTextAlignment.center
        let blackColor: UIColor = UIColor.black
        observacionTextField.layer.borderColor = blackColor.cgColor
        observacionTextField.layer.borderWidth = 1
        self.contentView.addSubview(observacionTextField)
        
        calificarButton.frame = CGRect(x:20.0, y:self.contentView.frame.height - 50.0, width:self.contentView.frame.width - 40.0, height:40.0)
        calificarButton.backgroundColor = UIColor(red: (11.0/256.0), green: (166.0/256.0), blue: (30.0/256.0), alpha: 1)
        calificarButton.setTitle("CALIFICAR", for: UIControlState.normal)
        calificarButton.setTitleColor(UIColor.white, for: UIControlState.normal)
        calificarButton.titleLabel!.font = UIFont(name: "helvetica-bold", size: 12.0)
        calificarButton.layer.cornerRadius = 15
        calificarButton.layer.masksToBounds = true
        calificarButton.addTarget(self, action: #selector(CalificarCitaViewController.calificar), for: UIControlEvents.touchUpInside)
        self.contentView.addSubview(calificarButton)

        self.view.backgroundColor = UIColor.clear
//        var floatValue: Float = NSString(string: self.puntuacionString!).floatValue
        let floatValue: Float = 10
        
        //Valor inicial para el slider
        self.calificarSlider.value = floatValue
        if (self.puntuacionString != "0") {
            self.puntuacionLabel.text = self.puntuacionString
        } else {
            self.puntuacionLabel.text = "10"
            self.puntuacionString = "10"
        }
        
        //Inicializamos la iamgen de puntuación
        self.changeEmoji(currentValue: floatValue)
    }
    
    func calificarSliderChange(sender: UISlider) {
        let currentValue = Int(sender.value)
        
        self.puntuacionLabel.text = "\(currentValue)"
        self.puntuacionString = "\(currentValue)"

        //Llamamos a la función que maneja las imágenes de calificación
        self.changeEmoji(currentValue: Float(currentValue))
    }
    
    func changeEmoji(currentValue: Float) {
        switch currentValue {
        case 0, 1, 2, 3, 4, 5, 6:
            self.mensajeLabel.text = "Nada recomendado"
            self.calificarImage.image = UIImage(named: "Votacion0")
        case 7, 8:
            self.mensajeLabel.text = "Me es indiferente"
            self.calificarImage.image = UIImage(named: "Votacion1")
        case 9:
            self.calificarImage.image = UIImage(named: "Votacion2")
        default:
            self.mensajeLabel.text = "Muy recomendado"
            self.calificarImage.image = UIImage(named: "Votacion3")
        }
    }
    
    func calificar() {
        //sendAnalytics(categoria: "Opciones Menu MiVeris", action: "Abrir", label: "Familia y Amigos")
        Usuario.sendAnalytics(categoria: "Opciones Menu MiVeris", action: "Abrir", label: "Calificar")
        let comentario = self.observacionTextField.text!.addingPercentEscapes(using: String.Encoding.utf8)
        //stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
        
        let urldef = URLFactory.urldefinida()

        let url = NSURL(string: "\(urldef)servicio/agenda/insertapuntaje?arg0=\(self.codigoRecetaString!)&arg1=9999&arg2=\(self.puntuacionString!)&arg3=\(comentario!)")
        //let request = NSURLRequest(URL: url!)
        print("url a enviar: \(url)")
        let request = NSMutableURLRequest(url: url! as URL)
        
        let str = "wsappusuario:ZA$57@9b86@$2r5"
        
        //let utf8str = str.dataUsingEncoding(NSUTF8StringEncoding)
        
        /*if let base64Encoded = utf8str?.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0)){
            
            print("Encoded:  \(base64Encoded)")
            
            ////request.addValue("Authorization", forHTTPHeaderField: "Basic \(base64Encoded)")
            request.setValue("Basic \(base64Encoded)", forHTTPHeaderField: "Authorization")
            //request.setValue("Basic \(base64Encoded)", forHTTPHeaderField: "Authorization")
            
            if let base64Decoded = NSData(base64EncodedString: base64Encoded, options:   NSDataBase64DecodingOptions(rawValue: 0))
                .map({ NSString(data: $0, encoding: NSUTF8StringEncoding) })
            {
                // Convert back to a string
                print("Decoded:  \(base64Decoded)")
            }
        }*/
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: "Ocurrio un error", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            }}
        self.dismiss(animated: true, completion: {})
    }
    
    func closeModalWindow() {
        self.dismiss(animated: true, completion: {})
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        animateViewMoving(up: true, moveValue: 200)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        animateViewMoving(up: false, moveValue: 200)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()        
        return true
    }
    
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement)
        UIView.commitAnimations()
    }
}

