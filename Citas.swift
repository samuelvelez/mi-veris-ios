//
//  MisCitasModel.swift
//  veris
//
//  Created by Felipe Lloret on 04/02/15.
//  Copyright (c) 2015 Felipe Lloret. All rights reserved.
//

import Foundation

class MisCitasModel: NSObject {
    let fecha: String
    let nombresucursal: String
    let prestacion: String
    let medico: String
    let puntuacion: String
    let reserva: String /*codigo de la reserva*/
    
    let codigoespecialidad : String
    let codigomedico : String
    let codigoempresa : String
    let codigosucursal : String
    let codigoregion : String
    
    let codigoprestacion :String
    let codigoempresaprestacion: String
    let nombreprestacion : String
    let nombrePaciente : String
    let usuario : Usuario
    let espagada : String
    
    override var description: String {
        return "fecha: \(fecha), nombresucursal: \(nombresucursal)\n, especialidad: \(prestacion)\n, medico: \(medico)\n, puntuacion: \(puntuacion)\n, reserva: \(reserva)\n, servicio: \(nombreprestacion), paciente: \(nombrePaciente), pagada: \(espagada)"
    }

    init(fecha: String?, nombresucursal: String?, prestacion: String?, medico: String?, puntuacion: String?, reserva: String?, codigoespecialidad: String?, codigomedico: String?, codigoempresa: String?, codigosucursal: String?, codigoregion: String?, codigoprestacion: String?, codigoempresapres: String?, nombreprestacion: String?, nombrePaciente: String?, usuario: Usuario?, espagada: String?) {
        self.fecha = fecha ?? ""
        self.nombresucursal = nombresucursal ?? ""
        self.prestacion = prestacion ?? ""
        self.medico = medico ?? ""
        self.puntuacion = puntuacion ?? ""
        self.reserva = reserva ?? ""
        
        self.codigoespecialidad = codigoespecialidad ?? ""
        self.codigomedico = codigomedico ?? ""
        self.codigoempresa = codigoempresa ?? ""
        self.codigosucursal = codigosucursal ?? ""
        self.codigoregion = codigoregion ?? ""
        /*cambio 23 junio 2015*/
        self.codigoprestacion = codigoprestacion ?? ""
        self.codigoempresaprestacion = codigoempresapres ?? ""
        self.nombreprestacion = nombreprestacion ?? ""
        /*fin cambio 23 junio 2015*/
        
        /** cambio con familia */
        self.nombrePaciente = nombrePaciente ?? ""
        self.usuario = usuario ?? Usuario()
        
        /*ver citas pagadas */
        self.espagada = espagada ?? ""
        
    }
}
