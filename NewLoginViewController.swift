//
//  NewLoginViewController.swift
//  Mi Veris
//
//  Created by Samuel Velez on 1/5/16.
//  Copyright © 2016 Programadores-iOS.net. All rights reserved.
//

import UIKit
import CryptoSwift

class NewLoginViewController: UIViewController, UITextFieldDelegate  {

    var scroll : UIScrollView = UIScrollView()
    var band : String?
    var txtPass : UITextField!
    var txtUsuario : UITextField!
    var listaUsuarios : NSMutableArray = NSMutableArray()
    var resultado : NSString = ""
    var token : NSString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(NewLoginViewController.hideKeyboard))
        scroll.addGestureRecognizer(singleTap)
        self.view.addSubview(scroll)
        
        self.navigationItem.title = ""
        
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        scroll.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        scroll.contentSize = CGSize(width: 0, height: 568) // 0, 568)
        scroll.backgroundColor = UIColor.white
        self.view.addSubview(scroll)
        
        let barWhite : UIView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width ,height:  30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width / 1.6, height: 30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x: barBlue.frame.width, y: 64, width: 45, height:30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 20, height: 30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "LOGIN"
        
        scroll.addSubview(barWhite)
        scroll.addSubview(barBlue)
        scroll.addSubview(img)
        scroll.addSubview(title)
        
        let image = UIImageView(frame: CGRect(x: 0, y: 30, width: self.view.frame.width, height: 135))
        image.backgroundColor = UIColor.gray
        image.image = UIImage(named: "loginbanner.png")
        scroll.addSubview(image)
        
        let etiqueta = UILabel(frame: CGRect(x: 20, y: 166, width: self.view.frame.width - 40, height: 20))
        etiqueta.font = UIFont(name: "helvetica-bold", size: 14)
        etiqueta.text = "Inicio de sesión en mi cuenta Veris"
        scroll.addSubview(etiqueta)
        
        let lbUsuario = UILabel(frame: CGRect(x: 30, y: 196, width: self.view.frame.width - 40, height: 20))
        lbUsuario.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        lbUsuario.text = "Identificación"
        scroll.addSubview(lbUsuario)
        
        txtUsuario = UITextField(frame: CGRect(x: 10, y: 221, width: self.view.frame.width - 20, height: 40))
        txtUsuario.layer.cornerRadius = 20
        txtUsuario.textAlignment = NSTextAlignment.center
        txtUsuario.delegate = self
        txtUsuario.placeholder = "Cédula / Pasaporte"
        txtUsuario.layer.borderColor = UIColor.lightGray.cgColor
        txtUsuario.layer.borderWidth = 1
        txtUsuario.autocorrectionType = UITextAutocorrectionType.no
        txtUsuario.addTarget(self, action: #selector(NewLoginViewController.upView), for: UIControlEvents.editingDidBegin)
        txtUsuario.addTarget(self, action: #selector(NewLoginViewController.downView), for: UIControlEvents.editingDidEndOnExit)
        scroll.addSubview(txtUsuario)
        
        let lbPass = UILabel(frame: CGRect(x: 30, y: 271, width: self.view.frame.width - 40, height: 20))
        lbPass.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        lbPass.text = "Clave"
        scroll.addSubview(lbPass)
        
        txtPass = UITextField(frame: CGRect(x: 10, y: 296, width: self.view.frame.width - 20, height: 40))
        txtPass.layer.cornerRadius = 20
        txtPass.textAlignment = NSTextAlignment.center
        txtPass.delegate = self
        txtPass.placeholder = "Contraseña"
        txtPass.isSecureTextEntry = true
        txtPass.layer.borderColor = UIColor.lightGray.cgColor
        txtPass.layer.borderWidth = 1
        txtPass.addTarget(self, action: #selector(NewLoginViewController.upView), for: UIControlEvents.editingDidBegin)
        txtPass.addTarget(self, action: #selector(NewLoginViewController.downView), for: UIControlEvents.editingDidEndOnExit)
        scroll.addSubview(txtPass)
        
        let btAceptar = UIButton(frame: CGRect( x: (self.view.frame.width / 2) - 85 , y: 360,  width: 170, height: 50))
        btAceptar.setTitle("Iniciar Sesión", for: UIControlState.normal)
        btAceptar.addTarget(self, action: #selector(NewLoginViewController.login), for: UIControlEvents.touchUpInside)
        btAceptar.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        btAceptar.layer.cornerRadius = 25
        scroll.addSubview(btAceptar)
        
        
        let btCrearCuenta = UIButton(frame: CGRect( x: 30, y: 518 - 64, width: (self.view.frame.width / 2) - 40 , height: 30))
        btCrearCuenta.setTitle("Crear Cuenta Veris", for: UIControlState.normal)
        btCrearCuenta.titleLabel!.font = UIFont(name: "Helvetica", size: 12);
        btCrearCuenta.addTarget(self, action: #selector(NewLoginViewController.createAccount), for: UIControlEvents.touchUpInside)
        //btCrearCuenta.backgroundColor = UIColor.grayColor()
        btCrearCuenta.setTitleColor(UIColor.black, for: UIControlState.normal)
        btCrearCuenta.setTitleColor(UIColor.gray, for: UIControlState.highlighted)
        scroll.addSubview(btCrearCuenta)
        
        let btResetearClave = UIButton(frame: CGRect(x: (self.view.frame.width / 2) + 10 , y: 518 - 64,  width: (self.view.frame.width / 2) - 40 , height:30))
        btResetearClave.setTitle("Resetear Clave", for: UIControlState.normal)
        btResetearClave.titleLabel!.font = UIFont(name: "Helvetica", size: 12);
        btResetearClave.addTarget(self, action: #selector(NewLoginViewController.resetPassword), for: UIControlEvents.touchUpInside)
        //btResetearClave.backgroundColor = UIColor.grayColor()
        btResetearClave.setTitleColor(UIColor.black, for: UIControlState.normal)
        btResetearClave.setTitleColor(UIColor.gray, for: UIControlState.highlighted)
        scroll.addSubview(btResetearClave)
        
}
    
    func upView(){
        let transitionOptions = UIViewAnimationOptions.showHideTransitionViews//transitionNone
        UIView.transition(with: self.view, duration: 0.2, options: transitionOptions, animations: {
            self.view.frame = CGRect(x: 0, y: -120, width: self.view.frame.width, height: self.view.frame.height)
            }, completion: { finished in
                
        })
    }
    
    func downView(){
        let transitionOptions = UIViewAnimationOptions.showHideTransitionViews//TransitionNone
        UIView.transition(with: self.view, duration: 0.2, options: transitionOptions, animations: {
            self.view.frame = CGRect(x: 0, y:0, width: self.view.frame.width, height: self.view.frame.height)
            }, completion: { finished in
                
        })
    }
    
    func login(){
        txtUsuario.resignFirstResponder()
        txtPass.resignFirstResponder()
        let transitionOptions = UIViewAnimationOptions.showHideTransitionViews//TransitionNone
        UIView.transition(with: self.view, duration: 0.2, options: transitionOptions, animations: {
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            }, completion: { finished in
                
        })
        
        if txtPass.text!.isEmpty || txtUsuario.text!.isEmpty {
            let alert = UIAlertController(title: "Veris", message: "Datos Incorrectos", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            ViewUtilidades.showLoader(controller: self, title: "Cargando...")
            
            
            let pass = txtPass.text!.sha1().lowercased()
            let usr = txtUsuario.text!
            var bool_codigo = true
            var idS : NSString!
            var tipoS : NSString!
            let urldef = URLFactory.urldefinida()

            let cadenaUrl = "\(urldef)servicio/login/login?arg0=\(usr)&arg1=\(pass)"
            let url = NSURL(string: cadenaUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)
            print ("url consultar:  \(url!)")
            //let request = NSURLRequest(URL: url!)
            let request = NSMutableURLRequest(url: url! as URL)
            
                    
            
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                request.timeoutInterval = 10
                if (error != nil) {
                    print(error!.localizedDescription)
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    do{
                      //  print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue))
                        
                        var err: NSError?
                        
                        let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        if (err != nil) {
                            print("JSON Error \(err!.localizedDescription)")
                        }
                        
                        let info =  jsonResult as NSDictionary
                        let data = info["lista"] as! [[String: Any]]
                        self.resultado = info["resultado"] as! NSString
                        if data.count > 0 {
                            let usuario = Usuario.getEntity as Usuario
                            for doctor in data {
                                
                                usuario.nombre = doctor["nombre"] as! NSString
                                usuario.numeroidentificacion = doctor["numeroidentificacion"] as! NSString
                                usuario.tipoid = doctor["tipoid"] as! NSString
                                //usuario.primeravez = doctor["primeravez"] as! NSString
                                let codCiudad = doctor["codigo_ciudad"] as! NSInteger
                                usuario.codigoCiudad = codCiudad
                                let codPais = doctor["codigo_pais"] as! NSInteger
                                usuario.codigoPais = codPais
                                let codProv = doctor["codigo_provincia"] as! NSInteger
                                usuario.codigoProvincia = codProv
                                let codReg = doctor["codigo_region"] as! NSInteger
                                usuario.codigoRegion = codReg
                                //usuario.aceptatermino = doctor["aceptatermino"] as NSString
                                if (doctor["aceptatermino"] is NSNull){
                                    usuario.aceptatermino = "N"
                                }else{
                                    usuario.aceptatermino = doctor["aceptatermino"] as! NSString
                                }
                                if(doctor["codigoActivacion"] is NSNull){
                                    print("no tiene codigo de activacion")
                                    bool_codigo = false
                                }else{
                                    print(doctor["codigoActivacion"])
                                    bool_codigo = true
                                }
                                tipoS = usuario.tipoid
                                idS = usuario.numeroidentificacion
                                
                               // self.obtenerCitas(tipo: tipoS as String, identificacion: idS as String)
                                
                                self.enviararegistrartoken(identificacion: usuario.numeroidentificacion as String, tipoid: usuario.tipoid as String)
                                // usuario.codigoToken = vtoken
                                self.listaUsuarios.add(usuario)
                            } 
                        }
                        
                        DispatchQueue.main.async {
                            ViewUtilidades.hiddenLoader(controller: self)
                            if self.resultado.isEqual(to: "ok") {
                                
                                //let usuario = self.listaUsuarios.objectAtIndex(0) as! Usuario
                                
                               // let primeravez = usuario.primeravez
                                //let aceptatermino = usuario.aceptatermino
                                if(bool_codigo){
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = storyboard.instantiateViewController(withIdentifier: "viewCodigo") as! IngresoCodigoViewController
                                    //vc.usuarios = usuario
                                    vc.tipo_id = tipoS
                                    vc.pass = pass as NSString!
                                    vc.identificacion = idS
                                    self.navigationController?.pushViewController(vc, animated: true)
                                    
                                    print("debe de ir a ingreso codigo view controller")
                                    

                                }else{
                                    print("LOGUEADO")
                                    let usuario = Usuario.getEntity as Usuario
                                    usuario.logueado = true
                                    //let root = self.navigationController?.viewControllers[0] as! ViewController
                                    //root.setPush = true
                                    //self.navigationController?.popToRootViewControllerAnimated(false)
                                    
                                    UserDefaults.standard.set(usuario.numeroidentificacion, forKey: "identificacion")
                                    UserDefaults.standard.synchronize()
                                    
                                    UserDefaults.standard.set(usuario.nombre, forKey: "nombre")
                                    UserDefaults.standard.synchronize()
                                    
                                    UserDefaults.standard.set(usuario.tipoid, forKey: "tipo")
                                    UserDefaults.standard.synchronize()
                                    
                                    UserDefaults.standard.set(usuario.codigoCiudad, forKey: "ciudad")
                                    UserDefaults.standard.synchronize()
                                    
                                    UserDefaults.standard.set(usuario.codigoPais, forKey: "pais")
                                    UserDefaults.standard.synchronize()
                                    
                                    UserDefaults.standard.set(usuario.codigoProvincia, forKey: "provincia")
                                    UserDefaults.standard.synchronize()
                                    
                                    UserDefaults.standard.set(usuario.codigoRegion, forKey: "region")
                                    UserDefaults.standard.set(usuario.codigoToken, forKey: "token")
                                    UserDefaults.standard.synchronize()
                                    
                                    if self.band != "mymenu"{
                                        let root = self.navigationController?.viewControllers[0] as! ViewController
                                        root.setPush = true
                                        self.navigationController?.popToRootViewController(animated: false)
                                    }else{
                                        print("algo salio mal")
                                        self.dismiss(animated: false, completion: nil)
                                        self.dismiss(animated: false, completion: nil)
                                    }
                                    
                                    
                                }
                               
                            } else {
                                
                                UtilidadesGeneral.mensaje(mensaje: self.resultado as String)
                                

                                
                            }
                        }
                    } catch {
                        ViewUtilidades.hiddenLoader(controller: self)
                        let alert = UIAlertController(title: "Veris", message: "Revise su conexión a Datos o Internet", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
            
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool // called when 'return' key pressed. return NO to ignore.
    {
        textField.resignFirstResponder()
        let transitionOptions = UIViewAnimationOptions.showHideTransitionViews
        UIView.transition(with: self.view, duration: 0.2, options: transitionOptions, animations: {
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            }, completion: { finished in
                
        })
        return true;
    }

    func hideKeyboard(){
        self.txtUsuario.resignFirstResponder()
        self.txtPass.resignFirstResponder()
        self.downView()
    }
    
    func createAccount(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "viewCrearCuenta") as! CreateViewController
        navigationController?.pushViewController(vc, animated: true)
 
    }
    
    func resetPassword(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "reseteoView") as! NewResetViewController
        navigationController?.pushViewController(vc, animated: true)
 
    }
    
    func enviararegistrartoken(identificacion : String, tipoid : String){
        var tok : String = ((UserDefaults.standard.object(forKey: "deviceToken") as AnyObject).description)!
        tok = tok.replacingOccurrences(of: "<", with: "")
        tok = tok.replacingOccurrences(of: ">", with: "")
        tok = tok.replacingOccurrences(of: " ", with: "")
        //tok = tok.stringByReplacingOccurrencesOfString("<", withString: "")
        //tok = tok.stringByReplacingOccurrencesOfString(">", withString: "")
        //tok = tok.stringByReplacingOccurrencesOfString(" ", withString: "")
        print(tok)
        UserDefaults.standard.set(tok, forKey: "deviceToken")
        //var codigo_tok : NSString = "6"
        let urldef = URLFactory.urldefinida()

        
        let cadenaUrl = "\(urldef)servicio/login/registrargcm?arg0=\(identificacion)&arg1=\(tipoid)&arg2=\(tok)&arg3=iOS"
        
        let url = NSURL(string: cadenaUrl )
        
        print(cadenaUrl)
        //let request = NSURLRequest(URL: url!)
        if url != nil {
            let request = NSMutableURLRequest(url: url! as URL)
            
            let str = "wsappusuario:ZA$57@9b86@$2r5"
            
            /*let utf8str = str.dataUsingEncoding(NSUTF8StringEncoding)
            
            if let base64Encoded = utf8str?.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0)){
                
                print("Encoded:  \(base64Encoded)")
                //request.addValue("Authorization", forHTTPHeaderField: "Basic \(base64Encoded)")
                request.setValue("Basic \(base64Encoded)", forHTTPHeaderField: "Authorization")
                
                if let base64Decoded = NSData(base64EncodedString: base64Encoded, options:   NSDataBase64DecodingOptions(rawValue: 0))
                    .map({ NSString(data: $0, encoding: NSUTF8StringEncoding) })
                {
                    // Convert back to a string
                    print("Decoded:  \(base64Decoded)")
                }
            }*/
            
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                
                if (error != nil) {
                    print(error!.localizedDescription)
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    do{
                        var err: NSError?
                        
                        let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        if (err != nil) {
                            print("JSON Error \(err!.localizedDescription)")
                        }
                        
                        print(jsonResult)
                        let info =  jsonResult as NSDictionary
                        //let val = false
                        //let data = info["lista"] as! NSArray
                        let resultado = info["resultado"] as! NSString
                        let codigo_token = info["texto"] as! NSString
                        //dispatch_async(DispatchQueue.main, {
                        DispatchQueue.main.async {
                            ViewUtilidades.hiddenLoader(controller: self)
                            print(resultado)
                            print("obtener las citas")
                            
                            if resultado.isEqual(to: "ok") {
                                self.token = codigo_token
                                let us = Usuario.getEntity as Usuario
                                us.codigoToken = codigo_token
                                self.obtenerCitas(tipo: us.tipoid as String, identificacion: us.numeroidentificacion as String)
                                //codigo_tok = codigo_token
                                //return codigo_token
                                
                                
                            } else {
                                let alert = UIAlertController(title: "Veris", message: "error", preferredStyle: UIAlertControllerStyle.alert)
                                let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                                    UIAlertAction in
                                    
                                }
                                alert.addAction(okAction)
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                        }
                    } catch {
                        print("ERROR")
                        ViewUtilidades.hiddenLoader(controller: self)
                        let alert = UIAlertController(title: "Veris", message: "Revise su conexión a Datos o Internet", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
            
        } else {
            ViewUtilidades.hiddenLoader(controller: self)
            let alert = UIAlertController(title: "Veris", message: "Ha ocurrido un error. Intente nuevamente", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                UIAlertAction in
                
            }
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }

       
    }
    
    func obtenerCitas(tipo : String , identificacion : String) {
        let urlAsString = URLFactory.obtenercitasvigentes(tipoidentificacion: tipo, identificacion: identificacion, esadmin: "S", usua_logeado: identificacion)
        print(urlAsString.absoluteString!)
        self.getJSONData(url: urlAsString.absoluteString!)
    }
    
    func getJSONData(url: String) {
        
        let url = NSURL(string: url as String)
        //let request = NSURLRequest(URL: url!)
        print("ver   que url esta enviando")
        print(url)
        let request = NSMutableURLRequest(url: url! as URL)
        
        let str = "wsappusuario:ZA$57@9b86@$2r5"
        
    
        let utf8str = str.data(using: String.Encoding.utf8)
        
        let base64Encoded = str.data(using: String.Encoding.utf8, allowLossyConversion: true)
        //let base64Encoded = str.dataUsingEncoding(NSUTF8StringEncoding)!.base64EncodedStringWithOptions([])
        print("Encoded: \(base64Encoded)")
        let base64DecodedData = NSData(base64Encoded: base64Encoded!, options: [])
        //NSData(base64EncodedString: base64Encoded, options: [])!
        //var base64DecodedString = String(data: base64DecodedData as! Data, encoding: String.Encoding.utf8)!
        //print("Decoded: \(base64DecodedString)")
       /* if let base64Encoded = utf8str?.base64EncodedStringWithOptions(NSData.Base64EncodingOptions(rawValue: 0)){
            // print("getJsonData()")
            //print("Encoded:  \(base64Encoded)")
            //request.addValue("Authorization", forHTTPHeaderField: "Basic \(base64Encoded)")
            request.setValue("Basic \(base64Encoded)", forHTTPHeaderField: "Authorization")
            
            if let base64Decoded = NSData(base64EncodedString: base64Encoded, options:   NSDataBase64DecodingOptions(rawValue: 0))
                .map({ NSString(data: $0, encoding: NSUTF8StringEncoding) })
            {
                // Convert back to a string
                print("Decoded:  \(base64Decoded)")
            }
        }
        */
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                let json = JSON(data: data!)
                
                //Comprobamos si todo ha salido bien, en caso contrario se le muestra el mensaje de error al usuario
                if json["resultado"].string == "ok" {
                    if let citasArray = json["lista"].array {
                        
                        //Creamos nuestro objeto Citas con los datos obtenidos del JSON
                        for citasD in citasArray {
                            
                            var npaciente = "\(citasD["primerNombre"]) \(citasD["segundoNombre"]) \(citasD["primerApellido"]) \(citasD["segundoApellido"])"
                            //npaciente = npaciente.stringByReplacingOccurrencesOfString("null", withString: "", options: NSString.CompareOptions.LiteralSearch, range: nil)
                            npaciente = npaciente.replacingOccurrences(of: "null", with: "")
                            let newUser = Usuario()
                            newUser.numeroidentificacion = citasD["numeroIdentificacion"].stringValue as NSString
                            newUser.nombre = npaciente as NSString
                            newUser.tipoid = citasD["tipoIdentificacion"].stringValue as NSString
                            var user_go : Usuario
                            let user_log = Usuario.getEntity as Usuario
                            if (newUser.numeroidentificacion != user_log.numeroidentificacion){
                                user_go = newUser
                            }else{
                                user_go = user_log
                            }
                            //let arr : Array = citasD["citasVigentes"].array!;
                            
                            /*for citasDic in arr {
                            let citaFecha: String? = citasDic["fecha"].stringValue
                            let citaSucursal: String? = citasDic["nombresucursal"].stringValue
                            let citaPrestacion: String? = citasDic["nombreespecialidad"].stringValue
                            let citaMedico: String? = citasDic["medico"].stringValue
                            let citaPuntuacion: String? = citasDic["puntaje"].stringValue
                            let citaReserva: String? = citasDic["codigoreserva"].stringValue
                            
                            let hcodigoespecialidad : String? = citasDic["codigoespecialidad"].stringValue
                            let hcodigomedico : String? = citasDic["codigomedico"].stringValue
                            let hcodigoempresa : String? = citasDic["codigoempresa"].stringValue
                            let hcodigosucursal : String? = citasDic["codigosucursal"].stringValue
                            let hcodigoregion : String? = citasDic["codigoregion"].stringValue
                            /*cambio 23 junio*/
                            let hcodigoprestacion :String? = citasDic["codigoprestacion"].stringValue
                            let hcodigoempresaprestacion :String? = citasDic["codigoempresaprestacion"].stringValue
                            let hnombreprestacion : String? = citasDic["prestacion"].stringValue
                            
                            //let cita = MisCitasModel(fecha: citaFecha, nombresucursal: citaSucursal, prestacion: citaPrestacion, medico: citaMedico, puntuacion: citaPuntuacion, reserva: citaReserva, codigoespecialidad: hcodigoespecialidad, codigomedico: hcodigomedico, codigoempresa: hcodigoempresa, codigosucursal: hcodigosucursal, codigoregion: hcodigoregion,codigoprestacion: hcodigoprestacion, codigoempresapres: hcodigoempresaprestacion, nombreprestacion: hnombreprestacion, nombrePaciente: "", usuario: user_log)
                            //let not : notificacion = notificacion()
                            
                            //not.addItem(cita.fecha)
                            }*/
                            //self.citas.append(cita)
                            
                            /*Fin cambio 23 junio*/
                        }
                        //Uso para debug
                        //println(self.citas)
                        
                    }
                } else {
                    let jsonError = json["resultado"].string
                    print(jsonError)
                }
                
                DispatchQueue.main.async {
                    ViewUtilidades.hiddenLoader(controller: self)
                }
            }
            
            
        }
        
        
    }



}
