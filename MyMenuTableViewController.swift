//
//  MyMenuTableViewController.swift
//  Mi Veris
//
//  Created by Samuel Velez on 26/7/16.
//  Copyright © 2016 Programadores-iOS.net. All rights reserved.
//
import UIKit

class MyMenuTableViewController:  UITableViewController {
    var selectedMenuItem : Int = 0
    var items:[String] = ["Familia y Amigos","Sincronización Salud Apple", "Contáctanos", "Ubícanos" , "Salir"]; //"Familia y Amigos",
    var imagenes : [String] = ["ic_group_36pt_2x.png", "ic_sync_3x.png", "ic_phone_in_talk_3x.png", "ic_room_3x.png", "ic_exit_to_app_36pt_2x.png"];
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Customize apperance of table view
        tableView.contentInset = UIEdgeInsetsMake(64.0, 0, 0, 0) //
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor.clear
        tableView.scrollsToTop = true
        
        // Preserve selection between presentations
        //tableView.clearsSelectionOnViewWillAppear = true
       
        tableView.register(MenuTableViewCell.self as AnyClass, forCellReuseIdentifier: "MenuCell");
        //tableView.selectRowAtIndexPath(NSIndexPath(forRow: selectedMenuItem, inSection: 0), animated: false, scrollPosition: .Middle)
        //self.view.addSubview(tableView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath as IndexPath) as! MenuTableViewCell
            
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            cell.icono.image = UIImage(named: imagenes[indexPath.row])
            cell.title.text = items[indexPath.row]
            
            
            return cell
    }
    /*
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath as IndexPath) as! MenuTableViewCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        cell.icono.image = UIImage(named: imagenes[indexPath.row])
        cell.title.text = items[indexPath.row]
        
        
        return cell
    }
    */
   /* override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("CELL")
        
        if (cell == nil) {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "CELL")
            cell!.backgroundColor = UIColor.clearColor()
            cell!.textLabel?.textColor = UIColor.darkGrayColor()
            let selectedBackgroundView = UIView(frame: CGRectMake(0, 0, cell!.frame.size.width, cell!.frame.size.height))
            selectedBackgroundView.backgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.2)
            cell!.selectedBackgroundView = selectedBackgroundView
        }
        
        cell!.textLabel?.text = items[indexPath.row]//"ViewController #\(indexPath.row+1)"
        
        return cell!
    }
 */
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    func sendAnalytics(categoria : String , action: String , label : String){
        let tracker = GAI.sharedInstance().defaultTracker
        tracker?.set(kGAIScreenName, value: "Inicio")
        
        //let builder = GAIDictionaryBuilder.createScreenView()
        tracker?.send(GAIDictionaryBuilder.createEvent(withCategory: categoria, action: action, label: label, value: nil).build() as [NSObject : AnyObject])
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        
        if indexPath.row == 0{
            let usuario = Usuario.getEntity as Usuario
            if usuario.logueado {
                sendAnalytics(categoria: "Opciones Menu MiVeris", action: "Abrir", label: "Familia y Amigos")
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "FamiliaView") as! FamiliaViewController
                 
                 viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Atrás", style: UIBarButtonItemStyle.plain, target: self, action: #selector(MyMenuTableViewController.goBack))
                 // Creating a navigation controller with viewController at the root of the navigation stack.
                 let navController = UINavigationController(rootViewController: viewController)
                 self.present(navController, animated:true, completion: nil)
                
            } else {
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "viewLogin") as! LoginViewController
                 viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Atrás", style: UIBarButtonItemStyle.plain, target: self, action: #selector(MyMenuTableViewController.goBack))
                 viewController.band = "mymenu"
                 let navController = UINavigationController(rootViewController: viewController)
                 self.present(navController, animated: true, completion: nil)
                
            }
        }
        if indexPath.row == 1{
            let usuario = Usuario.getEntity as Usuario
            if usuario.logueado {
                sendAnalytics(categoria: "Opciones Menu MiVeris", action: "Abrir", label: "Sincronización Apple Health")
                
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "MasterHealth") as! MasterHealthViewController
                 
                 viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Atrás", style: UIBarButtonItemStyle.plain, target: self, action: #selector(MyMenuTableViewController.goBack))
                 // Creating a navigation controller with viewController at the root of the navigation stack.
                 let navController = UINavigationController(rootViewController: viewController)
                 self.present(navController, animated:true, completion: nil)
                print("agergar")
                /*
                 let storyboard = UIStoryboard(name: "Main", bundle: nil)
                 let vc = storyboard.instantiateViewControllerWithIdentifier("FamiliaView") as! FamiliaViewController
                 
                 vc.navigationItem.hidesBackButton = false
                 
                 self.navigationController?.pushViewController(vc, animated: true)*/
                
            } else {
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "viewLogin") as! LoginViewController
                
                viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Atrás", style: UIBarButtonItemStyle.plain, target: self, action: #selector(MyMenuTableViewController.goBack))
                 // Creating a navigation controller with viewController at the root of the navigation stack.
                 let navController = UINavigationController(rootViewController: viewController)
                 self.present(navController, animated:true, completion: nil)
                //let root = mainStoryboard.instantiateViewControllerWithIdentifier("viewLogin") as! LoginViewController
                //root.setPush = true
                //self.navigationController?.popToRootViewControllerAnimated(false)
                /*let alert = UIAlertController(title: "Veris", message: "Debe de iniciar sesión con su cuenta para poder acceder a esta opción", preferredStyle: UIAlertControllerStyle.alert)
                // UIAlertActionStyle.default
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)*/
                
            }
        }
        else if indexPath.row == 2{
            sendAnalytics(categoria: "Opciones Menu MiVeris", action: "Abrir", label: "Contáctanos")
            
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "viewContactos") as! ContactosViewController
            
            viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Atrás", style: UIBarButtonItemStyle.plain, target: self, action: #selector(MyMenuTableViewController.goBack))
            // Creating a navigation controller with viewController at the root of the navigation stack.
            let navController = UINavigationController(rootViewController: viewController)
            self.present(navController, animated:true, completion: nil)
            print("agregar")
        }
        else if indexPath.row == 3 {
            sendAnalytics(categoria: "Opciones Menu MiVeris", action: "Abrir", label: "Ubícanos")
            
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "viewDondeEstamos") as! DondeEstamosViewController
             
             viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Atrás", style: UIBarButtonItemStyle.plain, target: self, action: "goBack")
             // Creating a navigation controller with viewController at the root of the navigation stack.
             let navController = UINavigationController(rootViewController: viewController)
             self.present(navController, animated:true, completion: nil)
            
            
        } else if indexPath.row == 4 {
            print("salir")
            let usuario = Usuario.getEntity as Usuario
            
            if usuario.logueado {
                self.showAlert()
            } else {
                print("no ha iniciado sesion ")
            }
        }/*else if indexPath.row == 4 {
             print("salir")
             let usuario = Usuario.getEntity as Usuario
             
             if usuario.logueado {
             self.showAlert()
             } else {
             print("no ha iniciado sesion ")
             }
             
             }*/
            
        else{
            print("toco fuera")
        }

        tableView.deselectRow(at: indexPath as IndexPath, animated: false);
        
    }
    
   
    func goBack(){
        dismiss(animated: true, completion: nil)
    }
    
    func showAlert(){
        let alert = UIAlertController(title: "Veris", message: "¿Esta seguro de cerrar sesión?", preferredStyle: UIAlertControllerStyle.alert)
        
        let user = Usuario.getEntity as Usuario
        print("usuario: \(user.numeroidentificacion) token: \(user.codigoToken)")
        let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
            UIAlertAction in
            Usuario.deleteLogin()
            var parent = Usuario.getParent as Usuario
            parent = Usuario()
            var usuario = Usuario.getEntity as Usuario
            usuario = Usuario()
            //let not : notificacion = notificacion()
            //not.borrarTodo()
            
            UserDefaults.standard.set("", forKey: "identificacion")
            //UserDefaults.standard.set(newValue as [NSString], forKey: "food")
            UserDefaults.standard.synchronize()
            
            UserDefaults.standard.set("", forKey: "nombre")
            //UserDefaults.standard.set(newValue as [NSString], forKey: "food")
            UserDefaults.standard.synchronize()
            
            UserDefaults.standard.set("", forKey: "tipo")
            UserDefaults.standard.synchronize()
            
            UserDefaults.standard.set(0, forKey: "ciudad")
            UserDefaults.standard.synchronize()
            
            UserDefaults.standard.set(0, forKey: "region")
            UserDefaults.standard.synchronize()
            
            UserDefaults.standard.set(0, forKey: "pais")
            UserDefaults.standard.synchronize()
            
            UserDefaults.standard.set(0, forKey: "provincia")
            
            UserDefaults.standard.set(nil, forKey: "sexokit")
            UserDefaults.standard.set(nil, forKey: "sangrekit")
            UserDefaults.standard.set(nil, forKey: "fechakit")
            UserDefaults.standard.set(nil, forKey: "alturakit")
            UserDefaults.standard.set(nil, forKey: "pesokit")
            UserDefaults.standard.set(nil, forKey: "imckit")
            UserDefaults.standard.set(nil, forKey: "contadorkit")
            UserDefaults.standard.set(nil, forKey: "drecorridakit")
            UserDefaults.standard.set(nil, forKey: "eqrkit")
            UserDefaults.standard.set(nil, forKey: "eqakit")
            UserDefaults.standard.set(nil, forKey: "frecuenciakit")
            UserDefaults.standard.synchronize()
            
            print("token: \(user.codigoToken)")
            self.enviarquitartoken(codigo: user.codigoToken)
            //self.btLogout.enabled = false
            self.showAlertLogout()
            
            
            
        }
        let cancelAction = UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            
        }
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertLogout(){
        let alert = UIAlertController(title: "Veris", message: "Usted ha cerrado sesión", preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
            UIAlertAction in
            
            let vc = ViewController()
            vc.hideMenu()
            
        }
        
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func enviarquitartoken(codigo: NSString){
        let urldef = URLFactory.urldefinida()

        let cadenaUrl = "\(urldef)servicio/login/borrargcm?arg0=\(codigo)"
        
        let url = NSURL(string: cadenaUrl )
        
        if url != nil {
            let request = NSMutableURLRequest(url: url! as URL)
            
            let str = "wsappusuario:ZA$57@9b86@$2r5"
            
            let utf8str = str.data(using: String.Encoding.utf8)
            
            let base64Encoded = str.data(using: String.Encoding.utf8, allowLossyConversion: true)
            let base64DecodedData = NSData(base64Encoded: base64Encoded!, options: [])
            
            
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                
                if (error != nil) {
                    print(error!.localizedDescription)
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    do{
                        var err: NSError?
                        
                        let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        if (err != nil) {
                            print("JSON Error \(err!.localizedDescription)")
                        }
                        
                        let info =  jsonResult as NSDictionary
                        let resultado = info["resultado"] as! NSString
                        _ = info["texto"] as! NSString
                        
                        DispatchQueue.main.async {
                            ViewUtilidades.hiddenLoader(controller: self)
                            print(resultado)
                            if resultado.isEqual(to: "ok") {
                                
                                
                            } else {
                                let alert = UIAlertController(title: "Veris", message: "error", preferredStyle: UIAlertControllerStyle.alert)
                                let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                                    UIAlertAction in
                                    
                                }
                                alert.addAction(okAction)
                                self.present(alert, animated: true, completion: nil)
                            }
                            
 
                        }
                        /*DispatchQueue.main.asynchronously(DispatchQueue.mainexecute: {
                            ViewUtilidades.hiddenLoader(self)
                            print(resultado)
                            if resultado.isEqualToString("ok") {
                                //codigo_tok = codigo_token
                                //return codigo_token
                                
                                
                            } else {
                                let alert = UIAlertController(title: "Veris", message: "error", preferredStyle: UIAlertControllerStyle.alert)
                                let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                                    UIAlertAction in
                                    
                                }
                                alert.addAction(okAction)
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                        })*/
                    } catch {
                        print("ERROR")
                        ViewUtilidades.hiddenLoader(controller: self)
                        let alert = UIAlertController(title: "Veris", message: "Revise su conexión a Datos o Internet", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
                
                
            }
            
        } else {
            ViewUtilidades.hiddenLoader(controller: self)
            let alert = UIAlertController(title: "Veris", message: "Ha ocurrido un error. Intente nuevamente", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                UIAlertAction in
                
            }
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
     // Get the new view controller using [segue destinationViewController].
     // Pass the selected object to the new view controller.
     }
     */
    
}

