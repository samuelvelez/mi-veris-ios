
//
//  ResultadosLabController.swift
//  Mi Veris
//
//  Created by Jorge Merchan on 5/2/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import UIKit
class VisorResultado: UIViewController, UIWebViewDelegate  {
    var ruta : NSString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(ruta)
        
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        let webV:UIWebView = UIWebView(frame: CGRect(x:0, y:60, width:UIScreen.main.bounds.width, height: self.view.frame.height - 64 ) )
        
        webV.loadRequest(NSURLRequest(url: NSURL(string: ruta as String)! as URL) as URLRequest)
        webV.delegate = self;
        webV.backgroundColor = UIColor.white
        webV.scalesPageToFit=true;
        
        
        
        
        
        self.view.addSubview(webV)
        
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print("Webview fail with error \(error)", terminator: "");
        ViewUtilidades.hiddenLoader(controller: self)
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return true;
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        print("Webview started Loading", terminator: "")
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        print("Webview did finish load", terminator: "")
        ViewUtilidades.hiddenLoader(controller: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
