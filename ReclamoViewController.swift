//
//  ReclamoViewController.swift
//  Mi Veris
//
//  Created by Jorge on 04/02/15.
//  Copyright (c) 2015 Firewall Soluciones All rights reserved.
//

import UIKit

class ReclamoViewController: UIViewController, UITextViewDelegate {
    
    var txtReclamo : UITextView = UITextView()
    var resultado : NSString = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        self.style();
        
        let scroll = UIScrollView(frame: CGRect(x: 0, y: 94, width: self.view.frame.width, height: self.view.frame.height - 94))
        scroll.backgroundColor = UIColor.clear
        let tapGesture = UITapGestureRecognizer(target: self, action: "hideKeyboard")
        scroll.addGestureRecognizer(tapGesture)
        self.view.addSubview(scroll)
        
        let lbTitle = UILabel(frame: CGRect(x: 10, y: 15, width: self.view.frame.width, height: 30))
        lbTitle.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        lbTitle.font = UIFont(name: "helvetica-bold", size: 14)
        lbTitle.text = "Ingrese su Reclamo"
        
        scroll.addSubview(lbTitle)
        
        txtReclamo.frame = CGRect(x: 10, y: 50, width: self.view.frame.width - 20, height: 150)
        txtReclamo.layer.cornerRadius = 10;
        txtReclamo.layer.borderWidth = 1;
        txtReclamo.layer.borderColor = UIColor.gray.cgColor
        txtReclamo.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.7)
        txtReclamo.delegate = self;
        
        scroll.addSubview(txtReclamo)
        
        let lbMax = UILabel(frame: CGRect(x: 0, y: 210, width: self.view.frame.width - 10, height: 30))
        lbMax.textColor = UIColor.gray
        lbMax.font = UIFont(name: "helvetica", size: 12)
        lbMax.font = UIFont.italicSystemFont(ofSize: 12)
        lbMax.textAlignment = NSTextAlignment.right;
        lbMax.text = "(Max 300 caracteres)"
        
        scroll.addSubview(lbMax)
        
        let btEnviar:UIButton = UIButton(frame: CGRect(x: self.view.frame.width - 110 , y: 240, width: 90, height: 35))
        btEnviar.backgroundColor = UIColor.black
        btEnviar.setTitle("Enviar", for: UIControlState.normal)
        btEnviar.addTarget(self, action: #selector(ReclamoViewController.enviar), for: UIControlEvents.touchUpInside)
        btEnviar.titleLabel!.font = UIFont(name: "helvetica", size: 14)
        btEnviar.tag = 22;
        
        scroll.addSubview(btEnviar)
    }
    
    
     // MARK: enviar: Método para enviar reclamo

    func style () {
        
        
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        let fondo : UIImageView = UIImageView(frame: self.view.bounds)
        fondo.image = UIImage(named:"fondocita.png")
        self.view.addSubview(fondo)
        
        let barWhite : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width , height: 30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width / 1.8, height: 30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x: barBlue.frame.width, y: 64, width: 45, height: 30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x: 0, y: 64, width: self.view.frame.width - 20, height: 30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "Reclamo"
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)

        let usuario = Usuario.getEntity as Usuario
        if usuario.logueado {
            let nombreUser : UILabel = UILabel(frame: CGRect(x: 10, y: 94, width: self.view.frame.width - 20, height: 30))
            nombreUser.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
            nombreUser.font = UIFont(name: "helvetica-bold", size: 12)
            nombreUser.text = usuario.nombre as String
            
            self.view.addSubview(nombreUser)
        }
    
    }
    
    // MARK: enviar: Método para enviar reclamo
    
    func enviar(){
        
        let usuario : Usuario = Usuario.getEntity as Usuario
        
        if usuario.logueado {
            ViewUtilidades.showLoader(controller: self, title: "Cargando...")
            
            var texto : NSString = txtReclamo.text as NSString
            texto = texto.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
            let urldef = URLFactory.urldefinida()
            let url = NSURL(string: "\(urldef)servicio/catalogos/insertarreclamo?arg0=\(usuario.numeroidentificacion)&arg1=\(usuario.tipoid)&arg2=\(texto)")
            let request = NSMutableURLRequest(url: url! as URL)
            
            let str = "wsappusuario:ZA$57@9b86@$2r5"
            
            let utf8str = str.data(using: String.Encoding.utf8)
            
            let base64DecodedData = utf8str!.base64EncodedString()
            request.addValue(base64DecodedData, forHTTPHeaderField: "Authorization")

            
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                
                if (error != nil) {
                    print(error!.localizedDescription)
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    do{
                    var err: NSError?
                    
                    //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    if (err != nil) {
                        print("JSON Error \(err!.localizedDescription)")
                    }
                    
                    //println(jsonResult);
                    print(jsonResult)
                    let info =  jsonResult as NSDictionary
                    self.resultado = info["resultado"] as! NSString
                    
                    //dispatch_async(dispatch_get_main_queue(), {
                        DispatchQueue.main.async {
                            
                        ViewUtilidades.hiddenLoader(controller: self)
                        if self.resultado.isEqual(to: "ok") {
                            UtilidadesGeneral.mensaje(mensaje: "Se ha enviado su reclamo correctente. Gracias")
                            self.navigationController?.popViewController(animated: true)
                        } else {
                            UtilidadesGeneral.mensaje(mensaje: self.resultado as String)
                        }
                    }
                    } catch {
                        print("ERROR")
                    }
                }
                
                
            }
            //////////////////////////
            
    
        } else {
            UtilidadesGeneral.mensaje(mensaje: "Para enviar un reclamo, debes estar registrado")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func hideKeyboard() {
        txtReclamo.resignFirstResponder()
    }

    
    @IBAction func hideKeyBoard(sender: AnyObject) {
        txtReclamo.resignFirstResponder()
    }
    
}
