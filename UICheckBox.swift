//
//  UICheckBox.swift
//  Mi Veris
//
//  Created by Jorge on 06/06/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import UIKit

class UICheckBox: UIButton {
    
    let checkedImage : UIImage = UIImage(named: "checked.png")!
    var isChecked : Bool = false {
        didSet{
            if isChecked == true {
                self.setImage(checkedImage, for: .normal)
            } else {
                self.setImage(nil, for: UIControlState.normal)
            }
        }
    }
    
    init( posX : CGFloat, posY : CGFloat ) {
        super.init(frame: CGRect (x: posX, y: posY, width: 25, height: 25))//( posX, posY, 25, 25 ) )
        self.isChecked = false
        self.addTarget(self, action: #selector(clicked(sender:)), for: UIControlEvents.touchUpInside)
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.borderWidth = 1
    }
    
    func clicked( sender : UIButton ){
        if ( sender == self ){
            if isChecked {
                isChecked = false;
            } else {
                isChecked = true
            }
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
