//
//  DetalleViewController.swift
//  Mi Veris
//
//  Created by Jorge on 04/02/15.
//  Copyright (c) 2015 Firewall Soluciones All rights reserved.
//

import UIKit

class DetalleViewController: UIViewController {
    
    var textdireccion : NSString!
    var texttelefonos : NSString!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.style();
        
        let lbDireccion = UILabel(frame: CGRect(x:10, y:110, width:self.view.frame.width, height:30))
        lbDireccion.textColor = UIColor(red: (105.0/256.0), green: (117.0/256.0), blue: (220.0/256.0), alpha: 1)
        lbDireccion.text = "Dirección"
        
        self.view.addSubview(lbDireccion)
        
        let direccion = UILabel(frame: CGRect(x:10, y:130, width:self.view.frame.width, height:30))
        direccion.textColor = UIColor.gray
        direccion.text = textdireccion as String
        direccion.font = UIFont(name: "helvetica", size: 14)
        
        self.view.addSubview(direccion)
        
        let lbTelefono = UILabel(frame: CGRect(x:10, y:160, width:self.view.frame.width, height:30))
        lbTelefono.textColor = UIColor(red: (105.0/256.0), green: (117.0/256.0), blue: (220.0/256.0), alpha: 1)
        lbTelefono.text = "Horarios"
        
        self.view.addSubview(lbTelefono)
        
        //\n
        
        //let newString = texttelefonos.stringByReplacingOccurrencesOfString(",", withString: "\n", options: NSString.CompareOptions.LiteralSearch, range: NSMakeRange(0, texttelefonos.length))
        let newString = texttelefonos.replacingOccurrences(of: ",", with: "\n", options: NSString.CompareOptions.literal, range: NSMakeRange(0, texttelefonos.length))
        
        let telefono = UILabel(frame: CGRect(x:10, y:190, width:self.view.frame.width - 20, height:30))
        telefono.textColor = UIColor.gray
        telefono.text = newString
        telefono.font = UIFont(name: "helvetica", size: 14)
        telefono.numberOfLines = 0;
        telefono.sizeToFit()
        
        self.view.addSubview(telefono)
        
        
    }
    
    
     // MARK: style: Estilo del controlador
    
    func style(){
        
        
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        let fondo : UIImageView = UIImageView(frame: self.view.bounds)
        fondo.image = UIImage(named:"back.png")
        self.view.addSubview(fondo)
        
        
        let barWhite : UIView = UIView(frame: CGRect(x:0, y:64, width:self.view.frame.width , height:30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x:0, y:64, width:self.view.frame.width / 2, height:30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x:barBlue.frame.width, y:64, width:45, height:30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x:0, y:64, width:self.view.frame.width - 20, height:30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "Información"
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)

    
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    }
