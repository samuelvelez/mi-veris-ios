//
//  RegistroExitViewController.swift
//  Mi Veris
//
//  Created by Samuel Velez on 27/4/16.
//  Copyright © 2016 Programadores-iOS.net. All rights reserved.
//

import UIKit

class RegistroExitViewController: UIViewController, UITextFieldDelegate {
    
    
    var scroll : UIScrollView = UIScrollView()
    
    var email : NSString!
    
    var vieneFamilia3 : NSString!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scroll.frame = CGRect(x: 0, y: -60, width: self.view.frame.width, height: self.view.frame.height+60)
        scroll.isUserInteractionEnabled = true
        
       // let singleTap = UITapGestureRecognizer(target: self, action: "hideKeyboard")
        //scroll.addGestureRecognizer(singleTap)
        self.view.addSubview(scroll)
        
        self.navigationItem.title = ""
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        let barWhite : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width , height: 30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width / 2.8, height: 30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x: barBlue.frame.width, y: 64, width: 45, height: 30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x: 0, y: 70, width: self.view.frame.width - 20, height: 30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "CREAR CUENTA"
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)

        let lbFelicidades = UILabel(frame: CGRect(x: 30, y: 105, width: self.view.frame.width - 40, height: 25))
        lbFelicidades.text = "Felicidades!!!"
        lbFelicidades.font = UIFont.systemFont(ofSize: 18)
        lbFelicidades.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        scroll.addSubview(lbFelicidades)
        
        let lbtexto2 = UILabel(frame: CGRect(x: 20, y: 135, width: self.view.frame.width - 40, height:100))
        lbtexto2.text = "Hemos enviado tu usuario y tu código de activación a la cuenta de correo:"
        lbtexto2.font = UIFont.systemFont(ofSize: 16)
        lbtexto2.lineBreakMode = NSLineBreakMode.byWordWrapping
        lbtexto2.numberOfLines = 0
        scroll.addSubview(lbtexto2)
        
        let lbmail = UILabel(frame: CGRect(x: 20, y: 235, width: self.view.frame.width - 40, height:25))
        lbmail.text = email.description
        lbmail.font = UIFont.systemFont(ofSize: 16)
        lbmail.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        lbmail.textAlignment = NSTextAlignment.center
        scroll.addSubview(lbmail)
        
        let lbtexto4 = UILabel(frame: CGRect(x: 20, y: 260, width: self.view.frame.width - 40, height: 100))
        lbtexto4.text = "Por favor inicia sesión para completar tu registro."
        lbtexto4.font = UIFont.systemFont(ofSize: 16)
        lbtexto4.lineBreakMode = NSLineBreakMode.byWordWrapping
        lbtexto4.numberOfLines = 0
        scroll.addSubview(lbtexto4)
        
        let lbtexto5 = UILabel(frame: CGRect(x: 20, y: 340, width: self.view.frame.width - 40, height: 100))
        lbtexto5.text = "Nota: Si no has recibido el correo de confirmación, por favor revise su bandeja de correo no deseado(spam) o comunícate con nosotros al 6009600."
        lbtexto5.font = UIFont.systemFont(ofSize: 16)
        lbtexto5.lineBreakMode = NSLineBreakMode.byWordWrapping
        lbtexto5.numberOfLines = 0
        scroll.addSubview(lbtexto5)
        
        let btAceptar = UIButton(frame: CGRect(x: 30, y: 470, width: self.view.frame.width - 60, height: 40))
        btAceptar.setTitle("Iniciar Sesión", for: UIControlState.normal)
        btAceptar.addTarget(self, action: #selector(RegistroExitViewController.siguiente), for: UIControlEvents.touchUpInside)
        btAceptar.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        btAceptar.layer.cornerRadius = 20
        if(vieneFamilia3==nil){
                scroll.addSubview(btAceptar)
        }else if(vieneFamilia3 == "si")
        {
            scroll.willRemoveSubview(btAceptar)
            
            //
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(1000), execute: {
                self.navigationController!.viewControllers.removeLast(3)
                let alert = UIAlertController(title: "Veris", message: "Usuario creado, ahora puede agregarlo a su grupo familiar", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            })

        }
        
        
        
        scroll.contentSize = CGSize(width: 0, height: 520)
        
        
        

    }
    
    func siguiente(){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "newLogin") as! NewLoginViewController
        
        vc.navigationItem.hidesBackButton = true
        
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
