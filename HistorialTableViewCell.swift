//
//  HistorialTableViewCell.swift
//  veris
//
//  Created by Felipe Lloret on 06/02/15.
//  Copyright (c) 2015 Felipe Lloret. All rights reserved.
//

import UIKit

class HistorialTableViewCell: UITableViewCell {
    
    var gradientView: UIView = UIView()
    var firstLineView: UIView = UIView()
    var secondLineView: UIView = UIView()
    var thirdLineView: UIView = UIView()
    var fourthLineView: UIView = UIView()
    
    var blueArrow : UIImageView = UIImageView()
    var calificarImageView: UIImageView = UIImageView()
    
    var sucursalLabel: UILabel = UILabel()
    var fechaLabel: UILabel = UILabel()
    var doctorLabel: UILabel = UILabel()
    var prestacionLabel: UILabel = UILabel()
    var separadorLabel: UILabel = UILabel()
    
    var reagendarButton: UIButton = UIButton()
    var nuevaCitaButton: UIButton = UIButton()
    var puntuarCitaButton: UIButton = UIButton()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width;
        
        gradientView.frame = CGRect(x:0.0, y:0.0, width:screenWidth, height:120.0)
        gradientView.backgroundColor = UIColor.lightGray
        gradientView.alpha = 0.3
        self.contentView.addSubview(gradientView)
        
        firstLineView.frame = CGRect(x:0.0,y: 0.0, width:screenWidth, height:1.0)
        firstLineView.backgroundColor = UIColor.black
        self.contentView.addSubview(firstLineView)
        
        secondLineView.frame = CGRect(x:0.0, y:24.0, width:screenWidth, height:1.0)
        secondLineView.backgroundColor = UIColor.black
        self.contentView.addSubview(secondLineView)
        
        thirdLineView.frame = CGRect(x:0.0, y:80.0, width:screenWidth, height:1.0)
        thirdLineView.backgroundColor = UIColor.black
        self.contentView.addSubview(thirdLineView)
        
        fourthLineView.frame = CGRect(x:0.0,y: 118.0, width:screenWidth, height:1.0)
        fourthLineView.backgroundColor = UIColor.black
        self.contentView.addSubview(fourthLineView)
        
        blueArrow.frame = CGRect(x:0.0, y:0.0, width:17.0, height:26.0)
        blueArrow.image = UIImage(named: "BlueArrow")
        self.contentView.addSubview(blueArrow)
        
        sucursalLabel.frame = CGRect(x:21.0, y:5.0, width:150.0, height:16.0)
        sucursalLabel.numberOfLines = 1
        sucursalLabel.font = UIFont(name: "helvetica-bold", size: 12.0)
        sucursalLabel.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        self.contentView.addSubview(sucursalLabel)
        
        fechaLabel.frame = CGRect(x:screenWidth - 121.0, y:5.0, width:121.0, height:16.0)
        fechaLabel.numberOfLines = 1
        fechaLabel.font = UIFont(name: "helvetica", size: 12)
        self.contentView.addSubview(fechaLabel)
        
        doctorLabel.frame = CGRect(x:50.0, y:30.0, width:screenWidth, height:20.0)
        doctorLabel.numberOfLines = 1
        doctorLabel.font = UIFont(name: "helvetica-bold", size: 11.0)
        self.contentView.addSubview(doctorLabel)
        
        prestacionLabel.frame = CGRect(x:65.0, y:45.0, width:250.0, height:40.0)
        prestacionLabel.numberOfLines = 2
        prestacionLabel.lineBreakMode = .byWordWrapping
        prestacionLabel.font = UIFont(name: "helvetica", size: 12.0)
        self.contentView.addSubview(prestacionLabel)
        
        nuevaCitaButton.frame = CGRect(x:150.0, y:85.0, width:70.0, height:35.0)
        nuevaCitaButton.setTitle("Nueva cita", for: UIControlState.normal)
        nuevaCitaButton.setTitleColor(UIColor.black, for: UIControlState.normal)
        nuevaCitaButton.titleLabel!.font = UIFont(name: "helvetica", size: 14.0)
        self.contentView.addSubview(nuevaCitaButton)
        
        separadorLabel.frame = CGRect(x:225.0, y:90.0, width:5.0, height:25.0)
        separadorLabel.text = "|"
        self.contentView.addSubview(separadorLabel)
        
        puntuarCitaButton.frame = CGRect(x:230.0, y:85.0, width:85.0, height:35.0)
        puntuarCitaButton.setTitle("Calificar cita", for: UIControlState.normal)
        puntuarCitaButton.setTitleColor(UIColor.black, for: UIControlState.normal)
        puntuarCitaButton.titleLabel!.font = UIFont(name: "helvetica", size: 14.0)
        self.contentView.addSubview(puntuarCitaButton)
        
        calificarImageView.frame = CGRect(x:250.0, y:80.0, width:40.0, height:40.0)
        calificarImageView.image = UIImage(named: "Votacion1")
        self.contentView.addSubview(calificarImageView)
        
        self.backgroundColor = UIColor.clear
        self.selectionStyle = UITableViewCellSelectionStyle.none;
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    //Con esta función haremos más simple la adjudicación a las labels desde HistorialViewController
    func setCell(sucursalLabelText: String, fechaLabelText: String, doctorLabelText: String, prestacionLabelText: String, calificarCitaButtonImage: UIImage!) {
        self.sucursalLabel.text = sucursalLabelText
        self.fechaLabel.text = fechaLabelText
        self.doctorLabel.text = doctorLabelText
        self.prestacionLabel.text = prestacionLabelText
        self.calificarImageView.image = calificarCitaButtonImage
    }
}
