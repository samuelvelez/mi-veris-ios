//
//  CreateViewController.swift
//  Mi Veris
//
//  Created by Samuel Velez on 27/4/16.
//  Copyright © 2016 Programadores-iOS.net. All rights reserved.
//

import UIKit

import CoreLocation

class CreateViewController: UIViewController, UITextFieldDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate{
    
    var sgm_tipo : UISegmentedControl = UISegmentedControl()
    var txt_identificacion : UITextField = UITextField()
    var txt_clave : UITextField = UITextField()
    var txt_confirmar : UITextField = UITextField()
    var txt_email : UITextField = UITextField()
    var txt_celular : UITextField = UITextField()
    var pickerNacimiento : UIDatePicker = UIDatePicker()
    
    var scroll : UIScrollView = UIScrollView()
    let limitLength = 10
    let limit = 8
    
    var locationManager : CLLocationManager!
    var coord : CLLocationCoordinate2D!
    
    let iOS8 = floor(NSFoundationVersionNumber) > floor(NSFoundationVersionNumber_iOS_7_1)
    
    var lat :Double = 0
    var lon :Double = 0
    
    var g_tipo : NSString!
    var g_p_apellido : NSString!
    var g_s_apellido : NSString!
    var g_p_nombre : NSString!
    var vieneFamilia : NSString!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scroll.frame = CGRect(x: 0, y: -60, width: self.view.frame.width, height: self.view.frame.height+60)
        scroll.isUserInteractionEnabled = true
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(CreateViewController.hideKeyboard))
        scroll.addGestureRecognizer(singleTap)
        self.view.addSubview(scroll)
        
        self.navigationItem.title = ""
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        let barWhite : UIView = UIView(frame: CGRect(x: 0, y:64, width: self.view.frame.width , height: 30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width / 2.8, height: 30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x: barBlue.frame.width, y:  64, width: 45, height: 30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x: 0, y: 70, width: self.view.frame.width - 20, height: 30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "CREAR CUENTA"
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)
        
        let items = ["Cédula", "Pasaporte"]
        sgm_tipo = UISegmentedControl(items: items)
        sgm_tipo.selectedSegmentIndex = 0
        sgm_tipo.tintColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        sgm_tipo.frame = CGRect(x: 40, y: 105, width: self.view.frame.width - 80, height: 40)
        sgm_tipo.addTarget(self, action: #selector(CreateViewController.cambio), for: .valueChanged);
        scroll.addSubview(sgm_tipo)
        
        
        
        txt_identificacion = UITextField(frame: CGRect(x: 40, y: 155, width: self.view.frame.width - 80, height: 40))
        txt_identificacion.delegate = self
        txt_identificacion.layer.cornerRadius = 20
        txt_identificacion.textAlignment = NSTextAlignment.center
        txt_identificacion.delegate = self
        txt_identificacion.placeholder="Identificación"
        txt_identificacion.layer.borderColor = UIColor.lightGray.cgColor
        txt_identificacion.layer.borderWidth = 1
        txt_identificacion.autocorrectionType = UITextAutocorrectionType.no
        scroll.addSubview(txt_identificacion)
        
        txt_clave = UITextField(frame: CGRect(x: 40, y: 205, width: self.view.frame.width - 80, height: 40))
        txt_clave.layer.cornerRadius = 20
        //txt_clave.delegate = self
        txt_clave.textAlignment = NSTextAlignment.center
        //txt_clave.keyboardType = UIKeyboardType.
        //txt_clave.keyboardAppearance = UIKeyInput.
        txt_clave.isSecureTextEntry = true
        txt_clave.placeholder="Clave (8 dígitos mínimo)"
        txt_clave.layer.borderColor = UIColor.lightGray.cgColor
        txt_clave.layer.borderWidth = 1
        txt_clave.autocorrectionType = UITextAutocorrectionType.no
        scroll.addSubview(txt_clave)
        
        txt_confirmar = UITextField(frame: CGRect(x: 40,y: 255, width: self.view.frame.width - 80, height: 40))
        txt_confirmar.layer.cornerRadius = 20
        txt_confirmar.textAlignment = NSTextAlignment.center
        txt_confirmar.isSecureTextEntry = true
        txt_confirmar.placeholder="Confirmar Clave"
        txt_confirmar.layer.borderColor = UIColor.lightGray.cgColor
        txt_confirmar.layer.borderWidth = 1
        txt_confirmar.autocorrectionType = UITextAutocorrectionType.no
        scroll.addSubview(txt_confirmar)
        
        
        txt_celular = UITextField(frame: CGRect(x: 40, y: 355, width: self.view.frame.width - 80, height: 40))
        txt_celular.delegate = self
        txt_celular.layer.cornerRadius = 20
        txt_celular.textAlignment = NSTextAlignment.center
        txt_celular.delegate = self
        txt_celular.keyboardType = UIKeyboardType.numberPad
        txt_celular.layer.borderColor = UIColor.lightGray.cgColor
        txt_celular.layer.borderWidth = 1
        txt_celular.placeholder="Teléfono Móvil"
        txt_celular.autocorrectionType = UITextAutocorrectionType.no
        scroll.addSubview(txt_celular)
        
        txt_email = UITextField(frame: CGRect(x: 40, y: 305, width: self.view.frame.width - 80, height:  40))
        txt_email.layer.cornerRadius = 20
        txt_email.textAlignment = NSTextAlignment.center
        txt_email.keyboardType = UIKeyboardType.emailAddress
        txt_email.placeholder="Correo Electrónico"
        txt_email.layer.borderColor = UIColor.lightGray.cgColor
        txt_email.layer.borderWidth = 1
        txt_email.autocorrectionType = UITextAutocorrectionType.no
        scroll.addSubview(txt_email)
        
        let lbFecha = UILabel(frame: CGRect(x: 30, y: 400, width: self.view.frame.width - 40, height: 25))
        lbFecha.text = "Fecha de Nacimiento:"
        lbFecha.font = UIFont.systemFont(ofSize: 14)
        scroll.addSubview(lbFecha)
        
        let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let currentDate: NSDate = NSDate()
        let components: NSDateComponents = NSDateComponents()
        
        components.year = 0
        let maxDate: NSDate = gregorian.date(byAdding: components as DateComponents, to: currentDate as Date, options: NSCalendar.Options(rawValue: 0))! as NSDate

        pickerNacimiento.frame = CGRect(x: 20, y: 420, width: self.view.frame.width - 40, height: 200)
        let calendar: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier(rawValue: NSGregorianCalendar))!
        // let calendar: NSCalendar = NSCalendar.currentCalendar()
        calendar.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        let componentes: NSDateComponents = NSDateComponents()
        componentes.year = 1980
        componentes.month = 1
        componentes.day = 2
        let defaultDate: NSDate = calendar.date(from: componentes as DateComponents)! as NSDate
        pickerNacimiento.datePickerMode = UIDatePickerMode.date
            //UIDatePickerMode.Date
        pickerNacimiento.setDate(defaultDate as Date, animated: false)
        pickerNacimiento.maximumDate = maxDate as Date
        scroll.addSubview(pickerNacimiento)
        
        let btAceptar = UIButton(frame: CGRect(x: 30, y: 610, width: self.view.frame.width - 60, height: 40))
        btAceptar.setTitle("Siguiente", for: UIControlState.normal)
        btAceptar.addTarget(self, action: #selector(CreateViewController.siguiente), for: UIControlEvents.touchUpInside)
        btAceptar.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        btAceptar.layer.cornerRadius = 20
        scroll.addSubview(btAceptar)
        
        
        scroll.contentSize = CGSize(width: 0, height: 660)
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        if iOS8 {
            locationManager.requestAlwaysAuthorization()
        }
        locationManager.startUpdatingLocation()
    }
    func locationManager(_ manager:CLLocationManager, didUpdateLocations locations:[CLLocation]) {
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        coord = locationObj.coordinate
        
        lat = coord.latitude
        lon = coord.longitude
    }

    
    func  cambio() {
        if self.sgm_tipo.selectedSegmentIndex==0{
           self.txt_identificacion.placeholder = "Num. Cédula"
        }else if self.sgm_tipo.selectedSegmentIndex==1{
           self.txt_identificacion.placeholder = "Num. Pasaporte"
        }
    }
    
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        //self.txtCedula.resignFirstResponder()
        self.txt_celular.resignFirstResponder()
        self.txt_email.resignFirstResponder()
        self.txt_identificacion.resignFirstResponder()
        self.txt_confirmar.resignFirstResponder()
        self.txt_clave.resignFirstResponder()
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        if(textField == txt_clave){
            return newLength <= 8
        }else{
            return newLength <= limitLength
        }
        
    }
    
    func hideKeyboard(){
        self.txt_celular.resignFirstResponder()
        self.txt_email.resignFirstResponder()
        self.txt_identificacion.resignFirstResponder()
        self.txt_clave.resignFirstResponder()
        self.txt_confirmar.resignFirstResponder()
        //let transitionOptions = UIViewAnimationOptions.TransitionNone
        /*UIView.transitionWithView(self.view, duration: 0.2, options: transitionOptions, animations: {
         if !self.downKeyboard {
         self.downKeyboard = true
         self.scroll.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.scroll.frame) + 230)
         }
         }, completion: { finished in
         
         })*/
    }
    
    func siguiente(){
        var valido = false
        var mensaje = "Datos Incorrectos"
        if self.sgm_tipo.selectedSegmentIndex==0{
            valido = true
           // valido = valido //&& validaCedula(cedula: self.txt_identificacion.text!)
           // mensaje = "Cedula Inválida"
        }else if self.sgm_tipo.selectedSegmentIndex==1{
            valido = true
        }
        else {
            valido = false
            mensaje = "Debe seleccionar si tiene cédula o pasaporte"
        }
        
        
        valido = valido && !(self.txt_identificacion.text?.isEmpty)!
        valido = valido && !(self.txt_email.text?.isEmpty)!
        valido = valido && !(self.txt_celular.text?.isEmpty)!
        valido = valido && !(self.txt_clave.text?.isEmpty)!
        valido = valido && !(self.txt_confirmar.text?.isEmpty)!
        
        if(self.isValidEmail(testStr: self.txt_email.text!)){
            valido = valido && true
        }else{
            valido = false
            mensaje = "Correo Invalido"
        }
        let claves = self.txt_clave.text as! NSString
        if claves.length < 8{
           valido = false
            mensaje = "La clave debe de tener 8 caracteres minimo"
        }
        let celular = self.txt_celular.text as! NSString
        if celular.length <= 9{
            valido = false
            mensaje = "Celular Invalido"
            print("es invalido")
        }else{
            print(celular.length)
        }
        
        let clave = self.txt_clave.text as! NSString
        let confirma_clave = self.txt_confirmar.text! as NSString
        if(clave.isEqual(to: confirma_clave as String)){
            valido = valido && true
        }else{
            valido = false
            mensaje = "Contraseña no coincide"

        }
        
        if valido{
            self.validarUsuario()
            //print("debe de crear el usuario")
        } else {
            let alert = UIAlertController(title: "Veris", message: mensaje, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func isValidEmail(testStr:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func validaCedula( cedula : String ) -> Bool{
        if cedula.isEqual(""){
            return false
        } else {
            var suma = 0
            var residuo = 0
            var privada = false
            var publica = false
            var natural = false
            let numeroProvincias = 24
            var digitoVerificador = 0
            var modulo = 11
            
            var d1 = 0, d2 = 0, d3 = 0, d4 = 0, d5 = 0, d6 = 0, d7 = 0, d8 = 0, d9 = 0, d10 = 0
            var p1 = 0, p2 = 0, p3 = 0, p4 = 0, p5 = 0, p6 = 0, p7 = 0, p8 = 0, p9 = 0
            
            if let n = Int(cedula) {
                print("\(n)")
            } else {
                return false
            }
            
            let nsstring2 = "\(cedula)" as NSString
            
            if nsstring2.length  < 10 || nsstring2.length > 13{
                return false
            }
            
            let nsstring = cedula as NSString
            let provincia = Int(nsstring.substring(with: NSRange(location: 0, length: 2)))
            
            if provincia! <= 0 || provincia! > numeroProvincias{
                return false
            }
            
            d1 = Int(nsstring.substring(with: NSRange(location: 0, length: 1)))!
            d2 = Int(nsstring.substring(with: NSRange(location: 1, length: 1)))!
            d3 = Int(nsstring.substring(with: NSRange(location: 2, length: 1)))!
            d4 = Int(nsstring.substring(with: NSRange(location: 3, length: 1)))!
            d5 = Int(nsstring.substring(with: NSRange(location: 4, length: 1)))!
            d6 = Int(nsstring.substring(with: NSRange(location: 5, length: 1)))!
            d7 = Int(nsstring.substring(with: NSRange(location: 6, length: 1)))!
            d8 = Int(nsstring.substring(with: NSRange(location: 7, length: 1)))!
            d9 = Int(nsstring.substring(with: NSRange(location: 8, length: 1)))!
            d10 = Int(nsstring.substring(with: NSRange(location: 9, length: 1)))!
            
            
            if d3 == 7 || d3 == 8{
                return false
            }
            
            if (d3 < 6) {
                natural = true;
                modulo = 10;
                p1 = d1 * 2;
                if (p1 >= 10){
                    p1 -= 9;
                }
                p2 = d2 * 1;
                if (p2 >= 10){
                    p2 -= 9;
                }
                p3 = d3 * 2;
                if (p3 >= 10){
                    p3 -= 9;
                }
                p4 = d4 * 1;
                if (p4 >= 10){
                    p4 -= 9;
                }
                p5 = d5 * 2;
                if (p5 >= 10){
                    p5 -= 9;
                }
                p6 = d6 * 1;
                if (p6 >= 10){
                    p6 -= 9;
                }
                p7 = d7 * 2;
                if (p7 >= 10){
                    p7 -= 9;
                }
                p8 = d8 * 1;
                if (p8 >= 10){
                    p8 -= 9;
                }
                p9 = d9 * 2;
                if (p9 >= 10){
                    p9 -= 9;
                }
            }
            
            if (d3 == 6) {
                publica = true;
                p1 = d1 * 3;
                p2 = d2 * 2;
                p3 = d3 * 7;
                p4 = d4 * 6;
                p5 = d5 * 5;
                p6 = d6 * 4;
                p7 = d7 * 3;
                p8 = d8 * 2;
                p9 = 0;
            }
            
            /* Solo para entidades privadas (modulo 11) */
            if (d3 == 9) {
                privada = true;
                p1 = d1 * 4;
                p2 = d2 * 3;
                p3 = d3 * 2;
                p4 = d4 * 7;
                p5 = d5 * 6;
                p6 = d6 * 5;
                p7 = d7 * 4;
                p8 = d8 * 3;
                p9 = d9 * 2;
            }
            
            suma = p1 + p2 + p3 + p4 + p5 + p6 + p7 + p8 + p9;
            residuo = suma % modulo;
            
            // Si residuo=0, dig.ver.=0, caso contrario 10 - residuo
            digitoVerificador = residuo == 0 ? 0 : modulo - residuo;
            let longitud = nsstring2.length // Longitud del string
            
            // ahora comparamos el elemento de la posicion 10 con el dig. ver.
            if (publica == true) {
                if (digitoVerificador != d9) {
                    // return
                    // "El ruc de la empresa del sector p˙blico es incorrecto.";
                    return false;
                }
                /* El ruc de las empresas del sector publico terminan con 0001 */
                if ( nsstring.substring(with: NSRange(location: 9, length: longitud)) != "0001" ) {
                    // return
                    // "El ruc de la empresa del sector p˙blico debe terminar con 0001.";
                    return false;
                }
            }
            
            if (privada == true) {
                if (digitoVerificador != d10) {
                    // return
                    // "El ruc de la empresa del sector privado es incorrecto.";
                    return false;
                }
                if ( nsstring.substring(with: NSRange(location: 10, length: longitud)) != "001" ) {
                    // return
                    // "El ruc de la empresa del sector privado debe terminar con 001.";
                    return false;
                }
            }
            
            if (natural == true) {
                if (digitoVerificador != d10) {
                    // return "El n˙mero de cÈdula es incorrecto.";
                    return false;
                }
                if ( cedula.characters.count > 10 && nsstring.substring(with: NSRange(location: 10, length: longitud)) != "001" ) {
                    // return
                    // "El ruc de la persona natural debe terminar con 001.";
                    return false;
                }
            }
            return true;
            
        }
    }
    
    
    
    func validarUsuario() {
        var band = true
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        
        var tipo_id : NSString = ""
        if sgm_tipo.selectedSegmentIndex == 0 {
            tipo_id = "2"
        } else if sgm_tipo.selectedSegmentIndex == 1 {
            tipo_id = "3"
        }else{
            //return false
            tipo_id="0"
            
        }
       
        let num_id = txt_identificacion.text!.addingPercentEscapes(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let urldef = URLFactory.urldefinida()

        let cadenaUrl = "\(urldef)servicio/login/validarusuario2?arg0=\(tipo_id)&arg1=\(num_id!)"
        
        let url = NSURL(string: cadenaUrl )
        
        if url != nil {
            let request = NSMutableURLRequest(url: url! as URL)
            
            _ = "wsappusuario:ZA$57@9b86@$2r5"
            
            
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                
                if (error != nil) {
                    print(error!.localizedDescription)
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    do{
                        var err: NSError?
                        
                        let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        if (err != nil) {
                            print("JSON Error \(err!.localizedDescription)")
                        }
                        
                        print(jsonResult)
                        let info =  jsonResult as NSDictionary
                        _ = false
                        let data = info["lista"] as! [[String: Any]]
                        let resultado = info["resultado"] as! NSString
                        
                        
                        //dispatch_async(dispatch_get_main_queue(), {
                        DispatchQueue.main.async(execute: {
                            ViewUtilidades.hiddenLoader(controller: self)
                            print(resultado)
                            if resultado.isEqual(to: "ok") {
                                for item in data {
                                    if ((item["virusu"]! as AnyObject).description != "<null>") {
                                        band = false
                                        let alert = UIAlertController(title: "Veris", message: "Cuenta ya existe", preferredStyle: UIAlertControllerStyle.alert)
                                        let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                                            UIAlertAction in
                                            
                                        }
                                        alert.addAction(okAction)
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                    else if((item["primer_nombre"]! as AnyObject).description != "<null>"){
                                        print("existe en phantom")
                                        print("\(tipo_id)")
                                        self.g_tipo = tipo_id as NSString
                                        self.g_p_nombre = item["primer_nombre"] as! NSString
                                        self.g_p_apellido = item["primer_apellido"] as! NSString
                                        
                                        self.g_s_apellido = item["segundo_apellido"] as! NSString
                                        self.siguiente2()
                                        band = false
                                    }else{
                                        print("no entro, q paso??")
                                    }
                                }
                                //band = true
                                let p_tipo = tipo_id as String
                                let p_identificacion = self.txt_identificacion.text! as String
                                let p_clave = self.txt_clave.text! as String
                                let p_email = self.txt_email.text! as String
                                let p_celular = self.txt_celular.text! as String
                                let p_fecha = self.pickerNacimiento.date
                                if band {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = storyboard.instantiateViewController(withIdentifier: "viewComplete") as! CompleteViewController
                                    vc.tipo = p_tipo as NSString!
                                    vc.identificacion = p_identificacion as NSString!
                                    vc.clave = p_clave as NSString!
                                    vc.email = p_email as NSString!
                                    vc.celular = p_celular as NSString!
                                    vc.fecha = p_fecha as NSDate!
                                    vc.vieneFamilia2 = self.vieneFamilia
                                    
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }else{
                                    print("no entro")
                                }
                                
                            } else {
                                band = false
                                let alert = UIAlertController(title: "Veris", message: "error", preferredStyle: UIAlertControllerStyle.alert)
                                let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                                    UIAlertAction in
                                    
                                }
                                alert.addAction(okAction)
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                        })
                    } catch {
                        print("ERROR")
                        ViewUtilidades.hiddenLoader(controller: self)
                        let alert = UIAlertController(title: "Veris", message: "Revise su conexión a Datos o Internet", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
                

            }
            
        } else {
            ViewUtilidades.hiddenLoader(controller: self)
            let alert = UIAlertController(title: "Veris", message: "Ha ocurrido un error. Intente nuevamente", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                UIAlertAction in
                
            }
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
    
    func siguiente2(){
        //tipo: NSString, apellido: NSString, nombre : NSString, apellido2 : NSString){
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        let valido = true
        let genero = ""
        let mensaje = "Datos Incorrectos"
        
        if valido{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/YYYY" //format style. Browse online to get a format that fits your needs.
            let dateString = dateFormatter.string(from: self.pickerNacimiento.date)
            
            let urldef = URLFactory.urldefinida()

            let cadenaUrl = "\(urldef)servicio/login/crearcuenta?arg0=\(self.g_tipo!)&arg1=\(self.txt_identificacion.text!)&arg2=\((self.g_p_apellido!))&arg3=\((self.g_s_apellido!))&arg4=\((self.g_p_nombre!))&arg5=\((self.txt_email.text)!)&arg6=\(dateString)&arg7=\(genero)&arg8=\((self.txt_celular.text)!)&arg9=&arg10=&arg11=&arg12=\(self.lat)&arg13=\(self.lon)&arg14=\((self.txt_clave.text)!)"
            
            
            let url = NSURL(string: cadenaUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)! )
            
            print(cadenaUrl)
            
            if url != nil {
                let request = NSMutableURLRequest(url: url! as URL)
                
               
                
                NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                    
                    if (error != nil) {
                        print(error!.localizedDescription)
                        ViewUtilidades.hiddenLoader(controller: self)
                        let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                    } else {
                        do{
                            var err: NSError?
                            
                            let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                            if (err != nil) {
                                print("JSON Error \(err!.localizedDescription)")
                            }
                            
                            print(jsonResult)
                            let info =  jsonResult as NSDictionary
                            _ = info["lista"] as! NSArray
                            let resultado = info["resultado"] as! NSString
                            
                            
                           // dispatch_async(dispatch_get_main_queue(), {
                            DispatchQueue.main.async(execute: {
                                ViewUtilidades.hiddenLoader(controller: self)
                                print(resultado)
                                if resultado.isEqual(to: "ok") {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = storyboard.instantiateViewController(withIdentifier: "FinRegistro") as! RegistroExitViewController
                                    vc.email = self.txt_email.text as NSString!
                                    self.navigationController?.pushViewController(vc, animated: true)
                                    
                                } else {
                                    //  band = false
                                    let alert = UIAlertController(title: "Veris", message: resultado as String, preferredStyle: UIAlertControllerStyle.alert)
                                    let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                                        UIAlertAction in
                                        
                                    }
                                    alert.addAction(okAction)
                                    self.present(alert, animated: true, completion: nil)
                                }
                                
                            })
                        } catch {
                            print("ERROR")
                            ViewUtilidades.hiddenLoader(controller: self)
                            let alert = UIAlertController(title: "Veris", message: "Revise su conexión a Datos o Internet", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                    }
                    
                    
                }
                
            } else {
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: "Ha ocurrido un error. Intente nuevamente", preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    
                }
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            }
            
        }else{
            ViewUtilidades.hiddenLoader(controller: self)
            let alert = UIAlertController(title: "Veris", message: mensaje, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        
        
    }

    
    
    
}
