//
//  ResuImgCell.swift
//  Mi Veris
//
//  Created by Jorge Merchan on 7/2/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import UIKit
class ResuImgCell: UITableViewCell {
    var separator : UIImageView = UIImageView();
    var numero_exa : UILabel = UILabel()
    var fecha : UILabel = UILabel()
    var btn_descarga : UIButton = UIButton()
    var descargar : UILabel = UILabel()
    var pdf : UILabel = UILabel()
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.clear
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width;
        
        
        
        separator.backgroundColor = UIColor.gray
        separator.frame = CGRect(x:0, y:0, width:600, height:0.5)
        
        
        
        numero_exa.frame = CGRect(x:5, y:0, width:200, height:24)
        numero_exa.font = UIFont(name: "helvetica-bold", size: 12)
        numero_exa.textColor = UIColor.black
        
        fecha.frame = CGRect(x:5, y:20, width:200, height:20)
        fecha.font = UIFont(name: "helvetica", size: 12)
        fecha.textColor = UIColor.gray
        
        descargar.frame = CGRect(x:screenWidth - 120, y:3, width:60, height:20)
        descargar.font = UIFont(name: "helvetica-Oblique", size: 11)
        descargar.textColor = UIColor.blue
        
        pdf.frame = CGRect(x:screenWidth - 65,y: 3, width:50, height:20)
        pdf.font = UIFont(name: "helvetica-bold", size: 11)
        pdf.textColor = UIColor.blue
        
        
        
        btn_descarga.frame = CGRect(x:screenWidth - 40, y:0, width:30, height:30)
        btn_descarga.setImage(UIImage (named: "descarga.png"), for: .normal)
        
        
        self.contentView.addSubview(separator)
        self.contentView.addSubview(numero_exa)
        self.contentView.addSubview(fecha)
        self.contentView.addSubview(descargar)
        self.contentView.addSubview(pdf)
        self.contentView.addSubview(btn_descarga)
        
        
        
        
        /*
        override func viewDidLoad() { super.viewDidLoad() let button = UIButton.buttonWithType(UIButtonType.System) as UIButton button.frame = CGRectMake(100, 100, 100, 50) button.backgroundColor = UIColor.greenColor() button.setTitle("Test Button", forState: UIControlState.Normal) button.addTarget(self, action: "buttonAction:", forControlEvents: UIControlEvents.TouchUpInside) self.view.addSubview(button) } func buttonAction(sender:UIButton!) { println("Button tapped") }
        */
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
}
