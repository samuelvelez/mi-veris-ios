
import UIKit

class VerDetalleController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    var tableView: UITableView  =   UITableView() /*Table view que muestra la lista de recomendaciones */
    var lista_detalles : NSMutableArray =  NSMutableArray()
    var resultadosel : ObjVerDetalle?
    var posicion: NSInteger = 0
    var cod_recomendacion : NSString = ""
    var cod_empresa : NSInteger = 0
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        
        let title : UILabel = UILabel(frame: CGRect(x:0, y:65, width:338, height:30))
        title.textAlignment = NSTextAlignment.center
        title.textColor = UIColor.black
        title.font = UIFont(name: "helvetica-bold", size: 12)
        title.text = "DETALLE DE LA ORDEN"
        title.backgroundColor = UtilidadesColor.colordehexadecimal(hex: 0xececec, alpha: 1.0)
        self.view.addSubview(title)
        cargardatos()
        
    }
    
    private func cargardatos(){
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        
        let url2 = URLFactory.obtenerDetallesCita(numeroorden: self.cod_recomendacion as String, codempresa: "\(self.cod_empresa)")

        
        let url = NSURL(string: "\(url2)")
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        let str = "wsappusuario:ZA$57@9b86@$2r5"
        
        let utf8str = str.data(using: String.Encoding.utf8)
        
        /*if let base64Encoded = utf8str?.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0)){
            
            print("Encoded:  \(base64Encoded)")
            //request.addValue("Authorization", forHTTPHeaderField: "Basic \(base64Encoded)")
            request.setValue("Basic \(base64Encoded)", forHTTPHeaderField: "Authorization")
            
            if let base64Decoded = NSData(base64EncodedString: base64Encoded, options:   NSDataBase64DecodingOptions(rawValue: 0))
                .map({ NSString(data: $0, encoding: NSUTF8StringEncoding) })
            {
                // Convert back to a string
                print("Decoded:  \(base64Decoded)")
                }
            }*/
            
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                
                //println(NSString(data: data, encoding: NSUTF8StringEncoding))
                
                if (error != nil) {
                    print(error!.localizedDescription)
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    do{
                    var err: NSError?
                    
                    //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                    
                    var jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    if (err != nil) {
                        print("JSON Error \(err!.localizedDescription)")
                    }
                    
                    let info =  jsonResult as NSDictionary
                        let data = info["lista"] as! [[String: Any]]
                    
                    if data.count > 0 {
                        //self.lista_resul_img = NSMutableArray();
                        
                        
                        for jsonobj  in data {
                            print("posision=",data.count)
                            let ver_detalle = ObjVerDetalle()
                            print("--test data--")
                            
                            if (jsonobj["numeroorden"] is NSNull){
                                ver_detalle.numeroorden = ""
                            }else
                            {
                                ver_detalle.numeroorden = jsonobj["numeroorden"] as! NSString
                            }
                            
                            
                            if (jsonobj["codigoempresa"] is NSNull){
                                ver_detalle.codigoempresa = 0
                            }else
                            {
                                ver_detalle.codigoempresa = jsonobj["codigoempresa"] as! NSInteger
                            }
                            if (jsonobj["lineadetalle"] is NSNull){
                                ver_detalle.lineadetalle = 0
                            }else
                            {
                                ver_detalle.lineadetalle = jsonobj["lineadetalle"] as! NSInteger
                            }
                            if (jsonobj["codigoprestacion"] is NSNull){
                                ver_detalle.codigoprestacion = ""
                            }else
                            {
                                ver_detalle.codigoprestacion = jsonobj["codigoprestacion"] as! NSString
                            }
                            if (jsonobj["nombreprestacion"] is NSNull){
                                ver_detalle.nombreprestacion = ""
                            }else
                            {
                                ver_detalle.nombreprestacion = jsonobj["nombreprestacion"] as! NSString
                            }
                            self.lista_detalles.add(ver_detalle)
                            
                        }
                    }
                    
                    //dispatch_async(dispatch_get_main_queue(), {
                        DispatchQueue.main.async(execute: {
                        self.show_ver_detalle()
                        ViewUtilidades.hiddenLoader(controller: self)
                    })
                    } catch {
                        print("ERROR")
                    }
                }
                
                
            }
            //////////////////////////
        

        
    }
    
    
    func show_ver_detalle(){
        tableView.frame = CGRect(x:5, y:100, width:self.view.frame.width, height:self.view.frame.height - 180)
        
        
        tableView.backgroundColor = UIColor.clear
        tableView.layer.cornerRadius = 10;
        tableView.isScrollEnabled = false
        tableView.rowHeight = 10
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorColor = UIColor.clear
        
        tableView.register(VerDetalleCell.self as AnyClass, forCellReuseIdentifier: "ver_detalle");
        
        self.view.addSubview(tableView)
        print("evento:\(lista_detalles.count)/")
        // self.contentView.addSubview(btn_descarga)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return lista_detalles.count;
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ver_detalle", for: indexPath) as! VerDetalleCell
        let resul_rec = lista_detalles.object(at: indexPath.row) as! ObjVerDetalle
        cell.datos.text = resul_rec.nombreprestacion as String
        print("test datos recomendacion: ")
        print(resul_rec.nombreprestacion)
        return cell
        
    }
    /*
    func tableView(tableView:UITableView!,
    heightForRowAtIndexPath indexPath:NSIndexPath)->CGFloat {
    return 90
    }*/
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: NSURLSession download in background
    
    
    
}
