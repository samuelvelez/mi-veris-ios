//
//  Doctor.swift
//  Mi Veris
//
//  Created by Jorge on 05/02/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import UIKit

class Doctor: NSObject {
    
    var idTipoID : NSInteger = 0
    var idDoctor : NSInteger = 0
    var idEspecialidad : NSInteger = 0
    var nombre : String = ""
    var identificacion : String = ""
    var nombreEspecialidad : String = ""
    var primerApellido : String = ""
    var segundoApellido : String = ""
    var segundoNombre : String = ""
    var secuenciaPersonal : String = ""
   
}
