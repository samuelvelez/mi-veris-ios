//
//  NewResetViewController.swift
//  Mi Veris
//
//  Created by Samuel Velez on 1/5/16.
//  Copyright © 2016 Programadores-iOS.net. All rights reserved.
//

import UIKit

class NewResetViewController: UIViewController {

    
    var scroll : UIScrollView = UIScrollView()
    var sgm_tipo : UISegmentedControl = UISegmentedControl()
    var txt_identificacion : UITextField = UITextField()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scroll.frame = CGRect(x:0, y: -60, width: self.view.frame.width, height: self.view.frame.height+60)
        scroll.isUserInteractionEnabled = true
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(NewResetViewController.hideKeyboard))
        scroll.addGestureRecognizer(singleTap)
        self.view.addSubview(scroll)
        
        self.navigationItem.title = ""
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        let barWhite : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width , height: 30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.width / 2.8, height: 30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x: barBlue.frame.width, y: 64, width: 45, height: 30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x: 0, y: 70, width: self.view.frame.width - 20, height: 30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "RESETEAR CLAVE"
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)
        
        let items = ["Cédula", "Pasaporte"]
        sgm_tipo = UISegmentedControl(items: items)
        sgm_tipo.selectedSegmentIndex = 0
        sgm_tipo.tintColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        sgm_tipo.frame = CGRect(x: 40, y: 150,width: self.view.frame.width - 80, height: 40)
        scroll.addSubview(sgm_tipo)
        
        
        
        txt_identificacion = UITextField(frame: CGRect(x: 40, y: 200, width: self.view.frame.width - 80, height: 40))
        txt_identificacion.layer.cornerRadius = 20
        txt_identificacion.textAlignment = NSTextAlignment.center
        //txt_identificacion.delegate = self
        txt_identificacion.placeholder="Identificación"
        txt_identificacion.layer.borderColor = UIColor.lightGray.cgColor
        txt_identificacion.layer.borderWidth = 1
        txt_identificacion.autocorrectionType = UITextAutocorrectionType.no
        scroll.addSubview(txt_identificacion)
        


        let btAceptar = UIButton(frame: CGRect(x: 30, y: 270,  width: self.view.frame.width - 60, height:  40))
        btAceptar.setTitle("Siguiente", for: UIControlState.normal)
        btAceptar.addTarget(self, action: #selector(NewResetViewController.siguiente), for: UIControlEvents.touchUpInside)
        btAceptar.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        btAceptar.layer.cornerRadius = 20
        scroll.addSubview(btAceptar)
        
        scroll.contentSize = CGSize(width: 0, height: 460)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func hideKeyboard(){
        self.txt_identificacion.resignFirstResponder()
        
    }
    func siguiente(){
        var band = false
        var mensaje = "Ha ocurrido un error. Intente nuevamente"
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        
        var tipo_id : NSString = ""
        if sgm_tipo.selectedSegmentIndex == 0 {
            tipo_id = "2"
            band = true
        } else if sgm_tipo.selectedSegmentIndex == 1 {
            tipo_id = "3"
            band = true
        }else{
            //return false
            tipo_id="0"
            mensaje = "Debe seleccionar si tiene cédula o pasaporte"
            band = false
        }
        
        let num_id = txt_identificacion.text!.addingPercentEscapes(using: String.Encoding.utf8)
        
        let urldef = URLFactory.urldefinida()

        let cadenaUrl = "\(urldef)servicio/login/solicitarreset?arg0=\(tipo_id)&arg1=\(num_id!)"
        
        let url = NSURL(string: cadenaUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!  )
        print(url)
        if (url != nil && band) {
            let request = NSMutableURLRequest(url: url! as URL)
            
            let str = "wsappusuario:ZA$57@9b86@$2r5"
            //arreglarutf
            let utf8str = str.data(using: String.Encoding.utf8)
            
            let base64Encoded = str.data(using: String.Encoding.utf8, allowLossyConversion: true)
            let base64DecodedData = utf8str!.base64EncodedString()
            request.addValue(base64DecodedData, forHTTPHeaderField: "Authorization")
            
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
                
                if (error != nil) {
                    print(error!.localizedDescription)
                    ViewUtilidades.hiddenLoader(controller: self)
                    let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    do{
                        var err: NSError?
                        
                        let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        if (err != nil) {
                            print("JSON Error \(err!.localizedDescription)")
                        }
                        
                        print(jsonResult)
                        let info =  jsonResult as NSDictionary
                        //let data = info["lista"] as! NSArray
                        let resultado = info["resultado"] as! NSString
                        
                        
                        //dispatch_async(dispatch_get_main_queue(), {
                        DispatchQueue.main.async(execute: {
                            ViewUtilidades.hiddenLoader(controller: self)
                            print(resultado)
                            if resultado.isEqual(to: "ok") {
                                let p_identificacion = self.txt_identificacion.text! as String
                                
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = storyboard.instantiateViewController(withIdentifier: "codigoReset") as! CodigoResetViewController
                                    vc.identificacion = p_identificacion as NSString!
                                    self.navigationController?.pushViewController(vc, animated: true)
                                
                            } else {
                                let alert = UIAlertController(title: "Veris", message: resultado as String, preferredStyle: UIAlertControllerStyle.alert)
                                let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                                    UIAlertAction in
                                    
                                }
                                alert.addAction(okAction)
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                        })
                    } catch {
                        print("ERROR")
                        ViewUtilidades.hiddenLoader(controller: self)
                        let alert = UIAlertController(title: "Veris", message: "Revise su conexión a Datos o Internet", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
                
                
            }
            
        } else {
            ViewUtilidades.hiddenLoader(controller: self)
            let alert = UIAlertController(title: "Veris", message: mensaje, preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
                UIAlertAction in
                
            }
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
