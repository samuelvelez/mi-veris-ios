//
//  FamiliaresViewController.swift
//  Mi Veris
//
//  Created by Jorge on 20/03/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import UIKit

class FamiliaresViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var familiares : NSMutableArray = NSMutableArray()
    var tableView : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.style();
        
        familiares = Usuario.getFamiliares
        
        tableView = UITableView()
        tableView.frame = CGRect(x: 0, y: 94, width: self.view.frame.width, height: self.view.frame.height - 94);
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.backgroundColor = UIColor.white
        tableView.register(ListaFamiliarTableViewCell.self as AnyClass, forCellReuseIdentifier: "cell");
        
        self.view.addSubview(tableView);
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return familiares.count + 1;
    }
    
    /*func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.separatorInset = UIEdgeInsetsZero
        cell.layoutMargins = UIEdgeInsetsZero
    }*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ListaFamiliarTableViewCell
        if indexPath.row == 0{
            //cell.textLabel?.text = "Mi Perfil"
            cell.nombre.text = "Mi Perfil"
        } else {
            let usuario = familiares.object( at: indexPath.row - 1 ) as! Usuario
            //cell.textLabel?.text = usuario.nombre as String
            cell.nombre.text = usuario.nombre as String
            cell.parentesco.text = usuario.parentesco as String
        }
        cell.textLabel?.font = cell.textLabel?.font.withSize(14)
        
        //cell.backgroundColor = UIColor.clearColor()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            //self.dismissViewControllerAnimated(true, completion: nil)
            let usuario = Usuario.getEntity
            let parent = Usuario.getParent
            if parent.nombre != ""{
                usuario.nombre = parent.nombre
                usuario.numeroidentificacion = parent.numeroidentificacion
                usuario.tipoid = parent.tipoid
                usuario.rol = 0
            }
            self.navigationController?.popViewController(animated: true)
        } else {
            let usuario = Usuario.getEntity
            let parent = Usuario.getParent
            if parent.nombre == ""{
                parent.nombre = usuario.nombre
                parent.numeroidentificacion = usuario.numeroidentificacion
                parent.tipoid = usuario.tipoid
            }
            let familia = familiares.object(at: indexPath.row - 1) as! Usuario
            usuario.nombre = familia.nombre
            usuario.rol = familia.rol
            usuario.numeroidentificacion = familia.numeroidentificacion
            usuario.tipoid = familia.tipoid
            self.navigationController?.popViewController(animated: true)
        }
        tableView.deselectRow(at: indexPath, animated: true);
    }
    
    func style() {
        
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        let fondo : UIImageView = UIImageView(frame: self.view.bounds)
        fondo.image = UIImage(named:"back.png")
        self.view.addSubview(fondo)
        
        
        let barWhite : UIView = UIView(frame: CGRect(x: 0, y:64, width:self.view.frame.width , height: 30))
        barWhite.backgroundColor = UIColor.white
        let barBlue : UIView = UIView(frame: CGRect(x: 0, y: 64, width: (self.view.frame.width) / 2.2, height: 30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        
        let img : UIImageView = UIImageView(frame: CGRect(x: barBlue.frame.width, y: 64, width:45, height:30))
        img.image = UIImage(named: "titulo_activity_diagonal.png")
        
        let title : UILabel = UILabel(frame: CGRect(x: 0, y: 64, width:self.view.frame.width - 20, height:30))
        title.textAlignment = NSTextAlignment.right
        title.textColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        title.font = UIFont(name: "helvetica-bold", size: 16)
        title.text = "Familiares"
        
        self.view.addSubview(barWhite)
        self.view.addSubview(barBlue)
        self.view.addSubview(img)
        self.view.addSubview(title)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
