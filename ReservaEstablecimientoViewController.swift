//
//  ReservaEstablecimientoViewController.swift
//  Mi Veris
//
//  Created by Andres Cantos on 2/6/15.
//  Copyright (c) 2015 Programadores-iOS.net. All rights reserved.
//

import UIKit

class ReservaEstablecimientoViewController: UIViewController,UITableViewDelegate, UITableViewDataSource,UIPickerViewDataSource, UIPickerViewDelegate {
    var tableView : UITableView = UITableView()
    var listalugares : NSMutableArray = NSMutableArray ()
    var listaregiones : NSMutableArray = NSMutableArray()
    var textoespecialidad : String = ""
    var txtRegion : UILabel!
    var especialidadsel : ObjEspecialidad!
    var regionciudadsel : ObjRegionCiudad!
    var establecimientosel : ObjEstablecimiento!
    var delegate : ReservaEstablecimientodelegate! = nil
    
    var listaregiones2 : NSMutableArray = NSMutableArray()
    
    
    var fondoPick : UIView?
    @IBOutlet var myPicker: UIPickerView! = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        agregarbarratitulo()
        cargarciudades()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func agregarbarratitulo(){
        let logo = UIImage(named: "loginlogo.png")
        let viewLogo = UIImageView(image: logo)
        navigationItem.titleView = viewLogo
        
        self.navigationController?.navigationItem.backBarButtonItem?.title = ""
        
        let barBlue : UIView = UIView(frame: CGRect(x:0, y:64, width:self.view.frame.width, height:30))
        barBlue.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        let barespe : UIView = UIView(frame: CGRect(x:0, y:94, width:self.view.frame.width, height:30))
        barespe.backgroundColor =  UtilidadesColor.colordehexadecimal(hex: 0xececec, alpha: 1.0)

        let title : UILabel = UILabel(frame: CGRect(x:0, y:64, width:self.view.frame.width, height:30))
        title.textAlignment = NSTextAlignment.center
        title.textColor = UIColor.white
        title.font = UIFont(name: "helvetica-bold", size: 12)
        title.text = "Seleccione un Establecimiento"
        
        let title2 : UILabel = UILabel(frame: CGRect(x:0, y:94, width:self.view.frame.width, height:30))
        title2.textAlignment = NSTextAlignment.center
        title2.textColor = UtilidadesColor.colordehexadecimal(hex: 0x868686, alpha: 1.0)
        title2.font = UIFont(name: "helvetica-bold", size: 12)
        title2.text = especialidadsel?.descripcion
        
        self.view.addSubview(barBlue)
        self.view.addSubview(barespe)
        self.view.addSubview(title)
        self.view.addSubview(title2)
        
        
        tableView.frame = CGRect(x:0, y:164, width:self.view.frame.width, height:self.view.frame.height - 200);
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.backgroundColor = UIColor.clear
        //tableView.registerClass(DondeEstamosTableViewCell.self as AnyClass, forCellReuseIdentifier: "establecimientoCell");
        self.view.addSubview(tableView);
        
        
        /*Diseño del fondo para el Picker.*/
        fondoPick = UIView(frame: CGRect(x:0, y:self.view.frame.height, width:self.view.frame.width, height:230))
        fondoPick?.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        fondoPick?.tag = 3000
        self.view.addSubview(fondoPick!)
        
        let btCancel:UIButton = UIButton(frame: CGRect( x:5 , y:0, width:70, height:40))
        btCancel.backgroundColor = UIColor.clear
        btCancel.setTitle("Cancelar", for: UIControlState.normal)
        btCancel.addTarget(self, action: #selector(ReservaEstablecimientoViewController.cancelPicker), for: UIControlEvents.touchUpInside)
        btCancel.titleLabel!.font = UIFont(name: "helvetica-bold", size: 15)
        btCancel.titleLabel?.textColor = UIColor.white
        btCancel.tag = 22;
        fondoPick?.addSubview(btCancel)
        let btAceptar:UIButton = UIButton(frame: CGRect( x:self.view.frame.width - 75 , y:0, width:70, height:40))
        btAceptar.backgroundColor = UIColor.clear
        btAceptar.setTitle("Aceptar", for: UIControlState.normal)
        btAceptar.addTarget(self, action: "acceptPicker", for: UIControlEvents.touchUpInside)
        btAceptar.titleLabel!.font = UIFont(name: "helvetica-bold", size: 15)
        btAceptar.titleLabel?.textColor = UIColor.white
        btAceptar.tag = 22;
        fondoPick?.addSubview(btAceptar)
        
        myPicker.dataSource = self
        myPicker.delegate = self
        myPicker.frame = CGRect(x:0, y:40, width:self.view.frame.width, height:300)
        myPicker.backgroundColor = UIColor.white
        fondoPick!.addSubview(myPicker)
        /*Fin del diseño del Picker*/
        
        txtRegion = UILabel(frame: CGRect(x:0, y:124, width:self.view.frame.width, height:40))
        txtRegion?.backgroundColor = UIColor(red: (5.0/256.0), green: (117.0/256.0), blue: (181.0/256.0), alpha: 1)
        txtRegion?.textColor = UIColor.white
        //regionciudadsel = listaregiones.objectAtIndex(2) as ObjRegionCiudad
        //txtRegion?.text = regionciudadsel.nombreciudad
        txtRegion?.textAlignment = NSTextAlignment.center
        txtRegion?.textAlignment = .center
        txtRegion?.font = UIFont(name: "helvetica-bold", size: 16)
        txtRegion?.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: "mostrarpicker")
        txtRegion?.addGestureRecognizer(tapGesture)
        self.view.addSubview(txtRegion!)  /*Muestro el primer elemento de la lista de ciudades*/
        
        
    }
    
    func cargarciudades(){
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        
        
        let url2 = URLFactory.obtenerRegionCiudades()
        let url = NSURL(string: "\(url2)")
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        print(" region ciudades \(url)")
        let str = "wsappusuario:ZA$57@9b86@$2r5"
        
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                var err: NSError?
                
                //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                if (err != nil) {
                    print("JSON Error \(err!.localizedDescription)")
                }
                
                //println(jsonResult);
                let info =  jsonResult as NSDictionary
                let data = info["lista"] as! [[String: Any]]
                if data.count > 0 {
                    //self.listaregiones = NSMutableArray();
                    for jsonobj in data {
                        let reg = ObjRegionCiudad()
                        reg.codigopais = Int(jsonobj["codigopais"] as! String)!
                        reg.codigoprovincia = Int(jsonobj["codigoprovincia"] as! String)!
                        reg.codigoregion = Int(jsonobj["codigoregion"] as! String)!
                        
                        reg.codigociudad = Int(jsonobj["codigociudad"] as! String)!
                        reg.nombreciudad = jsonobj["nombreciudad"]as! String
                        
                        self.listaregiones.add(reg)
                    }
                }
                    
                
                //dispatch_async(dispatch_get_main_queue(), {
                    DispatchQueue.main.async(execute: {
                    self.reordenar()
                    self.showregiones()
                    ViewUtilidades.hiddenLoader(controller: self)
                })
                } catch {
                    print("ERROR")
                }
            }
            
            
        }
        //////////////////////////
        

    }
    func reordenar(){
        var mitamano = 0
        var codigociudad = 0
        var cciudad = ObjRegionCiudad()
        print("tamaño total = \(listaregiones.count)")
        while mitamano < listaregiones.count {
           
            
            var mycity : ObjRegionCiudad = ObjRegionCiudad()
          for lista in listaregiones {
            mycity = lista as! ObjRegionCiudad
            
            if(codigociudad < mycity.codigociudad){
                print("entro porque es \(codigociudad) y es < a \(mycity.codigociudad) la ciudad es \(mycity.nombreciudad)")
                
                          }
            else{
                mycity = cciudad
            }
            cciudad = mycity
        }
         print("aqui es \(mycity.nombreciudad)")
        if(listaregiones2.count<1){
            codigociudad = mycity.codigociudad
            self.listaregiones2.add(mycity)
            mitamano+=1
        }else{
            print("entro al else")
            for lista in listaregiones {
                let mycity2 = lista as! ObjRegionCiudad
                print("estyo comparando \(mycity2.codigociudad) - \(mycity.codigociudad)")
                if(mycity2.codigociudad == mycity.codigociudad){
                    codigociudad = mycity.codigociudad
                    self.listaregiones2.add(mycity)
                    
                }
                //codigociudad = mycity2.codigociudad
            }
            mitamano+=1

        }
            
        
        print("resultados de lista2")
        for lista in listaregiones2 {
           let  mycity2 = lista as! ObjRegionCiudad
           
            print("la ciudad es: \(mycity2.nombreciudad) y tiene \(mycity2.codigociudad)")
            codigociudad = mycity2.codigociudad
        }
        }
        
    }
    func showregiones(){
        if listaregiones.count > 0 {
            
            let usuario = Usuario.getEntity
            
            var defect : ObjRegionCiudad?
            for ciudad in listaregiones {
                print("ciudad : \(ciudad)")
                // no hace nada
                /*if ((ciudad as! AnyObject).value("codigociudad")as! Int) == usuario.codigoCiudad {
                    defect = ciudad as? ObjRegionCiudad
                    break
                }*/
            }
            if defect != nil {
                regionciudadsel = defect
                txtRegion?.text = regionciudadsel.nombreciudad
            } else {
                regionciudadsel = listaregiones.object(at: 2) as! ObjRegionCiudad
                txtRegion?.text = regionciudadsel.nombreciudad
            }
            
            
            cargarestablecimientos()
        }
    }
    func cargarestablecimientos(){
        listalugares.removeAllObjects()
        
        ViewUtilidades.showLoader(controller: self, title: "Cargando...")
        let url2 = URLFactory.obtenersucursalesxregion(idpais: "\(regionciudadsel.codigopais)",idprovincia: "\(regionciudadsel.codigoprovincia)", idciudad: "\(regionciudadsel.codigociudad)", idespecialidad: "\(especialidadsel.codigo)")
        
        let url = NSURL(string: "\(url2)")
        
        print("establecimientos \(url2)")
        //let request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url! as URL)
        
        let str = "wsappusuario:ZA$57@9b86@$2r5"
        
        /*let utf8str = str.dataUsingEncoding(NSUTF8StringEncoding)
        
        if let base64Encoded = utf8str?.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0)){
            
            print("Encoded:  \(base64Encoded)")
            //request.addValue("Authorization", forHTTPHeaderField: "Basic \(base64Encoded)")
            request.setValue("Basic \(base64Encoded)", forHTTPHeaderField: "Authorization")
            
            if let base64Decoded = NSData(base64EncodedString: base64Encoded, options:   NSDataBase64DecodingOptions(rawValue: 0))
                .map({ NSString(data: $0, encoding: NSUTF8StringEncoding) })
            {
                // Convert back to a string
                print("Decoded:  \(base64Decoded)")
            }
        }*/
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, error) in
            
            
            if (error != nil) {
                print(error!.localizedDescription)
                ViewUtilidades.hiddenLoader(controller: self)
                let alert = UIAlertController(title: "Veris", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                do{
                var err: NSError?
                
                //var strData = NSString(data: params, encoding: NSASCIIStringEncoding)
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                if (err != nil) {
                    print("JSON Error \(err!.localizedDescription)")
                }
                
                //println(jsonResult)
                let info =  jsonResult as NSDictionary
                let data = info["lista"] as! NSArray
                if data.count > 0 {
                    for jsonobj in data {
                        let suc = ObjEstablecimiento(data: jsonobj as! NSDictionary)
                        self.listalugares.add(suc)
                        
                    }
                }
                
                //dispatch_async(dispatch_get_main_queue(), {
                    DispatchQueue.main.async(execute: {
                    self.showestablecimientos()
                    ViewUtilidades.hiddenLoader(controller: self)
                })
                } catch {
                    print("ERROR")
                }
            }
            
            
        }
        //////////////////////////
        

    }
    
    func showestablecimientos(){
        /*
        tableView.frame = CGRectMake(0, 164, self.view.frame.width, CGRectGetHeight(self.view.frame) - 200);
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.backgroundColor = UIColor.clearColor()
        
        self.view.addSubview(tableView);*/
        
        txtRegion?.text = regionciudadsel.nombreciudad
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listalugares.count;
    }
    
    /*func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.separatorInset = UIEdgeInsetsZero
        cell.layoutMargins = UIEdgeInsetsZero
    }*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "") as UITableViewCell!
            ?? UITableViewCell(style: .default, reuseIdentifier: ""),
        establec = listalugares.object(at: indexPath.row) as! ObjEstablecimiento
        
        cell.textLabel?.text = establec.descripcion as String
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        establecimientosel = listalugares.object(at: indexPath.row) as? ObjEstablecimiento
        establecimientosel.codigoregion = regionciudadsel.codigoregion
        delegate.retornareservaestablecimiento(controller: self)
    }

    /*Funciones del Delegate del Picker.*/
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return listaregiones.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let city = (listaregiones[row] as! ObjRegionCiudad).nombreciudad
        print("las regiones: \(row) \(city)")
        print("cuantos tiene? \(listaregiones2.count)")
        
        return (listaregiones2[row] as! ObjRegionCiudad).nombreciudad as String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        regionciudadsel = listaregiones2[row] as! ObjRegionCiudad
        
    }
    /*Fin de funciones del delegate del Picker.*/
    
    /*Funciones de seleccion del Picker...*/
    func mostrarpicker(){
        /*muestro el view del picker*/
        let transitionOptions = UIViewAnimationOptions.showHideTransitionViews//TransitionNone
        let back = UIView(frame: CGRect(x:0, y:0, width:self.view.frame.width, height:self.view.frame.height - 230))
        back.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
        back.alpha = 0
        back.tag = 3001
        self.view.addSubview(back)
        UIView.transition(with: self.view, duration: 0.4, options: transitionOptions, animations: {
            
            back.alpha = 1
            let view : UIView = self.view.viewWithTag(3000)! /* view de picker*/
            view.frame = CGRect(x:0 , y:self.view.frame.height - 230, width:self.view.frame.width, height:230)
            }, completion: { finished in
                
        })
        
        myPicker.reloadAllComponents()
    }
    func cancelPicker(){
        let transitionOptions = UIViewAnimationOptions.showHideTransitionViews//TransitionNone
        
        UIView.transition(with: self.view, duration: 0.3, options: transitionOptions, animations: {
            let back : UIView = self.view.viewWithTag(3001)!
            back.alpha = 0
            let view : UIView = self.view.viewWithTag(3000)!
            view.frame = CGRect(x:0 , y:self.view.frame.height, width:self.view.frame.width, height: 230)
            }, completion: { finished in
                let back : UIView = self.view.viewWithTag(3001)!
                back.removeFromSuperview()
        })
    }
    
    func acceptPicker(){
        /*quito el view del picker*/
        let transitionOptions = UIViewAnimationOptions.showHideTransitionViews//transitionNone
        
        UIView.transition(with: self.view, duration: 0.3, options: transitionOptions, animations: {
            let back : UIView = self.view.viewWithTag(3001)!
            back.alpha = 0
            let view : UIView = self.view.viewWithTag(3000)!
            view.frame = CGRect(x:0 , y:self.view.frame.height, width:self.view.frame.width, height:230)
            }, completion: { finished in
                let back : UIView = self.view.viewWithTag(3001)!
                back.removeFromSuperview()
        })
        
        cargarestablecimientos()
        
        /*Fin de Funciones de seleccion del picker*/
    }
}

protocol ReservaEstablecimientodelegate{
    func retornareservaestablecimiento(controller:ReservaEstablecimientoViewController)
}
